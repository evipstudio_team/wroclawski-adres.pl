<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pl" lang="pl">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<link href="<?= Yii::app()->theme->baseUrl?>/css/style.css" rel="stylesheet" type="text/css"/>
	<title><?= CHtml::encode($this->pageTitle); ?></title>
    <meta name="description" content="<?= $this->getPageState('description')?>" />
    <meta name="keywords" content="<?= $this->getPageState('keywords')?>" />
    <link rel="shortcut icon" href="<?= Yii::app()->theme->baseUrl?>/img/favicon.png" />
    <script type="text/javascript" src="https://www.google.com/recaptcha/api.js?hl=pl"></script>
</head>
<body>
<div id="top">
	<div class="kontener">
    <a href="/"><img class="top_logo" src="<?= Yii::app()->theme->baseUrl?>/img/logo.png" alt="Wirtualne biuro" /></a>
    <div class="user_panel" id="user_panel">
			<?= $this->renderPartial('//shared/_login_info', array('loginForm'=>new LoginForm))?>
		</div>
	<img class="circle_top" src="<?= Yii::app()->theme->baseUrl?>/img/circle_top.png" alt="" />
	</div>
</div>
<div id="menu">
	<div class="kontener">
    <?= $this->renderPartial('//shared/_menu')?>
	</div>
</div>

<div id="content">

	<div class="kontener">
    <?if(!Yii::app()->user->isGuest && Yii::app()->user->id == Yii::app()->params['demoUserId']):?>
    <div style="margin-top: 35px; text-align: center; color: white">
        Uwaga! Wersja pokazowa panelu klienta. Dokonane za jej pośrednictwem czynności nie są skuteczne.
      </div>
    <?endif?>
		<?= $content?>
	</div>
</div>
<div id="footer">
	<div class="kontener">
		<div class="ft_left">
      		<?= Yii::app()->params['varables']['FooterAddress']?>
		</div>
		<div class="ft_right">
			<div class="ft_right_top">
        <img class="phone_icon" src="<?= Yii::app()->theme->baseUrl?>/img/phone_icon.jpg" alt="" /><?= Varable::pickUp('FooterPhone')?>
<!-- 				<div class="icon">
						<a href="facebook_link"><img src="<?= Yii::app()->theme->baseUrl?>/img/fb_icon.png" alt=""/></a>
						<a href="twitter_link"><img src="<?= Yii::app()->theme->baseUrl?>/img/tw_icon.png" alt=""/></a>
						<a href="googleplus_link"><img src="<?= Yii::app()->theme->baseUrl?>/img/gp_icon.png" alt=""/></a>
				</div> -->
			</div>
			<span class="copryright">Wszelkie prawa zastrzeżone. Wirtualne Biuro Wrocław. Powered by Evipstudio.pl</span>
				<a href="/Kontakt.html"><img class="question" src="<?= Yii::app()->theme->baseUrl?>/img/question.png" alt="" onmouseover="this.src='<?= Yii::app()->theme->baseUrl?>/img/question_h.png';" onmouseout="this.src='<?= Yii::app()->theme->baseUrl?>/img/question.png';"/></a>
		</div>
		<img class="circle_footer" src="<?= Yii::app()->theme->baseUrl?>/img/building.png" alt=""/>
	</div>
</div>
<?= $this->renderPartial('//shared/_loading')?>
<?= $this->renderPartial('//shared/_flash')?>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-37967699-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</body>
</html>
