<?
$activeChilds = $page->activeChilds(array('with' => array('article', 'files')));
?>
<div class="header"><?= $page->article->title?></div>

<div class="left">
  <div class="text">
    <?= $page->article->content?>
  </div>
  <div id="faq">
  <? foreach ($activeChilds as $childPage): ?>
    <div class="question">
      <div class="title"><a href="#<?= $childPage->url->address ?>" class="faqLink"><?= $childPage->article->title ?></a></div>
      <div class="short_content"><?= $childPage->article->short_content ?></div>
      <div class="content" style="display: none"><?= $childPage->article->content ?></div>
    </div>
  <? endforeach; ?>
</div>
</div>



<script type="text/javascript">
  $(function() {
    $('.faqLink').click(function() {
      if($(this).hasClass('active')) {
          $(this).removeClass('active');
          var divInstance = $(this).parent().parent();
          $(divInstance).find('div.title a').removeClass('active');
          $(divInstance).find('div.content').slideUp(800);
      }
      else {
        var divInstance = $(this).parent().parent();
        $(divInstance).addClass('active');
        $(this).addClass('active');
        var divContent = $(divInstance).find('div.content');
        $(divContent).slideDown(800);
      }
    });

    if(window.location.hash) {
      $('#faq div.question div.title a').each(function() {
        if(window.location.hash == $(this).attr('href')) {
          $('html, body').animate({
            scrollTop: $(this).offset().top
          }, {duration: 1500});
          $(this).click();
          return false;
        }
      });
    }
  });

</script>
<?= $this->renderPartial('//shared/_gallery_preview')?>