<div style="float:left; width:220px; padding-right:25px">
  <div class="header"><?= $parentPage->article->title?></div>
<div class="left_sub">
  <?= $parentPage->article->content?>
</div>
</div>
<div class="right_sub">
  <?if($page->id==152):?>
    <?
      $this->widget('zii.widgets.CListView', array(
          'id'=>'article-list',
          'dataProvider'=>$searchPage->searchArticles(1,4, 'id DESC'),
          'itemView'=>'//shared/_blogArticle',
      ));
    ?>
    <script type="text/javascript">
      $(function() {
        if(window.location.hash) {
          $('.article_title a').each(function() {
            if(window.location.hash == $(this).attr('rel')) {
              $('html, body').animate({
                scrollTop: $(this).offset().top
              }, 1000);
              return false;
            }
          });
        }
      });

    </script>
  <?else:?>
    <?= $this->renderPartial('//show/article',array('page'=>$page))?>
  <?endif?>
</div>