<?= $this->renderPartial('//show/article',array('page'=>$page))?>

<?php $form=$this->beginWidget('CActiveForm',array('id'=>'form')); ?>

<div class="contactForm">
	<div class="contactError"><?=$form->error($contactForm,'name'); ?></div>	
	<div class="contactLeft"><?= $form->labelEx($contactForm,'name'); ?></div>
	<div class="contactRight"><?= $form->textField($contactForm,'name'); ?></div>

	<div class="contactError"><?=$form->error($contactForm,'email'); ?></div>	
	<div class="contactLeft"><?= $form->labelEx($contactForm,'email'); ?></div>
	<div class="contactRight req required"><?= $form->textField($contactForm,'email'); ?></div>

	
	<div class="contactError"><?=$form->error($contactForm,'phone'); ?></div>
	<div class="contactLeft"><?= $form->labelEx($contactForm,'phone'); ?></div>
	<div class="contactRight"><?= $form->textField($contactForm,'phone'); ?></div>
		
	<div class="contactError"><?=$form->error($contactForm,'body'); ?></div>
	<div class="contactLeft"><?= $form->labelEx($contactForm,'body'); ?></div>
	<div class="contactRight"><?= $form->textArea($contactForm,'body',array('rows'=>5, 'cols'=>20)); ?></div>

    <div class="contactRight">
        <?= $this->renderPartial('//shared/_recaptcha')?>
    <!--    --><?//= $this->renderPartial('//shared/_captcha')?>
    </div>
	<div class="contactRight" style="text-align: right; clear: both; padding-right: 5px;"><input class="button_send_label" type="submit" value="<?= Yii::t('cms', 'Wyślij') ?>" /></div>
	<div style="float:right; clear:both; padding-right: 10px; color: #C6D9E6;  font-family: Tahoma,Geneva,'DejaVu Sans Condensed',sans-serif; font-size: 12px;"><img src="themes/default/img/req.png" alt=""/> pola wymagane</div>
</div>

<?php $this->endWidget(); ?>