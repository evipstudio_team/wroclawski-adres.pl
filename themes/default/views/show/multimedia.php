<?Yii::app()->getClientScript()->registerScriptFile(Yii::app()->theme->baseUrl.'/js/imagepreview.js',CClientScript::POS_END)?>
<?Yii::app()->getClientScript()->registerScript('startpreview','$(document).ready(function(){imagePreview();})',CClientScript::POS_END);?>
<?
Yii::import('ext.jqPrettyPhoto');

$options = array(
    'slideshow'=>5000,
    'autoplay_slideshow'=>false,
    'show_title'=>false,
);?>
<?if($files):?>

<?$filesByType = array();?>
<?foreach($files as $file):?>
  <?if(!isset($filesByType[$file->type])) $filesByType[$file->type] = array()?>
  <?$filesByType[$file->type][] = $file?>

<?endforeach?>

<?foreach($filesByType as $type=>$files):?>
  <?if($files):?>
    <div class="upload_content">
      <h4><?= Multimedia::staticGetTypeTranslated($type, true)?></h4>
      <div style="margin-top: 10px">
	    <?foreach($files as $file):?>
				<?if($type=='Image'):?>
				<div class="<?=$insideDiv?>"<?if($file->isVerticaly()):?> style="background-color: white; text-align: center;"<?endif?>>
		           <a href="<?= $file->link()?>" class="preview">
                 <?if($file->isVerticaly()):?>
                 <img src="<?= $file->link(false,150)?>" alt="<?= $file->DefaultTitle?>" class="image" style="min-width: 0px !important;" title="<?= $file->DefaultTitle?>" />
                 <?else:?>
                  <img src="<?= $file->link(250)?>" alt="<?= $file->DefaultTitle?>" class="image" title="<?= $file->DefaultTitle?>" />
                 <?endif?>

		           </a>
				</div>
		  <?else:?>

				<div class="link">
				   <a href="<?= $file->link()?>" target="_blank"><?= $file->getDefaultTitle(true)?></a>
				</div>

		  <?endif?>
		<?endforeach?>
	  </div>
    </div>
  <?endif?>

<?endforeach?>
<? endif; ?>
<?jqPrettyPhoto::addPretty('.'.$insideDiv.' a',jqPrettyPhoto::PRETTY_GALLERY,jqPrettyPhoto::THEME_FACEBOOK, $options);?>
