<?if($page && $page->article):?>
<div class="header"><?= $page->article->title?></div>

<div class="left">
  <div class="text">
    <?= $page->article->short_content?>
  </div>
  <a href="<?=$page->url->activeUrl?>" class="button_more">Czytaj dalej</a>
</div>
<?endif?>