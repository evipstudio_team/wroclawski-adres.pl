<div class="header"><?= $pricePage->article->title?></div>
<div class="full">
  <div class="text">
    <?= $pricePage->article->content?>
    <?if($packegePage->activeChilds):?>
    <table class="packeges">
      <tr>
        <td colspan="2" style="border:none"></td>
        <?foreach($packegePage->activeChilds as $paPage):?>
          <th style="background-color: #9C589B; text-align: center;">
              <?= $paPage->article->title?>
          </th>
        <?endforeach?>
      </tr>
      <?if($pricePage->activeChilds):?>
      <?foreach($pricePage->activeChilds as $prPage):?>
        <?if($prPage->unit_price):?>
          <tr>
            <th rowspan="2" style="text-align:center">
                <?= $prPage->article->title?>
            </th>
            <td>ilość w pakiecie</td>
            <?foreach($packegePage->activeChilds as $paPage):?>
              <td style="text-align:center">
                <?if($paPage->pickUpProduct($prPage->id)):?>
                  <?= number_format($paPage->productPackege->free_limit,0,'',' ')?>
                <?else:?>

                <?endif?>
              </td>
            <?endforeach?>
          </tr>
          <tr>
            <td>cena po wykorzystaniu pakietu</td>
            <?foreach($packegePage->activeChilds as $paPage):?>
              <td style="text-align:center">
                <?if($paPage->pickUpProduct($prPage->id)):?>
                  <?= number_format($paPage->productPackege->unit_price,2,',',' ')?> zł
                <?else:?>

                <?endif?>
              </td>
            <?endforeach?>
          </tr>
        <?else:?>
          <tr>
            <th colspan="2">
              <?= $prPage->article->title?>
            </th>
            <?foreach($packegePage->activeChilds as $paPage):?>
              <td style="text-align:center">
                <?if($paPage->pickUpProduct($prPage->id)):?>
                  <img src="<?= Yii::app()->theme->baseUrl?>/img/inpackege.png" alt="W pakiecie" title="W pakiecie" />
                <?else:?>
                  <img src="<?= Yii::app()->theme->baseUrl?>/img/not_in_packege.png" style="" alt="Nie występuje" title="Nie występuje" />
                <?endif?>
              </td>
            <?endforeach?>
          </tr>
        <?endif?>
      <?endforeach?>
        <tr>
          <th rowspan="4" style="text-align:center">
            Cena za miesiąc korzystania z usług
          </th>
          <?$fieldName = '0_months_price';?>
          <td>
            <?= trim(str_replace('Cena', '', $pricePage->getAttributeLabel($fieldName)))?>
          </td>
          <?foreach($packegePage->activeChilds as $paPage):?>
            <td style="text-align: center">
              <?if($paPage->$fieldName>0):?>
                <?= number_format($paPage->$fieldName,2,',',' ')?> zł
              <?else:?>
                -
              <?endif?>
            </td>
          <?endforeach?>
        </tr>
        <?foreach(array('6_months_price','12_months_price','individual_price') as $fieldName):?>
          <tr>
            <td>
              <?= trim(str_replace('Cena', '', $pricePage->getAttributeLabel($fieldName)))?>
            </td>
            <?foreach($packegePage->activeChilds as $paPage):?>
              <td style="text-align: center">
                <?if($paPage->$fieldName || $paPage->$fieldName>0):?>
                  <?if($fieldName=='individual_price'):?>
                    <?= $paPage->$fieldName?>
                  <?else:?>
                    <?= number_format($paPage->$fieldName,2,',',' ')?> zł
                  <?endif?>

                <?else:?>
                  -
                <?endif?>
              </td>
            <?endforeach?>
          </tr>
        <?endforeach?>
      <?endif?>
    </table>
    <?endif?>
    <?if($otherPackegePage->activeChilds):?>
      <?foreach($otherPackegePage->activeChilds as $otherPaPage):?>
        <? $data = ProductPackege::prepareProductsToShow($otherPaPage->products); ?>
        <? $maxColumns = findMaxNumOfColumns($data); ?>
        <?if($data):?>
          <h2 style="margin-bottom: 10px;"><?= $otherPaPage->article->title?></h2>
          <table class="packeges">
            <?foreach($data as $element):?>
              <tr>
                <td style="width: 250px; background-color: #9C589B">
                    <?= $element['name'] ?>
                </td>
                <?foreach($element['prices'] as $index=>$price):?>
                <td style="text-align: center">
                    <?= number_format($price['price'],2,',',' ')?> zł
                    <?if($price['per_name']):?>
                      / <?= $price['per_name']?>
                    <?endif?>
                </td>
                <?endforeach?>
              </tr>
            <?endforeach?>
          </table>
        <?endif?>
      <?endforeach?>
    <?endif?>
  </div>
</div>