  <div style="float: left; width:220px; padding-right: 25px;">
  <div class="header">
    <a href="<?=$parentPage->url->activeUrl?>"><?= $parentPage->article->title?></a>
  </div>
  <div class="left_sub">
  <div class="text">
    <ul class="nav_subpage_ul">
      <?foreach($parentPage->activeChilds as $childPage):?>
        <li class="nav_subpage_li"><a <?if($childPage->id==$this->page_id):?>class="active"<?endif?> href="<?=$childPage->url->activeUrl?>"><?= $childPage->url->anchor?></a></li>
      <?endforeach?>
    </ul>
  </div>
 </div>
</div>
  <div style="float: left;">
  <?if($page->id!=$parentPage->id):?>
    <div class="header" style="margin-left: 20px; "><?= $page->article->title?></div>
  <?else:?>
  <div class="header" style="margin-left: 20px; "></div>
  <?endif?>
  <div class="right_sub">
  <div class="left">
    <div class="text">
      <?= $page->article->content?>
    </div>
    <?if($page->parent_id==103):?>
      <div class="goBack">
        <?$urls = $this->getPageState('urlHistory')?>
        <?$url = isset($urls[1])? $urls[1]:false?>
        <?if($url):?>
          <a href="<?= $url?>#<?=$page->article->url->address?>">Wróć</a>
        <?endif?>
      </div>
    <?endif?>
    <?if($page->id==Yii::app()->params['GalleryId']):?>
      <?= $this->renderPartial('//show/multimedia',array('files'=>Multimedia::model()->findAll(array('condition'=>'main=1','params'=>array())),'insideDiv'=>'multimedia','imageWidth'=>'200'))?>
    <?else:?>
      <?= $this->renderPartial('//show/multimedia',array('files'=>$page->files,'insideDiv'=>'multimedia','imageWidth'=>'200'))?>
    <?endif?>

  </div>
</div>
</div>