<div class="header">
  Formularz generowania nowego hasła
</div>
<div class="left">
  <div class="text">
    Poniższy formularz pomoże Ci w przypadku gdy zapomniałeś hasło do panelu klienta.<br />
    Po podaniu adresu e-mail na wskazany adres zostanie wysłana wiadomość z linkiem potwierdzającym prośbę o wygenerowanie nowego hasła.
    Po kliknięciu linku w tej wiadomości, zostanie wygenerowane losowe hasło które otrzymasz również e-mailem.
    Od tej chwili będziesz mógł używać tego hasła aby zalogować się do panelu. Będziesz mógł również zmienić to hasło po uprzednim zalogowaniu się do panelu.<br />
    Proszę wprowadzić adres e-mail służący do logowania się do panelu klienta.
  </div>
  <?$form = $this->beginWidget('CActiveForm', array('action'=>$this->createUrl('show/resetPass')));?>
  <div class="resetForm">
  	<div class="resetError"><?php echo $form->error($resetPassForm,'email'); ?></div>	
	<div class="resetLeft"><?php echo $form->labelEx($resetPassForm,'email'); ?></div>
	<div class="resetRight"><?php echo $form->textField($resetPassForm,'email',array('size'=>60,'maxlength'=>128)); ?></div>
	
    <div class="resetError"><?php echo $form->error($resetPassForm,'verifyCode'); ?></div>	
	<div class="resetLeft"><?php echo $form->labelEx($resetPassForm,'verifyCode'); ?></div>
	<div class="resetRight"><?=$form->textField($resetPassForm,'verifyCode'); ?>
							<div style="margin-top:-3px; float:right"><?php $this->widget('CCaptcha',array('buttonType'=>'button', 'buttonOptions' => array('class' => 'captchaRefresh'),  'buttonLabel' => '')); ?></div>
	</div>  
	<div style="float: right; margin-right: 100px">
   		 <input class="button_more_label" type="submit" value="Resetuj hasło" />
    </div>
   </div>

  <?php $this->endWidget(); ?>
</div>
<div class="right">

</div>