<div class="header"><?= $page->article->title?></div>

<div class="left">
  <div class="text">
    <?= $page->article->content?>
  </div>
  <?if($page->parent_id==152):?>
    <div class="goBack">
      <?$urls = $this->getPageState('urlHistory')?>
      <?$url = isset($urls[1])? $urls[1]:false?>
      <?if($url):?>
        <a href="<?= $url?>#<?=$page->article->url->address?>">Wróć</a>
      <?endif?>
    </div>
  <?endif?>
  <?= $this->renderPartial('//show/multimedia',array('files'=>$page->files,'insideDiv'=>'multimedia','imageWidth'=>'200'))?>
</div>
