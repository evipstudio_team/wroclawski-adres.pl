<div id="recaptcha" class="required" style="padding-left: 48px;">

</div>
<script type="text/javascript">
    var recaptchaRender = function() {
        grecaptcha.render('recaptcha', {
            'sitekey': '6LfzWl0UAAAAAO3fh9hYrJeGSWWTEgFdOHTbt2_M'
        });
    };
    var recaptchaSubmit = function() {
        $('#form').submit(function(event) {
            var recaptchaResponse = grecaptcha.getResponse();
            var error = false;
            $(this).find('.required').each(function() {
                if ($(this).find('input').val() === '') {
                    error = true;
                }
            });
            if (!error) {
                if (recaptchaResponse.length === 0) {
                    event.preventDefault();
                    alert('Uzupełnij captche');
                }
            }
        });
    };
    $(window).on('load', function() {
        recaptchaRender();
        recaptchaSubmit();
    });
</script>