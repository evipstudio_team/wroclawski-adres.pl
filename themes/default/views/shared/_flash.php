<div style="display: none">
<? $flashMessages = Yii::app()->user->getFlashes(); ?>
<? if ($flashMessages): ?>
  <?
  $this->beginWidget('zii.widgets.jui.CJuiDialog',
          array('id'=>'FlashDialog','options' => array(
          'modal' => true,
          'autoOpen'=>true,
          'width'=>'600',
          'buttons'=>array('Ok'=>'js:function(){$(this).dialog("close")}')
          ))
  );
  ?>
  <ul class="flashes" style="list-style: none; margin-top: 20px">
    <? foreach ($flashMessages as $key => $message): ?>
      <li><div class="flash-<?= $key ?>"><?= $message ?></div></li>
  <? endforeach ?>
  </ul>
  <? $this->endWidget('zii.widgets.jui.CJuiDialog'); ?>
<?endif?>
</div>