<div class="blog_entry">
  <div class="article_title">
    <a href="<?= $data->article->url->getActiveUrl()?>" rel="#<?=$data->article->url->address?>"><?= CHtml::encode($data->article->title);?></a>
  </div>
  <div class="article_content">
  	<?= CHtml::encode($data->article->short_content);?>
  </div>
  <div class="show_more">
  	<a href="<?= $data->article->url->getActiveUrl()?>">czytaj więcej</a>
  </div>
</div>