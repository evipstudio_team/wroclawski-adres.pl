<?$pageIds = PageRelations::getChildNodes(124,'length DESC',1);?>
<?if($pageIds):?>
  <?$files = Multimedia::model()->findAll('page_id IN ('.implode(',', CHtml::listData($pageIds, 'child_id', 'child_id')).') AND roller=1')?>
  <?if($files):?>
  <div class="right">
    <div class="slider">
    <div class="okno_slider">
      <a class="prev" style="z-index: 2" href="#" onclick="$('#gallery_roler').data('AnythingSlider').goBack();return false;" title="poprzedni"><img src="<?= Yii::app()->theme->baseUrl ?>/img/prev.png" alt="poprzedni" /></a>
      <a class="next" style="z-index: 2" href="#" onclick="$('#gallery_roler').data('AnythingSlider').goForward();return false;" title="następny"><img src="<?= Yii::app()->theme->baseUrl ?>/img/next.png" alt="następny" /></a>
      <div id="gallery_roler">
        <?foreach($files as $file):?>
          <?if($file->type=='Image'):?>
            <img class="img_box" src="<?= $file->link('358') ?>" alt=""/>
          <?endif?>
        <?endforeach?>
      </div>
    </div>
    </div>
  </div>
  <?Yii::app()->getClientScript()->registerCSSFile(Yii::app()->theme->baseUrl."/js/anythingSlider/anythingslider.css");?>
  <?Yii::app()->getClientScript()->registerScriptFile(Yii::app()->theme->baseUrl."/js/anythingSlider/jquery.anythingslider.min.js");?>
  <script type="text/javascript">
    $(window).load(function() {$('#gallery_roler').anythingSlider({
      theme: 'gallery',
      autoPlay: true,
      resizeContents : false,
      showMultiple: 1,
      buildArrows : false,
      buildNavigation : false,
      buildStartStop : false,
      hashTags: false,
      delay : 5000,
      startPanel : <?= rand(1, count($files)) ?>
    })});
  </script>
  <?endif?>
<?endif?>
