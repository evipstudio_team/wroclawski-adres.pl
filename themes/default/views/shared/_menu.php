<?// if($this->beginCache('menuBlock_'.$this->page_id, array('dependency'=>array('class'=>'system.caching.dependencies.CDbCacheDependency','sql'=>'SELECT MAX(last_update) FROM `blocks`')))) { ?>
      <?$block = Block::model()->find(array('condition'=>'`t`.`id`=23','with'=>array('activePages'=>array('select'=>'id,parent_id'),'activePages.url','activePages.parent'=>array('select'=>'id,parent_id'))));?>
      <?if($block && $block->activePages):?>
        <ul class="nav_ul">
          <?foreach($block->activePages as $page):?>
            <?$activeClass = ($this->page_id == $page->id || (isset($this->managedPageId) && $this->managedPageId == $page->id))? ' active':'';?>
            <li class="nav_li link<?=$page->id?><?=$activeClass?>">
              <a <?if($activeClass):?>class="active"<?endif?> href="<?= $page->url->activeUrl?>"<?if($page->url->blank):?> target="_blank"<?endif?>><?= $page->url->anchor?></a>
            </li>
          <?endforeach?>
        </ul>
      <?endif?>
<?// $this->endCache(); } ?>