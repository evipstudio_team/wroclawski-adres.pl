<? if (Yii::app()->user->isGuest): ?>
  <?if($loginForm->getErrors('password')):?>
    <div class="loginError"><?= implode('<br />',$loginForm->getErrors('password'))?></div>
  <?endif?>
  <span class="header">Panel klienta:</span>
  <?$form = $this->beginWidget('CActiveForm', array('id' => 'logowanie','action'=>$this->createUrl('show/login')));?>

    <?= $form->textField($loginForm, 'username', array('onfocus' => defaultOnFocus('Login'), 'onblur' => defaultOnBlur('Login'))); ?>
    <?= $form->passwordField($loginForm, 'password', array('onfocus' => defaultOnFocus('Hasło'), 'onblur' => defaultOnBlur('Hasło'))); ?>
    <input name="" id="enter" type="button" value="" class="login_button" onclick="
      $('#LoadingDialogBox').dialog('open');
      $.ajax({
        'url':$('#logowanie').attr('action'),
        'type':'post',
        'dataType':'json',
        'data':$('#logowanie').serializeArray(),
        'success':function(data) {
          if(data.redirect) {
            window.location.href = data.redirect;
          }
          else {
            $('#user_panel').html(data.form);
            $('#LoginForm_username').blur();
            $('#LoginForm_password').blur();
            $('#LoadingDialogBox').dialog('close');
          }
        }
      })" />
  <?php $this->endWidget(); ?>
    <span class="lost_pass"><a href="<?=$this->createUrl('show/resetPass')?>">Zapomniałem hasła</a></span>
  <script type="text/javascript">
    $('#LoginForm_username').blur();
    $('#LoginForm_password').blur();
  </script>
<? else: ?>
  <div class="logged_info">
    <?$user = User::model()->findByPk(Yii::app()->user->id)?>
    Zalogowany jako <?= $user->email?><br />
    <a href="<?= $this->createUrl('clientPanel/home')?>">Panel</a> <a href="<?= $this->createUrl('clientPanel/user')?>">Dane użytkownika</a> <?if(Yii::app()->user->checkAccess('CmsUser')):?><a href="<?= $this->createUrl('site/index')?>">CMS</a><?endif?> <a href="<?= $this->createUrl('show/logout')?>">Wyloguj</a>
  </div>
<?endif?>
