<?Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');?>
<div class="header"><?= $this->pageTitle?></div>
<div class="left">
  <?php $form=$this->beginWidget('CActiveForm', array('action'=>Yii::app()->createUrl($this->route),'method'=>'get', 'htmlOptions'=>array('style'=>'float: left; clear: both'))); ?>
    <label><a style="cursor: pointer" onclick="$('#searchForm').slideToggle()">Ogranicz wyniki</a></label>
    <div id="searchForm"<?if(!$settlement->date_from && !$settlement->date_to && !$settlement->byMonth):?> style="display: none;"<?endif?>>
    <?$options = $settlement->buildByMonthSelect(false);?>
    <?if($options):?>
      <label>Wskaż miesiąc: </label>
      <?= CHtml::activeDropDownList($settlement, 'byMonth', array(''=>'')+$options)?>
      <br />
    <?endif?>
      <label>data od</label>
    <?
    $this->widget('CJuiDateTimePicker', array(
              'attribute'=>'date_from',
              'language'=>Yii::app()->language,
              'model'=>$settlement,
              'options'=>array('dateFormat'=>'yy-mm-dd','timeFormat'=>'h:m')))
            ?>
    <label>data do</label>
    <?
    $this->widget('CJuiDateTimePicker', array(
              'attribute'=>'date_to',
              'language'=>Yii::app()->language,
              'model'=>$settlement,
              'options'=>array('dateFormat'=>'yy-mm-dd','timeFormat'=>'h:m')))
            ?>
    <?= CHtml::submitButton('Odśwież'); ?>
    </div>
  <?php $this->endWidget(); ?>

  <?php
    $this->widget('zii.widgets.grid.CGridView', array(
        'id'=>'settlements-grid',
        'dataProvider' => $settlement->search(false),
        'summaryText' => summaryTextLayout('settlements-grid'),
        'htmlOptions'=>array('style'=>'float: left; width: 100%'),
        'rowCssClassExpression'=>'"item_{$data->id}"',
        'filter' => $settlement,
        'enableSorting'=>true,
        'columns' => array(
            array(
                'name'=>'date',
                'value'=>'date(\'d/m/Y H:i\',strtotime($data->date))',
                'filter'=>false
            ),
            'title',
            'note',
            array(
                'name'=>'value',
                'value'=>'number_format($data->value,2,\',\',\' \').\' zł\'',
                'footer'=>'suma (wszystkich): <div style="text-align: right"><span style="white-space: nowrap">'.number_format($settlement->getSum(false),2,',',' ').' zł</span></div>'
            ),

        ),
    ));
  ?>
</div>