  <?if($block && $block->activePages):?>
    <?foreach($block->activePages as $page):?>
    <div style="float:left; width:400px; padding-right: 15px;">
     <div class="header"><?= $page->article->title?></div>
     <div class="center">
      <div class="text">
        <?= $page->article->content?>
      </div>
      <?= $this->renderPartial('//show/multimedia',array('files'=>$page->files,'insideDiv'=>'multimedia','imageWidth'=>'200'))?>
      <div style="clear: both"></div>
    <?endforeach?>
  <? endif; ?>
</div>
</div>
<div class="right2">
  <div class="header">Formularz kontaktowy</div>
  
  <div class="form_right">
    <? $form = $this->beginWidget('CActiveForm', array('id'=>'form','htmlOptions' => array('enctype' => 'multipart/form-data'))) ?>
      <div class="row">
        <?php echo $form->labelEx($contactForm, 'subject'); ?><br />
        <?= $form->textField($contactForm, 'subject') ?>
        <?= $form->error($contactForm, 'subject'); ?>
      </div>
      <div class="row">
        <?php echo $form->labelEx($contactForm, 'file'); ?><br />
        <?= $form->fileField($contactForm, 'file', array('class' => "file", 'size' => 10)) ?>
        <?= $form->error($contactForm, 'file'); ?>
      </div>
      <div class="row">
        <?php echo $form->labelEx($contactForm, 'comment'); ?><br />
        <?= $form->textArea($contactForm, 'comment', array('cols' => '28', 'rows' => '5')) ?>
        <?= $form->error($contactForm, 'comment'); ?>
      </div>
      <div class="contactRight" style="text-align: right; padding: 5px 0px 0 0"><input class="button_send_label" type="submit" value="<?= Yii::t('cms', 'Wyślij') ?>" /></div>
    <?php $this->endWidget(); ?>
  </div>
</div>