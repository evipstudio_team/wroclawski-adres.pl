<div id="eventView">
  <div class="date">
    <?= date('Y-m-d H:i',strtotime($event->date))?>
  </div>
  <div class="type">
    <?= $event->translateEnumValue('type')?>
  </div>
  <div class="comment">
    <?= $event->comment?>
  </div>
  <div class="files">
    <ul>
      <?foreach($event->files as $file):?>
        <li><a href="<?= $file->linkTo()?>"><?= $file->filename?></a></li>
      <?endforeach?>
    </ul>
  </div>
</div>