<?php $this->beginContent('//layouts/main'); ?>
<div class="left_sub">
  <div class="header">Menu</div>
  <div class="text">
    <div class="menu-left">
      <?$this->widget('zii.widgets.CMenu', array(
          'id'=>'menu-left',
          'items' => array_merge(array(
              array(
                  'label' => Yii::t('cms', 'Panel - strona główna'),
                  'url' => array('/clientPanel/home')
                  ),
              array(
                  'label' => Yii::t('cms', 'Wszystkie zdarzenia'),
                  'url' => $this->createUrl('/clientPanel/events'),
                  'active'=>((getCurrentUrl()==$this->createUrl('/clientPanel/events'))? true:false)
                  ),
              array(
                  'label' => Yii::t('cms', 'Dokumenty'),
                  'url' => $this->createUrl('/clientPanel/events',array('typ'=>'Dokument')),
                  'active'=>((getCurrentUrl()==$this->createUrl('/clientPanel/events',array('typ'=>'Dokument')))? true:false)
                  ),
              array(
                  'label' => Yii::t('cms', 'Obsługa telefoniczna'),
                  'url' => $this->createUrl('/clientPanel/events',array('typ'=>'Obsługa telefoniczna')),
                  'active'=>((getCurrentUrl()==$this->createUrl('/clientPanel/events',array('typ'=>'Obsługa telefoniczna')))? true:false)
                  ),
              array(
                  'label' => Yii::t('cms', 'Poczta przychodząca'),
                  'url' => $this->createUrl('/clientPanel/events',array('typ'=>'Poczta przychodząca')),
                  'active'=>((getCurrentUrl()==$this->createUrl('/clientPanel/events',array('typ'=>'Poczta przychodząca')))? true:false)
                  ),
              array(
                  'label' => Yii::t('cms', 'Księgowość'),
                  'url' => $this->createUrl('/clientPanel/events',array('typ'=>'Księgowość')),
                  'active'=>((getCurrentUrl()==$this->createUrl('/clientPanel/events',array('typ'=>'Księgowość')))? true:false)
                  ),
              array(
                  'label' => Yii::t('cms', 'Rezerwacja sali konferencyjnej'),
                  'url' => $this->createUrl('/clientPanel/reservations',array('typ'=>1)),
                  'active'=>((getCurrentUrl()==$this->createUrl('/clientPanel/reservations',array('typ'=>1)))? true:false)
                  ),
              array(
                  'label' => Yii::t('cms', 'Rezerwacja miejsca do pracy'),
                  'url' => $this->createUrl('/clientPanel/reservations',array('typ'=>2)),
                  'active'=>((getCurrentUrl()==$this->createUrl('/clientPanel/reservations',array('typ'=>2)))? true:false)
                  ),
              array(
                  'label' => Yii::t('cms', 'Rezerwacja miejsca do pracy z komputerem'),
                  'url' => $this->createUrl('/clientPanel/reservations',array('typ'=>3)),
                  'active'=>((getCurrentUrl()==$this->createUrl('/clientPanel/reservations',array('typ'=>3)))? true:false)
                  ),
              array(
                  'label' => Yii::t('cms', 'Rezerwacja biura'),
                  'url' => $this->createUrl('/clientPanel/reservations',array('typ'=>4)),
                  'active'=>((getCurrentUrl()==$this->createUrl('/clientPanel/reservations',array('typ'=>4)))? true:false)
                  ),
              array(
                  'label' => Yii::t('cms', 'Rozliczenie'),
                  'url' => $this->createUrl('/clientPanel/settlements'),
                  'active'=>((getCurrentUrl()==$this->createUrl('/clientPanel/settlements'))? true:false)
                  ),
          )),
      ));?>
    </div>
  </div>
</div>
<div class="right_sub">
  <?= $content?>
</div>
<?php $this->endContent(); ?>