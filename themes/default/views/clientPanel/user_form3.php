<?$form = $this->beginWidget('CActiveForm');?>
  <div class="resetForm">
    <div class="resetError"><?php echo $form->error($user,'post_postcode'); ?></div>
    <div class="resetLeft"><?php echo $form->labelEx($user,'post_postcode'); ?></div>
    <div class="resetRight"><?php echo $form->textField($user,'post_postcode'); ?></div>

    <div class="resetError"><?php echo $form->error($user,'post_city'); ?></div>
    <div class="resetLeft"><?php echo $form->labelEx($user,'post_city'); ?></div>
    <div class="resetRight"><?php echo $form->textField($user,'post_city'); ?></div>

    <div class="resetError"><?php echo $form->error($user,'post_street'); ?></div>
    <div class="resetLeft"><?php echo $form->labelEx($user,'post_street'); ?></div>
    <div class="resetRight"><?php echo $form->textField($user,'post_street'); ?></div>
    <div style="clear: both; float: left; margin-left: 188px; margin-top: 20px;">
      <input class="button_more_label" type="submit" name="save3" value="Zapisz zmiany" />
    </div>
  </div>
<?php $this->endWidget(); ?>