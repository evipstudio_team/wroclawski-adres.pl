<div class="header"><?= $this->pageTitle?></div>
<div class="left">
  <div class="form" style="margin-bottom: 10px">
    <?= CHtml::activeHiddenField($reservation, 'type', array('id'=>'ReservationTypeListChoose'))?>
    <?if($rooms):?>
      <div class="row">
        <?if($reservation->type==1):?>
          <label>Wskaż interesującą Ciebie salę</label>
        <?else:?>
          <label>Wskaż interesujące Ciebie biurko</label>
        <?endif?>
        <?= CHtml::activeDropDownList($reservation, 'room_id', array(''=>'')+CHtml::listData($rooms, 'id', 'name'), array('id'=>'ReservationRoomListChoose'))?>
      </div>
    <?endif?>
  </div>
  <div id="calendar" style="width: 700px"></div>
  <div class="legenda">
  	<h2>Legenda</h2>
  	<div class="resrv_blue"></div>Twoja rezerwacja<br/>
  	<div class="resrv_gray"></div>Rezerwacja
  </div>
  <div style="display: none">
    <?$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
      'id'=>'DialogBox',
      'options'=>array(
          'title'=>  '',
          'autoOpen'=>false,
          'modal'=>true,
          'width'=>'500',
          'height'=>'400',
          'open' => 'js:function(){
            $(this).dialog("option","buttons","");
            $(this).html($(\'#LoadingDialogBox\').html());
              jQuery.ajax({
                "url": $(this).data(\'url\'),
                "success":function(data){
                  $(\'#DialogBox\').html($(data));
                }
              });
            return false;}',
          'close'=>'js:function(){
            $(this).dialog("option","buttons","");
            $(this).html($(\'#LoadingDialogBox\').html());
          }',
      ),
    ));?>
    <?$this->endWidget('zii.widgets.jui.CJuiDialog');?>
  </div>
</div>



<?
  $baseUrl = Yii::app()->baseUrl;
  $cs = Yii::app()->getClientScript();
  $cs->registerScriptFile($baseUrl.'/js/fullcalendar/fullcalendar.js');
  $cs->registerCssFile($baseUrl.'/js/fullcalendar/fullcalendar.css');
  $cs->registerCoreScript('jquery');
  $cs->registerCoreScript('jquery.ui');
?>
<script type='text/javascript'>
  function refreshCalendar() {
    $('#LoadingDialogBox').dialog('open');
    var type = $('#ReservationTypeListChoose').val();
    var room_id = ($('#ReservationRoomListChoose').length)? $('#ReservationRoomListChoose').val():'';
    $.ajax({
      url: '<?= $this->createUrl('clientPanel/reservations',array('typ'=>$reservation->type)) ?>?ajax=true',
      type: 'post',
      data: 'Reservation[type]='+type+'&'+'Reservation[room_id]='+room_id,
      success: function(data) {
        $('#content div.right_sub').html(data);
        $('#LoadingDialogBox').dialog('close');
      }
    });
  }
  $(function() {
    $('#ReservationTypeListChoose').change(function() {
      refreshCalendar();
    });
    $('#ReservationRoomListChoose').change(function() {
      refreshCalendar();
    });

		var calendar = $('#calendar').fullCalendar({
      theme: true,
			header: {
				left: 'prev,next today',
				center: 'title',
				right: 'agendaWeek'
			},
      defaultView: 'agendaWeek',
      columnFormat: {
        month: 'ddd',    // Mon
        week: 'ddd d MMM', // Mon 9/7
        day: 'dddd d MMM'  // Monday 9/7
      },
      timeFormat: {
        agenda: 'H:mm{ - H:mm}',
        month: 'H(:mm){ - H:mm}'
      },
      firstDay: 1,
			selectable: true,
			selectHelper: true,
      disableResizing: true,
			select: function(start, end, allDay) {
        date_from = start.getTime();
        date_to = end.getTime();

        $('#DialogBox').data('url','<?=$this->createUrl('reservation/createReservation',array('ajax'=>true))?>?Reservation[room_id]='+$('#ReservationRoomListChoose').val()+'&'+'Reservation[type]='+$('#ReservationTypeListChoose').val()+'&'+'Reservation[date_from]='+date_from+'&'+'Reservation[date_to]='+date_to+'&'+'Reservation[allDay]='+(allDay? 1:0));
        $('#DialogBox').dialog('open');
				calendar.fullCalendar('unselect');
			},
      eventAfterRender: function(event, element) {
        if(!event.allDay) {
        if(event.end-event.start>1800000) {
          var hour = event.start.getHours();
          var minutes = event.start.getMinutes();
          if(minutes<10) minutes = '0'+minutes;
          $(element).find('.fc-event-time:first').html(hour+':'+minutes);
          var hour = event.end.getHours();
          var minutes = event.end.getMinutes();
          if(minutes<10) minutes = '0'+minutes;

          var style = $(element).find('.fc-event-time:first').parent().attr('style');
          if(style) style = ' style="'+style+'"';
          $(element).children().append('<div class="fc-event-bottom fc-event-skin"'+style+'><div class="fc-event-time">'+hour+':'+minutes+'</div></div>');
        }
        else {
          if(event.end!=null) {
            var hour = event.start.getHours();
            var minutes = event.start.getMinutes();
            if(minutes<10) minutes = '0'+minutes;

            var hour2 = event.end.getHours();
            var minutes2 = event.end.getMinutes();
            if(minutes2<10) minutes2 = '0'+minutes2;
            $(element).find('.fc-event-time:first').html(hour+':'+minutes+' - '+hour2+':'+minutes2);
          }

        }
      }

    },
			editable: true,
      axisFormat: 'H:mm',
      minTime: 8,
      maxTime: 18,
			events: <?=  json_encode(Reservation::reservationsToJsonForClientView($reservations, $reservation)) ?>,
      eventClick: function(calEvent, jsEvent, view) {
        if(calEvent.id!=undefined) {
          $('#DialogBox').data('url','<?=$this->createUrl('reservation/editReservation')?>/id/'+calEvent.id);
          $('#DialogBox').dialog('open');
        }
        else return false;
      },
      eventDrop: function(event,dayDelta,minuteDelta,allDay,revertFunc) {
        if(event.id!=undefined) {
          $('#DialogBox').data('url','<?=$this->createUrl('reservation/editFromMoveReservation')?>/id/'+event.id+'?Reservation[date_from]='+event.start.getTime()+'&'+'Reservation[date_to]='+((event.end!=null)? event.end.getTime():event.start.getTime())+'&'+'Reservation[allDay]='+(allDay? 1:0));
          $('#DialogBox').dialog('open');
        }
        else revertFunc();
      }

		});
  });
</script>