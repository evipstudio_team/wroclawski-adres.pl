<div class="header"><?= $this->pageTitle?></div>
<div style="width: 720px">
  <?if($unseenEvents):?>
  <div class="unseenEvents">
    <strong></strong>Uwaga, na Twoim koncie istnieją nieprzeczytane zdarzenia.</strong><br />
    Kliknij <a href="<?= $this->createUrl('clientPanel/events',array('pokaz'=>'nieprzeczytane'))?>">pokaż nieprzeczytane zdarzenia</a> aby obejrzeć
    <br />lub oznacz je wszystkie jako przeczytane klikając <a href="<?= $this->createUrl('clientPanel/checkAllAsSeen')?>">oznacz jako przeczytane</a>.
  </div>
  <?endif?>
  <div class="text">
    <?php
    $this->widget('zii.widgets.grid.CGridView', array(
        'id' => 'events-grid',
        'dataProvider' => $event->clientSearch(),
        'summaryText' => ($event->seen =='unseen')? '':summaryTextLayout('events-grid'),
        'rowCssClassExpression'=>'"item_{$data->id}"',
        'afterAjaxUpdate'=>'function() {initDetailsLink()}',
        'filter' => $event,
        'columns' => array(
            array(
                'name'=>'date',
                'value'=>'date(\'d/m/Y H:i\',strtotime($data->date))',
                'htmlOptions'=>array('class'=>'center'),
                'filter'=>false
            ),
            array(
                'name'=>'type',
                'value'=>'$data->TranslateEnumValue(\'type\')',
                'filter'=>ZHtml::enumItem($event,'type'),
                'visible'=>($typ)? false:true
            ),
            array(
                'name'=>'files',
                'value'=>'$data->filesToString()',
                'filter'=>false,
                'type'=>'raw'
            ),
            array(
                'name'=>'commentShort',
                'htmlOptions'=>array('style'=>'max-width: 400px'),
            ),
            array(
                'header'=>'',
                'htmlOptions'=>array('class'=>'details'),
                'headerHtmlOptions'=>array('class'=>'details'),
                'filterHtmlOptions'=>array('class'=>'details'),
                'value'=>'\'<a class="details" href="\'.Yii::app()->createUrl(\'clientPanel/eventView\',array(\'id\'=>$data->id)).\'"><img src="'.Yii::app()->theme->baseUrl.'/img/\'.$data->viewDetailsIcon().\'" alt="Szczegóły" title="Kliknij aby zobaczyć szczegóły" /></a>\'',
                'type'=>'raw',
                'filter'=>false
            ),
            array(
              'class' => 'CCheckBoxColumn',
              'header' => '',
              'selectableRows'=>2,
              'headerTemplate'=>'{item}',
              'value'=>'$data->id',
              'htmlOptions'=>array('class'=>'center'),
            ),
        ),
    ));
    ?>
    <div style="text-align: right">
    <select id="selectAction" onchange="if($(this).val()=='setAsSeen'){gridSetSeenElements(['events-grid'], 'events-grid_c5[]','<?=$this->createUrl('clientPanel/setSeenEventsByIds')?>');}">
      <option>Zaznaczone:</option>
      <option value="setAsSeen">Oznacz jako przeczytane</option>
    </select>
      </div>
  </div>
</div>
<div class="legend">
  <h2>Legenda</h2>
  <div>
    <img src="<?= Yii::app()->theme->baseUrl?>/img/view_details_unseen.png" alt="" title="" /> - kliknij aby zobaczyć szczegóły nowego zdarzenia (jeszcze nie odczytane).
  </div>
  <div>
    <img src="<?= Yii::app()->theme->baseUrl?>/img/view_details.png" alt="" title="" /> - kliknij aby zobaczyć szczegóły zdarzenia.
  </div>
</div>

<script type="text/javascript">
  function gridSetSeenElements(gridIds, name, url) {
  if(confirm("Czy jesteś pewien?")) {
    $('#LoadingDialogBox').dialog('open');
    var ids = [];
    $('input[name="'+name+'"]').each(function(i,z) {
      if($(z).is(':checked')) {
        ids.push($(z).val());

      }
    });
    if(ids.length) {
      $.ajax({
          url: url,
          type: 'post',
          data: {ids: ids},
          success: function() {
            $.each(gridIds, function(index, gridId) {
              $.fn.yiiGridView.update(gridId);
            });
            $('#LoadingDialogBox').dialog('close');
          }
        });
    }
    else {
      $('#LoadingDialogBox').dialog('close');
      alert("Żaden element nie został zaznaczony.");
    }
  }
  $('#selectAction').val('');
}
  function initDetailsLink() {
    $('a.details').click(function() {
      $('#DialogBox').data('url', $(this).attr('href'));
      $('#DialogBox').dialog('open');
      return false;
    });
  }
  $(function() {
    initDetailsLink();
  })
</script>

<?$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id'=>'DialogBox',
    'options'=>array(
        'title'=>  '',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>'700',
        'open' => 'js:function(){
            $(this).html($(\'#LoadingDialogBox\').html());
            jQuery.ajax({
              "url": $(this).data(\'url\'),
              "success":function(data){
                $(\'#DialogBox\').html($(data));
              }
            });
          return false;}',
        'close'=>'js:function(){
            $(this).html($(\'#LoadingDialogBox\').html());
            $.fn.yiiGridView.update(\'events-grid\');}',
        'buttons'=>array(
            Yii::t('cms', 'Zamknij')=>'js:function(){$(this).dialog("close");}',
        )
    ),
));?>
<?$this->endWidget('zii.widgets.jui.CJuiDialog');?>