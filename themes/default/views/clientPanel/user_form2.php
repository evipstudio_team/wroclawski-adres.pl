<?$form = $this->beginWidget('CActiveForm');?>
  <div class="resetForm">
    <div class="resetError"><?php echo $form->error($user,'nip'); ?></div>
    <div class="resetLeft"><?php echo $form->labelEx($user,'nip'); ?></div>
    <div class="resetRight"><?php echo $form->textField($user,'nip'); ?></div>

  	<div class="resetError"><?php echo $form->error($user,'company_name'); ?></div>
    <div class="resetLeft"><?php echo $form->labelEx($user,'company_name'); ?></div>
    <div class="resetRight"><?php echo $form->textField($user,'company_name'); ?></div>

    <div class="resetError"><?php echo $form->error($user,'postcode'); ?></div>
    <div class="resetLeft"><?php echo $form->labelEx($user,'postcode'); ?></div>
    <div class="resetRight"><?php echo $form->textField($user,'postcode'); ?></div>

    <div class="resetError"><?php echo $form->error($user,'city'); ?></div>
    <div class="resetLeft"><?php echo $form->labelEx($user,'city'); ?></div>
    <div class="resetRight"><?php echo $form->textField($user,'city'); ?></div>

    <div class="resetError"><?php echo $form->error($user,'street'); ?></div>
    <div class="resetLeft"><?php echo $form->labelEx($user,'street'); ?></div>
    <div class="resetRight"><?php echo $form->textField($user,'street'); ?></div>
    <div style="clear: both; float: left; margin-left: 188px; margin-top: 20px;">
      <input class="button_more_label" type="submit" name="save2" value="Zapisz zmiany" />
    </div>
  </div>
<?php $this->endWidget(); ?>