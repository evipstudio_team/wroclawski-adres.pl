<?$form = $this->beginWidget('CActiveForm');?>
  <div class="resetForm">
    <div class="resetError"><?php echo $form->error($user,'password'); ?></div>
    <div class="resetLeft"><?php echo $form->labelEx($user,'password'); ?></div>
    <div class="resetRight"><?php echo $form->passwordField($user,'password', array('value'=>'')); ?></div>

    <div class="resetError"><?php echo $form->error($user,'repeat_password'); ?></div>
    <div class="resetLeft"><?php echo $form->labelEx($user,'repeat_password'); ?></div>
    <div class="resetRight"><?php echo $form->passwordField($user,'repeat_password', array('value'=>'')); ?></div>

    <div style="clear: both; float: left; margin-left: 188px; margin-top: 20px;">
      <input class="button_more_label" type="submit" name="save4" value="Zapisz zmiany" />
    </div>
  </div>
<?php $this->endWidget(); ?>