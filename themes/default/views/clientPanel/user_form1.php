<?$form = $this->beginWidget('CActiveForm');?>
  <div class="resetForm">
  	<div class="resetError"><?php echo $form->error($user,'name'); ?></div>
    <div class="resetLeft"><?php echo $form->labelEx($user,'name'); ?></div>
    <div class="resetRight"><?php echo $form->textField($user,'name'); ?></div>

    <div class="resetError"><?php echo $form->error($user,'surname'); ?></div>
    <div class="resetLeft"><?php echo $form->labelEx($user,'surname'); ?></div>
    <div class="resetRight"><?php echo $form->textField($user,'surname'); ?></div>

    <div class="resetError"><?php echo $form->error($user,'email'); ?></div>
    <div class="resetLeft"><?php echo $form->labelEx($user,'email'); ?></div>
    <div class="resetRight"><?php echo $form->textField($user,'email'); ?></div>

    <div class="resetError"><?php echo $form->error($user,'phone'); ?></div>
    <div class="resetLeft"><?php echo $form->labelEx($user,'phone'); ?></div>
    <div class="resetRight"><?php echo $form->textField($user,'phone'); ?></div>
    <?= $form->hiddenField($user,'role', array('value'=>'root'))?>
    <div style="clear: both; float: left; margin-left: 188px; margin-top: 20px;">
      <input class="button_more_label" type="submit" name="save1" value="Zapisz zmiany" />
    </div>
  </div>
<?php $this->endWidget(); ?>