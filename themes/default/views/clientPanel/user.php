<div class="header"><?= $this->pageTitle?></div>
<div class="left">
  <?
  $this->widget('CTabView', array(
      'tabs'=>array(
          'tab1'=>array(
              'title'=>'Dane podstawowe',
              'view'=>'user_form1',
          ),
          'tab2'=>array(
              'title'=>'Dane do faktury',
              'view'=>'user_form2',
          ),
          'tab3'=>array(
              'title'=>'Dane do korespondencji',
              'view'=>'user_form3',
          ),
          'tab4'=>array(
              'title'=>'Zmiana hasła',
              'view'=>'user_form4',
          ),
      ),
      'viewData'=>array('user'=>$user),
      'activeTab'=>$tab
  ))?>
</div>