<?foreach($model->activePages(array('with'=>'activeLinks')) as $page):?>
  <?foreach($page->activeLinks as $link):?>
  <div class="box" ><a href="<?= Url::getActiveUrl($link->element)?>"><span class="link"><?= $link->element->activeAnchor?></span></a>  <!-- box lewy -->
    <div class="header" style="text-transform: uppercase"><h4><?=$link->element->activeTranslation->title?></h4></div>
    <img class="img" src="<?= $link->files[0]->link()?>" alt=""/>
    <div class="text">
      <h5><?=$link->element->activeTranslation->short_content?></h5>
    </div>
  </div>
<?endforeach?>
<?endforeach?>