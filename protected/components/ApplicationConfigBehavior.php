<?php

/**
 * ApplicationConfigBehavior is a behavior for the application.
 * It loads additional config paramenters that cannot be statically
 * written in config/main
 */
class ApplicationConfigBehavior extends CBehavior {

  /**
   * Declares events and the event handler methods
   * See yii documentation on behaviour
   */
  public function events() {
    return array_merge(parent::events(), array(
                'onBeginRequest' => 'beginRequest',
            ));
  }

  /**
   * Load configuration that cannot be put in config/main
   */
  public function beginRequest() {
    if ($this->owner->user->getState('applicationLanguage'))
      $this->owner->language = $this->owner->user->getState('applicationLanguage');
    else
      $this->owner->language = 'pl';
    $this->owner->params['langs'] = Lang::model()->findAll(array('order' => 'position'));
    if(count($this->owner->params['langs'])>1) {
      $this->owner->params['lang'] = Lang::model()->find('`short`=:short', array(':short' => Yii::app()->language));
      $this->owner->params['defaultLang'] = Lang::getDefaultLang();
    }
    else {
      $this->owner->params['lang'] = $this->owner->params['langs'][0];
      $this->owner->params['defaultLang'] = $this->owner->params['langs'][0];
    }
    $this->owner->params['pricePageId'] = 153;
    $this->owner->params['packegesPageId'] = 176;
    $this->owner->params['clientPanelBlockId'] = 24;

    $this->owner->params['demoUserId'] = 63;

    $this->owner->params['GalleryId'] = 124;

    $this->owner->params['AvailabilityFrom'] = Varable::pickUp('AvailabilityFrom');
    $this->owner->params['AvailabilityTo'] = Varable::pickUp('AvailabilityTo');

    Yii::app()->setComponents(array(
        'widgetFactory' => array(
            'widgets' => array(
                'CGridView' => array(
                    'cssFile' => Yii::app()->theme->baseUrl . '/css/gridview/gridview.css',
                ),
                'CTabView' => array(
                    'cssFile' => Yii::app()->theme->baseUrl . '/css/jquery.yiitab.css',
                ),
                'CLinkPager' => array(
                    'cssFile' => Yii::app()->theme->baseUrl . '/css/pager.css',
                ),
            ),
            )), true);
  }

}

?>