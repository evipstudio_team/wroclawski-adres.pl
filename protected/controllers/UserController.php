<?php

class UserController extends Controller {

  /**
   * @return array action filters
   */
  public function filters() {
    return array(
        'accessControl', // perform access control for CRUD operations
    );
  }

  public function actions() {
    return array(
        'captcha' => array(
            'class' => 'CCaptchaAction',
            'backColor' => 0xFFFFFF
        ),
    );
  }

  public function actionError() {
    if ($error = Yii::app()->errorHandler->error) {
      if (Yii::app()->request->isAjaxRequest)
        echo $error['message'];
      else
        $this->render('error', $error);
    }
  }

  public function accessRules() {
    return array(
        array('allow',
            'actions' => array('View', 'View', 'Update', 'ChangePass', 'ChangeStatus', 'Admin'),
            'roles' => array('Root')
        ),
        array('allow',
            'actions' => array('Index', 'Create'),
            'roles' => array('Administrator')
        ),
        array('allow',
            'actions' => array('Edit'),
            'roles' => array('User:Edit'=>array('id'=>isset($_GET['id'])? $_GET['id']:0))
        ),
        array('allow',
            'actions' => array('Delete'),
            'roles' => array('User:Delete'=>array('id'=>isset($_GET['id'])? $_GET['id']:0))
        ),
        array('allow',
            'actions' => array('ResetPasswordStep2'),
            'users' => array('*')
        ),
        array('deny',
            'users' => array('*'),
        ),
    );
  }

  public function actionEdit($id) {
    $user = User::model()->findByPk($id);
    $user->setScenario('edit');
    if(!$user || ($user && !in_array($user->role, User::model()->findByPk(Yii::app()->user->id)->getPossibleRoles(false)))) {
      throw new CHttpException(404, 'The requested page does not exist.');
      Yii::app()->end();
    }

    if(isset($_POST['User'])) {
      $user->setAttributes($_POST['User']);
      if($user->save()) {
        Yii::app()->user->setFlash('succes','Zmiany zostały zapisane.');
        $this->refresh();
      }
    }
    $this->render('edit',array('user'=>$user));
  }

  public function actionView($id) {
    $this->render('view', array(
        'model' => $this->loadModel($id),
    ));
  }

  public function actionCreate() {
    $model = new User;

    if (isset($_POST['User'])) {
      $model->attributes = $_POST['User'];
      $password = $model->password;
      if ($model->save()) {
        $emailSended = Email::clientUserAccountActivated($model->email, $model->email, $password);
        Yii::app()->user->setFlash('success','Użytkownik został zapisany');
        $this->redirect(array('index'));
      }

    }

    $this->render('create', array(
        'model' => $model,
    ));
  }

  /**
   * Updates a particular model.
   * If update is successful, the browser will be redirected to the 'view' page.
   * @param integer $id the ID of the model to be updated
   */
  public function actionUpdate($id) {
    $model = $this->loadModel($id);

    // Uncomment the following line if AJAX validation is needed
    // $this->performAjaxValidation($model);

    if (isset($_POST['User'])) {
      $model->attributes = $_POST['User'];
      if ($model->save())
        $this->redirect(array('view', 'id' => $model->id));
    }

    $this->render('update', array(
        'model' => $model,
    ));
  }

  /**
   * Deletes a particular model.
   * If deletion is successful, the browser will be redirected to the 'admin' page.
   * @param integer $id the ID of the model to be deleted
   */
  public function actionDelete($id) {
    if (Yii::app()->request->isPostRequest) {
      // we only allow deletion via POST request
      $this->loadModel($id)->delete();

      // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
      if (!isset($_GET['ajax']))
        $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }
    else
      throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
  }

  /**
   * Change Password a particular model.
   * @param integer $id the ID of the model to be updated
   */
  public function actionChangePass($id) {
    $model = $this->loadModel($id);
    $model->password = null;
    $model->scenario = 'ChangePass';

    if (isset($_POST['User'])) {
      $model->attributes = $_POST['User'];
      if ($model->save())
        $this->redirect(array('/user/view', 'id' => $model->id));
    }

    $this->render('change_pass', array(
        'model' => $model,
    ));
  }

  /**
   * Reset Password a particular model.
   * @param string $email the email of the model
   */
  public function actionResetPasswordStep1() {
    $model = new User();
    $model->scenario = 'ResetPassword';
    if (isset($_POST['User'])) {
      $userByEmail = $model->findByAttributes(array('email'=>$_POST['User']['email'],'status'=>'active'));
      if($userByEmail) {
        $cData = new ConfirmationData();
        $cData->setAttributes(array(
            'obj_name'=>'UserController',
            'field_name'=>'ResetPasswordStep2',
            'obj_id'=>$userByEmail->id,
            'new_value'=>null
        ),false);
        $cDataExist = ConfirmationData::model()->find(
                'obj_name=:obj_name AND field_name=:field_name AND obj_id=:obj_id AND served_at IS NULL',
                array(':obj_name'=>$cData->obj_name,':field_name'=>$cData->field_name,':obj_id'=>$userByEmail->id)
                );
        if($cDataExist) {
          Yii::app()->user->setFlash('error-message', Yii::t('cms', 'Istnieje już niezakończony proces resetowania hasła. Proszę zakonczyć poprzedni proces klikając link w wysłanej wiadomości e-mail.'));
        }
        else {
          $cData->save();
          Email::reset_password_step1($userByEmail->email, $cData->checksum);
          Yii::app()->user->setFlash('success-message', Yii::t('cms', 'Dziękujemy, na wskazany adres e-mail została wysłana wiadomość z linkiem potwierdzającym chęć przydzielenia nowego hasła. Aby zakończyć proces, prosimy kliknąć w wysłany link.'));
        }
        CCaptcha::reset_captcha();
        $this->redirect('/');
      }
      else {
        Yii::app()->user->setFlash('error-message', Yii::t('cms', 'Podany adres e-mail nie został znaleziony.'));
        CCaptcha::reset_captcha();
        $this->refresh();
      }
    }
    $this->render('reset_password', array('model'=>$model));
  }

  public function actionResetPasswordStep2($checksum) {
    $cData = ConfirmationData::model()->find('checksum=:checksum AND served_at IS NULL',array(':checksum'=>$checksum));
    if($cData) {
      $user = User::model()->find('id=:id AND status=:status',array(':id'=>$cData->obj_id,':status'=>'active'));

      if($user) {

        $pass = generatePassword();
        $user->password = md5($pass);
        if($user->save()) {
          Email::reset_password_step2($user->email, $pass);
          Yii::app()->user->setFlash('success-message', Yii::t('cms', 'Twoje hasło zostało zmienione i wysłane na Twoj adres e-mail.'));
          $cData->close();
          $this->redirect($this->createUrl('show/index'));
        }
        else {
          throw new CHttpException(404, 'The requested page does not exist.');
        }
      }
      else {
        throw new CHttpException(404, 'The requested page does not exist.');
      }
    }
    else {
      throw new CHttpException(404, 'The requested page does not exist.');
    }
    die();
  }

  /**
   * Change Status a particular model.
   * @param integer $id the ID of the model to be updated
   */
  public function actionChangeStatus($id) {
    $model = $this->loadModel($id);
    $model->scenario = 'ChangeStatus';

    if (isset($_POST['User'])) {
      $model->attributes = $_POST['User'];
      if ($model->save())
        $this->redirect(array('/user/view', 'id' => $model->id));
    }

    $this->render('change_status', array(
        'model' => $model,
    ));
  }

  public function actionIndex() {
    $user = new User('search');
    if(isset($_GET['User'])) {
      $user->setAttributes($_GET['User']);
    }
    else {
      $user->status = '';
      $user->role = '';
    }
    if (isset($_GET['pageSize'])) {
        Yii::app()->user->setState('pageSize',(int)$_GET['pageSize']);
        unset($_GET['pageSize']);
    }
    $loggedUser = User::model()->findByPk(Yii::app()->user->id);

    $this->render('index', array(
        'user'=>$user,
        'loggedUser'=>$loggedUser
    ));
  }

  /**
   * Manages all models.
   */
  public function actionAdmin() {
    $model = new User('search');
    $model->unsetAttributes();  // clear any default values
    if (isset($_GET['User']))
      $model->attributes = $_GET['User'];

    $this->render('admin', array(
        'model' => $model,
    ));
  }

  /**
   * Registration User
   */
  public function actionRegistration() {
    $model = new User;
    $model->scenario = 'registration';
    if (isset($_POST['User'])) {
      $model->attributes = $_POST['User'];
      if ($model->save()) {
        $confirmationData = new ConfirmationData;
        $confirmationData->setAttributes(array(
            'obj_name' => 'User',
            'field_name' => 'status',
            'obj_id' => $model->id,
            'new_value' => 'active'
                ), false);
        $confirmationData->save();
        Email::user_registration($model->email, $confirmationData->checksum);
        Yii::app()->user->setFlash('success-message', Yii::t('cms', 'Dziękujemy, na wskazany adres e-mail została wysłana wiadomość z linkiem potwierdzającym rejestrację. Aby zakończyć proces rejestracji, prosimy kliknąć w wysłany link.'));
        CCaptcha::reset_captcha();
        $this->redirect(array('/'));
      }
    }
    $this->render('registration', array(
        'model' => $model,
    ));
  }

  /**
   * Returns the data model based on the primary key given in the GET variable.
   * If the data model is not found, an HTTP exception will be raised.
   * @param integer the ID of the model to be loaded
   */
  public function loadModel($id) {
    $model = User::model()->findByPk($id);
    if ($model === null)
      throw new CHttpException(404, 'The requested page does not exist.');
    return $model;
  }

  /**
   * Performs the AJAX validation.
   * @param CModel the model to be validated
   */
  protected function performAjaxValidation($model) {
    if (isset($_POST['ajax']) && $_POST['ajax'] === 'user-form') {
      echo CActiveForm::validate($model);
      Yii::app()->end();
    }
  }

}
