<?php

class LinkController extends Controller {

  public $layout = '//layouts/column2';
  public $menu2;
  public $page_id;

  public function filters() {
    return array(
        'accessControl',
    );
  }

  public function accessRules() {
    return array(
        array('allow',
            'roles'=>array('CmsUser')
        ),
        array('deny',
            'users' => array('*'),
        ),
    );
  }

  public function actionIndex($page_id, $mode = 'existing') {
    $this->page_id = $page_id;
    $page = Page::model()->findByPk($page_id);

    $this->setPageTitle(Yii::t('cms', 'Lista odnośników/linków'));
    $link = new Link();
    $link->page_id = isset($_GET['Link']) ? $_GET['Link']['page_id'] : $page->id;

    $pageIds = Page::getTreeIds($page->id, false);

    $this->render('index', array(
        'page' => $page,
        'link' => $link,
        'pageIds' => $pageIds,
        'mode' => $mode
    ));
  }

  public function actionAssignPageListing() {
    $page = new Page();
    if (isset($_GET['Page']))
      $page->setAttributes($_GET['Page']);
    $this->renderPartial('AssignPageListing', array(
        'page' => $page
            ), false, true);
  }

  public function actionAssignArticleListing() {
    $page = Page::model()->findByPk(1);
    $this->page_id = $page->id;
    $articleTranslation = new ArticleTranslation('search');
    $articleTranslation->unsetAttributes();
    if (isset($_GET['ArticleTranslation']))
      $articleTranslation->attributes = $_GET['ArticleTranslation'];

    $article = new Article();
    $article->page_id = isset($_GET['Article']) ? $_GET['Article']['page_id'] : $page->id;

    $pageIds = Page::getTreeIds($article->page_id, false);

    $this->renderPartial('AssignArticleListing', array(
        'page' => $page,
        'articleTranslation' => $articleTranslation,
        'pageIds' => $pageIds,
        'article' => $article
            ), false, true);
  }

  public function getTabs($model) {
    $possibleTabs = array(
        'index' => array(
            'translated' => Yii::t('cms', '<-- Wróć do listy linków'),
            'url' => $this->createUrl('link/index', array('page_id' => $model->page_id)),
            'view' => 'index'),
//        'view' => array(
//            'translated' => Yii::t('cms', 'Informacje podstawowe'),
//            'url' => $this->createUrl('link/view', array('id' => $model->id)),
//            'view' => 'view'),
        'CreateRelated' => array(
            'translated' => Yii::t('cms', 'Edycja linku do istniejącej podstrony'),
            'url' => $this->createUrl('link/CreateRelated', array('page_id' => $model->page_id, 'id' => $model->id)),
            'view' => '_existing_page'),
        'files' => array(
            'translated' => Yii::t('cms', 'Obrazki i pliki'),
            'url' => $this->createUrl('link/files', array('id' => $model->id)),
            'view' => 'files'),
        'blocks' => array(
            'translated' => Yii::t('cms', 'Bloki i wystąpienia'),
            'url' => $this->createUrl('link/blocks', array('id' => $model->id)),
            'view' => 'blocks'),
    );
    $tabs = array();
    foreach ($possibleTabs as $action => $translated) {
      if (Yii::app()->getController()->action->id == $action) {
        $tabs[$action] = array(
            'title' => Yii::t('cms', $translated['translated']),
            'view' => $translated['view'],
        );
      } else {
        $tabs[$action] = array(
            'title' => Yii::t('cms', $translated['translated']),
            'url' => $translated['url'],
        );
      }
    }
    return $tabs;
  }

  public function actionView($id) {
    $link = Link::model()->findByPk($id);
    $this->page_id = $link->page->id;

    $this->render('//shared/tabs', array(
        'page' => $link->page,
        'model' => $link,
        'tabs' => $this->getTabs($link),
        'additionalParams' => array('module_id' => Link::getModuleId())
    ));
  }

  public function actionBlocks($id) {
    $link = Link::model()->findByPk($id);
    $this->setPageTitle(Yii::t('cms', 'Bloki na stronie i wystąpienia elementu'));
    $this->page_id = $link->page->id;
    $this->render('tabs', array(
        'page' => $link->page,
        'model' => $link,
        'tabs' => $this->getTabs($link),
        'additionalParams' => array('module_id' => Link::getModuleId())
    ));
  }

  public function actionFiles($id) {
    $link = Link::model()->findByPk($id);
    $this->setPageTitle(Yii::t('cms', 'Obrazki i pliki elementu'));
    $this->page_id = $link->page->id;
    $this->render('tabs', array(
        'page' => $link->page,
        'model' => $link,
        'tabs' => $this->getTabs($link),
        'additionalParams' => array('module_id' => Link::getModuleId())
    ));
  }

  public function actionCreateRelated($page_id, $id = false) {
    $page = Page::model()->findByPk($page_id);
    $this->page_id = $page->id;
    $this->setPageTitle(Yii::app()->name . ' - ' . $page->name . ' - ' . Yii::t('cms', 'Dodaj element'));
    if ($id) {
      $link = Link::model()->findByPk($id);
      $link->setScenario('createRelated');
    } else {
      $link = new Link('createRelated');
      $link->setAttribute('page_id', $page_id);
    }
    $url = new Url('create');
    $url->setAttributes(array('lang_id' => Yii::app()->params['lang']->id));
    if (Yii::app()->request->isPostRequest) {
      $transaction = Yii::app()->db->beginTransaction();
      $commit = true;
      $link->setAttributes($_POST['Link']);
      if (!$link->save()) {
        $commit = false;
      }

      if ($commit) {
        $transaction->commit();
        if ($id) {
          Yii::app()->user->setFlash('success', Yii::t('cms', 'Zmiany zostały zapisane.'));
        } else {
          Yii::app()->user->setFlash('success', Yii::t('cms', 'Nowy link został utworzony.'));
        }
        $this->redirect($this->createUrl('link/index', array('page_id' => $page->id)));
      } else {
        $transaction->rollback();
      }
    }
    if ($id) {
      $this->setPageTitle(Yii::t('cms', 'Edycja elementu'));
      $this->render('tabs', array(
          'page' => $link->page,
          'model' => $link,
          'tabs' => $this->getTabs($link),
          'additionalParams' => array('module_id' => Link::getModuleId(), 'url' => $url, 'id' => $id, 'link' => $link, 'page' => $link->page)
      ));
    } else {
      $this->setPageTitle(Yii::t('cms', 'Dodaj nowy odnośnik/link'));
      $this->render('create', array(
          'link' => $link,
          'page' => $page,
          'url' => $url,
          'id' => $id
      ));
    }
//
  }

  public function actionCreateNotRelated($page_id, $id = false) {
    $page = Page::model()->findByPk($page_id);
    $this->setPageTitle(Yii::app()->name . ' - ' . $page->name . ' - ' . Yii::t('cms', 'Dodaj element'));
    if ($id) {
      $link = Link::model()->findByPk($id);
      $link->setScenario('createNotRelated');
      $url = Url::model()->findByPk(array('element_name' => 'Link', 'element_id' => $id, 'lang_id' => Yii::app()->params['lang']->id));
    } else {
      $link = new Link('createNotRelated');
      $link->setAttribute('page_id', $page_id);
      $url = new Url('create');
      $url->setAttributes(array('lang_id' => Yii::app()->params['lang']->id));
    }

    if (Yii::app()->request->isPostRequest) {
      $transaction = Yii::app()->db->beginTransaction();
      $commit = true;
      if (!$link->save()) {
        $commit = false;
      }
      if (isset($_FILES) && $_FILES['files']['name'][0]) {
        $messagesPerFile = array();
        $messages = array('success' => array(), 'error' => array());
        foreach ($_FILES['files']['name'] as $index => $filePost) {
          $fileData = array(
              'name' => $_FILES['files']['name'][$index],
              'type' => $_FILES['files']['type'][$index],
              'tmp_name' => $_FILES['files']['tmp_name'][$index],
              'error' => $_FILES['files']['error'][$index],
              'size' => $_FILES['files']['size'][$index]
          );
          if ($id && $link->multimediaCount) {
            $messagesPerFile[] = Multimedia::uploadFile($fileData, 'Link', $link->id, $link->multimedia[0]->id);
          } else {
            $messagesPerFile[] = Multimedia::uploadFile($fileData, 'Link', $link->id);
          }
        }
        foreach ($messagesPerFile as $message)
          foreach (array('success', 'error') as $field)
            if (isset($message[$field]))
              $messages[$field][] = $message[$field];
        if (isset($messages['error']) && $messages['error'])
          $commit = false;
      }
      elseif (!$id) {
        $link->addError('related_page_id', "Obrazek nie został wgrany.");
        $commit = false;
      }
      $url->setAttributes($_POST['Url']);
      $url->setAttributes(array('element_name' => 'Link', 'element_id' => $link->id));
      if (!$url->save())
        $commit = false;

      if ($commit) {
        $transaction->commit();
        if ($id) {
          Yii::app()->user->setFlash('success', Yii::t('cms', 'Zmiany zostały zapisane.'));
          //$this->redirect($this->createUrl('link/createNotRelated', array('page_id' => $page->id,'id'=>$link->id)));
        } else {
          Yii::app()->user->setFlash('success', Yii::t('cms', 'Nowy link został utworzony.'));
        }
        $this->redirect($this->createUrl('link/index', array('page_id' => $page->id, 'mode' => 'nonexisting')));
      } else {
        $transaction->rollback();
      }
    }
    $this->render('create', array(
        'link' => $link,
        'page' => $page,
        'url' => $url,
        'id' => $id
    ));
  }

  public function actionDelete($id) {
    $link = Link::model()->findByPk($id);
    if ($link->delete()) {
      Yii::app()->user->setFlash('success', Yii::t('cms', 'Element został usunięty.'));
    }
    $this->redirect($this->createUrl('link/index', array('page_id' => $link->page_id, 'mode' => (($link->related_element_id) ? 'existing' : 'nonexisting'))));
  }

  public function actionStatusSwitch($id) {
    $model = Link::model()->findByPk($id);
    if ($model->status == 1)
      $model->status = 0;
    elseif ($model->status == 0)
      $model->status = 1;
    $model->save();
  }

  public function actionSort() {
    if (isset($_POST['items']) && is_array($_POST['items'])) {
      foreach ($_POST['items'] as $i => $item) {
        $link = Link::model()->findByPk($item);
        $link->position = $i + 1;
        $link->save();
      }
    }
  }

}
