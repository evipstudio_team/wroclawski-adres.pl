<?php

class EventController extends Controller {

  var $subMenu = array();

  public function filters() {
    return array(
        'accessControl',
    );
  }

  public function accessRules() {
    return array(
        array('allow',
            'roles' => array('CmsUser')
        ),
        array('deny',
            'users' => array('*'),
        ),
    );
  }

  public function beforeAction($action) {
    $this->subMenu = array(
        array(
            'label' => Yii::t('cms', 'Wszystkie zdarzenia'),
            'url' => Yii::app()->createUrl('event/index'),
            'active' => (Yii::app()->getController()->action->id=='edit' || (Yii::app()->getController()->action->id=='index' && ((isset($_GET['Event'])==false || $_GET['Event']['type']=='') &&  (isset($_GET['typ'])==false || $_GET['typ']==''))) ? true : false)
        ),
        array(
            'label' => Yii::t('cms', 'Dodaj wpis'),
            'url' => Yii::app()->createUrl('event/create'),
            'active' => (getCurrentUrl() == $this->createUrl('event/create') ? true : false)
        ),
        array(
            'label' => Yii::t('cms', 'Dokumenty'),
            'url' => Yii::app()->createUrl('event/index', array('typ' => 'Dokument')),
            'active' => (getCurrentUrl() == Yii::app()->createUrl('event/index', array('typ' => 'Dokument'))) ? true : false,
        ),
        array(
            'label' => Yii::t('cms', 'Obsługa telefoniczna'),
            'url' => Yii::app()->createUrl('event/index', array('typ' => 'Obsługa telefoniczna')),
            'active' => (getCurrentUrl() == Yii::app()->createUrl('event/index', array('typ' => 'Obsługa telefoniczna'))) ? true : false,
        ),
        array(
            'label' => Yii::t('cms', 'Poczta przychodząca'),
            'url' => Yii::app()->createUrl('event/index', array('typ' => 'Poczta przychodząca')),
            'active' => (getCurrentUrl() == Yii::app()->createUrl('event/index', array('typ' => 'Poczta przychodząca'))) ? true : false,
        ),
        array(
            'label' => Yii::t('cms', 'Księgowość'),
            'url' => Yii::app()->createUrl('event/index', array('typ' => 'Księgowość')),
            'active' => (getCurrentUrl() == Yii::app()->createUrl('event/index', array('typ' => 'Księgowość'))) ? true : false,
        ),
    );
    return parent::beforeAction($action);
  }

  public function actionIndex($select = null, $typ=null) {
    $event = new Event('search');
    $user = new User();

    if(isset($_GET['User'])) {
      $user->setAttributes($_GET['User']);
    }
    if(isset($_GET['Event'])) {
      $event->setAttributes($_GET['Event']);
    }
    else {
      if($typ) {
        $possibleTypes = ZHtml::enumItem($event, 'type');
        foreach($possibleTypes as $typeIndex=>$type)
          if($type==$typ) {
            $event->type = $typeIndex;
            break;
          }
      }
    }

    $this->render('index', array(
        'event' => $event,
        'user' => $user,
        'select'=>$select,
        'typ'=>$typ
    ));
  }

  public function actionCreate() {
    $event = new Event();
    $event->setAttributes(array(
          'date' => date('Y-m-d H:i:s'),
          'created_by' => Yii::app()->user->id,
          'created_at' => date('Y-m-d H:i:s')
      ));
    if (isset($_POST['Event'])) {
      $event->setAttributes($_POST['Event']);
      $event->formFiles = CUploadedFile::getInstancesByName('Event[formFiles]');
      $transaction = Yii::app()->db->beginTransaction();
      if (isset($_POST['save']) && $event->save()) {
        $transaction->commit();
        $event->sendEmail();
        Yii::app()->user->setFlash('success','Zdarzenie zostało dodane.');
        $this->redirect($this->createUrl('event/index',array('select'=>$event->id)));
      } else {
        $transaction->rollback();
      }
    }

    $this->render('create', array(
        'event' => $event,
    ));
  }

  public function actionDelete($id) {
    $model = Event::model()->findByPk($id);
    if ($model) {
      $model->delete();
    }
    Yii::app()->end();
  }

  public function actionFileDelete($id) {
    $eventFile = EventFile::model()->findByPk($id);
    if ($eventFile) {
      $eventFile->delete();
      Yii::app()->user->setFlash('success', 'Dokument został usunięty.');
      $this->redirect('event/view', array('id' => $eventFile->event_id));
    }
  }

  public function actionDeleteByIds() {
    $ids = $_POST['ids'];
    foreach($ids as $id) {
      $model = Event::model()->findByPk($id);
      if($model) $model->delete();
    }
    Yii::app()->end();
  }

  public function actionEdit($id) {
    $event = Event::model()->findByPk($id);
    $event->setScenario('edit');
    if (isset($_POST['Event'])) {
      $event->setAttributes($_POST['Event']);
      $event->formFiles = CUploadedFile::getInstancesByName('Event[formFiles]');
      $transaction = Yii::app()->db->beginTransaction();
      if ($event->save()) {
        $transaction->commit();
        $event->sendEmail();
        Yii::app()->user->setFlash('success','Zmiany zostaly zapisane.');
        $this->refresh();
      } else {
        $transaction->rollback();
      }
    }

    $eventFile = new EventFile();
    $eventFile->event_id = $event->id;

    $this->render('edit', array(
        'event' => $event,
        'eventFile'=>$eventFile
    ));
  }

}
