<?php

class GalleryController extends Controller {

  public function filters() {
    return array(
        'accessControl',
    );
  }

  public function accessRules()
	{
		return array(
			array('allow',
				'roles'=>array('CmsUser')
			),
      array('deny',
				'users'=>array('*'),
			),
		);
	}

  public function actionIndex($id=null,$page_id=null,$select=null) {
    $page_id = $id? $id:$page_id;
    $page = Page::model()->findByPk($page_id);

    $managedPage_id = PageRelations::getManagedNode($page->id);
    if(!$select) $select = $page_id;
    $subPages = new PageRelations();
    $subPages->parent_id = $managedPage_id->id;

    $this->page_id = $page->id;
    $this->setPageTitle(Yii::t('cms', $page->name.' - Lista elementów'));
    $this->render('index', array('page' => $page, 'subPages'=>$subPages,'select'=>$select));
  }

  public function actionCreate($page_id) {
    $page = Page::model()->findByPk($page_id);
    $this->page_id = $page->id;
    $this->setPageTitle(Yii::t('cms', 'Dodaj nowy element'));

    $gallery = new Gallery('search');

    $this->render('create', array(
        'page' => $page,
        'gallery' => $gallery
    ));
  }

  public function actionCreateCategory($parent_id) {
    $this->setPageTitle(Yii::t('cms', 'Dodaj nową podkategorię'));
    $parentPage = Page::model()->findByPk($parent_id);

    $this->page_id = $parent_id;

    $newPage = new Page('createCategory');
    if(isset($_POST['Page'])) {
      $newPage->setAttributes($_POST['Page']);
      $newPage->parent_id = $parentPage->id;
      if($newPage->save()) {
        Yii::app()->user->setFlash('success','Nowa podkategoria została zapisana.');
        $this->redirect($this->createUrl('gallery/index',array('id'=>$newPage->id)));
      }
    }

    $this->render('createCategory',array('page'=>$parentPage,'newPage'=>$newPage));
  }

  public function actionFiles($id) {
    $this->redirect($this->createUrl('gallery/index',array('page_id'=>$id)));
  }

  public function actionEdit($id) {
    $this->redirect($this->createUrl('gallery/index',array('page_id'=>$id)));
  }

}