<?php

class ReservationController extends Controller {

  var $subMenu = array();
  public $defaultAction = 'AddReservation';

  public function filters() {
    return array(
        'accessControl',
    );
  }

  public function accessRules() {
    return array(
        array('allow',
            'roles' => array('CmsUser')
        ),
        array('allow',
            'actions'=>array('CreateReservation','ShowRoomPossibilites','ReservationSuccess','EditReservation','EditFromMoveReservation'),
            'users' => array('@')
        ),
        array('deny',
            'users' => array('*'),
        ),
    );
  }

  public function beforeAction($action) {
    $this->subMenu = array(
        array(
            'label' => Yii::t('cms', 'Kalendarz'),
            'url' => Yii::app()->createUrl('reservation/addReservation'),
            'active'=>(getCurrentUrl()==Yii::app()->createUrl('reservation/addReservation'))? true:false
        ),
        array(
            'label' => Yii::t('cms', 'Tabela'),
            'url' => Yii::app()->createUrl('reservation/index'),
            'active'=>(getCurrentUrl()==Yii::app()->createUrl('reservation/index'))? true:false
        ),
        array(
            'label' => Yii::t('cms', 'Lista produktów'),
            'url' => Yii::app()->createUrl('reservation/rooms'),
            'active'=>(getCurrentUrl()==Yii::app()->createUrl('reservation/rooms'))? true:false
        ),
    );
//    $jTheme = 'custom';
//    $cs = Yii::app()->getClientScript();
//    $cs->scriptMap["jquery-ui.css"]= Yii::app()->request->baseUrl . "/css/jqueryui/$jTheme/jquery-ui-1.9.2.custom.css";
//    $cs->scriptMap["jquery-ui.min.css"]= Yii::app()->request->baseUrl . "/css/jqueryui/$jTheme/jquery-ui-1.9.2.custom.min.css";
    return parent::beforeAction($action);
  }

  public function actionIndex($select = null, $typ=null) {
    $reservation = new Reservation('search');
    $user = new User();

    if(isset($_GET['User'])) {
      $user->setAttributes($_GET['User']);
    }
    if(isset($_GET['Reservation'])) {
      $reservation->setAttributes($_GET['Reservation']);
    }
    else {
      if($typ) {
        $possibleTypes = ZHtml::enumItem($reservation, 'type');
        foreach($possibleTypes as $typeIndex=>$type)
          if($type==$typ) {
            $reservation->type = $typeIndex;
            break;
          }
      }
    }

    $this->render('index', array(
        'reservation' => $reservation,
        'user' => $user,
        'select'=>$select,
        'typ'=>$typ
    ));
  }

  public function actionAddReservation($ajax = null, $type = null, $room_id = null) {
    $reservation = new Reservation();
    //$action = $this->createUrl('reservation/addReservation',array('ajax'=>$ajax));
    if($type) {
      $reservation->type = $type;
      $rooms = Room::pickUpRooms($type, $reservation);
      
    }
    else $rooms = array();

    if($room_id) {
      $reservation->room_id = $room_id;
    }

    $reservations = $reservation->pickUpReservations();
    if ($ajax) {
      $this->renderPartial('addReservation', array(
          'reservation'=>$reservation,
          'ajax'=>$ajax,
          'rooms'=>$rooms,
          'reservations'=>$reservations
      ), false, true);
    } else {
      $this->render('addReservation', array(
          'reservation'=>$reservation,
          'ajax'=>$ajax,
          'rooms'=>$rooms,
          'reservations'=>$reservations
      ));
    }
  }

  public function actionCreateReservation($ajax = false) {
    $reservation = new Reservation();
    $action = $this->createUrl('reservation/createReservation',array('ajax'=>$ajax));

    if(isset($_GET['Reservation'])) {
      $date_from = $_GET['Reservation']['date_from']/1000;
      $date_to = $_GET['Reservation']['date_to']/1000;
      $_GET['Reservation']['date_from'] = date('Y-m-d',$date_from);
      $_GET['Reservation']['time_from'] = date('H:i',$date_from);
      $_GET['Reservation']['date_to'] = date('Y-m-d',$date_to);
      $_GET['Reservation']['time_to'] = date('H:i',$date_to);

      if($_GET['Reservation']['allDay']) {
        $_GET['Reservation']['from_all_day'] = true;
        $_GET['Reservation']['to_all_day'] = true;
      }
      $reservation->setAttributes($_GET['Reservation']);
    }

    if(isset($_POST['Reservation'])) {
      $reservation->setAttributes($_POST['Reservation']);

      if($reservation->validate()) {
        $reservation->save();
        Yii::app()->user->setFlash('success','Rezerwacja została zapisana.');
        $this->redirect($this->createUrl('reservation/reservationSuccess'));
      }
    }

    if($ajax) {
      $this->renderPartial('createReservation', array(
        'reservation' => $reservation,
        'action'=>$action
      ),false,true);
    }
    else {
      $this->render('createReservation', array(
        'reservation' => $reservation,
        'action'=>$action
      ));
    }
  }

  public function actionShowRoomPossibilites($type, $id, $date_from, $time_from, $from_all_day, $date_to, $time_to, $to_all_day) {
//    $date_from = date('Y-m-d H:i:s',$date_from);
//    $date_to = date('Y-m-d H:i:s',$date_to);

    $reservation = new Reservation();
    if($id) $reservation->id = $id;
    $reservation->setAttributes(array(
        'date_from'=>$date_from,
        'time_from'=>$time_from,
        'from_all_day'=>($from_all_day=='true')? 1:0,
        'date_to'=>$date_to,
        'time_to'=>$time_to,
        'to_all_day'=>($to_all_day=='true')? 1:0,

    ),false);
    $reservation->validate();
//    var_dump($reservation->from_all_day, $reservation->date_from_helper);
//    var_dump($reservation->to_all_day);
    $rooms = Room::pickUpRooms($type, $reservation);

    $this->renderPartial('showRoomPossibilites',array('rooms'=>$rooms,'type'=>$type));
  }

  public function actionReservationSuccess() {
    $this->renderPartial('reservationSuccess', array(),false,true);
  }

  public function actionEditReservation($id) {
    $reservation = Reservation::model()->findByPk($id);
    $action = $this->createUrl('reservation/editReservation',array('id'=>$reservation->id));

    if(isset($_GET['Reservation'])) {
      $date_from = $_GET['Reservation']['date_from']/1000;
      $date_to = $_GET['Reservation']['date_to']/1000;
      $_GET['Reservation']['date_from'] = date('Y-m-d',$date_from);
      $_GET['Reservation']['time_from'] = date('H:i',$date_from);
      $_GET['Reservation']['date_to'] = date('Y-m-d',$date_to);
      $_GET['Reservation']['time_to'] = date('H:i',$date_to);

      if($_GET['Reservation']['allDay']) {
        $_GET['Reservation']['from_all_day'] = true;
        $_GET['Reservation']['to_all_day'] = true;
      }
      $reservation->setAttributes($_GET['Reservation']);
    }

    if(isset($_POST['Reservation'])) {
      $reservation->setAttributes($_POST['Reservation']);
      if($reservation->validate()) {
        $reservation->save();
        Yii::app()->user->setFlash('success','Zmiany w rezerwacji zostały zapisane.');
        $this->redirect($this->createUrl('reservation/reservationSuccess'));
      }
    }

    $this->renderPartial('editReservation', array(
        'reservation' => $reservation,
        'action'=>$action
      ),false,true);
  }

  public function actionEditFromMoveReservation($id) {
    $reservation = Reservation::model()->findByPk($id);
    $action = $this->createUrl('reservation/editReservation',array('id'=>$reservation->id));

    if(isset($_GET['Reservation'])) {
      $date_from = $_GET['Reservation']['date_from']/1000;
      $date_to = $_GET['Reservation']['date_to']/1000;
      $_GET['Reservation']['date_from'] = date('Y-m-d',$date_from);
      $_GET['Reservation']['time_from'] = date('H:i',$date_from);
      $_GET['Reservation']['date_to'] = date('Y-m-d',$date_to);
      $_GET['Reservation']['time_to'] = date('H:i',$date_to);

      if($_GET['Reservation']['allDay']) {
        $_GET['Reservation']['from_all_day'] = true;
        $_GET['Reservation']['to_all_day'] = true;
      }
      $reservation->setAttributes($_GET['Reservation']);
    }
    if($reservation->validate()) {
      if($reservation->save()) {
        Yii::app()->user->setFlash('success','Zmiany w rezerwacji zostały zapisane.');
        $this->redirect($this->createUrl('reservation/reservationSuccess'));
      }
    }
    Yii::app()->user->setFlash('error','Niestety, podany termin jest już zajęty.');
    $this->redirect($this->createUrl('reservation/reservationSuccess'));
    Yii::app()->end();
//    $this->renderPartial('editFromMoveReservation', array(
//        'reservation' => $reservation,
//        'action'=>$action
//      ),false,true);
  }

  public function actionDelete($id) {
    $reservation = Reservation::model()->findByPk($id);
    $reservation->delete();
    Yii::app()->end();
  }

  public function actionDeleteByIds() {
    $ids = $_POST['ids'];
    foreach($ids as $id) {
      $model = Reservation::model()->findByPk($id);
      if($model) $model->delete();
    }
    Yii::app()->end();
  }

  public function actionRooms() {
    $room = new Room();

    $this->render('rooms',array('room'=>$room));
  }

  public function actionEditRoom($id) {
    $room = Room::model()->findByPk($id);
    $action = $this->createUrl('reservation/editRoom',array('id'=>$id));

    if(isset($_POST['Room'])) {
      $room->setAttributes($_POST['Room']);
      if($room->save()) {
        Yii::app()->user->setFlash('success','Zmiany zostały zapisane');
        $this->refresh();
      }
    }

    $this->renderPartial('editRoom',array('room'=>$room,'action'=>$action));
  }

  public function actionCreateRoom() {
    $room = new Room();
    $action = $this->createUrl('reservation/createRoom');

    if(isset($_POST['Room'])) {
      $room->setAttributes($_POST['Room']);
      if($room->save()) {
        Yii::app()->user->setFlash('success','Zmiany zostały zapisane');
        $this->refresh();
      }
    }

    $this->renderPartial('createRoom',array('room'=>$room,'action'=>$action));
  }

  public function actionDeleteRoomsByIds() {
    $ids = $_POST['ids'];
    foreach($ids as $id) {
      $model = Room::model()->findByPk($id);
      if($model) $model->delete();
    }
    Yii::app()->end();
  }

  public function actionDeleteRoom($id) {
    $model = Room::model()->findByPk($id);
    if($model) $model->delete();
    Yii::app()->end();
  }
}
