<?php

class SiteController extends Controller {

  public function filters() {
    return array(
        'accessControl', // perform access control for CRUD operations
    );
  }

  public function accessRules() {
    return array(
        array('allow',
            'actions' => array('login'),
            'users' => array('*')
        ),
        array('allow',
            'actions' => array('ServeConfirmationData'),
            'users' => array('*')
        ),
        array('allow',
            'actions' => array('logout'),
            'expression' => '!$user->isGuest'
        ),
//        array('allow',
//            'actions' => array('Index'),
//            'roles' => array(
//                'CmsUser',
////                'Administrator'
//                )
//        ),
        array('allow',
          'roles'=>array('CmsUser')
        ),
        array('deny',
            'users' => array('*'),
        ),
    );
  }

  public function actionSimpletree() {
    Yii::import('application.extensions.SimpleTreeWidget');
    SimpleTreeWidget::performAjax();
  }

  /**
   * Declares class-based actions.
   */
  public function actions() {
    return array(
        // captcha action renders the CAPTCHA image displayed on the contact page
        'captcha' => array(
            'class' => 'CCaptchaAction',
            'backColor' => 0xFFFFFF,
        ),
        // page action renders "static" pages stored under 'protected/views/site/pages'
        // They can be accessed via: index.php?r=site/page&view=FileName
        'page' => array(
            'class' => 'CViewAction',
        ),
        'fileUploaderConnector' => "ext.ezzeelfinder.ElFinderConnectorAction",
    );
  }

  public function actionFileUploader() {
    $this->layout = "empty";
    $this->render('FileUploader');
  }

  /**
   * This is the default 'index' action that is invoked
   * when an action is not explicitly requested by users.
   */
  public function actionIndex() {
    $this->render('index');
  }

  /**
   * This is the action to handle external exceptions.
   */
  public function actionError() {
    if ($error = Yii::app()->errorHandler->error) {
      if (Yii::app()->request->isAjaxRequest)
        echo $error['message'];
      else
        $this->render('error', $error);
    }
  }

  /**
   * Displays the contact page
   */
  public function actionContact() {
    $model = new ContactForm;
    if (isset($_POST['ContactForm'])) {
      $model->attributes = $_POST['ContactForm'];
      if ($model->validate()) {
        $headers = "From: {$model->email}\r\nReply-To: {$model->email}";
        mail(Yii::app()->params['adminEmail'], $model->subject, $model->body, $headers);
        Yii::app()->user->setFlash('contact', 'Thank you for contacting us. We will respond to you as soon as possible.');
        $this->refresh();
      }
    }
    $this->render('contact', array('model' => $model));
  }

  /**
   * Displays the login page
   */
  public function actionLogin() {
    $this->layout = '//layouts/login';
    $model = new LoginForm;

    // if it is ajax validation request
    if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
      echo CActiveForm::validate($model);
      Yii::app()->end();
    }

    // collect user input data
    if (isset($_POST['LoginForm'])) {
      $model->attributes = $_POST['LoginForm'];
      // validate user input and redirect to the previous page if valid
      if ($model->validate() && $model->login())
        $this->redirect($this->createUrl('site/index'));
//        if(Yii::app()->user->returnUrl)
//          $this->redirect(Yii::app()->user->returnUrl);
//        else

    }
    // display the login form
    $this->render('login', array('model' => $model));
  }

  /**
   * Logs out the current user and redirect to homepage.
   */
  public function actionLogout() {
    Yii::app()->user->logout();
    $this->redirect($this->createUrl('site/login'));
  }

  public function actionServeConfirmationData($checksum) {
    $model = ConfirmationData::model()->findByAttributes(array('checksum' => $checksum));
    if ($model) {
      if (!$model->served_at) {
        if ($model->serve()) {
          Yii::app()->user->setFlash('success', Yii::t('cms', 'Dziękujemy, proces zakończony pomyślnie.'));
          $this->redirect($this->createUrl('show/index'));
        } else {
          Yii::app()->user->setFlash('error', Yii::t('cms', 'Przepraszamy, wystąpił błąd, w przypadku powtórzenia się tej sytuacji prosimy o kontakt.'));
          $this->redirect($this->createUrl('show/index'));
        }
      } else {
        Yii::app()->user->setFlash('error', Yii::t('cms', 'Adres został już wykorzystany.'));
        $this->redirect($this->createUrl('show/index'));
      }
    } else {
      throw new CHttpException(404, 'The requested page does not exist.');
    }
  }

}