<?php

class VarableController extends Controller {

  public function filters() {
    return array(
        'accessControl',
    );
  }

  public function accessRules() {
    return array(
        array('allow',
            'actions' => array('Create'),
            'roles' => array('Administrator')
        ),
        array('allow',
        'actions' => array('Index','Create','Edit'),
				'users'=>array('@'),
			),
        array('deny',
            'users' => array('*'),
        ),
    );
  }

  public function getTabs() {
    $possibleTabs = array(
        'index' => array(
            'translated' => Yii::t('cms', 'Lista zmiennych aplikacji'),
            'url' => $this->createUrl('varable/index'),
            'view' => 'index'),
        'create' => array(
            'translated' => Yii::t('cms', 'Dodaj zmienną'),
            'url' => $this->createUrl('varable/create'),
            'view' => 'create',
            'visible' => Yii::app()->user->checkAccess('Root')
        ),
    );
    $tabs = array();
    foreach ($possibleTabs as $action => $translated) {
      if (Yii::app()->getController()->action->id == $action) {
        $this->setPageTitle(Yii::app()->name . ' - ' . $translated['translated']);
        $tabs[$action] = array(
            'title' => Yii::t('cms', $translated['translated']),
            'view' => $translated['view'],
            'visible' => isset($translated['visible'])? $translated['visible']:true
        );
      } else {
        $tabs[$action] = array(
            'title' => Yii::t('cms', $translated['translated']),
            'url' => $translated['url'],
            'visible' => isset($translated['visible'])? $translated['visible']:true
        );
      }
    }
    return $tabs;
  }

  public function actionIndex() {
    $varable = new Varable();
    if (isset($_GET['Varable'])) {
      $varable->setAttributes($_GET['Varable'], false);
    }
    if (isset($_GET['pageSize'])) {
      Yii::app()->user->setState('pageSize', (int) $_GET['pageSize']);
      unset($_GET['pageSize']);
    }
    $this->render('//shared/tabs', array(
        'model' => $varable,
        'tabs' => $this->getTabs()
    ));
  }

  public function actionEdit($name, $user_id) {
    $varable = Varable::model()->findByPk(array('name' => $name, 'user_id' => $user_id));

    if (isset($_POST['Varable'])) {
      $varable->setAttributes($_POST['Varable'], false);
      if ($varable->save()) {
        Yii::app()->user->setFlash('success', Yii::t('cms', 'Zapisano zmiany.'));
        $this->redirect($this->createUrl('varable/edit', array('name' => $varable->name, 'user_id' => $varable->user_id)));
      }
    }

    $this->renderPartial('edit', array('varable' => $varable), false, true);
  }

  public function actionCreate() {
    $varable = new Varable();

    if (isset($_POST['Varable'])) {
      $varable->setAttributes($_POST['Varable'], false);
      if ($varable->save()) {
        Yii::app()->user->setFlash('success', Yii::t('cms', 'Zapisano zmiany.'));
        $this->redirect($this->createUrl('varable/index'));
      }
    }
    $this->render('//shared/tabs', array(
        'model' => $varable,
        'tabs' => $this->getTabs(),
        'additionalParams' => array('model' => $varable)
    ));
    //$this->renderPartial('create',array('varable'=>$varable),false,true);
  }

}
