<?php

class CauseController extends Controller {

  public function filters() {
    return array(
        'accessControl',
    );
  }

  public function accessRules() {
    return array(
        array('allow',
            'roles'=>array('CmsUser')
        ),
        array('deny',
            'users' => array('*'),
        ),
    );
  }

  public function actionIndex() {
    $cause = new Cause('search');
    if (isset($_GET['Cause'])) {
      $cause->setAttributes($_GET['Cause']);
    }

    $user = new User();
    if(isset($_GET['User'])) {
      $user->setAttributes($_GET['User']);
    }

    $this->render('index', array(
        'cause' => $cause,
        'user'=>$user
    ));
  }

  public function ActionActivateUser($id) {
    $cause = Cause::model()->findByPk($id);
    if ($_POST) {
      if ($cause->user->status == 'new') {
        $transaction = Yii::app()->db->beginTransaction();
        $user = $cause->user;

        $password = generatePassword();

        $user->setAttributes(array(
            'status' => 'active',
            'password' => md5($password)
                ), false);

        $emailSended = Email::clientUserAccountActivated($user->email, $user->email, $password);

        if ($user->save() && $emailSended) {
          $transaction->commit();
          Yii::app()->user->setFlash('success', 'Użytkownik został aktywowany, wysłano informację mailową.');
          $this->refresh();
        } else {
          $transaction->rollback();
          Yii::app()->user->setFlash('error', 'Wystapił nieoczekiwany błąd.');
          $this->refresh();
        }
      } else {
        Yii::app()->user->setFlash('error', 'Użytkownik jest już aktywny.');
        $this->refresh();
      }
    }
    $this->renderPartial('activateUser', array('cause' => $cause), false, true);
  }

  public function ActionAddComment($id) {
    $cause = Cause::model()->findByPk($id);
    $nextSteps = $cause->getNextPossibleSteps();

    $causeComment = new CauseComment('addComment');

    $causeComment->setAttributes(array(
        'cause_id' => $cause->id,
        'created_by' => Yii::app()->user->id,
//        'step'=>  key($nextSteps)
            ), false);

    if ($_POST) {
      $causeComment->setAttributes($_POST['CauseComment']);
      $transaction = Yii::app()->db->beginTransaction();
      if ($causeComment->save()) {
        if ($causeComment->sendEmail == 2 || Email::changeCauseStatusToClient($cause->user->email, $cause)) {
          $transaction->commit();
          Yii::app()->user->setFlash('success', 'Zmiany zostały zapisane');
          $this->refresh();
        } else {
          $transaction->rollback();
          Yii::app()->user->setFlash('error', 'Wystapił nieoczekiwany błąd.');
          $this->refresh();
        }
      }
    }

    $this->renderPartial('addComment', array('cause' => $cause, 'causeComment' => $causeComment, 'nextSteps' => $nextSteps), false, true);
  }

  public function ActionCommentsHistory($id) {
    $cause = Cause::model()->findByPk($id);

    $this->renderPartial('commentsHistory',array('cause'=>$cause), false, true);
  }

  public function ActionFilesHistory($id, $ajax = true) {
    $cause = Cause::model()->findByPk($id);
    $causeFile = new CauseFile();
    $causeFile->cause_id = $cause->id;

    $step2Form = new Step2Form();
    if(isset($_POST['Step2Form'])) {
      if(isset($_POST['Step2Form']))
        $step2Form->setAttributes($_POST['Step2Form']);
      if($step2Form->saveFiles) {
        $step2Form->file = CUploadedFile::getInstance($step2Form,'file');
      }
      if($step2Form->validate() && $step2Form->serve($cause->id,Yii::app()->user->id)) {
        Yii::app()->user->setFlash('success','Dokument został zapisany.');
        $this->refresh();
      }
    }

    if (isset($_GET['pageSize'])) {
      Yii::app()->user->setState('pageSize', (int) $_GET['pageSize']);
      unset($_GET['pageSize']);
    }

    if($ajax) {
      Yii::app()->clientScript->scriptMap['jquery.js'] = false;
      $this->renderPartial('filesHistory',array('cause'=>$cause, 'causeFile'=>$causeFile, 'step2Form'=>$step2Form, 'ajax'=>$ajax),false,true);
    }
    else {
      $this->render('filesHistory',array('cause'=>$cause, 'causeFile'=>$causeFile, 'step2Form'=>$step2Form, 'ajax'=>$ajax));
    }

  }

  public function actionDelete($id) {
    $cause = Cause::model()->findByPk($id);
    if($cause) {
      $cause->delete();
    }
    Yii::app()->end();
  }

  public function actionFileDelete($id) {
    $causeFile = CauseFile::model()->findByPk($id);
    if($causeFile) {
      $causeFile->delete();
      Yii::app()->user->setFlash('success','Dokument został usunięty.');
      $this->redirect('cause/filesHistory',array('id'=>$causeFile->cause_id));
    }
  }

}
