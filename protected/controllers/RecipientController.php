<?php

class RecipientController extends Controller {

  public $layout = '//layouts/column2';
  public $menu2;
  public $page_id;

  public function filters() {
    return array(
        'accessControl',
    );
  }

  public function accessRules()
	{
		return array(
			array('allow',
				'roles'=>array('CmsUser')
			),
      array('deny',
				'users'=>array('*'),
			),
		);
	}

  public function getTabs($model,$page) {
    $possibleTabs = array(
        'newsletter' => array(
            'translated' => Yii::t('cms', '<-- Wróć do listy wiadomości'),
            'url' => $this->createUrl('newsletter/index',array('page_id'=>$page->id)),
            'view' => ''),
        'index' => array(
            'translated' => Yii::t('cms', 'Lista odbiorców'),
            'url' => $this->createUrl('recipient/index', array('page_id'=>$page->id)),
            'view' => 'index'),
        'groups' => array(
            'translated' => Yii::t('cms', 'Grupy odbiorców'),
            'url' => $this->createUrl('recipient/groups', array('page_id'=>$page->id)),
            'view' => 'groups'),
        'add' => array(
            'translated' => Yii::t('cms', 'Dodaj odbiorców'),
            'url' => $this->createUrl('recipient/add', array('page_id'=>$page->id)),
            'view' => 'add'),
    );
    $tabs = array();
    foreach ($possibleTabs as $action => $translated) {
      if (Yii::app()->getController()->action->id == $action) {
        $tabs[$action] = array(
            'title' => Yii::t('cms', $translated['translated']),
            'view' => $translated['view'],
        );
      } else {
        $tabs[$action] = array(
            'title' => Yii::t('cms', $translated['translated']),
            'url' => $translated['url'],
        );
      }
    }
    return $tabs;
  }

  public function actionIndex($page_id) {
    $page = Page::model()->findByPk($page_id);
    $this->page_id = $page->id;
    $this->setPageTitle(Yii::t('cms', 'Lista wszystkich odbiorców'));
    $recipient = new Recipient('search');
    $group = new Group();
    if(isset($_GET['Recipient'])) {
      $recipient->setAttributes($_GET['Recipient']);
    }
    if(isset($_GET['Group'])) {
      $group->setAttributes($_GET['Group'],false);
    }

    $this->render('tabs', array(
        'page' => $page,
        'model' => false,
        'tabs' => $this->getTabs(false,$page),
        'additionalParams' => array('module_id' => Newsletter::getModuleId(),'recipient'=>$recipient,'group'=>$group)
    ));
  }

  public function actionGroups($page_id) {
    $page = Page::model()->findByPk($page_id);
    $this->setPageTitle(Yii::t('cms', 'Grupy odbiorców'));
    $this->page_id = $page->id;
    $group = new Group();

    $this->render('tabs', array(
        'page' => $page,
        'model' => false,
        'tabs' => $this->getTabs(false,$page),
        'additionalParams' => array('module_id' => Newsletter::getModuleId(), 'group'=>$group)
    ));
  }

  public function actionGroupEdit($id) {
    $group = Group::model()->findByPk($id);
    if(Yii::app()->request->isPostRequest) {
      $group->setAttributes($_POST['Group']);
      if($group->save()) {
        Yii::app()->user->setFlash('success', Yii::t('cms', 'Zmiany zostały zapisane.'));
        $this->refresh();
      }
    }

    $this->renderPartial('groupEdit',array('group'=>$group),false,true);
  }

  public function actionGroupAdd() {
    $group = new Group('create');
    if(isset($_POST['Group'])) {
      $group->setAttributes($_POST['Group']);
      if($group->save()) {
        Yii::app()->user->setFlash('success', Yii::t('cms', 'Dane zostały zapisane.'));
        $this->refresh();
      }
    }
    $this->renderPartial('addGroup', array(
        'group'=>$group
    ),false,true);
  }

  public function actionView($id) {
    $recipient = Recipient::model()->findByPk($id);
    if(Yii::app()->request->isPostRequest) {
      $recipient->setAttributes($_POST['Recipient']);
      $selectedGroups = isset($_POST['groups'])? $_POST['groups']:array();
      foreach(Group::model()->findAll() as $group) {
        $recipientGroupParams = array('recipient_id'=>$recipient->id,'group_id'=>$group->id);
        if(in_array($group->id, $selectedGroups)) {
          RecipientGroup::saveNew($recipientGroupParams['recipient_id'], $recipientGroupParams['group_id']);
        }
        else {
          $exist = RecipientGroup::model()->findByPk($recipientGroupParams);
          if($exist) $exist->delete();
        }
      }
      if($recipient->save()) {
        Yii::app()->user->setFlash('success', Yii::t('cms', 'Zmiany zostały zapisane.'));
        $this->refresh();
      }
    }

    $this->renderPartial('view',array('recipient'=>$recipient,'recipient_groups'=>  CHtml::listData($recipient->groups, 'id', 'id')),false,true);
  }

  public function actionAdd($page_id) {
    $page = Page::model()->findByPk($page_id);
    $this->page_id = $page->id;
    $this->setPageTitle(Yii::t('cms', 'Dodaj odbiorców'));
    $recipient = new Recipient();
    $emails = array();
    $errors = array();
    $groups = array();
    if(Yii::app()->request->isPostRequest) {
      $groups = isset($_POST['groups'])? $_POST['groups']:$groups;
      $emails = $_POST['emails'];
      preg_match_all(emailRegexp(),$emails,$matches);
      $emails = $matches[0];
      $emails = array_unique($emails);
      if(!$groups) {
        array_push($errors, Yii::t('cms', 'Wskaż do jakich grup przypisać odbiorców (musisz wskazać przynajmniej jedną grupę).'));
      }
      if(!$emails) {
        array_push($errors, Yii::t('cms', 'Podaj adresy email.'));
      }
      if(!$errors && isset($_POST['save'])) {
        $savedRecipients = array();
        $unsavedRecipients = array();
        $existingRecipients = array();
        foreach($emails as $email) {
          $recipient = Recipient::model()->find('`email`=:email',array(':email'=>$email));
          if(!$recipient) {
            $recipient = new Recipient('create');
            $recipient->setAttributes(array('email'=>$email));
            if($recipient->save()) {

              array_push($savedRecipients, $email);
            }
            else {
              array_push($unsavedRecipients, $email);
            }
          }
          else {
            array_push($existingRecipients, $email);
          }
          if(!$recipient->getErrors())
            foreach($groups as $group_id) RecipientGroup::saveNew ($recipient->id, $group_id);
        }
        Yii::app()->user->setFlash('success', Yii::t('cms', 'Liczba dodanych odbiorców: ').count($savedRecipients).'<br />'.  Yii::t('cms', 'Liczba odbiorców którzy już istnieli w systemie: ').  count($existingRecipients));
        if($unsavedRecipients)
          Yii::app()->user->setFlash('error', Yii::t('cms', 'Liczba odbiorców przy których wystąpił problem: ').count($unsavedRecipients).'<br />'.  implode(', ', $unsavedRecipients));
          $this->refresh();
      }
    }
    $this->render('tabs', array(
        'page' => $page,
        'model' => false,
        'tabs' => $this->getTabs(false,$page),
        'additionalParams' => array('module_id' => Newsletter::getModuleId(),'emails'=>  $emails,'errors'=>$errors,'groups'=>$groups)
    ));
  }

}