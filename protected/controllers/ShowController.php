<?php

class ShowController extends Controller {

  public $managedPageId = 0;

  public function init() {
    Yii::app()->theme = 'default';
    Yii::app()->setComponents(array(
        'errorHandler' => array(
            'errorAction' => 'show/error',
        ),
        'widgetFactory' => array(
            'widgets' => array(
                'CLinkPager' => array(
                    'cssFile' => Yii::app()->theme->baseUrl . '/css/pager.css',
                    'header' => '',
                    'prevPageLabel' => Yii::t('yii', 'Poprzednia'),
                    'nextPageLabel' => Yii::t('yii', 'Następna'),
                ),
                'CListView' => array(
                    'summaryText' => '{start}-{end} z {count}'
                ),

            ),
            )), true);

    $criteria = new CDbCriteria();
    $dependency = new CDbCacheDependency('SELECT MAX(last_update) FROM `varables`');
    $criteria->addInCondition('`name`', array('MetaDescription', 'MetaKeywords', 'MetaTitle', 'FooterAddress'));
    $varables = Varable::model()->findAll($criteria);

    $defaultVarables = array();
    foreach ($varables as $varable)
      $defaultVarables[$varable->name] = $varable->value;
    Yii::app()->setParams(array('varables' => $defaultVarables));
  }

  public function urlhistory() {
    $urls = isset(Yii::app()->session['urls']) ? Yii::app()->session['urls'] : array();
    if (!isset($urls[0]) || $urls[0] != getCurrentUrl()) {
      array_unshift($urls, getCurrentUrl());
    }
    if (count($urls) >= 2)
      unset($urls[2]);
    $urls = Yii::app()->session['urls'] = $urls;
    $this->setPageState('urlHistory', $urls);
  }

  public function actions() {
    return array(
        'captcha' => array(
            'class' => 'CCaptchaAction',
            'backColor' => 0x721472,
            'foreColor' => 0x14e343,
            'width' => 80,
            'height' => 35,
        ),
    );
  }

  public function filters() {
    return array(
        'accessControl', // perform access control for CRUD operations
    );
  }

  public function accessRules() {
    return array(
        array('allow',
            'actions' => array('index', 'homeByUrl',
                'Error',
                'Captcha',
                'Login',
                'resetPass',
                'packeges'
            ),
            'users' => array('*')
        ),
        array('allow',
            'actions' => array('logout'),
            'expression' => '!$user->isGuest'
        ),
        array('deny',
            'users' => array('*'),
        ),
    );
  }

  public function actionError() {
//    $this->layout = '//layouts/error';
    if ($error = Yii::app()->errorHandler->error) {
      $this->render('error', array('error' => $error));
    }
  }

  public function setTitle($url) {
    if ($url && $url->title)
      $this->setPageTitle($url->title);
    else
      $this->setPageTitle(Yii::app()->params['varables']['MetaTitle']);
  }

  public function setDescription($url) {
    if ($url && $url->description)
      $this->setPageState('description', $url->description);
    else
      $this->setPageState('description', Yii::app()->params['varables']['MetaDescription']);
  }

  public function setKeywords($url) {
    if ($url && $url->keywords)
      $this->setPageState('keywords', $url->keywords);
    else
      $this->setPageState('keywords', Yii::app()->params['varables']['MetaKeywords']);
  }

  public function setMeta($url) {
    $this->setTitle($url);
    $this->setDescription($url);
    $this->setKeywords($url);
  }

  public function actionIndex() {
    $this->setMeta(null);
    $this->page_id = 142;
    $page = Page::model()->findByPk($this->page_id);
    $this->render('index',array('page'=>$page));
  }

  public function actionhomeByUrl($address) {
    $this->urlhistory();
    $url = Url::model()->with('page', 'page.article', 'page.files')->find('`address`=:address', array(':address' => $address));
    if ($url) {
      $this->setMeta($url);
      $page = $url->page;
      $this->page_id = $page->id;
        if($page->parent && $page->parent->view_template=='subpage') {
          $this->managedPageId = $page->parent->id;
          $this->render('subpage',array('page'=>$page, 'parentPage'=>$page->parent));
        }
        elseif($page->parent && $page->parent->view_template=='blog') {
          $this->managedPageId = $page->parent->id;
          $searchPage = new Page();
          $searchPage->parent_id = $page->parent->id;
          $this->render('blog',array('page'=>$page, 'parentPage'=>$page->parent, 'searchPage'=>$searchPage));
        }
        elseif($page->view_template) {

          if($page->view_template=='subpage') {
            $this->render($page->view_template, array('page' => $page, 'parentPage'=>$page));
          }
          elseif($page->view_template=='blog') {
            $searchPage = new Page();
            $searchPage->parent_id = $page->id;
            $this->render($page->view_template, array('page' => $page, 'parentPage'=>$page, 'searchPage'=>$searchPage));
          }
          elseif($page->view_template=='contact') {
            $contactForm = new ContactForm();
            if(isset($_POST['ContactForm'])) {
              $contactForm->setAttributes($_POST['ContactForm']);
              if($contactForm->validate() && $contactForm->send()) {
                CCaptcha::reset_captcha();
                Yii::app()->user->setFlash('success','Dziękujemy, formularz został wysłany');
                $this->refresh();
              }
            }
            $this->render($page->view_template, array('page' => $page, 'contactForm'=>$contactForm));
          }
          else
            $this->render($page->view_template, array('page' => $page));
        }
        else {
          $this->render('article_with_gallery',array('page'=>$page));
        }
    } else {
      $this->redirect('/');
    }
  }

  public function actionLogin() {
    Yii::app()->clientScript->scriptMap['jquery.js'] = false;
    $loginForm = new LoginForm;
    if (isset($_POST['LoginForm'])) {
      $loginForm->attributes = $_POST['LoginForm'];
      if ($loginForm->validate() && $loginForm->login()) {
        echo json_encode(array('redirect' => $this->createUrl('clientPanel/home')));
        Yii::app()->end();
      }
    }
    echo json_encode(array('form' => $this->renderPartial('//shared/_login_info', array('loginForm' => $loginForm), true, false)));
    Yii::app()->end();
  }

  public function actionLogout() {
    Yii::app()->user->logout();
    $this->redirect($this->createUrl('show/index'));
  }

  public function actionResetPass() {
    $resetPassForm = new ResetPassForm();
    $formClassName = get_class($resetPassForm);
    if(isset($_POST[$formClassName])) {
      $resetPassForm->setAttributes($_POST[$formClassName]);
      if($resetPassForm->validate() && $resetPassForm->serve()) {
        Yii::app()->user->setFlash('success','Na wskazany adres została wysłana wiadomość z linkiem potwierdzającym chęć wygenerowania nowego hasła, prosimy o kliknięcie we wskazany w wiadomości link (lub skopiowanie go do okna przeglądarki).');
        $this->redirect($this->createUrl('show/index'));
      }
    }
    $this->render('resetPass', array('resetPassForm'=>$resetPassForm));
  }

  public function actionPackeges() {
    $this->page_id = Yii::app()->params['pricePageId'];
    $pricePage = Page::model()->with(array('activeChilds'=>array('condition'=>'`activeChilds`.`price_packege`=1')))->findByPk(Yii::app()->params['pricePageId']);
    $packegePage = Page::model()->with(array('activeChilds'=>array('condition'=>'`activeChilds`.`price_packege`=1')))->findByPk(Yii::app()->params['packegesPageId']);

    $otherPackegePage = Page::model()->with(array('activeChilds'=>array('condition'=>'`activeChilds`.`price_packege` IS NULL')))->findByPk(Yii::app()->params['packegesPageId']);
    $this->render('packeges',array('pricePage'=>$pricePage,'packegePage'=>$packegePage, 'otherPackegePage'=>$otherPackegePage));

  }

}
