<?php

class PageController extends Controller {

  public function filters() {
    return array(
        'accessControl',
    );
  }

  public function accessRules() {
    return array(
        array('allow',
            'actions' => array(
                'Create',
                'GoToIndex',
                'Delete',
                'View',
                'Urls',
                'Blocks',
                'StatusSwitch',
                'contentEdit',
                'contentDelete',
                'files',
                'moveUp',
                'moveDown',
                'DeleteByIds',
                'SwitchPricePackege'
                ),
            'roles'=>array('CmsUser')
        ),
        array('allow',
            'actions' => array('Edit'),
            'roles' => array(
                'Page:Edit'=>array('id'=>isset($_GET['id'])? $_GET['id']:0),
                'Root'
                )
        ),
        array('allow',
            'actions' => array('Index'),
            'roles' => array('Root')
        ),
        array('deny',
            'users' => array('*'),
        ),
    );
  }

  public function actionIndex($parent_id = null, $select = null) {
    $this->setPageTitle(Yii::t('cms', 'Struktura aplikacji'));
    $page = new Page('search');
    //$page->parent_id = $parent_id;
    $this->render('index', array('page' => $page, 'select'=>$select));
  }

  public function actionStatusSwitch($id) {
    Page::switchStatus($id);
    Yii::app()->end();
  }

  public function getTabs($model) {
    $possibleTabs = array(
        'index' => array(
            'translated' => Yii::t('cms', '<-- Wróć do listy elementów'),
            'url' => $this->createUrl($model->module->controller . '/index', array('page_id' => $model->id)),
            'view' => 'index'),
        'edit' => array(
            'translated' => Yii::t('cms', 'Edycja danych podstawowych'),
            'url' => $this->createUrl('page/edit', array('id' => $model->id)),
            'view' => 'edit'),
        'contentEdit' => array(
            'translated' => Yii::t('cms', 'Edycja treści'),
            'url' => $this->createUrl('page/contentEdit', array('id' => $model->id, 'lang_id' => Yii::app()->params['lang']->id)),
            'view' => 'content_edit'),
        'files' => array(
            'translated' => Yii::t('cms', 'Obrazki i pliki'),
            'url' => $this->createUrl('page/files', array('id' => $model->id)),
            'view' => '//shared/files'),
        'urls' => array(
            'translated' => Yii::t('cms', 'Adresy i Meta tagi'),
            'url' => $this->createUrl('page/urls', array('id' => $model->id, 'lang_id' => Yii::app()->params['lang']->id)),
            'view' => '//shared/urls'),
        'blocks' => array(
            'translated' => Yii::t('cms', 'Powiązania'),
            'url' => $this->createUrl('page/blocks', array('id' => $model->id)),
            'view' => 'blocks'),
    );
    $tabs = array();
    foreach ($possibleTabs as $action => $translated) {
      if (Yii::app()->getController()->action->id == $action) {
        $tabs[$action] = array(
            'title' => Yii::t('cms', $translated['translated']),
            'view' => $translated['view'],
        );
      } else {
        $tabs[$action] = array(
            'title' => Yii::t('cms', $translated['translated']),
            'url' => $translated['url'],
        );
      }
    }
    return $tabs;
  }

  public function actionCreate($parent_id = 0) {
    $page = new Page();
    if ($parent_id) {
      $parent = Page::model()->findByPk($parent_id);
      $page->parent_id = $parent->id;
      $page->inheritance_url = $parent->inheritance_url;
    }
    if (isset($_POST['Page'])) {
      $page->setAttributes($_POST['Page']);
      if ($page->save()) {
        Yii::app()->user->setFlash('success', Yii::t('cms', 'Podstrona została dodana.'));
        $this->redirect($this->createUrl('page/index', array('select'=>$page->id)));
      }
    }
    $this->render('create', array('page' => $page));
  }

  public function actionContentEdit($id, $lang_id) {
    $this->page_id = $id;
    $page = Page::model()->findByPk($id);
    $this->setPageTitle(Yii::t('cms', 'Edycja treści podstrony'));
    $translation = Article::model()->findByPk(array('page_id' => $id, 'lang_id' => $lang_id));
    if (!$translation) {
      $translation = new Article();
      $translation->setAttributes(array('page_id' => $id, 'lang_id' => $lang_id));
    }
    //$translation = $page->getTranslation($lang_id);
    if (Yii::app()->request->isPostRequest) {
      $translation->setAttributes($_POST['Article']);
      if ($translation->save()) {
        Yii::app()->user->setFlash('success', Yii::t('cms', 'Zmiany zostały zapisane'));
        $this->refresh();
      }
    }
    $this->render('tabs', array(
        'page' => $page,
        'tabs' => $this->getTabs($page),
        'additionalParams' => array('translation' => $translation)
    ));
  }

  public function actionContentDelete($id, $lang_id) {
    $pageTranslation = Article::model()->findByPk(array('page_id' => $id, 'lang_id' => $lang_id));
    if ($pageTranslation) {
      $pageTranslation->delete();
      Yii::app()->user->setFlash('success', 'Treść kategorii została usunięta.');
    }
    $this->redirect($this->createUrl('page/contentEdit', array('id' => $id, 'lang_id' => $lang_id)));
  }

  public function actionBlocks($id) {
    $page = Page::model()->findByPk($id);
    $this->setPageTitle('Powiązania');
    $this->page_id = $page->id;
    $this->render('tabs', array(
        'page' => $page,
        'tabs' => $this->getTabs($page)
    ));
  }

  public function actionUrls($id, $lang_id) {
    $page = Page::model()->findByPk($id);
    $this->setPageTitle('Adresy URL i Meta tagi kategorii');
    $this->page_id = $page->id;
    $this->render('tabs', array(
        'page' => $page,
        'tabs' => $this->getTabs($page),
        'additionalParams' => array('lang_id' => $lang_id, 'page' => $page)
    ));
  }

  public function actionFiles($id) {
    $page = Page::model()->findByPk($id);
    $this->setPageTitle('Obrazki i pliki');
    $this->page_id = $page->id;
    $this->render('tabs', array(
        'page' => $page,
        'tabs' => $this->getTabs($page),
        'additionalParams' => array('page' => $page)
    ));
  }

  public function actionEdit($id) {
    $page = Page::model()->findByPk($id);
    $this->setPageTitle('Edycja danych podstawowych');
    $this->page_id = $page->id;
    if (Yii::app()->request->isPostRequest) {
      $page->setAttributes($_POST['Page']);
      if ($page->save()) {
        Yii::app()->user->setFlash('success', Yii::t('cms', 'Dane zostały zapisane'));
        $this->redirect($this->createUrl('page/edit', array('id' => $page->id)));
      }
    }
    $this->render('tabs', array(
        'page' => $page,
        'tabs' => $this->getTabs($page)
    ));
  }

  public function actionGoToIndex($id) {
    $page = Page::model()->findByPk($id);
    $this->page_id = $page->id;
    $this->redirect($this->createUrl($page->module->controller . '/index', array('page_id' => $page->id)));
  }

  public function actionDelete($id) {
    $page = Page::model()->findByPk($id);
    $this->page_id = $page->id;
    $page->delete();
    Yii::app()->user->setFlash('success', Yii::t('cms', 'Element został usunięty.'));
    Yii::app()->end();
  }

  public function actionMoveUp($id) {
    $page = Page::model()->findByPk($id);
    $page->moveUp();
    Yii::app()->end();
  }

  public function actionMoveDown($id) {
    $page = Page::model()->findByPk($id);
    $page->moveDown();
    Yii::app()->end();
  }

  public function actionDeleteByIds() {
    $ids = $_POST['ids'];
    foreach($ids as $id) {
      $model = Page::model()->findByPk($id);
      if($model) $model->delete();
    }
    Yii::app()->end();
  }

  public function actionSwitchPricePackege($id) {
    $page = Page::model()->findByPk($id);
    if(!$page->price_packege) $page->price_packege = 1;
    else $page->price_packege = 0;

    $page->save();
    Yii::app()->end();
  }

}
