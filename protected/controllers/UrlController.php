<?php

class UrlController extends Controller {

  public $layout = '//layouts/column2';
  public $menu2;

  public function filters() {
    return array(
        'accessControl',
    );
  }

  public function accessRules()
	{
		return array(
			array('allow',
				'roles'=>array('CmsUser')
			),
      array('deny',
				'users'=>array('*'),
			),
		);
	}

  public function actionEdit($page_id, $lang_id) {
    $page = Page::model()->findByPk($page_id);
    $module = $page->module;
    $attributes = array('page_id' => $page->id, 'lang_id' => $lang_id);
    $url = Url::model()->findByPk($attributes);
    if (!$url) {
      $url = new Url();
      $url->setAttributes($attributes);
    }
    if($page->url_edit_form) {
      $url->setScenario ($page->url_edit_form);
      $template = $page->url_edit_form;
    }
    else {
      $template = '_form';
    }
    if (Yii::app()->request->isPostRequest) {
      $url->setAttributes($_POST['Url']);
      if ($url->save()) {
        Yii::app()->user->setFlash('success', Yii::t('cms', 'Zmiany zostały zapisane'));
        $this->refresh();
        //$this->redirect($this->createUrl('url/edit', $attributes));
      }
    }
    $this->renderPartial('edit', array(
        'page'=>$page,
        'url' => $url,
        'attributes' => $attributes,
        'template'=> $template
      ),false,true);
  }

  public function actionDelete($module_id, $element_id, $lang_id) {
    $params = array('module_id'=>$module_id,'element_id'=>$element_id,'lang_id'=>$lang_id);
    $url = Url::model()->findByPk($params);
    $url->delete();
    Yii::app()->user->setFlash('success', Yii::t('cms', 'Element został usunięty.'));
    $this->redirect($this->createUrl(Module::model()->findByPk($module_id)->controller.'/urls',array('id'=>$element_id,'lang_id'=>$lang_id)));

  }

}