<?php

class ClientPanelController extends Controller {

  public function init() {
    Yii::app()->theme = 'default';
    $juiTheme = array(
        'themeUrl' => Yii::app()->theme->baseUrl . '/css/jqueryui',
        'theme' => 'base',
    );
    Yii::app()->setComponents(array(
        'errorHandler' => array(
            'errorAction' => 'show/error',
        ),
        'widgetFactory' => array(
            'widgets' => array(
                'CLinkPager' => array(
                    'cssFile' => Yii::app()->theme->baseUrl . '/css/pager.css',
                    'header' => '',
                    'prevPageLabel' => Yii::t('yii', 'Poprzednia'),
                    'nextPageLabel' => Yii::t('yii', 'Następna'),
                ),
                'CGridView' => array(
                    'cssFile' => Yii::app()->theme->baseUrl . '/css/gridview/gridview.css',
                ),
                'CTabView' => array(
                    'cssFile' => Yii::app()->theme->baseUrl . '/css/ctabview.css',
                ),
                'CListView' => array(
                    'summaryText' => '{start}-{end} z {count}'
                ),
                'CJuiAccordion'=>$juiTheme,
                'CJuiAutoComplete'=>$juiTheme,
                'CJuiButton'=>$juiTheme,
                'CJuiDatePicker'=>$juiTheme,
                'CJuiDialog'=>$juiTheme,
                'CJuiDraggable'=>$juiTheme,
                'CJuiDroppable'=>$juiTheme,
                'CJuiInputWidget'=>$juiTheme,
                'CJuiProgressBar'=>$juiTheme,
                'CJuiResizable'=>$juiTheme,
                'CJuiSelectable'=>$juiTheme,
                'CJuiSlider'=>$juiTheme,
                'CJuiSliderInput'=>$juiTheme,
                'CJuiSortable'=>$juiTheme,
                'CJuiTabs'=>$juiTheme,
                'CJuiWidget' =>$juiTheme,
            ),
            )), true);

//    $jTheme = 'base';
//    $cs = Yii::app()->getClientScript();
//    $cs->scriptMap["jquery-ui.css"]= Yii::app()->request->baseUrl . "/css/jqueryui/$jTheme/jquery-ui-1.9.2.custom.css";
//    $cs->scriptMap["jquery-ui.min.css"]= Yii::app()->request->baseUrl . "/css/jqueryui/$jTheme/jquery-ui-1.9.2.custom.min.css";

    $criteria = new CDbCriteria();
    $dependency = new CDbCacheDependency('SELECT MAX(last_update) FROM `varables`');
    $criteria->addInCondition('`name`', array('MetaDescription', 'MetaKeywords', 'MetaTitle', 'FooterAddress'));
    $varables = Varable::model()->findAll($criteria);

    $defaultVarables = array();
    foreach ($varables as $varable)
      $defaultVarables[$varable->name] = $varable->value;
    Yii::app()->setParams(array('varables' => $defaultVarables));

    $this->layout = '//clientPanel/layout';
  }

  public function filters() {
    return array(
        'accessControl',
    );
  }

  public function accessRules() {
    return array(
        array('allow',
            'actions' => array(
                'home',
                'events',
                'eventView',
                'getFile',
                'CheckAllAsSeen',
                'user',
                'SetSeenEventsByIds',
                'reservations',
                'settlements'
                ),
            'users' => array('@')
        ),
        array('deny',
            'users' => array('*'),
        ),
    );
  }

  public function actionHome() {
    $block = Block::model()->with('activePages','activePages.article','activePages.files')->findByPk(Yii::app()->params['clientPanelBlockId']);

    $contactForm = new ContactForm2();
    if(isset($_POST['ContactForm2'])) {
      $contactForm->setAttributes($_POST['ContactForm2']);
      if($contactForm->validate()) {
        if($contactForm->send()) {
          Yii::app()->user->setFlash('success','Wiadomość została wysłana, dziękujemy.');
        }
        else {
          Yii::app()->user->setFlash('error','Przepraszamy, wystąpił błąd podczas wysyłania wiadomości. Jeśli problem będzie się powtarzał - prosimy o kontakt telefoniczny.');
        }
        $this->refresh();
      }
    }
    $this->render('home',array('block'=>$block,'contactForm'=>$contactForm));
  }

  public function actionEvents($typ = null, $pokaz = null) {
    $event = new Event('clientSearch');
    $event->user_id = Yii::app()->user->id;
    if($typ) {
      $possibleTypes = ZHtml::enumItem($event, 'type');
      foreach($possibleTypes as $typeIndex=>$type) {
        if($type==$typ) {
          $event->type = $typeIndex;
          break;
        }
      }
      if(!$event->type) {
        Yii::app()->user->setFlash('error','Przepraszamy, wskazany parametr nie istnieje.');
        $this->redirect($this->createUrl('clientPanel/events'));
      }
      else {
        $this->setPageTitle('Lista zdarzeń: '.$event->translateEnumValue('type'));
      }
    }
    else {
      $this->setPageTitle('Lista wszystkich zdarzeń');
    }
    if($pokaz) {
      $event->seen = 'unseen';
      $this->setPageTitle('Lista nieprzeczytanych zdarzeń');
    }
    if(isset($_GET['Event'])) {
      $event->setAttributes($_GET['Event']);
    }

    if (isset($_GET['pageSize'])) {
        Yii::app()->user->setState('pageSize',(int)$_GET['pageSize']);
        unset($_GET['pageSize']);
    }

    $unseenEvents = Event::model()->find('user_id=:user_id AND seen IS NULL',array(':user_id'=>Yii::app()->user->id));

    $this->render('events', array('event'=>$event, 'typ'=>$typ, 'unseenEvents'=>$unseenEvents));
  }

  public function actionEventView($id) {
    $event = Event::model()->findByPk($id);
    if($event->notSeen()) $event->setSeen();
    $this->renderPartial('eventView',array('event'=>$event),false,true);
  }

  public function actionGetFile($hash1, $hash2) {
    $file = EventFile::model()->find('`hidden_filename`=:hidden_filename',array(':hidden_filename'=>$hash2));
    if($file) {
      if($file->createLinkHash1()==$hash1) {
        $event = Event::model()->findByPk($file->event_id);
        $filepath = Yii::app()->basePath.'/'.EventFile::getFolderPath($event->user_id,$event->type,$event->id).'/'.$file->hidden_filename;
        if(file_exists($filepath)) {
          header('Content-type: '.mime_content_type2($file->filename));
          header('Content-Disposition: attachment; filename="'.$file->filename.'"');
          readfile($filepath);
          Yii::app()->end();
        }
      }
    }
    throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
  }

  public function actionCheckAllAsSeen() {
    Event::checkAllAsSeen(Yii::app()->user->id);

    Yii::app()->user->setFlash('success','Wszystkie zdarzenia zostały oznaczone jako przeczytane.');
    $this->redirect($this->createUrl('clientPanel/events'));
  }

  public function actionUser($tab=null) {
    if($_POST && Yii::app()->user->id == Yii::app()->params['demoUserId']) {
      Yii::app()->user->setFlash('success','Użytkownik demonstracyjny nie może edytować swoich danych.');
      $this->refresh();
    }
    $this->setPageTitle('Dane użytkownika');
    $user = User::model()->findByPk(Yii::app()->user->id);
    $user->setScenario('clientEdit');
    if($_POST) {
      if(isset($_POST['save1'])) {
        $tab = 'tab1';
        $user->setAttributes(array(
            'name'=>$_POST['User']['name'],
            'surname'=>$_POST['User']['surname'],
            'email'=>$_POST['User']['email'],
            'phone'=>$_POST['User']['phone'],
        ));
        if($user->save()) {
          Yii::app()->user->setFlash('success','Zmiany zostały zapisane.');
          $this->redirect($this->createUrl('clientPanel/user',array('tab'=>$tab)));
        }
      }
      elseif(isset($_POST['save2'])) {
        $tab = 'tab2';
        $user->setAttributes(array(
            'nip'=>$_POST['User']['nip'],
            'company_name'=>$_POST['User']['company_name'],
            'postcode'=>$_POST['User']['postcode'],
            'city'=>$_POST['User']['city'],
            'street'=>$_POST['User']['street'],
        ));
        if($user->save()) {
          Yii::app()->user->setFlash('success','Zmiany zostały zapisane.');
          $this->redirect($this->createUrl('clientPanel/user',array('tab'=>$tab)));
        }
      }
      elseif(isset($_POST['save3'])) {
        $tab = 'tab3';
        $user->setAttributes(array(
            'post_postcode'=>$_POST['User']['post_postcode'],
            'post_city'=>$_POST['User']['post_city'],
            'post_street'=>$_POST['User']['post_street'],
        ));
        if($user->save()) {
          Yii::app()->user->setFlash('success','Zmiany zostały zapisane.');
          $this->redirect($this->createUrl('clientPanel/user',array('tab'=>$tab)));
        }
      }
      elseif(isset($_POST['save4'])) {
        $tab = 'tab4';
        $user->setScenario('ChangePass');
        $user->password = $_POST['User']['password'];
        $user->repeat_password = $_POST['User']['repeat_password'];
        if($user->save()) {
          Yii::app()->user->setFlash('success','Zmiany zostały zapisane.');
          $this->redirect($this->createUrl('clientPanel/user',array('tab'=>$tab)));
        }
      }
    }

    $this->render('user',array('user'=>$user, 'tab'=>($tab)? $tab:'tab1'));
  }

  public function actionSetSeenEventsByIds() {
    $ids = $_POST['ids'];
    foreach($ids as $id) {
      $model = Event::model()->findByPk($id);
      if($model->user_id==Yii::app()->user->id) {
        if($model->notSeen()) $model->setSeen();
      }
    }
    Yii::app()->end();
  }

  public function actionReservations($typ) {
    $this->setPageTitle('Rezerwacje');
    $reservation = new Reservation('clientSearch');
    $reservation->assignTypeFromInput($typ);

    $reservation->type = $typ;
    switch ($reservation->type) {
    case 1:
    case 2:
    case 3:
    case 4:
      $this->setPageTitle($reservation->translateEnumValue('type'));
      break;

    default:
      $this->redirect ($this->createUrl('clientPanel/reservations',array('typ'=>1)));
      break;
    }

    if(isset($_POST['Reservation'])) {
      $reservation->setAttributes($_POST['Reservation']);
    }

    if($reservation->type) {
      $rooms = Room::pickUpRooms($reservation->type, $reservation);
    }
    else $rooms = array();

    $reservations = $reservation->pickUpReservations();

    if(isset($_GET['ajax'])) {
      $this->renderPartial('reservations',array(
          'reservation'=>$reservation,
          'rooms'=>$rooms,
          'reservations'=>$reservations
          ),false,true);
    }
    else {
      $this->render('reservations',array(
          'reservation'=>$reservation,
          'rooms'=>$rooms,
          'reservations'=>$reservations
          ));
    }
  }

  public function actionSettlements() {
    $this->setPageTitle('Rozliczenie');
    $settlement = new Settlement();
    $settlement->user_id = Yii::app()->user->id;

    if (isset($_GET['pageSize'])) {
        Yii::app()->user->setState('pageSize',(int)$_GET['pageSize']);
        unset($_GET['pageSize']);
    }

    if(isset($_GET['Settlement'])) {
      $settlement->date_from = $_GET['Settlement']['date_from'];
      $settlement->date_to = $_GET['Settlement']['date_to'];
      $settlement->byMonth = $_GET['Settlement']['byMonth'];
    }

    $this->render('settlements',array('settlement'=>$settlement));
  }
}
