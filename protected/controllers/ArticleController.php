<?php

class ArticleController extends Controller {

  public function filters() {
    return array(
        'accessControl',
    );
  }

  public function accessRules()
	{
		return array(
			array('allow',
          'roles'=>array('CmsUser')
			),
      array('deny',
				'users'=>array('*'),
			),
		);
	}

  public function actionIndex($page_id) {
    $page = Page::model()->findByPk($page_id);
    $this->setPageTitle(Yii::t('cms', 'Lista'));
    $this->page_id = $page->id;

    $searchPage = new Page('searchArticle');
    $searchPage->parent_id = $page->id;

    if (isset($_GET['pageSize'])) {
        Yii::app()->user->setState('pageSize',(int)$_GET['pageSize']);
        unset($_GET['pageSize']);
    }

    $block = new Block();
    if(isset($_GET['Block']))
      $block->setAttributes ($_GET['Block'],false);

    $this->render('index', array(
        'page'=>$page,
        'searchPage'=>$searchPage,
        'block'=>$block
    ));
  }

  public function actionCreate($page_id) {
    $parentPage = Page::model()->findByPk($page_id);
    $this->page_id = $parentPage->id;

    $this->setPageTitle(($page_id==153)? Yii::t('cms', 'Dodaj nowy produkt'):Yii::t('cms', 'Dodaj nowy artykuł'));

    $page = new Page();
    $page->setAttributes(array('module_id'=>Module::getModuleId('Article'),'parent_id'=>$page_id));

    $article = new Article();
    $article->lang_id = Yii::app()->params['lang']->id;

    if (Yii::app()->request->isPostRequest) {
      $article->setAttributes($_POST['Article']);
      $page->setAttributes($_POST['Page']);

      $transaction = Yii::app()->db->beginTransaction();
      $commit = true;
      if($page->save()) {
        $article->setAttribute('page_id', $page->id);
        if(!$article->save())
          $commit=false;
      }
      else $commit = false;

      if($commit) {
        $transaction->commit();
        Yii::app()->user->setFlash('success', Yii::t('cms', 'Nowy artykuł został utworzony.'));
        $this->redirect($this->createUrl('article/edit', array('id' => $page->id,'lang_id'=>$article->lang_id)));
      }
      else {
        $transaction->rollback();
      }
    }

    $this->render('create', array(
        'page'=>$page,
        'article'=>$article,
        'parentPage' => $parentPage,
    ));
  }

  public function actionCreateCategory($parent_id) {
    $page = Page::model()->findByPk($parent_id);
    $this->page_id = $page->id;
    $newPage = new Page('create');
    $newPage->setAttributes(array(
        'parent_id'=>$page->id,
        'type'=>'Articles',
    ));

    if (Yii::app()->request->isPostRequest) {
      $newPage->setAttributes($_POST['Page']);
      if($newPage->save()) {
        Yii::app()->user->setFlash('success', Yii::t('cms', 'Nowa kategoria została utworzona.'));
        $this->redirect($this->createUrl('article/index', array('page_id' => $newPage->id)));
      }
    }

    $this->render('createCategory', array(
        'page'=>$page,
        'newPage'=>$newPage
    ));
  }

  public function getTabs($page) {
    $possibleTabs = array(
        'edit' => array(
            'translated' => Yii::t('cms', 'Edycja treści'),
            'url' => $this->createUrl('article/edit', array('id' => $page->id, 'lang_id'=>Yii::app()->params['defaultLang']->id)),
            'view' => 'edit'),
        'files' => array(
            'translated' => Yii::t('cms', 'Obrazki i pliki'),
            'url' => $this->createUrl('article/files', array('id' => $page->id)),
            'view' => '//shared/files'),
        'urls' => array(
            'translated' => Yii::t('cms', 'Adresy i Meta tagi'),
            'url' => $this->createUrl('article/urls', array('id' => $page->id, 'lang_id'=>Yii::app()->params['defaultLang']->id)),
            'view' => '//shared/urls'),
        'blocks' => array(
            'translated' => Yii::t('cms', 'Bloki i wystąpienia'),
            'url' => $this->createUrl('article/blocks', array('id' => $page->id)),
            'view' => 'blocks'),
    );
    if($page->parent_id==Yii::app()->params['pricePageId']) {
      $possibleTabs = array_merge($possibleTabs, array(
          'prices' => array(
            'translated' => Yii::t('cms', 'Ceny/Pakiety'),
            'url' => $this->createUrl('article/prices', array('id' => $page->id)),
            'view' => 'prices'),
      ));
    }
    if($page->parent_id==Yii::app()->params['packegesPageId']) {
      $possibleTabs = array_merge($possibleTabs, array(
          'products' => array(
            'translated' => Yii::t('cms', 'Produkty w pakiecie'),
            'url' => $this->createUrl('article/products', array('id' => $page->id)),
            'view' => 'products'),
      ));
      $possibleTabs = array_merge($possibleTabs, array(
          'prices2' => array(
            'translated' => Yii::t('cms', 'Ceny'),
            'url' => $this->createUrl('article/prices2', array('id' => $page->id)),
            'view' => 'prices2'),
      ));
    }
    $tabs = array();
    foreach ($possibleTabs as $action => $translated) {
      if (Yii::app()->getController()->action->id == $action) {
        $tabs[$action] = array(
            'title' => Yii::t('cms', $translated['translated']),

            'view' => $translated['view'],
        );
      } else {
        $tabs[$action] = array(
            'title' => Yii::t('cms', $translated['translated']),
            'url' => $translated['url'],
        );
      }
    }
    return $tabs;
  }

  public function actionEdit($id, $lang_id = null) {
    if(!$lang_id) $lang_id = Yii::app()->params['lang']->id;
    $page = Page::model()->findByPk($id);
    $this->page_id = $page->id;
    $this->setPageTitle(Yii::t('cms', 'Edycja treści artykułu'));
    $article = Article::model()->findByPk(array('page_id'=>$page->id,'lang_id'=>$lang_id));
    if(!$article) {
      $article = new Article();
      $article->setAttributes(array('page_id'=>$page->id,'lang_id'=>$lang_id));
    }
    if (Yii::app()->request->isPostRequest) {
      $article->setAttributes($_POST['Article']);
      if($article->save()) {
        Yii::app()->user->setFlash('success', Yii::t('cms', 'Zmiany zostały zapisane'));
        $this->redirect($this->createUrl('article/edit', array('id'=>$article->page_id,'lang_id' => $article->lang_id)));
      }
    }
    $this->render('tabs', array(
        'page' => $page,
        'tabs'=>$this->getTabs($page),
        'article'=>$article,
    ));
  }

  public function actionFiles($id) {
    $page = Page::model()->findByPk($id);
    $this->setPageTitle(Yii::t('cms', 'Lista plików artykułu'));
    $this->page_id = $page->id;
    $this->render('tabs', array(
        'page' => $page,
        'tabs'=>$this->getTabs($page),
        'article'=>null,
        'additionalParams'=>array('page'=>$page)
    ));
  }

  public function actionPrices($id) {
    $page = Page::model()->findByPk($id);
    $this->setPageTitle(Yii::t('cms', 'Ceny produktu/usługi'));
    if(isset($_POST['Page'])) {
      $page->setAttributes($_POST['Page']);
      if($page->save()) {
        Yii::app()->user->setFlash('success','Zmiany zostały zapisane.');
        $this->refresh();
      }
    }
    $this->render('tabs', array(
        'page' => $page,
        'tabs'=>$this->getTabs($page),
        'article'=>null,
        'additionalParams'=>array('page'=>$page)
    ));
  }

  public function actionPrices2($id) {
    $page = Page::model()->findByPk($id);
    $this->setPageTitle(Yii::t('cms', 'Ceny pakietu'));
    if(isset($_POST['Page'])) {
      $page->setAttributes($_POST['Page']);
      if($page->save()) {
        Yii::app()->user->setFlash('success','Zmiany zostały zapisane.');
        $this->refresh();
      }
    }
    $this->render('tabs', array(
        'page' => $page,
        'tabs'=>$this->getTabs($page),
        'article'=>null,
        'additionalParams'=>array('page'=>$page)
    ));
  }

  public function actionProducts($id) {
    $page = Page::model()->findByPk($id);
    $this->setPageTitle(Yii::t('cms', 'Dostępne produkty/usługi'));

    $product = new Page();
    $product->parent_id = Yii::app()->params['pricePageId'];
    $this->render('tabs', array(
        'page' => $page,
        'tabs'=>$this->getTabs($page),
        'article'=>null,
        'additionalParams'=>array('page'=>$page,'product'=>$product)
    ));
  }

  public function actionBlocks($id, $ajax = false) {
    $page = Page::model()->findByPk($id);
    $this->setPageTitle(Yii::t('cms', 'Bloki na stronie i wystąpienia artykułu'));
    $this->page_id = $id;
    if ($ajax) {
      Yii::app()->clientScript->scriptMap['jquery.js'] = false;
      Yii::app()->clientScript->scriptMap['jquery.ba-bbq.js'] = false;
      Yii::app()->clientScript->scriptMap['jquery.yiigridview.js'] = false;
      $this->renderPartial('blocks', array('page' => $page),false,true);
    } else {
      $this->render('tabs', array(
          'page' => $page,
          'tabs' => $this->getTabs($page),
          'article'=>null,
          'additionalParams' => array('page'=>$page)
      ));
    }
  }

  public function actionUrls($id, $lang_id) {
    $page = Page::model()->findByPk($id);
    $this->setPageTitle(Yii::t('cms', 'Adresy URL i Meta Tagi strony'));
    $this->page_id = $page->id;
    $this->render('tabs', array(
        'page'=>$page,
        'article'=>null,
        'tabs'=>$this->getTabs($page),
        'additionalParams'=>array('lang_id'=>$lang_id,'page'=>$page)
    ));
  }

  public function actionDelete($id, $lang_id) {
    $page = Page::model()->findByPk($id);
    $article = Article::model()->findByPk(array('page_id'=>$id,'lang_id'=>$lang_id))->delete();
    Yii::app()->user->setFlash('success', Yii::t('cms', 'Element został usunięty.'));
    if(Page::model()->findByPk($id))
      $this->redirect($this->createUrl('article/edit', array('id' => $id,'lang_id'=>$lang_id)));
    else
      $this->redirect($this->createUrl('article/index', array('page_id' => $page->parent_id)));
  }

  public function actionDeleteAll($page_id) {
    $page = Page::model()->findByPk($page_id);
    $page->delete();
    Yii::app()->user->setFlash('success', Yii::t('cms', 'Element został usunięty.'));
    $this->redirect($this->createUrl('article/index', array('page_id' => $page->parent_id)));
  }

  public function actionStatusSwitch($id, $confirm = 0) {
    $model = Page::model()->findByPk($id);
    if($model->status==1) $model->status = 0;
    elseif($model->status==0) $model->status = 1;
    $model->save();
    if($confirm) {
      Yii::app()->user->setFlash('success', Yii::t('cms', 'Artykuł został aktywowany.'));
      $this->redirect($this->createUrl('article/edit', array('id' => $id)));
    }
  }

//  public function actionSort() {
//    if (isset($_POST['items']) && is_array($_POST['items'])) {
//      foreach ($_POST['items'] as $i => $item) {
//        $model = Article::model()->findByPk($item);
//        $model->position = $i + 1;
//        $model->save();
//      }
//    }
//  }

  public function actionSort() {
    if (isset($_POST['items']) && is_array($_POST['items'])) {
      $positionPrefix = isset($_POST['positionPrefix'])? intval($_POST['positionPrefix']):0;
      foreach ($_POST['items'] as $i => $item) {
        $model = Page::model()->findByPk($item);
        //$model->position = $positionPrefix + $i + 1;
        $model->pageOrder($positionPrefix + $i + 1);
        //$model->save();
      }
    }
  }

  public function actionAssignProduct($packegeId, $productId) {
    $productPackege = new ProductPackege();
    $productPackege->setAttributes(array(
        'product_id'=>$productId,
        'packege_id'=>$packegeId
    ),false);
    $exist = ProductPackege::model()->findByPk(array('product_id'=>$productId,'packege_id'=>$packegeId));
    if($exist) {
      $exist->delete();
      Yii::app()->user->setFlash('success','Powiązanie zostało usunięte.');
      $this->redirect($this->createUrl('article/assignProductCompleted'));
      Yii::app()->end();
    }
    else {
      $productPackege->unit_price = $productPackege->productPage->unit_price;
      if(isset($_POST['ProductPackege'])) {
        $productPackege->setAttributes($_POST['ProductPackege'],false);
        if($productPackege->save()) {
          Yii::app()->user->setFlash('success','Nowe powiązanie zostało zapisane.');
          $this->redirect($this->createUrl('article/assignProductCompleted'));
        }
      }
      $this->renderPartial('assignProduct', array('productPackege'=>$productPackege),false,true);
    }
  }

  public function actionAssignProductCompleted() {
    $this->renderPartial('assignProductCompleted',array(),false,true);
  }
}
