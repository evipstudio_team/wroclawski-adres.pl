<?php

class BannerController extends Controller {

  public $layout = '//layouts/column2';
  public $menu2;
  public $page_id;

  public function filters() {
    return array(
        'accessControl',
    );
  }

  public function accessRules()
	{
		return array(
			array('allow',
				'roles'=>array('CmsUser')
			),
      array('deny',
				'users'=>array('*'),
			),
		);
	}

  public function actionIndex($page_id) {
    $page = Page::model()->findByPk($page_id);
    $this->page_id = $page->id;
    $this->setPageTitle(Yii::t('cms', 'Lista elementów reklamowych'));

    $banner = new Banner('search');
    $banner->unsetAttributes();
    if (isset($_GET['Banner']))
      $banner->attributes = $_GET['Banner'];

    $this->render('index', array(
        'page' => $page,
        'banner' => $banner
    ));
  }

  public function actionCreate($page_id) {
    $page = Page::model()->findByPk($page_id);
    $this->page_id = $page->id;
    $this->setPageTitle(Yii::t('cms', 'Dodaj nowy element reklamowy'));

    $banner = new Banner();
    if (Yii::app()->request->isPostRequest) {
      $transaction = Yii::app()->db->beginTransaction();
      $commit = true;

      $banner->page_id = $page_id;
      if(!$banner->save()) $commit = false;
      $fileUploadResult = Multimedia::uploadFiles($_FILES, $banner->page->module->id, $banner->id);
      if($fileUploadResult['error']) {
        $commit = false;
        $banner->addError('page_id', implode('<br />', $fileUploadResult['error']));
      }

      if($commit) {
        $transaction->commit();
        Yii::app()->user->setFlash('success', Yii::t('cms', 'Nowy element został utworzony.'));
        $this->redirect($this->createUrl('banner/index', array('page_id' => $banner->page_id)));
      }
      else {
        $transaction->rollback();
      }
    }

    $this->render('create', array(
        'page' => $page,
        'banner' => $banner
    ));
  }

  public function getTabs($banner) {
    $possibleTabs = array(
//        'view' => array(
//            'translated' => Yii::t('cms', 'Informacje podstawowe'),
//            'url' => $this->createUrl('banner/view', array('id' => $banner->id)),
//            'view' => 'view'),
        'edit' => array(
            'translated' => Yii::t('cms', 'Edycja'),
            'url' => $this->createUrl('banner/edit', array('id' => $banner->id)),
            'view' => 'edit'),
        'files' => array(
            'translated' => Yii::t('cms', 'Obrazki i pliki'),
            'url' => $this->createUrl('banner/files', array('id' => $banner->id)),
            'view' => 'files'),
        'urls' => array(
            'translated' => Yii::t('cms', 'Adresy i Meta tagi'),
            'url' => $this->createUrl('banner/urls', array('id' => $banner->id, 'lang_id'=>Yii::app()->params['defaultLang']->id)),
            'view' => 'urls'),

//        'linkToExistingElement' => array(
//            'translated' => Yii::t('cms', 'Link do istniejącego elementu'),
//            'url' => $this->createUrl('banner/linkToExistingElement', array('id' => $banner->id)),
//            'view' => 'linkToExistingElement'),
//        'linkToExternal' => array(
//            'translated' => Yii::t('cms', 'Link do zewnętrznego adresu'),
//            'url' => $this->createUrl('banner/linkToExternal', array('id' => $banner->id)),
//            'view' => 'linkToExternal'),
    );
    $tabs = array();
    foreach ($possibleTabs as $action => $translated) {
      if (Yii::app()->getController()->action->id == $action) {
        $tabs[$action] = array(
            'title' => Yii::t('cms', $translated['translated']),
            'view' => $translated['view'],
        );
      } else {
        $tabs[$action] = array(
            'title' => Yii::t('cms', $translated['translated']),
            'url' => $translated['url'],
        );
      }
    }
    return $tabs;
  }

  public function actionView($id) {
    $banner = Banner::model()->findByPk($id);
    $this->page_id = $banner->page->id;
    $this->setPageTitle(Yii::app()->name.' - '.$banner->page->name.' - '.  Yii::t('cms', 'Karta wpisu'));

    $this->render('tabs', array(
        'page' => $banner->page,
        'tabs'=>$this->getTabs($banner),
        'banner' => $banner
    ));
  }

  public function actionEdit($id) {
    $banner = Banner::model()->findByPk($id);
    $this->page_id = $banner->page->id;
    $this->setPageTitle(Yii::t('cms', 'Edycja elementu reklamowego'));

    $this->render('tabs', array(
        'page' => $banner->page,
        'tabs'=>$this->getTabs($banner),
        'banner' => $banner
    ));
  }

  public function actionUrls($id, $lang_id) {
    $banner = Banner::model()->findByPk($id);
    $this->page_id = $banner->page->id;
    $this->setPageTitle(Yii::t('cms', 'Adresy URL i Meta Tagi elementu reklamowego'));
    $this->render('tabs', array(
        'page' => $banner->page,
        'tabs'=>$this->getTabs($banner),
        'banner' => $banner,
        'additionalParams'=>array('lang_id'=>$lang_id,'module_id'=>Banner::getModuleId())
    ));
  }

  public function actionFiles($id) {
    $banner = Banner::model()->findByPk($id);
    $this->page_id = $banner->page->id;
    $this->setPageTitle(Yii::t('cms', 'Lista obrazków i plików elementu reklamowego'));

    $this->render('tabs', array(
        'page' => $banner->page,
        'tabs'=>$this->getTabs($banner),
        'banner' => $banner
    ));
  }

  public function actionSort() {
    if (isset($_POST['items']) && is_array($_POST['items'])) {
      foreach ($_POST['items'] as $i => $item) {
        $model = Banner::model()->findByPk($item);
        $model->position = $i + 1;
        $model->save();
      }
    }
  }

}