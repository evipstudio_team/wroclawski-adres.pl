<?php

class SettlementController extends Controller {

  public function filters() {
    return array(
        'accessControl',
    );
  }

  public function behaviors() {
    return array(
        'eexcelview' => array(
            'class' => 'ext.eexcelview.EExcelBehavior',
        ),
    );
  }

  public function accessRules() {
    return array(
        array('allow',
            'roles' => array('CmsUser')
        ),
        array('deny',
            'users' => array('*'),
        ),
    );
  }

  public function actionIndex() {
    $settlement = new Settlement('search');
    $user = new User();

    if (isset($_GET['User'])) {
      $user->setAttributes($_GET['User']);
    }
    if (isset($_GET['Settlement'])) {
      $settlement->setAttributes($_GET['Settlement']);
    }

    if (isset($_GET['pageSize'])) {
      Yii::app()->user->setState('pageSize', (int) $_GET['pageSize']);
      unset($_GET['pageSize']);
    }

    if (isset($_GET['xls'])) {
      $dP = $settlement->search($user);
      $dP->pagination = false;
      $this->toExcel($dP, array(
          'user.company_name',
          'user.name',
          'user.email',
          'date',
          'title',
          'note',
          'formattedValue',
          ), 'Test File', array('creator' => Yii::app()->name), 'Excel5');
    } else {
      $this->render('index', array(
          'settlement' => $settlement,
          'user' => $user,
      ));
    }
  }

  public function actionCreate() {
    $settlement = new Settlement();
    $settlement->date = date('Y-m-d H:i:s');
    if (isset($_GET['User'])) {
      $email = $_GET['User']['email'];
      $user = User::model()->find('`email`=:email', array(':email' => $email));
      if ($user)
        $settlement->user_id = $user->id;
    }
    if (isset($_POST['Settlement'])) {
      $settlement->setAttributes($_POST['Settlement']);
      if ($settlement->save()) {
        Yii::app()->user->setFlash('success', 'Pozycja została zapisana.');
        $this->redirect($this->createUrl('settlement/message'));
      }
    }
    $this->renderPartial('create', array('settlement' => $settlement), false, true);
  }

  public function actionEdit($id) {
    $settlement = Settlement::model()->findByPk($id);
    if (isset($_POST['Settlement'])) {
      $settlement->setAttributes($_POST['Settlement']);
      if ($settlement->save()) {
        Yii::app()->user->setFlash('success', 'Zmiany zostały zapisane.');
        $this->redirect($this->createUrl('settlement/message'));
      }
    }
    $this->renderPartial('edit', array('settlement' => $settlement), false, true);
  }

  public function actionDelete($id) {
    $model = Settlement::model()->findByPk($id);
    $model->delete();
    Yii::app()->end();
  }

  public function actionDeleteByIds() {
    $ids = $_POST['ids'];
    foreach ($ids as $id) {
      $model = Settlement::model()->findByPk($id);
      if ($model)
        $model->delete();
    }
    Yii::app()->end();
  }

  public function actionMessage() {
    $this->renderPartial('//shared/message', array(), false, true);
  }

}
