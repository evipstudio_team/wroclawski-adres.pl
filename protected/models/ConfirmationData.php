<?php

class ConfirmationData extends CActiveRecord {

  public static function model($className = __CLASS__) {
    return parent::model($className);
  }

  /**
   * @return string the associated database table name
   */
  public function tableName() {
    return 'confirmation_data';
  }

  public function primaryKey() {
    return 'id';
  }

  public function beforeSave() {
    if ($this->isNewRecord) {
      $this->created_at = date('Y-m-d H:i:s');
      $this->checksum = md5($this->obj_name.$this->field_name.$this->obj_id.$this->new_value.time());
    }
    return parent::beforeSave();
  }

  public function serve() {
    $obj_name = $this->obj_name;
    if(strpos($obj_name, 'Controller')!==false) {
      return $this->serveByController();
    }
    else {
      return $this->serveByModel();
    }

  }

  public function serveByController() {
    $controler = str_replace('Controller', '', $this->obj_name);
    Yii::app()->getController()->forward($controler.'/'.$this->field_name);
  }

  public function serveByModel() {
    $obj_name = $this->obj_name;
    switch ($this->obj_name) {
      case 'User':
        $related_obj = User::model()->findByPk($this->obj_id);
        break;
      case 'Recipient':
        $related_obj = Recipient::model()->findByPk($this->obj_id);
        break;

      default:
        break;
    }
    ;
    $related_obj->{$this->field_name} = $this->new_value;
    if($related_obj->save()) {
      $this->close();
      return true;
    }
    else return false;
  }

  public function close() {
    $this->served_at = date('Y-m-d H:i:s');
    $this->save();
  }

  /**
   * @return array relational rules.
   */
  public function relations() {
    // NOTE: you may need to adjust the relation name and the related
    // class name for the relations automatically generated below.
    return array(
    );
  }

}