<?php

Yii::import('application.extensions.phpmailer.JPhpMailer');

class Email extends CActiveRecord {

  protected $phpmailer_email;

  public static function model($className = __CLASS__) {
    return parent::model($className);
  }

  public function tableName() {
    return 'emails';
  }

  public function primaryKey() {
    return 'id';
  }

  static public function perform_new_mail($config = false) {
    $email = new Email();
    $email->phpmailer_email = new JPhpMailer;
    $email->phpmailer_email->IsSMTP();

      $email->phpmailer_email->SMTPOptions = array(
          "ssl" => array(
              "verify_peer" => false,
              "verify_peer_name" => false,
              "allow_self_signed" => true
          )
      );

    $email->phpmailer_email->Host = Varable::pickUp('SenderHost',null,0);
    $email->phpmailer_email->Username = Varable::pickUp('SenderLogin',null,0);
    $email->phpmailer_email->Port = 587;
    $email->phpmailer_email->Password = Varable::pickUp('SenderPassword',null,0);
    $email->phpmailer_email->SetFrom(Varable::pickUp('SenderEmail',null,0), Varable::pickUp('SenderName',null,0));
    $email->phpmailer_email->SMTPSecure = 'tls';
    $email->phpmailer_email->Sender = Varable::pickUp('SenderEmail',null,0);

    $email->phpmailer_email->SMTPAuth = true;
    $email->phpmailer_email->SMTPKeepAlive = true;
    $email->phpmailer_email->Mailer = "smtp";
    $email->phpmailer_email->SMTPAuth = true;
    $email->phpmailer_email->CharSet = 'utf-8';
    $email->phpmailer_email->SMTPDebug = 0;

    return $email;
  }

  static public function performNewMail($config = false) {
    return self::perform_new_mail($config);
  }

  public function rules() {
    return array(
        array('from, to, title', 'required'),
    );
  }

  public function send_mail() {

    $this->setAttributes(
            array(
        'from' => $this->phpmailer_email->FromName . '<' . $this->phpmailer_email->From,
        'to' => $this->phpmailer_email->getTo(),
        'title' => $this->phpmailer_email->Subject,
        //'body_html' => $this->phpmailer_email->Body,
        //'body_text' => $this->phpmailer_email->AltBody
            ), true);

    if ($this->save()) {
      if ($this->phpmailer_email->Send()) {
        $this->sended_at = date('Y-m-d H:i:s');
        $this->save();
        return true;
      }
      else {

      }
    }
    return false;
  }

  public function beforeSave() {
    if ($this->isNewRecord) {
      $this->created_at = date('Y-m-d H:i:s');
    }
    return parent::beforeSave();
  }

  static public function user_registration($to, $checksum) {
    $rendered_file_txt = CConsoleCommand::renderFile(Yii::app()->basePath . '/views/user/email_templates/user_registration_txt.php', array('checksum' => $checksum), true);
    $email = self::perform_new_mail(Yii::app()->params['smtp']['default']);
    $email->phpmailer_email->AddAddress($to);
    $email->phpmailer_email->Subject = Yii::app()->name . '- rejestracja, link aktywacyjny';
    $email->phpmailer_email->AltBody = strip_tags($rendered_file_txt);
    $email->phpmailer_email->MsgHTML(nl2br($rendered_file_txt));
    $email->send_mail();
  }

  static public function reset_password_step1($to, $checksum) {
    $rendered_file_txt = CConsoleCommand::renderFile(Yii::app()->basePath . '/views/email_templates/reset_password_step1_txt.php', array('checksum' => $checksum), true);
    $email = self::performNewMail();
    $email->phpmailer_email->AddAddress($to);
    $email->phpmailer_email->Subject = Yii::app()->name . '- link potwierdzający prośbę o wygenerowanie nowego hasła';
    $email->phpmailer_email->AltBody = strip_tags($rendered_file_txt);
    $email->phpmailer_email->MsgHTML(nl2br($rendered_file_txt));
    return $email->send_mail();
  }

  static public function reset_password_step2($to, $pass) {
    $rendered_file_txt = CConsoleCommand::renderFile(Yii::app()->basePath . '/views/user/email_templates/reset_password_step2_txt.php', array('pass' => $pass, 'email' => $to), true);
    $email = self::perform_new_mail(Yii::app()->params['smtp']['default']);
    $email->phpmailer_email->AddAddress($to);
    $email->phpmailer_email->Subject = Yii::app()->name . '- wygenerowano nowe hasło';
    $email->phpmailer_email->AltBody = strip_tags($rendered_file_txt);
    $email->phpmailer_email->MsgHTML(nl2br($rendered_file_txt));
    $email->send_mail();
  }

  static public function contactForm($contactForm) {
    $rendered_file_txt = CConsoleCommand::renderFile(Yii::app()->basePath . '/views/email_templates/contactForm.php', array('contactForm' => $contactForm), true);
    $email = self::perform_new_mail();
    $email->phpmailer_email->AddAddress(Varable::pickUp('ContactEmail',null,0));
    $email->phpmailer_email->ClearReplyTos();
    $email->phpmailer_email->AddReplyTo($contactForm->email);
    $email->phpmailer_email->Subject = stripFromWww(stripFromHttp(Yii::app()->request->hostInfo)) . '- formularz kontaktowy';
    $email->phpmailer_email->AltBody = strip_tags($rendered_file_txt);
    $email->phpmailer_email->MsgHTML(nl2br($rendered_file_txt));
    return $email->send_mail();
  }

  static public function newsletterRecipientAdd($to, $checksum) {
    $rendered_file_txt = CConsoleCommand::renderFile(Yii::app()->theme->basePath . '/views/emails/newsletterRecipientAdd_txt.php', array('checksum' => $checksum), true);
    $email = self::perform_new_mail(Yii::app()->params['smtp']['default']);
    $email->phpmailer_email->AddAddress($to);
    $email->phpmailer_email->Subject = stripFromWww(stripFromHttp(Yii::app()->request->hostInfo)) . '- dopisanie do listy mailingowej, link aktywacyjny';
    $email->phpmailer_email->AltBody = strip_tags($rendered_file_txt);
    $email->phpmailer_email->MsgHTML(nl2br($rendered_file_txt));
    $email->send_mail();
  }

  static public function clientUserAccountActivated($to, $login, $password) {
    $rendered_file_txt = CConsoleCommand::renderFile(Yii::app()->basePath . '/views/email_templates/clientUserAccountActivated_txt.php', array('password' => $password, 'login'=>$login), true);
    $email = self::perform_new_mail();
    $email->phpmailer_email->AddAddress($to);
    $email->phpmailer_email->Subject = stripFromWww(stripFromHttp(Yii::app()->request->hostInfo)) . '- Twoje konto zostało aktywowane';
    $email->phpmailer_email->AltBody = strip_tags($rendered_file_txt);
    $email->phpmailer_email->MsgHTML(nl2br($rendered_file_txt));
    return $email->send_mail();
  }

  static public function changeCauseStatusToClient($to, $cause) {
    $rendered_file_txt = CConsoleCommand::renderFile(Yii::app()->theme->basePath . '/views/emails/changeCauseStatusToClient_txt.php', array('cause' => $cause), true);
    $email = self::perform_new_mail();
    $email->phpmailer_email->AddAddress($to);
    $email->phpmailer_email->Subject = stripFromWww(stripFromHttp(Yii::app()->request->hostInfo)) . '- Zmiana statusu zgłoszenia z dnia '.date('d-m-Y',strtotime($cause->created_at));
    $email->phpmailer_email->AltBody = strip_tags($rendered_file_txt);
    $email->phpmailer_email->MsgHTML(nl2br($rendered_file_txt));
    return $email->send_mail();
  }

  static public function newCauseInformation($to, $cause) {
    $rendered_file_txt = CConsoleCommand::renderFile(Yii::app()->theme->basePath . '/views/emails/newCause_txt.php', array('cause' => $cause), true);
    $email = self::perform_new_mail();
    $email->phpmailer_email->AddAddress($to);
    $email->phpmailer_email->Subject = stripFromWww(stripFromHttp(Yii::app()->request->hostInfo)) . '- Nowe zgłoszenie od '.$cause->user->email;
    $email->phpmailer_email->AltBody = strip_tags($rendered_file_txt);
    $email->phpmailer_email->MsgHTML(nl2br($rendered_file_txt));
    return $email->send_mail();
  }

  static public function EventInformation($event, $emailVariant) {
    $email = self::perform_new_mail();
    $email->phpmailer_email->AddAddress($event->user->email);
    switch ($emailVariant) {
      case 'new':
        $rendered_file_txt = CConsoleCommand::renderFile(Yii::app()->basePath . '/views/email_templates/eventNew.php', array('event' => $event), true);
        $email->phpmailer_email->Subject = stripFromWww(stripFromHttp(Yii::app()->request->hostInfo)) . '- Nowe zdarzenie typy: '.$event->translateEnumValue('type');
        break;
      case 'edit':
        $rendered_file_txt = CConsoleCommand::renderFile(Yii::app()->basePath . '/views/email_templates/eventEdit.php', array('event' => $event), true);
        $email->phpmailer_email->Subject = stripFromWww(stripFromHttp(Yii::app()->request->hostInfo)) . '- Edytowano zdarzenie typu '.$event->translateEnumValue('type');
        break;
      case 'exist':
      default:
        $rendered_file_txt = CConsoleCommand::renderFile(Yii::app()->basePath . '/views/email_templates/eventExist.php', array('event' => $event), true);
        $email->phpmailer_email->Subject = stripFromWww(stripFromHttp(Yii::app()->request->hostInfo)) . '- Zdarzenie typu '.$event->translateEnumValue('type');
        break;
    }
    foreach ($event->files as $file) {
        $filepath = Yii::app()->basePath . '/' . EventFile::getFolderPath($event->user_id,$event->type,$event->id) . '/' . $file->hidden_filename;
        if (file_exists($filepath)) {
          $email->phpmailer_email->AddAttachment($filepath, $file->filename, 'base64', mime_content_type2($file->filename));
        }
    }
    $email->phpmailer_email->ClearReplyTos();
    $email->phpmailer_email->AddReplyTo(Varable::pickUp('ReplyTo'));
    $email->phpmailer_email->AltBody = strip_tags($rendered_file_txt);
    $email->phpmailer_email->MsgHTML(nl2br($rendered_file_txt));
    return $email->send_mail();
  }

  static public function ContactEmail($params) {
    $email = self::perform_new_mail();
    $email->phpmailer_email->AddAddress(Varable::pickUp('ContactEmail',null,0));
    $email->phpmailer_email->Subject = $params['subject'];
    if(isset($params['attachments']) && $params['attachments']) {
      foreach($params['attachments'] as $attachment) {
        $email->phpmailer_email->AddAttachment($attachment['path'], $attachment['name'], 'base64', $attachment['type']);
      }
    }
    $rendered_file_txt = CConsoleCommand::renderFile(Yii::app()->basePath . '/views/email_templates/contactEmail.php', array('params' => $params), true);
    $email->phpmailer_email->ClearReplyTos();
    $email->phpmailer_email->AddReplyTo($params['from']['email']);
    $email->phpmailer_email->AltBody = strip_tags($rendered_file_txt);
    $email->phpmailer_email->MsgHTML(nl2br($rendered_file_txt));
    return $email->send_mail();
  }
}