<?php

/**
 * ContactForm class.
 * ContactForm is the data structure for keeping
 * contact form data. It is used by the 'contact' action of 'SiteController'.
 */
class ContactForm extends CFormModel {

  public $name;
  public $phone;
  public $email;
  public $body;
  public $verifyCode;

//  public function init() {
//    parent::init();
//    $this->setDefaultValues();
//  }

  public function getAttributes() {
    return get_class_vars(get_class($this));
  }

  public function defaultValue($fieldName) {
    switch ($fieldName) {
      case 'name':
        return Yii::t('cms', 'Imię i nazwisko/ firma');
        break;
      case 'phone':
        return Yii::t('cms', 'Numer telefonu');
        break;
      case 'email':
        return Yii::t('cms', 'Adres e-mail');
        break;
      case 'date':
        return Yii::t('cms', 'Data otrzymania nakazu');
        break;
      case 'body':
        return Yii::t('cms', 'Dodatkowa wiadomość');
        break;
      case 'verifyCode':
        return Yii::t('cms', 'Przepisz kod z obrazka');
        break;

      default:
        break;
    }
  }

  public function setDefaultValues() {
    foreach ($this->attributes as $fieldName => $value)
      if (!$this->$fieldName)
        $this->$fieldName = $this->defaultValue($fieldName);
  }

  /**
   * Declares the validation rules.
   */
  public function rules() {
    return array(
        array('email', 'required'),
        array('body, name, phone', 'safe'),
        array('phone', 'numerical', 'allowEmpty'=>true),
        array('phone', 'match', 'allowEmpty'=>true, 'pattern'=>'/^[0-9]{9,11}$/', 'message'=>'Proszę wprowadzić prawidłowy numer telefonu.'),
        array('email', 'email'),
        array('verifyCode', 'captcha', 'allowEmpty' => !CCaptcha::checkRequirements()),
    );
  }

  public function beforeValidate() {
    $this->phone = preg_replace('/[^0-9]/', '', $this->phone);
    return parent::beforeValidate();
  }

  public function afterValidate() {
//    $this->setDefaultValues();
    return parent::afterValidate();
  }

  /**
   * Declares customized attribute labels.
   * If not declared here, an attribute would have a label that is
   * the same as its name with the first letter in upper case.
   */
  public function attributeLabels() {
    return array(
        'verifyCode' => Yii::t('cms', 'Kod weryfikujący'),
        'name' => Yii::t('cms', 'Imię i nazwisko/ firma'),
        'phone' => $this->defaultValue('phone'),
        'body' => $this->defaultValue('body'),
        'email' => $this->defaultValue('email'),
        'date' => $this->defaultValue('date'),
    );
  }

  public function send() {
    return Email::contactForm($this);
  }

}
