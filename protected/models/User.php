<?php

/**
 * This is the model class for table "users".
 *
 * The followings are the available columns in table 'users':
 * @property integer $id
 * @property string $name
 * @property string $email
 * @property string $password
 */
class User extends CActiveRecord {

  /**
   * Returns the static model of the specified AR class.
   * @param string $className active record class name.
   * @return User the static model class
   */
  public static function model($className = __CLASS__) {
    return parent::model($className);
  }

  // holds the password confirmation word
  public $repeat_password;
  //will hold the encrypted password for update actions.
  public $initialPassword;

  public $verifyCode;
  public $terms;

  /**
   * @return string the associated database table name
   */
  public function tableName() {
    return 'users';
  }

  public function primaryKey() {
    return 'id';
    // For composite primary key, return an array like the following
    // return array('pk1', 'pk2');
  }

  public function beforeSave() {
    if ($this->isNewRecord) {
      $this->created_at = date('Y-m-d H:i:s');
      $this->password = md5($this->password);
      if(!$this->status) {
        $this->status = 'new';
      }
    }
    if($this->scenario=='ChangePass') {
      $this->password = md5($this->password);
    }
    // in this case, we will use the old hashed password.
//        if(empty($this->password) && empty($this->repeat_password) && !empty($this->initialPassword))
//            $this->password=$this->repeat_password=$this->initialPassword;
//
    return parent::beforeSave();
  }

  /**
   * @return array validation rules for model attributes.
   */
  public function rules() {
    // NOTE: you should only define rules for those attributes that
    // will receive user inputs.
    return array(
        array('password, repeat_password', 'required', 'on' => 'insert, ChangePass, registration, edit'),
        array('name, email, role', 'required'),
        array('terms', 'required', 'on'=>'registration', 'message'=>Yii::t('cms','Musisz zaakceptować warunki regulaminu aby dokonać rejestracji.')),
        array('password, repeat_password', 'length', 'max' => 40, 'min' => 6),
        array('name, surname, email', 'length', 'max' => 128),
        array('email', 'email'),
        array('email', 'unique'),
        array('password', 'compare', 'compareAttribute' => 'repeat_password', 'on'=>'insert, ChangePass, registration, edit'),
        array('status', 'in', 'range' => array('new', 'active', 'inactive')),
        array('phone', 'default', 'value'=>null),
        array('phone', 'match', 'pattern'=>'/^[0-9]{9,11}$/', 'allowEmpty'=>true, 'message'=>'Proszę wprowadzić prawidłowy numer telefonu.'),
        // The following rule is used by search().
        // Please remove those attributes that should not be searched.
        array('id, name, surname, email, role, status', 'safe', 'on' => 'search'),
        array('verifyCode', 'captcha', 'allowEmpty'=>!CCaptcha::checkRequirements(), 'on'=>'registration, ResetPassword'),
        array('nip, company_name, postcode, city, street, post_postcode, post_city, post_street', 'safe'),
        array('postcode, post_postcode','match','allowEmpty'=>true,'pattern'=>'/[0-9]{2}-[0-9]{3}/'),
        array('nip','validateNip', 'allowEmpty'=>true),
    );
  }

  public function validateNip($attribute, $params) {
    if ((isset($params['allowEmpty']) && $params['allowEmpty'] && !$this->$attribute)) {

    } else {
      if (!Validator_CheckNIP($this->$attribute)) {
        $this->addError('nip', 'Numer nip jest niepoprawny.');
      }
    }
  }

  /**
   * @return array relational rules.
   */
  public function relations() {
    // NOTE: you may need to adjust the relation name and the related
    // class name for the relations automatically generated below.
    return array(
        'sumEvents'=>array(self::STAT,'Event','user_id'),
        'events'=>array(self::HAS_MANY,'Event','user_id','order'=>'id')
    );
  }

//  public function afterDelete() {
//    foreach(array('causes') as $name) {
//      if($this->$name) {
//        foreach($this->$name as $element) $element->delete();
//      }
//    }
//    return parent::afterDelete();
//  }

  public function beforeValidate() {
    if($this->phone)
      $this->phone = preg_replace('/[^0-9]/', '', $this->phone);
    if($this->scenario == 'edit') {
      if(!$this->password) {
        $old = User::model()->findByPk($this->id);
        $this->password = $old->password;
        $this->repeat_password = $old->password;
      }
      else {
        $this->password = md5($this->password);
        $this->repeat_password = md5($this->repeat_password);
      }
    }
    return parent::beforeValidate();
  }

  public function afterSave() {
    $this->checkAndAssignRole();
    return parent::afterSave();
  }

  public function checkAndAssignRole() {

    $aM = Yii::app()->getAuthManager();
    $exist = $aM->getAuthItem($this->role);
    if(!$exist) {
      $aM->createAuthItem(
					$this->role,
					2,
					''
				);
    }
    $assign = $aM->getAuthAssignment($this->role, $this->id);
    if(!$assign) {
      $assign = $aM->assign($this->role, $this->id);
    }
    $applicationRoles = ZHtml::enumItem($this, 'role');
    foreach($applicationRoles as $roleName=>$translated) {
      if($roleName!=$this->role && $aM->getAuthAssignment($roleName, $this->id)) {
        $aM->revoke($roleName, $this->id);
      }
    }
  }

  /**
   * @return array customized attribute labels (name=>label)
   */
  public function attributeLabels() {
    return array(
        'id' => 'ID',
        'name' => 'Imię',
        'surname' => 'Nazwisko',
        'email' => 'Email',
        'password' => 'Hasło',
        'repeat_password' => 'Powtórz hasło',
        'status' => 'Status',
        'verifyCode'=>'Kod weryfikujący',
        'terms'=>'Regulamin',
        'created_at'=>'Data utworzenia',
        'role'=>'Rola',
        'phone'=>'Numer telefonu',
        'company_name'=>'Nazwa firmy',
        'nip'=>'Nip',
        'postcode'=>'Kod pocztowy',
        'city'=>'Miasto',
        'street'=>'Ulica',
        'post_postcode'=>'Kod pocztowy do wysyłki',
        'post_city'=>'Miasto do wysyłki',
        'post_street'=>'Ulica do wysyłki',
    );
  }

  public function getStatusTranslated() {
    switch ($this->status) {
      case 'new':
        return 'Nowy';
        break;
      case 'active':
        return 'Aktywny';
        break;
      case 'inactive':
        return 'Nieaktywny';
        break;

      default:
        break;
    }
  }

  public function getEnumOptions($fieldName) {
    $values = false;
    switch ($fieldName) {
      case 'role':
        $values = array(
            'Root' => Yii::t('cms', 'Root'),
            'Administrator' => Yii::t('cms', 'Administrator'),
            'Client' => Yii::t('cms', 'Klient'),
        );
        break;
      case 'status':
        $values = array(
            'new' => Yii::t('cms', 'Nowe'),
            'active' => Yii::t('cms', 'Aktywne'),
            'inactive' => Yii::t('cms', 'Nieaktywne'),
        );
        break;
      default:
        break;
    }
    return $values;
  }

  public function TranslateEnumValue($fieldName, $value = null) {
    if($value===null) $value = $this->$fieldName;
    $values = $this->getEnumOptions($fieldName);
    if (isset($values[$value])) {
      return $values[$value];
    }
    else
      return $value;
  }

  public function getPossibleRoles($translate = true) {
    $roles = array();
    switch ($this->role) {
      case 'Root':
        $roles = array('Root','Administrator','Client');
        break;
      case 'Administrator':
        $roles = array('Administrator','Client');
        break;
      case 'Client':
        break;
      default:
        break;
    }
    if($translate) {
      $data = array();
      foreach($roles as $role) {
        $data[$role] = $this->TranslateEnumValue('role',$role);
      }
      return $data;
    }
    return $roles;
  }

  /**
   * Retrieves a list of models based on the current search/filter conditions.
   * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
   */
  public function search() {
    // Warning: Please modify the following code to remove attributes that
    // should not be searched.

    $criteria = new CDbCriteria;

    $criteria->compare('id', $this->id);
    $criteria->compare('name', $this->name, true);
    $criteria->compare('surname', $this->surname, true);
    $criteria->compare('email', $this->email, true);
    if($this->role && in_array($this->role, User::model()->findByPk(Yii::app()->user->id)->getPossibleRoles(false))) {
      $criteria->compare('role', $this->role, true);
    }
    else {
      $criteria->addInCondition('role',User::model()->findByPk(Yii::app()->user->id)->getPossibleRoles(false));
    }

    $criteria->compare('status', $this->status, true);

    return new CActiveDataProvider($this, array(
                'criteria' => $criteria,
        'pagination' => array(
                    'pageSize' => Yii::app()->user->getState('pageSize', Yii::app()->params['defaultPageSize']),
                ),
        'sort' => array(
                    'defaultOrder' => 'id DESC',
                ),
            ));
  }

  public function getFullName() {
    return $this->name." ".$this->surname;
  }

  static public function getUserFilesPath($id) {
    $path = "/userFiles/".$id;
    if(!file_exists(Yii::app()->basePath.'/..'.$path)) {
      $oldumask = umask(0);
      mkdir(Yii::app()->basePath.'/..'.$path, 0777, true);
      umask($oldumask);
    }
    return $path;
  }

  static public function getUserFilesFullPath($id) {
    return Yii::app()->basePath.'/..'.self::getUserFilesPath($id);
  }

  public function getEmailAndPhone($separator = '<br />') {
    $txt = $this->email;
    if($this->phone) $txt .= $separator.$this->phone;
    return $txt;
  }

  public function getSumSettlements() {
    $settlement = new Settlement();
    $settlement->user_id = $this->id;
    return $settlement->getSum();
  }

}