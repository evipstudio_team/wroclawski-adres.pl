<?php

class Reservation extends CActiveRecord {

  public static function model($className = __CLASS__) {
    return parent::model($className);
  }

  public function tableName() {
    return 'reservations';
  }

  public function rules() {
    return array(
        array('user_id, created_at, created_by, date_from, date_to', 'required'),
        array('created_at', 'date', 'format' => 'yyyy-M-d H:m:s'),
        array('date_from, date_to', 'date', 'format' => 'yyyy-M-d'),
        array('time_from, time_to', 'date', 'format' => 'H:m:s'),
        array('type', 'numerical', 'min' => 1, 'tooSmall' => 'Wybierz typ rezerwacji', 'allowEmpty' => false, 'message'=>''),
        array('type', 'required','message'=>''),
        array('room_id', 'required','message'=>'Wskaż salę/biurko'),
        array('comment, from_all_day, to_all_day', 'safe'),
        array('id, type', 'safe', 'on' => 'search'),
        array('type, room_id', 'safe', 'on' => 'clientSearch'),
    );
  }

  public function relations() {
    return array(
        'user' => array(self::BELONGS_TO, 'User', 'user_id'),
        'author' => array(self::BELONGS_TO, 'User', 'created_by'),
        'room' => array(self::BELONGS_TO, 'Room', 'room_id'),
    );
  }

  public function attributeLabels() {
    return array(
        'comment' => 'Treść notatki',
        'user_id' => 'Klient',
        'type' => 'Typ rezerwacji',
        'created_by' => 'Dodający',
        'created_at' => 'Data utworzenia',
        'date_from' => 'Rezerwuj od',
        'date_to' => 'Rezerwuj do',
        'formFiles' => 'Pliki dołączone do zdarzenia',
        'files' => 'Pliki',
        'commentShort' => 'Notatka skrócona',
        'sendEmail' => 'Wyślij wiadomość do klienta',
        'room_id' => 'Sala/biurko'
    );
  }

  public function getEnumOptions($fieldName) {
    $values = false;
    switch ($fieldName) {
      case 'type':
        $values = array(
            1 => Yii::t('cms', 'Rezerwacja sali konferencyjnej'),
            2 => Yii::t('cms', 'Rezerwacja biurka z komputerem'),
            3 => Yii::t('cms', 'Rezerwacja miejsca do pracy z komputerem'),
            4 => Yii::t('cms', 'Rezerwacja biura'),
        );
        break;
      default:
        break;
    }
    return $values;
  }

  public function TranslateEnumValue($fieldName) {
    $values = $this->getEnumOptions($fieldName);
    if (isset($values[$this->$fieldName])) {
      return $values[$this->$fieldName];
    }
    else
      return $this->$fieldName;
  }

  public function beforeValidate() {
    if(!Yii::app()->user->checkAccess('CmsUser') && $this->user_id!=Yii::app()->user->id)
      $this->user_id = Yii::app()->user->id;

    $this->date_from = date('Y-m-d',strtotime($this->date_from));
    $this->date_to = date('Y-m-d',strtotime($this->date_to));

    $this->time_from = date('H:i:s',strtotime($this->time_from));
    $this->time_to = date('H:i:s',strtotime($this->time_to));

    if(!Yii::app()->user->checkAccess('CmsUser')) {
      if(in_array(date('w',strtotime($this->date_from)),array(0,6))) {
        $this->addError('date_from', 'Biuro jest dostępne od poniedziałku do piątku.');
      }
      elseif(in_array(date('w',strtotime($this->date_to)),array(0,6))) {
        $this->addError('date_to', 'Biuro jest dostępne od poniedziałku do piątku.');
      }
    }

    if($this->date_from<=date('Y-m-d')) {
      $this->addError('date_from', 'Nie można dokonać rezerwacji z datą wcześniejszą niż następny dzień (tj. '.date('d-m-Y').')');
    }

    if ($this->isNewRecord) {
      if (!$this->created_at)
        $this->created_at = date('Y-m-d H:i:s');
      if (!$this->created_by)
        $this->created_by = Yii::app()->user->id;
    }
    $dateFrom = $this->date_from;
    if(!$this->from_all_day) $dateFrom .= ' '.$this->time_from;
    else {
      $dateFrom .= ' '.Yii::app()->params['AvailabilityFrom'].':00';
    }
    $this->date_from_helper = $dateFrom;

    $dateTo = $this->date_to;
    if(!$this->to_all_day) $dateTo .= ' '.$this->time_to;
    else {
      $dateTo .= ' '.Yii::app()->params['AvailabilityTo'].':00';
    }

    $this->date_to_helper = $dateTo;

    if(strtotime($this->date_from_helper)>strtotime($this->date_to_helper)) {
      $this->addError('date_from', 'Data rozpoczęcia rezerwacji nie może być późniejsza niż data jej końca');
      $this->addError('date_to','');
    }

    if(strtotime($this->date_from_helper)<strtotime(date('Y-m-d',strtotime($this->date_from_helper)).' '.Yii::app()->params['AvailabilityFrom']) ||
            strtotime($this->date_from_helper)>strtotime(date('Y-m-d',strtotime($this->date_from_helper)).' '.Yii::app()->params['AvailabilityTo'])) {
      $this->addError('time_from', 'Biuro jest czynne w godzinach od '.Yii::app()->params['AvailabilityFrom'].' do '.Yii::app()->params['AvailabilityTo'].'.');
    }
    if(strtotime($this->date_to_helper)>strtotime(date('Y-m-d',strtotime($this->date_to_helper)).' '.Yii::app()->params['AvailabilityTo']) ||
            strtotime($this->date_to_helper)<strtotime(date('Y-m-d',strtotime($this->date_to_helper)).' '.Yii::app()->params['AvailabilityFrom'])) {
      $this->addError('time_to', 'Biuro jest czynne w godzinach od '.Yii::app()->params['AvailabilityFrom'].' do '.Yii::app()->params['AvailabilityTo'].'.');
    }

    if($this->type && $this->date_from && $this->date_to) {
      //sprawdź czy element jest dostepny tego dnia
      $avaibleRooms = Room::pickUpRooms($this->type, $this);

      if(!$avaibleRooms) {
        $msg = 'Przepraszamy, w wybranym okresie nie ma ';
        if($this->type==1) {
          $msg .= 'wolnych sal.';
        }
        else {
          $msg .= 'wolnych biurek.';
        }
          $this->addError ('type', $msg);
        }
    }
    if($this->room_id) {

    }
    else {

    }

    return parent::beforeValidate();
  }

  public function search($user) {
    $criteria = new CDbCriteria;
    $criteria->with = array('user');
    $criteria->compare('`t`.`id`', $this->id);
    $criteria->compare('`t`.`type`', $this->type);
    $criteria->compare('`t`.`comment`', $this->comment, true);
//    $criteria->compare('`t`.`actual_step`', $this->actual_step);
    $criteria->compare('user.email', $user->email, true);
    if ($user->name) {
      $criteria->addcondition("(user.name LIKE '%" . $user->name . "%' OR user.surname LIKE '%" . $user->name . "%')");
    }

    return new CActiveDataProvider($this, array(
                'criteria' => $criteria,
                'pagination' => array(
                    'pageSize' => Yii::app()->user->getState('pageSize', Yii::app()->params['defaultPageSize']),
                ),
                'sort' => array(
                    'defaultOrder' => 'date_from_helper',
                ),
            ));
  }

  public function assignTypeFromInput($typ) {
    $possibleTypes = ZHtml::enumItem($this, 'type');
    foreach ($possibleTypes as $typeIndex => $type)
      if ($type == $typ) {
        $this->type = $typeIndex;
        break;
      }
  }

  public function formatTime($name) {
    if($this->$name) return date('H:i',strtotime($this->$name));
    else return false;
  }

  public function formatDate($name) {
    if($this->$name) return date('d/m/Y',strtotime($this->$name));
    return false;
  }

  public function pickUpReservationsCriteria() {
    $criteria = new CDbCriteria;

    if ($this->type) {
      switch ($this->type) {
        case 1:
          $criteria->compare('`t`.`type`', $this->type);
          $criteria->compare('`t`.`room_id`', $this->room_id);
          break;
        case 2:
        case 3:
          $criteria->addInCondition('type', array(2,3,4));
          if ($this->room_id) {
            $criteria->addInCondition('room_id', Room::getRelatedRooms($this->type, $this->room_id));
          } else {
            //$criteria->addInCondition('room_id', Room::getRelatedRooms($this->type));
          }
          break;
        case 4:
          $criteria->addInCondition('type', array(2,3,4));
          if ($this->room_id) {
            $criteria->addInCondition('room_id', Room::getRelatedRooms($this->type, $this->room_id));
          } else {
            //$criteria->addInCondition('room_id', Room::getRelatedRooms($this->type));
          }
          break;

        default:
          break;
      }
    }

    if($this->date_from_helper)
      $criteria->addCondition ("`t`.`date_to_helper`>'$this->date_from_helper'");
    if($this->date_to_helper)
      $criteria->addCondition ("`t`.`date_from_helper`<'$this->date_to_helper'");
//    $criteria->compare('`t`.`date_from_helper`', $this->date_from_helper);
//    $criteria->compare('`t`.`date_to_helper`', $this->date_to_helper);
    if($this->id) {
      $criteria->addCondition('`t`.`id`!='.$this->id);
    }
    if(Yii::app()->user->id != Yii::app()->params['demoUserId']) {
      $criteria->addCondition('`t`.`user_id`!='.Yii::app()->params['demoUserId']);
    }
    return $criteria;
  }

  public function pickUpReservations() {
    $reservations = Reservation::model()->findAll($this->pickUpReservationsCriteria());
    return $reservations;
  }

  public function prepareReservationJsonEntry() {
    $rData = array();
    $rData2 = array();

    if ($this->from_all_day && $this->to_all_day) {
      $rData['start'] = date('Y-m-d', strtotime($this->date_from));
      $rData['end'] = date('Y-m-d', strtotime($this->date_to));
      $rData['allDay'] = true;
    } elseif ($this->from_all_day) {
      $rData['start'] = date('Y-m-d', strtotime($this->date_from));
      $rData['end'] = date('Y-m-d', strtotime("-1 Day", strtotime($this->date_to)));
      $rData['allDay'] = true;
      $rData2 = $rData;
      $rData2['start'] = date('Y-m-d', strtotime($this->date_to)) .' '.Yii::app()->params['AvailabilityFrom'].':00';
//      $rData2['title'] .= ' Koniec kilkudniowej rezerwacji';
      $rData2['end'] = date('Y-m-d', strtotime($this->date_to)) . ' ' . date('H:i:s', strtotime($this->time_to));
      $rData2['allDay'] = false;
    } elseif ($this->to_all_day) {
      $rData['start'] = date('Y-m-d', strtotime("+1 Day", strtotime($this->date_from)));
      $rData['end'] = date('Y-m-d', strtotime($this->date_to));
      $rData['allDay'] = true;
      $rData2 = $rData;
//      $rData2['title'] .= ' Początek kilkudniowej rezerwacji';
      $rData2['start'] = date('Y-m-d', strtotime($this->date_from)) . ' ' . date('H:i:s', strtotime($this->time_from));
      $rData2['end'] = date('Y-m-d', strtotime($this->date_from)) .' '.Yii::app()->params['AvailabilityTo'].':00';
      $rData2['allDay'] = false;
    } else {
      $rData['start'] = date('Y-m-d', strtotime($this->date_from)) . ' ' . date('H:i:s', strtotime($this->time_from));
      $rData['end'] = date('Y-m-d', strtotime($this->date_to)) . ' ' . date('H:i:s', strtotime($this->time_to));
      $rData['allDay'] = false;
    }
    return array($rData,$rData2);
  }

  static public function reservationsToJsonForClientView($reservations, $reservation) {
    $data = array();
    if($reservation->type && $reservation->room_id) {
      //tutaj po prostu wyswietlam rezerwacje i nic więcej
      foreach($reservations as $r) {
        $rData = array();
        $rData['title'] = '';
        if ($r->user_id == Yii::app()->user->id) {
          $rData['id'] = $r->id;
        }
        else
          $rData['color'] = '#CCC';

        $preparedEntry = $r->prepareReservationJsonEntry();
        $rData = array_merge($rData,$preparedEntry[0]);
        $rData2 = $preparedEntry[1];

        array_push($data, $rData);
        if($rData2) array_push ($data, $rData2);
      }
    }
    else {
      $dataByDate = array();
      //tutaj musze sprawdzic czy w dany dzień są jeszcze miejsca wolne po wybranych parametrach i zając tylko te daty w ktorych nie ma wylnych miejsc,
      foreach($reservations as $r) {
        if($r->user_id==Yii::app()->user->id) {
          $rData = array();
          $rData['title'] = '';
          $rData['id'] = $r->id;
          $preparedEntry = $r->prepareReservationJsonEntry();
          $rData = array_merge($rData,$preparedEntry[0]);
          $rData2 = $preparedEntry[1];
          array_push($data, $rData);
          if($rData2) array_push ($data, $rData2);
        }
        else {
          unset($r->id);
          $r2 = $r;
          unset($r2->room_id);
          $rooms = Room::pickUpRooms($r2->type, $r2);
          if(!$rooms) {
            $dateFrom = strtotime($r->date_from_helper);
            $dateTo = strtotime($r->date_to_helper);
            for($i = $dateFrom;$i<=$dateTo;$i = strtotime("+30 minutes",$i)) {
              $r2->date_from_helper = date('Y-m-d H:i:s',$i);
              $r2->date_to_helper = date('Y-m-d H:i:s',strtotime("+30 minutes",$i));
              $rooms = Room::pickUpRooms($r2->type, $r2);
              if(!$rooms) {
                array_push($dataByDate, array('start'=>$r2->date_from_helper,'end'=>$r2->date_to_helper));
              }
            }
          }
        }
      }
      if($dataByDate) {
        $dataByDate = processDateRanges($dataByDate);
        for($i=0;$i<count($dataByDate);$i+=2) {
          $rData = array();
          $rData['color'] = '#CCC';
          $rData['title'] = '';
          $rData['start'] = $dataByDate[$i];
          $rData['end'] = $dataByDate[($i+1)];
          if(date('H:i',strtotime($rData['start']))=='08:00' && date('H:i',strtotime($rData['end']))=='17:00')
            $rData['allDay'] = true;
          else
            $rData['allDay'] = false;
          array_push($data, $rData);
        }
      }
    }

    return $data;
  }

  static public function reservationsToJson($reservations) {
    $data = array();
    foreach($reservations as $r) {
      $rData = array();
      $rData2 = array();
      $rData['title'] = $r->user->company_name.' '.$r->room->name;
      $rData['id'] = $r->id;
      if($r->type==3) {
        $rData['title'] .= ' Stanowisko z komputerem';
      }

      $preparedEntry = $r->prepareReservationJsonEntry();
      $rData = array_merge($rData,$preparedEntry[0]);
      $rData2 = $preparedEntry[1];

      array_push($data, $rData);
      if($rData2) array_push ($data, $rData2);
    }
    return $data;
  }

  public function saveButtonText() {
    if($this->isNewRecord) return "Zapisz";
    return "Zapisz zmiany";
  }

}