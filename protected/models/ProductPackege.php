<?php

class ProductPackege extends CActiveRecord {

  public static function model($className = __CLASS__) {
    return parent::model($className);
  }

  public function relations() {
    return array(
        'productPage' => array(self::BELONGS_TO, 'Page', 'product_id'),
        'packegePage' => array(self::BELONGS_TO, 'Page', 'packege_id')
    );
  }

  public function tableName() {
    return 'products_packeges';
  }

  public function rules() {
    return array(
        array('unit_price', 'match', 'pattern'=>'/^[0-9]{1,3}(\.[0-9]{0,2})?$/', 'allowEmpty'=>true),
        array('unit_price','default','value'=>null),
    );
  }

  public function attributeLabels() {
    return array(
        'id' => 'ID',
        'unit_price'=>  Yii::t('cms', 'Cena jednostki po przekroczeniu darmowego limitu'),
        'free_limit'=>  Yii::t('cms', 'Ilość darmowych jednostek w pakiecie')
    );
  }

  static public function prepareProductsToShow($products) {
    $data = array();
    foreach($products as $product) {
      $productPage = $product->productPage;
      $name = $productPage->article->title;
      $name = explode('/', $name);
      if(count($name)>1) {
        $name[0] = trim($name[0]);
        $name[1] = trim($name[1]);
        $exist = false;
        foreach($data as $index=>$element) {
          if($element['name']==$name[0]) {
            $exist = true;
            array_push($data[$index]['prices'],array(
                'per_name'=>$name[1],
                'price'=>$productPage->unit_price
            ));
          }
        }
        if(!$exist) {
          array_push($data, array(
              'name'=>$name[0],
              'prices'=>array(
                  array(
                      'per_name'=>$name[1],
                      'price'=>$productPage->unit_price
                  )
                  )
              ));
        }
      }
      else {
        array_push($data, array(
              'name'=>$name[0],
              'prices'=>array(
                  array(
                      'per_name'=>'',
                      'price'=>$productPage->unit_price
                  )
                  )
              ));
      }
    }
    return $data;
  }

}

?>