<?php

class Event extends CActiveRecord {
  public $sendEmail;
  public $formFiles;

  public static function model($className = __CLASS__) {
    return parent::model($className);
  }

  public function tableName() {
    return 'events';
  }

  public function rules() {
    return array(
        array('user_id, created_at, created_by, date', 'required'),
        array('date','date','format'=>'yyyy-M-d H:m:s'),
        array('created_at','date','format'=>'yyyy-M-d H:m:s'),
        //array('file', 'file', 'types' => 'jpg, gif, png, pdf, doc, docx', 'allowEmpty' => true),
        array('type','numerical','min'=>1,'tooSmall'=>'Wybierz typ zdarzenia', 'allowEmpty' => false),
        array('comment','safe'),
        array('sendEmail','required', 'on'=>'insert, edit'),
        array('id, type', 'safe', 'on'=>'search'),
        array('type', 'safe', 'on'=>'clientSearch'),
    );
  }

  public function relations() {
    return array(
        'user' => array(self::BELONGS_TO, 'User', 'user_id'),
        'author' => array(self::BELONGS_TO, 'User', 'created_by'),
        'files'=>array(self::HAS_MANY, 'EventFile','event_id', 'order'=>'id DESC'),
        'filesCount'=>array(self::STAT, 'EventFile','event_id')
    );
  }

  public function attributeLabels() {
    return array(
        'comment'=>'Treść notatki',
        'user_id'=>'Klient',
        'type'=>'Typ zdarzenia',
        'created_by'=>'Dodający',
        'created_at'=>'Data utworzenia',
        'file'=>'Dołączony dokument',
        'date'=>'Data zdarzenia',
        'formFiles'=>'Pliki dołączone do zdarzenia',
        'files'=>'Pliki',
        'commentShort'=>'Notatka skrócona',
        'sendEmail'=>'Wyślij wiadomość do klienta'
    );
  }

  public function getEnumOptions($fieldName) {
    $values = false;
    switch ($fieldName) {
      case 'type':
        $values = array(
//            0 => Yii::t('cms', ''),
            1 => Yii::t('cms', 'Dokument'),
            2 => Yii::t('cms', 'Obsługa telefoniczna'),
            3 => Yii::t('cms', 'Poczta przychodząca'),
            4 => Yii::t('cms', 'Księgowość'),
        );
        break;
      default:
        break;
    }
    return $values;
  }

  public function TranslateEnumValue($fieldName) {
    $values = $this->getEnumOptions($fieldName);
    if (isset($values[$this->$fieldName])) {
      return $values[$this->$fieldName];
    }
    else
      return $this->$fieldName;
  }

  public function beforeValidate() {
    if ($this->isNewRecord) {
      if(!$this->created_at)
        $this->created_at = date('Y-m-d H:i:s');
      if(!$this->created_by)
        $this->created_by = Yii::app()->user->id;

      if(!$this->comment && !$this->formFiles) {
        $this->addError('id', 'Musisz podać notatkę lub pliki do zapisu.');
        $this->addError('formFiles', '');
        $this->addError('comment', '');
      }
    }
    if($this->formFiles) {
      $formFiles = array();
      foreach($this->formFiles as $fileInstance) {
        $eventFile = new EventFile();
        $eventFile->file = $fileInstance;
        if($eventFile->validate()) {
          array_push ($formFiles, $eventFile);
        }
        else {
          $this->addError('formFiles', CHtml::errorSummary($eventFile,'','',array()));
        }
      }
      if($formFiles) $this->formFiles = $formFiles;
    }
    return parent::beforeValidate();
  }

  public function afterSave() {
    if ($this->formFiles) {
      foreach ($this->formFiles as $eventFile) {
        $eventFile->event_id = $this->id;
        $eventFile->save();
      }
    }
    return parent::afterSave();
  }

  public function sendEmail() {
    if($this->sendEmail) {
      Email::EventInformation($this, $this->sendEmail);
      unset($this->sendEmail);
    }
  }

  public function search($user) {
    $criteria = new CDbCriteria;
    $criteria->with = array('user');
    $criteria->compare('`t`.`id`', $this->id);
    $criteria->compare('`t`.`type`', $this->type);
    $criteria->compare('`t`.`comment`', $this->comment,true);
//    $criteria->compare('`t`.`actual_step`', $this->actual_step);
    $criteria->compare('user.email', $user->email, true);
    if($user->name) {
      $criteria->addcondition("(user.name LIKE '%".$user->name."%' OR user.surname LIKE '%".$user->name."%')");
    }

    return new CActiveDataProvider($this, array(
                'criteria' => $criteria,
                'pagination' => array(
                    'pageSize' => Yii::app()->user->getState('pageSize', Yii::app()->params['defaultPageSize']),
                ),
                'sort' => array(
                    'defaultOrder' => 't.id DESC',
                ),
            ));
  }

  public function clientSearch() {
    $criteria = new CDbCriteria;
    $criteria->compare('user_id', $this->user_id);
    $criteria->compare('`type`', $this->type);
    $criteria->compare('`comment`', $this->comment, true);
    $pagination = array(
                    'pageSize' => Yii::app()->user->getState('pageSize', Yii::app()->params['defaultPageSize']),
                );
    if($this->seen == 'unseen') {
      $criteria->addCondition('seen IS NULL');
      $pagination = false;
    }

    return new CActiveDataProvider($this, array(
                'criteria' => $criteria,
                'pagination' => $pagination,
                'sort' => array(
                    'defaultOrder' => 'date DESC',
                ),
            ));
  }

  public function beforeDelete() {
    foreach(array('files') as $name) {
      if($this->$name) {
        foreach($this->$name as $element) $element->delete();
      }
    }
    return parent::beforeDelete();
  }

  public function filesToString($separator = '<br />') {
    $files = array();
    foreach($this->files(array('order'=>'id ASC')) as $file) {
      array_push($files, '<a href="'.$file->linkTo().'">'.$file->filename.'</a>');
    }
    return implode($separator, $files);
  }

  public function getCommentShort($lenght = 100) {
    if($this->comment) {
      if(strlen($this->comment)>$lenght) {
        $dotPos = strpos($this->comment, '.');
        if($dotPos<=$lenght) {
          return substr($this->comment, 0, $dotPos).' ...';
        }
        else
          return substr($this->comment, 0, $lenght).' ...';
      }
    }
    return $this->comment;
  }

  public function viewDetailsIcon() {
    if($this->seen) {
      return 'view_details.png';
    }
    else {
      return 'view_details_unseen.png';
    }
  }

  public function setSeen() {
    $this->seen = date('Y-m-d H:i:s');
    $this->save();
  }

  public function notSeen() {
    if(!$this->seen || $this->seen=='0000-00-00 00:00:00') return true;
    else return false;
  }

  public function checkAllAsSeen($user_id) {
    $sql = "UPDATE `events` SET `seen`='".date('Y-m-d H:i:s')."' WHERE user_id=$user_id AND (`seen` IS NULL OR `seen`='0000-00-00 00:00:00')";
    Yii::app()->getDb()->createCommand($sql)->query();
  }
}