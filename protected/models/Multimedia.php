<?php

/**
 * This is the model class for table "multimedia".
 *
 * The followings are the available columns in table 'multimedia':
 * @property string $id
 * @property string $page_id
 * @property string $path
 * @property integer $created_by
 * @property string $created_at
 * @property integer $position
 * @property string $extension
 */
class Multimedia extends CActiveRecord {

  var $translations = array();

  /**
   * Returns the static model of the specified AR class.
   * @param string $className active record class name.
   * @return Multimedia the static model class
   */
  public static function model($className = __CLASS__) {
    return parent::model($className);
  }

  static public function getModuleId() {
    return Module::model()->find('`name`=:name', array(':name' => 'Multimedia'))->id;
  }

  /**
   * @return string the associated database table name
   */
  public function tableName() {
    return 'multimedia';
  }

  /**
   * @return array validation rules for model attributes.
   */
  public function rules() {
    // NOTE: you should only define rules for those attributes that
    // will receive user inputs.
    return array(
        array('page_id, path, filename, created_by, created_at, position, extension', 'required'),
        array('position', 'numerical', 'integerOnly' => true),
        array('created_by', 'in', 'range' => array_keys($this->getEnumOptions('created_by')), 'allowEmpty' => false),
        array('path, filename', 'length', 'max' => 255),
        array('extension', 'length', 'max' => 4),
        // The following rule is used by search().
        // Please remove those attributes that should not be searched.
        array('roller, main', 'safe'),
        array('id, path, created_by, created_at, position, extension, filename, roller', 'safe', 'on' => 'search'),
    );
  }

  /**
   * @return array relational rules.
   */
  public function relations() {
    // NOTE: you may need to adjust the relation name and the related
    // class name for the relations automatically generated below.
    return array(
//        'page'=>array(self::BELONGS_TO, 'Page', 'page_id'),
        'translation' => array(self::HAS_ONE, 'MultimediaTranslation', 'multimedia_id'),
        'activeTranslation' => array(self::HAS_ONE, 'MultimediaTranslation', 'multimedia_id', 'condition' => '`activeTranslation`.`lang_id`=:lang_id', 'params' => array(':lang_id' => Lang::getDefaultLang()->id)),
        'page' => array(self::BELONGS_TO, 'Page', 'page_id'),
    );
  }

  public function getEnumOptions($fieldName) {
    $values = false;
    switch ($fieldName) {
      case 'created_by':
        $values = CHtml::listData($this->possibleAuthors(), 'id', 'fullName');
        break;
      default:
        break;
    }
    return $values;
  }

  public function possibleAuthors() {
    return User::model()->findAll('status=:status', array(':status' => 'active'));
  }

  public function getParentName() {
    $element = getElement($this->module_id, $this->element_id);
    switch (get_class($element)) {
      case 'Article':
        return $element->translations[0]->title;
        break;
      case 'Banner':
      case 'Link':
        return $element->page->name;
        break;
      case 'Page':
        return $element->name;
        break;

      default:
        break;
    }
  }

  public function TranslateEnumValue($fieldName) {
    if ($fieldName == 'created_by')
      return $this->author->fullName;

    $values = $this->getEnumOptions($fieldName);
    if (isset($values[$this->$fieldName])) {
      return $values[$this->$fieldName];
    }
    else
      return $this->$fieldName;
  }

  public function beforeValidate() {
    if ($this->getIsNewRecord()) {
      $this->created_by = Yii::app()->user->id;
      $this->created_at = date('Y-m-d H:i:s');
      $last = Multimedia::model()->find(array(
          'condition' => 'page_id=:page_id',
          'params' => array(
              ':page_id' => $this->page_id,
          ),
          'order' => 'position DESC'
              ));
      $this->position = ($last) ? $last->position + 1 : 1;
    }
    return parent::beforeValidate();
  }

  /**
   * @return array customized attribute labels (name=>label)
   */
  public function attributeLabels() {
    return array(
        'id' => 'ID',
        'page_id' => 'Page',
        'path' => 'Path',
        'typeTranslated' => 'Typ',
        'created_by' => 'Dodany przez',
        'created_at' => 'Dodany dnia',
        'position' => 'Pozycja',
        'extension' => 'Rozszerzenie',
        'filename' => Yii::t('cms', 'Nazwa pliku'),
        'defaultTitle' => 'Tytuł w języku ' . Lang::getDefaultLang()->getNameVariety(),
        'defaultDescription' => 'Opis w języku ' . Lang::getDefaultLang()->getNameVariety(),
        'roller' => 'Roler',
    );
  }

  public function getImageTypes() {
    return array('jpg', 'png', 'jpeg', 'bmp', 'gif');
  }

  public function getVideoTypes() {
    return array('avi');
  }

  public function getDocumentTypes() {
    return array('doc', 'docx', 'xlsx', 'xls', 'pdf');
  }

  static public function getPossibleTypes() {
    return array('Image', 'Video', 'Document');
  }

  public function getType() {
    foreach ($this->possibleTypes as $type) {
      $methodName = "get{$type}Types";
      if (in_array($this->extension, $this->$methodName())) {
        return $type;
      }
    }
    return 'Other';
  }

  public function getTypeTranslated($many = false) {
    return self::staticGetTypeTranslated($this->type, $many);
  }

  static public function staticGetTypeTranslated($type, $many = false) {
    switch ($type) {
      case 'Image':
        if ($many)
          return Yii::t('cms', 'Zdjęcia');
        else
          return Yii::t('cms', 'Zdjęcie');
        break;
      case 'Video':
        if ($many)
          return Yii::t('cms', 'Filmy');
        else
          return Yii::t('cms', 'Film');
        break;
      case 'Document':
        if ($many)
          return Yii::t('cms', 'Dokumenty');
        else
          return Yii::t('cms', 'Dokument');
        break;
      case 'Other':
        if ($many)
          return Yii::t('cms', 'Inne');
        else
          return Yii::t('cms', 'Inne');
        break;
      default:
        return $type;
        break;
    }
  }

  public function getMini() {
    if ($this->type == 'Image')
      return '<img src="' . $this->link(100) . '" alt="" title="" />';
    else
      return '<a href="' . $this->link() . '" target="_blank"><img src="/images/icons/preview.png" alt="" title="" /></a>';
  }

  public function getDefaultTranslation() {
    return $this->translation(array('condition' => 'lang_id=:lang_id', 'params' => array(':lang_id' => Lang::getDefaultLang()->id)));
  }

  public function getDefaultTitle($elseFilename = false) {
    $translation = $this->getDefaultTranslation();
    if ($translation)
      return $translation->title;
    else {
      if ($elseFilename)
        return $this->filename;
      return null;
    }
  }

  public function getDefaultDescription() {
    $translation = $this->getDefaultTranslation();
    if ($translation)
      return $translation->description;
    else
      return null;
  }

  public function link($width = false, $height = false) {
    $filepath = $this->path . $this->filename;
    if (file_exists($filepath)) {
      if ($this->type == 'Image' && ($width !== false || $height !== false)) {
        $href_path_format = '{FILENAME_WITHOUT_EXTENSION}';
        $save_path_base_format = dirname($filepath);
        $save_path_format = "{FILENAME_WITHOUT_EXTENSION}_{WIDTH}x{HEIGHT}.jpg";
        $filename_without_extension = basename($filepath, end(explode('.', $filepath)));
        $save_path_format = str_replace('{FILENAME_WITHOUT_EXTENSION}', $filename_without_extension, $save_path_format);

        if ($height !== false) {
          $save_path_format = str_replace('{HEIGHT}', $height, $save_path_format);
        } else {
          $save_path_format = str_replace('{HEIGHT}', '', $save_path_format);
        }
        if ($width !== false) {
          $save_path_format = str_replace('{WIDTH}', $width, $save_path_format);
        } else {
          $save_path_format = str_replace('{WIDTH}', '', $save_path_format);
        }
        $href_path_format = str_replace('{FILENAME_WITHOUT_EXTENSION}', $save_path_format, $href_path_format);
        if (!file_exists($save_path_base_format . '/' . $save_path_format)) {
          Yii::import('ext.simpleimage.simpleimage');
          $image = new simpleimage();
          $image->load($filepath);
          if ($height !== false && $width !== false) {
            if ($image->getWidth() > $width || $image->getHeight() > $height)
              $image->resize($height, $width);
          } elseif ($height !== false) {
            if ($image->getHeight() > $height)
              $image->resizeToHeight($height);
          } elseif ($width !== false) {
            if ($image->getWidth() > $width)
              $image->resizeToWidth($width);
          }
          $image->save($save_path_base_format . '/' . $save_path_format, IMAGETYPE_JPEG, 100, 0777);
          if (!file_exists($save_path_base_format . '/' . $save_path_format)) {
            die('Kompresja zdjęcia nie powiodła się');
          }
        }
        return '/' . $save_path_base_format . '/' . $save_path_format;
      } else {
        return '/' . $filepath;
      }
    }
    else
      return $filepath;
  }

  /**
   * Retrieves a list of models based on the current search/filter conditions.
   * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
   */
//	public function search()
//	{
//		// Warning: Please modify the following code to remove attributes that
//		// should not be searched.
//
//		$criteria=new CDbCriteria;
//
//		$criteria->compare('id',$this->id,true);
//		$criteria->compare('path',$this->path,true);
//    $criteria->compare('filename',$this->path,true);
//		$criteria->compare('created_by',$this->created_by);
//		$criteria->compare('created_at',$this->created_at,true);
//		$criteria->compare('position',$this->position);
//		$criteria->compare('extension',$this->extension,true);
//
//		return new CActiveDataProvider($this, array(
//			'criteria'=>$criteria,
//		));
//	}

  public function searchByPage() {
    $criteria = new CDbCriteria;

    $criteria->compare('id', $this->id, true);
    $criteria->compare('filename', $this->filename, true);
    $criteria->compare('created_by', $this->created_by);
    $criteria->compare('extension', $this->extension, true);
    $criteria->addCondition("`page_id`=$this->page_id");
//    $criteria->order = 'position';

    return new CActiveDataProvider($this, array(
                'criteria' => $criteria,
                'sort' => array(
                    'defaultOrder' => 'position',
                ),
            ));
  }

  public function search($withChilds) {
    $criteria = new CDbCriteria;

//    $criteria->compare('id', $this->id, true);
//    $criteria->compare('filename', $this->filename, true);
//    $criteria->compare('extension', $this->extension, true);
    if($withChilds) {
      $criteria->addInCondition('page_id', CHtml::listData(PageRelations::getChildNodes($this->page_id), 'child_id', 'child_id'));
    }
    else {
      $criteria->addCondition("`page_id`=$this->page_id");
    }

//    $criteria->order = 'position';

    return new CActiveDataProvider($this, array(
                'criteria' => $criteria,
        'pagination'=>false,
//                'pagination' => array(
//                    'pageSize' => Yii::app()->user->getState('pageSize', Yii::app()->params['defaultPageSize']),
//                ),
                'sort' => array(
                    'defaultOrder' => 'position',
                ),
            ));
  }

  static public function getFolderPath($page_id) {
    return "multimedia/$page_id";
  }

  public function afterDelete() {
    self::deleteFiles($this->path, $this->filename, $this->extension);
//    if(file_exists($this->path.$this->filename))
//      unlink($this->path.$this->filename);
//    foreach($this->getRelatedFiles() as $relatedFile) unlink ($this->path.$relatedFile);
    parent::afterDelete();
  }

  static public function deleteFiles($path, $filename, $extension) {
    if (file_exists($path . $filename))
      unlink($path . $filename);
    $relatedFiles = self::getRelatedFiles($path, $filename, $extension);
    foreach ($relatedFiles as $relatedFile)
      unlink($path . $relatedFile);
  }

  static public function getRelatedFiles($path, $filename, $extension) {
    $filename = basename($filename, '.' . $extension);
    $files = scandir($path);
    $relatedFiles = array();
    foreach ($files as $file) {
      if (preg_match('/^' . $filename . '\._[0-9]+x\.' . $extension . '$/', $file) ||
              preg_match('/^' . $filename . '\._x[0-9]+\.' . $extension . '$/', $file) ||
              preg_match('/^' . $filename . '\._[0-9]+x[0-9]+\.' . $extension . '$/', $file)
      ) {
        array_push($relatedFiles, $file);
      }
    }
    return $relatedFiles;
  }

  public function loadTranslation($lang_id) {
    $translation = $this->translation(array('condition' => 'lang_id=:lang_id', 'params' => array(':lang_id' => $lang_id)));
    if (!$translation) {
      $translation = new MultimediaTranslation();
      $translation->setAttributes(array(
          'multimedia_id' => $this->id,
          'lang_id' => $lang_id), false);
    }
    return $translation;
  }

  public function loadTranslations() {
    if (!$this->translations) {
      foreach (Yii::app()->params['langs'] as $lang) {
        $this->translations[$lang->id] = $this->loadTranslation($lang->id);
      }
    }
  }

  public function loadUrl($lang_id) {
    $url = $this->url(array('condition' => 'lang_id=:lang_id', 'params' => array(':lang_id' => $lang_id)));
    if (!$url) {
      $url = new Url();
      $url->setAttributes(array(
          'module_id' => self::getModuleId(),
          'element_id' => $this->id,
          'lang_id' => $lang_id), false);
    }
    return $url;
  }

  public function loadUrls() {
    if (!$this->urls) {
      foreach (Yii::app()->params['langs'] as $lang) {
        $this->urls[$lang->id] = $this->loadUrl($lang->id);
      }
    }
  }

  public function getActiveTranslationTitle() {
    if ($this->activeTranslation)
      return $this->activeTranslation->title;
    else
      return false;
  }

  public function getActiveTranslationDescription() {
    if ($this->activeTranslation)
      return $this->activeTranslation->description;
    else
      return false;
  }

  static public function uploadFile($fileData, $page_id, $id = false) {
    $message = array();

    Yii::import('ext.upload.upload');


    $fileUpload = new upload($fileData, 'pl_PL');
    if ($fileUpload->uploaded) {
      $dir = Multimedia::getFolderPath($page_id);
      if (!file_exists($dir)) {
        $oldumask = umask(0);
        mkdir($dir, 0777, true);
        umask($oldumask);
      }
      $fileUpload->process($dir);
      if ($fileUpload->processed) {
        $oldumask = umask(0);
        chmod($fileUpload->file_dst_pathname, 0777);
        umask($oldumask);
        if ($id) {
          $model = Multimedia::model()->findByPk($id);
          $old_file = array(
              'path' => $model->path,
              'filename' => $model->filename,
              'extension' => $model->extension
          );
        } else {
          $model = new Multimedia;
          $model->page_id = $page_id;
        }
        $model->setAttributes(array(
            'path' => $fileUpload->file_dst_path,
            'filename' => $fileUpload->file_dst_name,
            'extension' => $fileUpload->file_dst_name_ext
        ));
        if ($model->save()) {
          $message['success'] = 'Plik ' . $fileData['name'] . ' zapisany poprawnie.';
          if (isset($old_file)) {
            Multimedia::deleteFiles($old_file['path'], $old_file['filename'], $old_file['extension']);
          }
        } else {
          if (file_exists($fileUpload->file_dst_pathname))
            unlink($fileUpload->file_dst_pathname);
          $message['error'] = 'Wystąpił problem podczas zapisu pliku ' . $fileData['name'] . '.';
        }
      }
      else {
        $message['error'] = 'Wystąpił problem podczasy przenoszenia pliku.';
      }
    } else {
      $message['error'] = 'Wystąpił problem podczasy zapisu pliku.';
    }
    return $message;
  }

  static public function uploadFiles($files, $page_id) {
    $messages = array('success' => array(), 'error' => array());
    foreach ($_FILES['files']['name'] as $index => $filePost) {
      $fileData = array(
          'name' => $_FILES['files']['name'][$index],
          'type' => $_FILES['files']['type'][$index],
          'tmp_name' => $_FILES['files']['tmp_name'][$index],
          'error' => $_FILES['files']['error'][$index],
          'size' => $_FILES['files']['size'][$index]
      );
      $messagePerFile = Multimedia::uploadFile($fileData, $page_id);
      foreach ($messages as $eIndex => $array)
        if (isset($messagePerFile[$eIndex]))
          $messages[$eIndex] = array_merge($messages[$eIndex], array($messagePerFile[$eIndex]));
    }
    return $messages;
  }

  public function getFullPath() {
    return Yii::app()->basePath . '/../' . $this->path;
  }

  public function getMimeType() {
    return getMimeType($this->getFullPath() . $this->filename);
  }

  public function getActiveHref() {
    $href = '';
    $url = $this->loadUrl(Yii::app()->params['lang']->id);
    if ($url && !$url->getIsNewRecord()) {
      $href = $url->address;
    }
    return normalizeHref($href);
  }

  public function switchRoller() {
    if($this->roller) $this->roller = 0;
    else $this->roller = 1;
    return ($this->save());
  }

  public function switchMain() {
    if($this->main) $this->main = 0;
    else $this->main = 1;
    return ($this->save());
  }

  public function afterSave() {
    if ($this->type == 'Image') {
      $filepath = $this->path . $this->filename;
      $maxWidth = Varable::pickUp('maxWidth', null, 0);
      $maxHeight = Varable::pickUp('maxHeight', null, 0);
      if ($maxWidth && $maxHeight) {
        if (file_exists($filepath)) {
          Yii::import('ext.simpleimage.simpleimage');
          $image = new simpleimage();
          $image->load($filepath);
          if ($image->getWidth() > $maxWidth) {
            $image->resizeToWidth($maxWidth);
          }
          if ($image->getHeight() > $maxHeight) {
            $image->resizeToHeight($maxHeight);
          }
        }
        switch ($this->extension) {
          case 'jpg':
          case 'jpeg':
            $image->save($filepath, IMAGETYPE_JPEG, 100, 0777);
            break;
          case 'png':
            $image->save($filepath, IMAGETYPE_PNG, 100, 0777);
            break;
          case 'gif':
            $image->save($filepath, IMAGETYPE_GIF, 100, 0777);
            break;
          default:
            break;
        }
      }
    }
    return parent::afterSave();
  }

  public function checkRatio() {
    $filepath = $this->path . $this->filename;
    if (file_exists($filepath)) {
      Yii::import('ext.simpleimage.simpleimage');
      $image = new simpleimage();
      $image->load($filepath);
      if($image->getWidth()>$image->getHeight()) {
        return 'horizontaly';
      }
      elseif($image->getWidth()<$image->getHeight()) {
        return 'verticaly';
      }
      elseif($image->getWidth()==$image->getHeight()) {
        return 'horizontaly';
      }
    }
    return false;
  }

  public function isHorizontaly() {
    $ratio = $this->checkRatio();
    if($ratio=='horizontaly') return true;
    return false;
  }

  public function isVerticaly() {
    $ratio = $this->checkRatio();
    if($ratio=='verticaly') return true;
    return false;
  }


}