<?php

class ResetPassForm extends CFormModel {

  public $email;
  public $verifyCode;

  public function rules() {
    return array(
        array('email', 'email'),
        array(
            'email',
            'exist',
            'className'=>'User',
            'criteria'=>array('condition'=>'`status`=\'active\''),
            'message'=>'Konto dla podanego adresu e-mail nie istnieje lub jest nieaktywne.'
            ),
        array('verifyCode', 'captcha', 'allowEmpty' => !CCaptcha::checkRequirements()),
    );
  }

  public function attributeLabels() {
    return array(
        'email'=>'Adres e-mail',
        'verifyCode' => Yii::t('cms', 'Kod weryfikujący'),
    );
  }

  public function serve() {
    $commit = true;
    $transaction = Yii::app()->db->beginTransaction();
    CCaptcha::reset_captcha();
    $user = User::model()->find('`email`=:email',array(':email'=>$this->email));
    $cData = new ConfirmationData();
    $cData->setAttributes(array(
        'obj_name'=>'UserController',
        'field_name'=>'ResetPasswordStep2',
        'obj_id'=>$user->id,
        'new_value'=>null
    ),false);
    if($cData->save()) {
      $emailSended = Email::reset_password_step1($user->email, $cData->checksum);
      if(!$emailSended) {
        $commit = false;
        $this->addError('email', 'Na wskazany adres nie został wysłany email potwierdzający, skontaktuj się z administratorem.');
      }
    }
    else {
      $commit = false;
      $this->addError('email', 'Wystapił błąd z obsługą zdarzenia, skontaktuj się z administratorem.');
    }

    if ($commit) {
      $transaction->commit();
    }
    else {
      $transaction->rollback();
    }
    return $commit;
  }

}
