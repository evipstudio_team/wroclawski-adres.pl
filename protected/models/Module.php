<?php

class Module extends CActiveRecord {

  public static function model($className = __CLASS__) {
    return parent::model($className);
  }

  public function tableName() {
    return 'modules';
  }

  public function search() {
    $criteria = new CDbCriteria;
    return new CActiveDataProvider($this, array(
                'criteria' => $criteria,
            ));
  }

  public function getController() {
    switch ($this->name) {
      case 'Articles':
      case 'Article':
        return 'article';
        break;
      case 'Links':
        return 'link';
        break;
      case 'Newsletter':
        return 'newsletter';
        break;
      case 'Newsletters':
        return 'newsletter';
        break;
      case 'Banners':
        return 'banner';
        break;
      case 'Pages':
        return 'page';
        break;
      case 'Galleries':
      case 'Gallery':
        return 'gallery';
        break;
      case 'Multimedia':
        return 'multimedia';
        break;
      default:
        return 'Nierozpoznany Controller';
        break;
    }
  }

  public function getTranslatedName() {
    switch ($this->name) {
      case 'Articles':
        return 'Artykuły';
        break;
      case 'Article':
        return 'Artykuł';
        break;
      case 'Links':
        return 'Linki';
        break;
      case 'Newsletter':
        return 'Newsletter';
        break;
      case 'Newsletters':
        return 'Newsletter';
        break;
      case 'Banners':
        return 'Banery';
        break;
      case 'Pages':
        return 'Podstrona';
        break;
      case 'Galleries':
        return 'Galerie';
        break;
      case 'Gallery':
        return 'Galeria';
        break;
      default:
        return 'Nierozpoznany';
        break;
    }
  }

  static public function getModuleId($name) {
    $module = self::model()->find('name=:name', array(':name' => $name));
    if ($module)
      return $module->id;
    else
      throw new CHttpException(404, 'Module not exist.');
  }

  public function getSubmenu($page_id) {
    switch ($this->name) {
      case 'Articles':
      case 'Article':
        if($page_id==Yii::app()->params['pricePageId']) {
          return array(
            array(
                'label' => Yii::t('cms', 'Lista produktów'),
                'url' => Yii::app()->createUrl('article/index', array('page_id' => $page_id)),
                'active' => (getCurrentUrl() == Yii::app()->createUrl('article/index', array('page_id' => $page_id))) ? true : false
            ),
            array(
                'label' => Yii::t('cms', 'Dodaj produkt'),
                'url' => Yii::app()->createUrl('article/create', array('page_id' => $page_id)),
                'active' => (getCurrentUrl() == Yii::app()->createUrl('article/create', array('page_id' => $page_id))) ? true : false
            ),
            array(
                'label' => Yii::t('cms', 'Edycja strony głównej'),
                'url' => Yii::app()->createUrl('page/edit', array('id' => $page_id)),
                'active' => (Yii::app()->Controller->id == 'page') ? true : false,
                'visible'=>Yii::app()->user->checkAccess('Page:Edit',array('id'=>$page_id))
            ),
        );
        }
        elseif($page_id==Yii::app()->params['packegesPageId']) {
          return array(
            array(
                'label' => Yii::t('cms', 'Lista pakietów'),
                'url' => Yii::app()->createUrl('article/index', array('page_id' => $page_id)),
                'active' => (getCurrentUrl() == Yii::app()->createUrl('article/index', array('page_id' => $page_id))) ? true : false
            ),
            array(
                'label' => Yii::t('cms', 'Dodaj pakiet'),
                'url' => Yii::app()->createUrl('article/create', array('page_id' => $page_id)),
                'active' => (getCurrentUrl() == Yii::app()->createUrl('article/create', array('page_id' => $page_id))) ? true : false
            ),
            array(
                'label' => Yii::t('cms', 'Edycja strony głównej'),
                'url' => Yii::app()->createUrl('page/edit', array('id' => $page_id)),
                'active' => (Yii::app()->Controller->id == 'page') ? true : false,
                'visible'=>Yii::app()->user->checkAccess('Page:Edit',array('id'=>$page_id))
            ),
        );
        }
        else {

        }
        return array(
            array(
                'label' => Yii::t('cms', 'Lista artykułów'),
                'url' => Yii::app()->createUrl('article/index', array('page_id' => $page_id)),
                'active' => (getCurrentUrl() == Yii::app()->createUrl('article/index', array('page_id' => $page_id))) ? true : false
            ),
            array(
                'label' => Yii::t('cms', 'Dodaj artykuł'),
                'url' => Yii::app()->createUrl('article/create', array('page_id' => $page_id)),
                'active' => (getCurrentUrl() == Yii::app()->createUrl('article/create', array('page_id' => $page_id))) ? true : false
            ),
            array(
                'label' => Yii::t('cms', 'Edycja strony głównej'),
                'url' => Yii::app()->createUrl('page/edit', array('id' => $page_id)),
                'active' => (Yii::app()->Controller->id == 'page') ? true : false,
                'visible'=>Yii::app()->user->checkAccess('Page:Edit',array('id'=>$page_id))
            ),
        );
        break;
      case 'Gallery':
        return array(
            array(
                'label' => Yii::t('cms', 'Lista elementów'),
                'url' => Yii::app()->createUrl('gallery/index', array('page_id' => $page_id)),
                'active' => (getCurrentUrl() == Yii::app()->createUrl('gallery/index', array('page_id' => $page_id))) ? true : false
            ),
//            array(
//                'label' => Yii::t('cms', 'Dodaj element'),
//                'url' => Yii::app()->createUrl('gallery/create', array('page_id' => $page_id)),
//                'active' => (getCurrentUrl() == Yii::app()->createUrl('gallery/create', array('page_id' => $page_id))) ? true : false
//            ),
            array(
                'label' => Yii::t('cms', 'Dodaj podkategorię'),
                'url' => Yii::app()->createUrl('gallery/createCategory', array('parent_id' => $page_id)),
                'active' => (Yii::app()->controller->action->id == 'createCategory') ? true : false
            ),
            array(
                'label' => Yii::t('cms', 'Edycja strony głównej'),
                'url' => Yii::app()->createUrl('page/edit', array('id' => $page_id)),
                'active' => (Yii::app()->Controller->id == 'page') ? true : false,
            ),
        );
        break;

      default:
        break;
    }
  }

}