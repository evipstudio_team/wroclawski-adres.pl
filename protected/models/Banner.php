<?php

class Banner extends CActiveRecord {

  public static function model($className = __CLASS__) {
    return parent::model($className);
  }

  static public function getModuleId() {
    return Module::model()->find('`name`=:name', array(':name' => 'Banners'))->id;
  }

  public function tableName() {
    return 'banners';
  }

  public function rules() {
    return array(
        array('page_id', 'required'),
        array('related_module_id', 'safe'),
        array('status, position, time', 'numerical', 'integerOnly' => true),
//        array('related_element_id','numerical', 'integerOnly' => true, 'allowEmpty' => false, 'on'=>'createRelated'),
//        array('related_element_id', 'required', 'on'=>'createRelated'),
    );
  }

  public function beforeValidate() {
    if ($this->getIsNewRecord()) {
      if (!isset($this->status))
        $this->status = 1;
      if (!isset($this->position))
        $this->position = ($this->sumInPage) + 1;
    }
    if (!$this->time)
      $this->time = 10;
    if (!$this->related_module_id)
      $this->related_module_id = null;
    return parent::beforeValidate();
  }

  public function relations() {
    return array(
        'page' => array(self::BELONGS_TO, 'Page', 'page_id'),
        'relatedPage' => array(self::BELONGS_TO, 'Page', 'related_element_id'),
        'files' => array(self::HAS_MANY, 'Multimedia', 'element_id', 'condition' => '`module_id`=:module_id', 'order' => 'position', 'params' => array(':module_id' => self::getModuleId())),
        'sumInPage' => array(self::STAT, 'Banner', 'page_id'),
        'elementBlocks' => array(self::HAS_MANY, 'ElementBlock', 'element_id', 'condition' => 'module_id=:module_id', 'params' => array(':module_id' => self::getModuleId())),
        'url' => array(self::HAS_ONE, 'Url', array('element_id' => 'id'), 'condition' => '`lang_id`=:lang_id AND `module_id`=:module_id', 'params' => array(':lang_id' => Yii::app()->params['lang']->id,'module_id'=>Banner::getModuleId())),
    );
  }

  public function afterDelete() {
    foreach ($this->files as $file)
      $file->delete();
    foreach ($this->elementBlocks as $elementBlock)
      $elementBlock->delete();
    return parent::afterDelete();
  }

  public function attributeLabels() {
    return array(
        'id' => 'ID',
        'FilesPreview' => Yii::t('cms', 'Podgląd pliku'),
        'page' => Yii::t('cms', 'Podstrona'),
    );
  }

  public function getFilesPreview($separator = ' ') {
    $filesHtml = array();
    foreach ($this->files as $file) {
      array_push($filesHtml, '<a href="' . $file->link() . '" target="_blank">' . '<img src="' . $file->link(100) . '" alt="" title="" /></a>');
    }
    return implode($separator, $filesHtml);
  }

  public function search() {
    $criteria = new CDbCriteria;
    $criteria->order = 'status, position';

    return new CActiveDataProvider($this, array(
                'criteria' => $criteria,
            ));
  }

  static public function getSubMenu($page_id) {
    return array(
        array(
            'label' => Yii::t('cms', 'Lista elementów'),
            'url' => Yii::app()->createUrl('banner/index', array('page_id' => $page_id)),
            'active'=>(getCurrentUrl()==Yii::app()->createUrl('banner/index', array('page_id' => $page_id)))? true:false,
        ),
        array(
            'label' => Yii::t('cms', 'Dodaj nowy'),
            'url' => Yii::app()->createUrl('banner/create', array('page_id' => $page_id)),
            'active'=>(getCurrentUrl()==Yii::app()->createUrl('banner/create', array('page_id' => $page_id)))? true:false,
        ),
        array(
            'label' => Yii::t('cms', 'Edycja kategorii'),
            'url' => Yii::app()->createUrl('page/edit', array('id' => $page_id)),
            'active'=>(getCurrentUrl()==Yii::app()->createUrl('page/edit', array('id' => $page_id)))? true:false,
//            'visible' => Yii::app()->user->checkAccess('Administrator')
        ),
    );
  }

  public function getActiveHref() {
    $href = '';
    $url = $this->url;
    if ($url) {
      if ($url->inheritance) {
        $page = $this->page;
        $href = $page->activeHref . '/' . $url->address;
      }
      else
        $href = $url->address;
    }
    return normalizeHref($href);
  }

}