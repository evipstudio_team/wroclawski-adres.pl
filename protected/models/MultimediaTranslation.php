<?php

/**
 * This is the model class for table "multimedia_translations".
 *
 * The followings are the available columns in table 'multimedia_translations':
 * @property integer $multimedia_id
 * @property integer $lang_id
 * @property string $title
 * @property string $description
 */
class MultimediaTranslation extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return MultimediaTranslation the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'multimedia_translations';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('multimedia_id, lang_id, title', 'required'),
			array('multimedia_id, lang_id', 'numerical', 'integerOnly'=>true),
			array('title', 'length', 'max'=>255),
      array('description', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('multimedia_id, lang_id, title, description', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
        'parent'=>array(self::BELONGS_TO,'Multimedia','multimedia_id')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'multimedia_id' => 'Multimedia',
			'lang_id' => 'Lang',
			'title' => 'Tytuł',
			'description' => 'Krótki opis',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('multimedia_id',$this->multimedia_id);
		$criteria->compare('lang_id',$this->lang_id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('description',$this->description,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}