<?php

class PageRelations extends CActiveRecord {

  public static function model($className = __CLASS__) {
    return parent::model($className);
  }

  public function relations() {
    return array(
        'parent' => array(self::BELONGS_TO, 'Page', 'parent_id'),
        'parent' => array(self::BELONGS_TO, 'Page', 'parent_id'),
        'activeParent' => array(self::BELONGS_TO, 'Page', 'parent_id', 'condition'=>'status=1'),
        'child' => array(self::BELONGS_TO, 'Page', 'child_id'),
        'activeChild' => array(self::BELONGS_TO, 'Page', 'child_id', 'condition'=>'status=1'),
    );
  }

  public function tableName() {
    return 'page_relations';
  }

  static public function getNodes($childId, $order = 'length DESC') {
    return self::model()->findAll(array('condition'=>'`child_id`=:child_id','params'=>array(':child_id'=>$childId),'order'=>$order));
  }

  static public function getChildNodes($parentId, $order = 'length DESC', $active = 0) {
    if($active) {
      return self::model()->findAll(array('condition'=>'`t`.`parent_id`=:parent_id','params'=>array(':parent_id'=>$parentId),'order'=>$order, 'with'=>'activeChild'));
    }
    else
      return self::model()->findAll(array('condition'=>'`parent_id`=:parent_id','params'=>array(':parent_id'=>$parentId),'order'=>$order));
  }

  static public function getManagedNode($childId) {
    $pageRelations = self::getNodes($childId);
    foreach($pageRelations as $pageRelation) {
      $page = $pageRelation->parent;
      if($page->managed) return $page;
    }
    return null;
  }

  public function search() {
    $criteria = new CDbCriteria;

    $criteria->compare('`t`.`parent_id`', $this->parent_id);

    $criteria->with = array('parent','child');
    $criteria->order = '`child`.`matrix`';

    return new CActiveDataProvider($this, array(
                'criteria' => $criteria,
        'pagination'=>false
//                'sort' => array(
//                    'defaultOrder' => 'matrix',
//                ),
            ));
  }

}