<?php

class BlockAssignment extends CActiveRecord {

  public function relations() {
    return array(
        'module'=>array(self::BELONGS_TO,'Module','module_id'),
        'block'=>array(self::BELONGS_TO,'Block','block_id'),
        'page'=>array(self::BELONGS_TO,'Page','page_id')
    );
  }

  public function rules() {
    return array(
        array('block_id', 'required'),
        array('module_id, page_id', 'default', 'value'=>0),
    );
  }

  public function beforeValidate() {
    if(self::model()->findByPk(array('block_id'=>$this->block_id,'module_id'=>$this->module_id,'page_id'=>$this->page_id))) {
      $this->addError('block_id', Yii::t('cms', 'Podany zestaw już istnieje.'));
    }
    return parent::beforeValidate();
  }

  public static function model($className = __CLASS__) {
    return parent::model($className);
  }

  static public function createNew($params) {
    $blockAssignment = new BlockAssignment();
    $blockAssignment->setAttributes($params, false);
    $blockAssignment->save();
    return $blockAssignment;
  }

  public function tableName() {
    return 'blocks_assignments';
  }

  public function search() {
    $criteria = new CDbCriteria;
    $criteria->compare('block_id', $this->block_id, false);
    return new CActiveDataProvider($this, array(
                'criteria' => $criteria,
                'sort' => array(
                    'defaultOrder' => 'module_id, page_id',
                ),
            ));
  }

}