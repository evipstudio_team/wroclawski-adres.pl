<?php

class Cause extends CActiveRecord {
  public $lastComment_created_at;

  public static function model($className = __CLASS__) {
    return parent::model($className);
  }

  public function tableName() {
    return 'causes';
  }

  public function rules() {
    return array(
        array('user_id, created_at, rule_date', 'required'),
        array('id, created_at, actual_step, last_response_date', 'safe', 'on'=>'search')
    );
  }

  public function relations() {
    return array(
        'user' => array(self::BELONGS_TO, 'User', 'user_id'),
        'comments' => array(self::HAS_MANY, 'CauseComment', 'cause_id', 'order' => 'id DESC'),
        'lastComment' => array(self::HAS_ONE, 'CauseComment', 'cause_id', 'order' => 'lastComment.id DESC'),
        'files'=>array(self::HAS_MANY, 'CauseFile','cause_id', 'order'=>'id DESC'),
        'filesCount'=>array(self::STAT, 'CauseFile','cause_id')
    );
  }

  public function attributeLabels() {
    return array(
        'created_at'=>'Data utworzenia',
        'user.email'=>'Adres e-mail',
        'user.fullName'=>'Nazwa użytkownika',
        'pass_code'=>'Kod dostepu',
        'actual_step'=>'Aktualny krok',
        'stepTranslated'=>'Aktualny krok',
        'last_response_date'=>'Data ostatniej aktualizacji',
        'CommentsHistory' => 'Historia zmian',
        'rule_date'=>'Data otrzymania nakazu'
    );
  }

  public function beforeValidate() {
    if ($this->isNewRecord) {
      $this->created_at = date('Y-m-d H:i:s');
    }
    return parent::beforeValidate();
  }

  public function afterSave() {
    if (!$this->comments) {
      $causeComment = new CauseComment();
      $causeComment->setAttributes(array(
          'cause_id' => $this->id,
          'created_by' => $this->user_id,
          'comment' => 'Użytkownik ' . $this->user->email . ' dodał nową sprawę.',
          'step' => 1
              ), false);
      $causeComment->save();
    }
    return parent::afterSave();
  }

  public function getCommentsHistory() {
    $comments = array();
    foreach ($this->comments as $comment) {
      if($comment->created_by)
        array_push($comments, date('Y-m-d', strtotime($comment->created_at)).' - '.$comment->comment);
    }
    return $comments;
  }

  public function search($user) {
    $criteria = new CDbCriteria;
    $criteria->with = array('lastComment','user');
    $criteria->compare('`t`.`id`', $this->id);
    $criteria->compare('`t`.`actual_step`', $this->actual_step);
    $criteria->compare('user.email', $user->email, true);
    if($user->name) {
      $criteria->addcondition("(user.name LIKE '%".$user->name."%' OR user.surname LIKE '%".$user->name."%')");
    }

    return new CActiveDataProvider($this, array(
                'criteria' => $criteria,
                'pagination' => array(
                    'pageSize' => Yii::app()->user->getState('pageSize', Yii::app()->params['defaultPageSize']),
                ),
                'sort' => array(
                    'defaultOrder' => 't.created_at DESC',
                ),
            ));
  }

  public function getStepTranslated($step = null) {
    if ($step === null)
      $step = $this->actual_step;
    switch ($step) {
      case 1:
        return 'Krok 1';
        break;
      case 2:
        return 'Krok 2';
        break;
      case 3:
        return 'Krok 3';
        break;
      case 4:
        return 'Krok 4';
        break;
      case 5:
        return 'Zamknięta pozytywnie';
        break;
      case 6:
        return 'Zamknięta negatywnie';
        break;
      default:
        break;
    }
  }

  public function getNextPossibleSteps($step = null) {
    if($step===null) $step = $this->actual_step;
    $steps = array();
    switch ($step) {
      case 0:
        $steps = array(
            1,2,3,4,1,5,6
        );
        break;
      case 1:
        $steps = array(
            2,3,4,1,5,6
        );
        break;
      case 2:
        $steps = array(
            3,4,2,1,5,6
        );
        break;
      case 3:
        $steps = array(
            4,3,2,5,6
        );
        break;
      case 4:
        $steps = array(
            4,5,6
        );
        break;
      case 5:
        $steps = array(
            5,6
        );
        break;
      case 6:
        $steps = array(
            6,5
        );
        break;
      default:
        break;
    }
    $possibleSteps = array();
    foreach($steps as $step) {
      $possibleSteps[$step] = $this->getStepTranslated($step);
    }
    return $possibleSteps;
  }

  public function afterDelete() {
    foreach(array('comments','files') as $name) {
      if($this->$name) {
        foreach($this->$name as $element) $element->delete();
      }
    }
    return parent::afterDelete();
  }

  public function filesToString($separator = '<br />') {
    $files = array();
    foreach($this->files(array('order'=>'id ASC')) as $file) {
      array_push($files, date('d/m/Y',strtotime($file->created_at)).' - '.$file->filename);
    }
    return implode($separator, $files);
  }

  public function prepareTimeLineView() {
    $causeComments = $this->comments;
    $causeFiles = $this->files;

    $data = array();
    foreach (array('causeComments') as $elementsName) {
      foreach ($$elementsName as $element) {
        $date = date('Y-m-d', strtotime($element->created_at));
        if (!isset($data[$date]))
          $data[$date] = array();
        if (!isset($data[$date][$element->created_by]))
          $data[$date][$element->created_by] = array();


        switch (get_class($element)) {
          case 'CauseComment':
            $elementArray = array(
              'type' => get_class($element),
              'comment' => $element->comment,
              'created_at' => $element->created_at,
              'user'=>$element->author,
              'step' => $element->step
            );
            break;
          case 'CauseFile':
            $elementArray = array(
              'type' => get_class($element),
              'comment' => $element->filename,
              'files' => array($element->filename),
              'user'=>$element->author,
              'created_at' => $element->created_at,
            );
            break;
          default:
            $elementArray = array();
            break;
        }
        if(get_class($element)=='CauseFile' && isset($data[$date][$element->created_by])) {
          if(!isset($data[$date][$element->created_by][0]['files'])) $data[$date][$element->created_by][0]['files'] = array();
          $data[$date][$element->created_by][0]['files'][] = $elementArray['comment'];
        }
        else {
          $data[$date][$element->created_by][] = $elementArray;
        }
      }
    }

    return $data;
  }
}