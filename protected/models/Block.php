<?php

class Block extends CActiveRecord
{
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return 'blocks';
	}

	public function rules() {
    return array(
        array('name', 'required'),
        array('name', 'length', 'max' => 128),
        array('description, last_update', 'safe'),
        array('managed', 'in', 'range' => array_keys($this->getEnumOptions('managed')), 'allowEmpty' => false),
        array('id, name', 'safe', 'on' => 'search'),
    );
  }

	public function relations()
	{

		return array(
      'pages'=>array(self::MANY_MANY, 'Page','pages_blocks(block_id,page_id)', 'order'=>'`pages_pages`.position'),
      'activePages'=>array(self::MANY_MANY, 'Page','pages_blocks(block_id,page_id)','condition'=>'`activePages`.status=1','order'=>'`activePages_activePages`.position'),
      'modules'=>array(self::MANY_MANY, 'Module','modules_blocks(module_id,block_id)'),
      'assignments'=>array(self::HAS_MANY, 'BlockAssignment','block_id'),
      'blockPages'=>array(self::HAS_MANY, 'BlockPage','block_id'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => Yii::t('cms', 'Nazwa'),
      'description' => Yii::t('cms', 'Krótki opis'),
      'managed' => Yii::t('cms', 'Osobne menu'),
		);
	}

	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('name',$this->name,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

  public function afterDelete() {
    foreach($this->assignments as $assignment) $assignment->delete();
    foreach($this->blockPages as $blockPages) $blockPages->delete();
    return parent::afterDelete();
  }

//  public function searchForModule($module_id)
//	{
//		$criteria=new CDbCriteria;
//    $criteria->addInCondition('`id`', CHtml::listData(ModuleBlock::model()->findAll('`module_id`=:module_id',array(':module_id'=>$module_id)), 'block_id', 'block_id'));
//    $criteria->order = 'name';
//		return new CActiveDataProvider($this, array(
//			'criteria'=>$criteria,
//		));
//	}
//
//  public function searchPages()
//	{
//		// Warning: Please modify the following code to remove attributes that
//		// should not be searched.
//
//		$criteria=new CDbCriteria;
//
////		$criteria->compare('id',$this->id,true);
////		$criteria->compare('name',$this->name,true);
//
//    $criteria->with = array('pages');
//		return new CActiveDataProvider($this, array(
//			'criteria'=>$criteria,
//		));
//	}
//
//  protected function loadElement($module_id,$element_id) {
//    $model = ElementBlock::model()->findByPk(array('module_id'=>$module_id,'element_id'=>$element_id,'block_id'=>$this->id));
//    return $model;
//  }
//
  public function isAssignedTranslate($page_id) {
    if($this->pages(array('condition'=>'page_id=:page_id','params'=>array(':page_id'=>$page_id)))) {
      return 'Elementy są powiązane, kliknij aby wypisać.';
    }
    else {
      return 'Kliknij aby powiązać elementy.';
    }
  }

  public function isAssignedIcon($page_id) {
    if($this->pages(array('condition'=>'page_id=:page_id','params'=>array(':page_id'=>$page_id)))) {
      return '/images/icons/active.png';
    }
    else {
      return '/images/icons/inactive.png';
    }
  }

  public function beforeSave() {
    $this->setLastUpdate();
    return parent::beforeSave();
  }

  public function setLastUpdate() {
    $this->last_update = date('Y-m-d H:i:s');
  }

  public function getEnumOptions($fieldName) {
    $values = false;
    switch ($fieldName) {
      case 'managed':
        $values = array(
            1 => Yii::t('cms', 'Tak'),
            0 => Yii::t('cms', 'Nie')
        );
        break;
      default:
        break;
    }
    return $values;
  }

  public function TranslateEnumValue($fieldName) {
    $values = $this->getEnumOptions($fieldName);
    if (isset($values[$this->$fieldName])) {
      return $values[$this->$fieldName];
    }
    else
      return $this->$fieldName;
  }

  public function managedMenu() {
    $menu = array();
    $models = self::model()->findAll('managed=:managed',array(':managed'=>1));
    foreach($models as $model) {
      array_push($menu,array('label'=>$model->name, 'url'=>array('block/view','id'=>$model->id), 'visible'=>Yii::app()->user->checkAccess('CmsUser')));
    }
    return $menu;
  }

//  public function isAssignedToBlockTranslate($element_name,$element_id) {
//    if($this->loadBlock($blockId)) {
//      return 'Podstrona jest przypisana, kliknij aby wypisać podstronę z tego bloku.';
//    }
//    else {
//      return 'Kliknij aby przypisać podstronę do tego bloku.';
//    }
//  }
//
//  public function isAssignedToBlockIcon($element_name,$element_id) {
//    if($this->loadBlock($blockId)) {
//      return '/images/icons/icon_assigned.png';
//    }
//    else {
//      return '/images/icons/icon_unassigned.png';
//    }
//  }
}