<?php

class Room extends CActiveRecord {

  public static function model($className = __CLASS__) {
    return parent::model($className);
  }

  public function tableName() {
    return 'rooms';
  }

  public function rules() {
    return array(
        array('name', 'required'),
        array('cost_per_hour, cost_per_day', 'match', 'pattern' => '/^[0-9]{1,3}(\.[0-9]{0,2})?$/'),
        array('parent_id, child_count', 'default', 'value' => null),
    );
  }

  public function relations() {
    return array(
        'childCount' => array(self::STAT, 'Room', 'parent_id', 'condition' => 'parent_id IS NOT NULL'),
        'parent' => array(self::BELONGS_TO, 'Room', 'parent_id'),
    );
  }

  public function attributeLabels() {
    return array(
        'parent_id' => 'Sala/Biurko',
        'name' => 'Nazwa',
        'cost_per_hour' => 'Cena/1h',
        'cost_per_day' => 'Cena/ cały dzień',
    );
  }

  public function beforeValidate() {
    $this->child_count = $this->childCount;
    return parent::beforeValidate();
  }

  public function afterSave() {
    if ($this->parent_id) {
      $this->parent->child_count = $this->parent->childCount;
      $this->parent->save();
    }
  }

  static public function pickUpRooms($type, $reservationModel = false) {
    if ($reservationModel->date_from_helper || $reservationModel->date_to_helper) {
      $excludeIds = CHtml::listData($reservationModel->pickUpReservations(), 'room_id', 'room_id');
    }
    else
      $excludeIds = array();
    switch ($type) {
      case '1':
        $rooms = self::pickUpHalls($excludeIds);
        break;
      case '2':
        $rooms = self::pickUpDesks($excludeIds);
        break;
      case '3':
        $rooms = self::pickUpComputerDesks($excludeIds);
        break;
      case '4':
        $rooms = self::pickUpOffices($excludeIds);
        break;

      default:
        $rooms = array();
        break;
    }
    return $rooms;
  }

  static public function pickUpHalls($excludeIds) {
    $criteria = new CDbCriteria;
    $criteria->addCondition('`parent_id` IS NULL AND `child_count` = 0');
    $criteria->addNotInCondition('`id`', $excludeIds);
    $halls = Room::model()->findAll($criteria);
    return $halls;
  }

  static public function pickUpDesks($excludeIds) {
    $criteria = new CDbCriteria;
    $criteria->addCondition('`parent_id` IS NOT NULL AND `child_count` = 0');
    $criteria->addNotInCondition('`id`', $excludeIds);
    $desks = Room::model()->findAll($criteria);
    return $desks;
  }

  static public function pickUpComputerDesks($excludeIds) {
    $criteria = new CDbCriteria;
    $criteria->addCondition('`parent_id` IS NOT NULL AND `child_count` = 0');
    $criteria->addNotInCondition('`id`', $excludeIds);
    $desks = Room::model()->findAll($criteria);
    return $desks;
  }

  static public function pickUpOffices($excludeIds) {
    $criteria = new CDbCriteria;
    $criteria->addCondition('`parent_id` IS NULL AND `child_count` > 0');
    $criteria->addNotInCondition('`id`', $excludeIds);
    $desks = Room::model()->findAll($criteria);
    return $desks;
  }

  public function search() {
    $criteria = new CDbCriteria;
    $criteria->compare('`t`.`id`', $this->id);

    return new CActiveDataProvider($this, array(
                'criteria' => $criteria,
                'pagination' => array(
                    'pageSize' => Yii::app()->user->getState('pageSize', Yii::app()->params['defaultPageSize']),
                ),
                'sort' => array(
                    'defaultOrder' => 'id',
                ),
            ));
  }

  public function getType() {
    if (!$this->parent_id && $this->child_count) {
      return 'Biuro';
    } elseif (!$this->parent_id && !$this->child_count) {
      return 'Sala';
    } elseif ($this->parent_id) {
      return 'Miejsce do pracy z komputerem';
    }
    return 'Nierozpoznane';
  }

  static public function getRelatedRooms($type = 0, $id = 0) {
    if ($id) {
      switch ($type) {
        case 1:

          break;
        case 2:
        case 3:
          $room = Room::model()->findbyPk($id);

          $criteria = new CDbCriteria;
          $criteria->addCondition("`parent_id`=$room->parent_id AND `child_count`=0");
          $desks = CHtml::listData(self::model()->findAll($criteria), 'id', 'id');

          $criteria = new CDbCriteria;
          $criteria->addCondition("id=$room->parent_id");
          $office = CHtml::listData(self::model()->findAll($criteria), 'id', 'id');

          return $desks+$office;
          break;
        case 4:
          $room = Room::model()->findbyPk($id);

          $criteria = new CDbCriteria;
          $criteria->addCondition("`parent_id`=$room->id AND `child_count`=0");
          $desks = CHtml::listData(self::model()->findAll($criteria), 'id', 'id');

          $criteria = new CDbCriteria;
          $criteria->addCondition("id=$room->id");
          $office = CHtml::listData(self::model()->findAll($criteria), 'id', 'id');
          return $desks+$office;
          break;

        default:
          break;
      }
    } else {
      if ($type) {
        switch ($type) {
          case 1:
            $criteria = new CDbCriteria;
            $criteria->addCondition('`parent_id` IS NULL AND `child_count` IS NULL');
            return CHtml::listData(self::model()->findAll($criteria), 'id', 'id');
            break;
          case 2:
          case 3:
          case 4:
            $criteria = new CDbCriteria;
            $criteria->addCondition('`parent_id` IS NOT NULL AND `child_count` IS NULL');
            $desks = CHtml::listData(self::model()->findAll($criteria), 'id', 'id');

            $criteria = new CDbCriteria;
            $criteria->addCondition('`parent_id` IS NOT NULL AND `child_count` IS NOT NULL');
            $offices = CHtml::listData(self::model()->findAll($criteria), 'id', 'id');
            return $desks + $offices;
            break;

          default:
            break;
        }
      } else {
        return CHtml::listData(self::model()->findAll(), 'id', 'id');
      }
    }
    return array();
  }

}