<?php

class Page extends CActiveRecord {

//  var $urls = array();
  var $childrens = array();
  var $productPackege;

  public static function model($className = __CLASS__) {
    return parent::model($className);
  }

  static public function getModuleId() {
    return Module::model()->find('`name`=:name', array(':name' => 'Pages'))->id;
  }

  public function relations() {
    return array(
        'parent' => array(self::BELONGS_TO, 'Page', 'parent_id'),
        'childCount' => array(self::STAT, 'Page', 'parent_id'),
        'childs' => array(self::HAS_MANY, 'Page', 'parent_id'),
        'activeChilds' => array(self::HAS_MANY, 'Page', 'parent_id', 'condition'=>'`activeChilds`.`status`=1', 'order'=>'`activeChilds`.`matrix`'),
        'activeChildsCount' => array(self::STAT, 'Page', 'parent_id', 'condition'=>'`status`=1'),
        'urls' => array(self::HAS_MANY, 'Url', 'page_id', 'order'=>'FIELD(lang_id,'.implode(',',CHtml::listData(Yii::app()->params['langs'], 'id', 'id')).')'),
        'url'=>array(self::HAS_ONE, 'Url','page_id','condition'=>'lang_id = '.Yii::app()->params['lang']->id),
        'articles' => array(self::HAS_MANY, 'Article', 'page_id', 'order'=>'FIELD(lang_id,'.implode(',',CHtml::listData(Yii::app()->params['langs'], 'id', 'id')).')'),
        'articlesCount' => array(self::STAT, 'Article', 'page_id'),
        'article'=>array(self::HAS_ONE,'Article','page_id','condition'=>'`article`.`lang_id`=:lang_id','params'=>array(':lang_id'=>Yii::app()->params['lang']->id)),
        'blocks' => array(self::MANY_MANY, 'Block', 'pages_blocks(page_id,block_id)'),
        'pagesBlocks' => array(self::HAS_MANY, 'BlockPage', 'page_id'),
        'module' => array(self::BELONGS_TO, 'Module', 'module_id'),
        'files' => array(self::HAS_MANY, 'Multimedia', 'page_id'),
        'filesCount' => array(self::STAT, 'Multimedia', 'page_id'),
        'products' => array(self::HAS_MANY, 'ProductPackege', 'packege_id'),
    );
  }

  public function rules() {
    return array(
        array('name', 'required', 'on'=>'createCategory'),
        array('status, managed, inheritance_url, module_id', 'required'),
        array('parent_id','default','value'=>null),
        array('url_edit_form, view_template, name','safe'),
        array('unit_price, 0_months_price, 6_months_price, 12_months_price', 'match', 'pattern'=>'/^[0-9]{1,3}(\.[0-9]{0,2})?$/', 'allowEmpty'=>true),
        array('unit_price, 0_months_price, 6_months_price, 12_months_price','default','value'=>null),
        array('price_packege,individual_price','default','value'=>null),
    );
  }



  public function beforeValidate() {
    if($this->scenario == 'createCategory') {
      $parent = $this->parent;
      $this->inheritance_url = $parent->inheritance_url;
      $this->module_id = $parent->module_id;
      $this->status = $parent->status;
      $this->view_template = $parent->view_template;
      $this->url_edit_form = $parent->url_edit_form;
    }
    return parent::beforeValidate();
  }


  public function tableName() {
    return 'pages';
  }

  public function countDepth() {
    $firstPage = self::firstPage();
    $page = $this;
    $depth = 0;
    while ($page && $page->id != $firstPage->id) {
      $page = Page::model()->findByPk($page->parent_id);
      if ($page)
        $depth++;
    }
    return $depth;
  }

  static public function firstPage() {
    return Page::model()->find('`parent_id`=0');
  }

  public function getEnumOptions($fieldName) {
    $values = false;
    switch ($fieldName) {
      case 'status':
        $values = array(
            0 => Yii::t('cms', 'Nieaktywna'),
            1 => Yii::t('cms', 'Aktywna'),
        );
        break;
      case 'inheritance_url':
      case 'managed':
        $values = array(
            1 => Yii::t('cms', 'Tak'),
            0 => Yii::t('cms', 'Nie')
        );
        break;
      default:
        break;
    }
    return $values;
  }

  public function TranslateEnumValue($fieldName) {
    $values = $this->getEnumOptions($fieldName);
    if (isset($values[$this->$fieldName])) {
      return $values[$this->$fieldName];
    }
    else
      return $this->$fieldName;
  }

  public function primaryKey() {
    return 'id';
  }

  public function urlExist() {
    if ($this->urlsCount)
      return true;
    else
      return false;
  }

  public function nameDepthSeparator($char = '&nbsp;', $depth = null) {
    if($depth==null) $depth = $this->countDepth();
    $i = 0;
    $separator = '';
    while ($i <= $depth) {
      $separator.=$char;
      $i++;
    }
    return $separator;
  }

  public function attributeLabels() {
    return array(
        'id' => 'ID',
        'name' => Yii::t('cms', 'Nazwa'),
        'parent_id' => Yii::t('cms', 'Należy do'),
        'managed' => Yii::t('cms', 'Osobne menu'),
        'status' => Yii::t('cms', 'Status'),
        'created_by' => Yii::t('cms', 'Autor'),
        'created_at' => Yii::t('cms', 'Data utworzenia'),
        'articleCount' => Yii::t('cms', 'Ilość artykułów'),
        'inheritance_url' => Yii::t('cms', 'Dziedziczenie adresu url'),
        'TypeTranslated' => Yii::t('cms', 'Moduł'),
        'module_id' => Yii::t('cms', 'Moduł'),
        'url_edit_form' => Yii::t('cms', 'Formularz modelu URL'),
        'view_template' => Yii::t('cms', 'Widok po stronie klienta'),
        'unit_price'=>  Yii::t('cms', 'Cena jednostkowa'),
        '0_months_price'=> Yii::t('cms', 'Cena w przypadku umowy na 3 miesiące i płatności z góry'),
        '6_months_price'=> Yii::t('cms', 'Cena w przypadku umowy na 6 miesięcy i płatności z góry'),
        '12_months_price'=> Yii::t('cms', 'Cena w przypadku umowy na 12 miesięcy i płatności z góry'),
        'individual_price'=> Yii::t('cms', 'Indywidualna stawka za wybrane usługi')

    );
  }

  public function search() {
    $criteria = new CDbCriteria;

    if($this->parent_id)
      $criteria->compare('parent_id', $this->parent_id);
    return new CActiveDataProvider($this, array(
                'criteria' => $criteria,
                'sort' => array(
                    'defaultOrder' => 'matrix',
                ),
            ));
  }

  public function searchProducts() {
    $criteria = new CDbCriteria;

    if($this->parent_id)
      $criteria->compare('parent_id', $this->parent_id);
    return new CActiveDataProvider($this, array(
                'criteria' => $criteria,
                'pagination'=>false,
                'sort' => array(
                    'defaultOrder' => 'matrix',
                ),
            ));
  }

  public function searchForBlockAssign($block) {
    $criteria = new CDbCriteria;

    $blockAssignments = $block->assignments;
    if ($blockAssignments) {
      foreach ($blockAssignments as $blockAssignment) {
        if ($blockAssignment->module_id && !$blockAssignment->page_id) {
          $criteria->addCondition("module_id = $blockAssignment->module_id", 'OR');
        } elseif ($blockAssignment->module_id && $blockAssignment->page_id) {
          $criteria->addCondition("module_id = $blockAssignment->module_id AND (id=$blockAssignment->page_id OR parent_id=$blockAssignment->page_id)", 'OR');
        } elseif (!$blockAssignment->module_id && $blockAssignment->page_id) {
          $criteria->addCondition("(id=$blockAssignment->page_id OR parent_id=$blockAssignment->page_id)", 'OR');
        }
      }
      $criteria->addNotInCondition('id', CHtml::listData($block->pages, 'id', 'id'));
    } else {
      $criteria->addCondition("1=0");
    }
    return new CActiveDataProvider($this, array(
                'criteria' => $criteria,
                'sort' => array(
                    'defaultOrder' => 'position',
                ),
            ));
  }

  public function searchPossibleBlocks() {
    $criteria = new CDbCriteria;
    $criteria->addCondition("module_id = $this->module_id AND page_id=0", 'OR');
    $criteria->addCondition("module_id = 0 AND (page_id=$this->id OR page_id=$this->parent_id)", 'OR');
    $criteria->addCondition("module_id = $this->module_id AND (page_id=$this->id OR page_id=$this->parent_id)", 'OR');
    $criteria->group = 'block_id';
    $criteria->select = 'block_id';
    $blockAssignments = BlockAssignment::model()->findAll($criteria);

    $criteria = new CDbCriteria;
    if($blockAssignments) {
      $criteria->addInCondition('id', CHtml::listData($blockAssignments, 'block_id', 'block_id'));
    }
    else {
      $criteria->addCondition('1=0');
    }
    return new CActiveDataProvider('Block', array(
                'criteria' => $criteria,
                'sort' => array(
                    'defaultOrder' => 'name',
                ),
            ));

  }

  public function searchArticles($status = null, $perPage = null, $defaultOrder = 'position') {
    $criteria = new CDbCriteria;

    $criteria->compare('parent_id', $this->parent_id, true);
    $criteria->compare('module_id', Module::getModuleId('Article'));
    if($status!==null) {
      $criteria->compare('status', $status);
    }

    if($perPage) $pagination = array('pageSize'=>$perPage);
    else $pagination = false;
    return new CActiveDataProvider($this, array(
                'criteria' => $criteria,
                'sort' => array(
                    'defaultOrder' => $defaultOrder,
                ),
        'pagination'=>$pagination
            ));
  }

  public function searchArticlesByBlock($status = null, $perPage = null, $defaultOrder = 'position', $blockId) {
    $criteria = new CDbCriteria;

    $criteria->compare('module_id', Module::getModuleId('Article'));
    if($status!==null) {
      $criteria->compare('status', $status);
    }
    $criteria->with = array('blocks'=>array('condition'=>'`blocks`.`id`='.$blockId));

    if($perPage) $pagination = array('pageSize'=>$perPage);
    else $pagination = false;
    return new CActiveDataProvider($this, array(
                'criteria' => $criteria,
                'sort' => array(
                    'defaultOrder' => $defaultOrder,
                ),
        'pagination'=>$pagination
            ));
  }

  public function managedMenu($activePageId) {
    $menu = array();
    $managedPages = self::model()->findAll('managed=:managed ORDER BY position', array(':managed' => 1));

    foreach ($managedPages as $page) {
      $url = Yii::app()->createUrl($page->module->controller . '/index', array('page_id' => $page->id));
      $page->buildTree('id');
      $childIds = $page->getChildrenIdsMerged();
      $active = ($page->id == $activePageId || in_array($activePageId, $childIds)) ? true : false;
      $submenu = array();
      if ($active) {
        $submenu = $page->module->getSubmenu($page->id);
      }
      $submenuActive = false;
      if($submenu)
      foreach ($submenu as $element) {
        if (isset($element['active']) && $element['active']) {
          $submenuActive = true;
          break;
        }
      }
      if ($submenu && $submenuActive == false)
        $submenu[0]['active'] = true;
      array_push($menu, array(
          'label' => $page->name,
          'url' => $url,
          'active' => (!$submenu && getCurrentUrl() == $url) ? true : false,
          'submenuOptions' => array('class' => 'sub_ul_left'),
          'items' => $submenu
      ));
    }
    return $menu;
  }

  public function buildTree($select = "*") {
    $childrens = Page::model()->findAll(array('select' => $select, 'condition' => '`parent_id`=:parent_id', 'order' => 'position', 'params' => array(':parent_id' => $this->id)));
    $this->childrens = $childrens;
    foreach ($this->childrens as &$child) {
      $child->buildTree($select);
    }
  }

  public function getChildrenIdsMerged($parentsOnly = false) {
    $ids = array();
    foreach ($this->childrens as $child) {
      if ($parentsOnly && $child->childrens) {
        array_push($ids, $child->id);
      } elseif (!$parentsOnly) {
        array_push($ids, $child->id);
      }
      $childIds = $child->getChildrenIdsMerged($parentsOnly);
      $ids = array_merge($ids, $childIds);
    }
    $criteria = new CDbCriteria;
    $criteria->select = '`id`';
    $criteria->addInCondition('`id`', $ids);
    $criteria->order = 'position';
    $ids = CHtml::listData(Page::model()->findAll($criteria), 'id', 'id');
    return $ids;
  }

  static public function getTreeCriteria($parentsOnly = false) {
    $page = page::model()->findByPk(1);
    $page->buildTree('id');
    return $page->articlesPagesTreeCriteria();
  }

  static public function getTreeIds($id, $parentsOnly = false) {
    $page = Page::model()->findByPk($id);
    $page->buildTree('`id`');
    $ids = array_merge(array($id), $page->getChildrenIdsMerged($parentsOnly));
    return $ids;
  }

  public function afterDelete() {
    $relatedElements = array('articles', 'pagesBlocks', 'urls');
    foreach ($relatedElements as $relatedElement) {
      foreach ($this->$relatedElement as $element)
        $element->delete();
    }
    return parent::afterDelete();
  }

  public function getActiveAnchor() {
    return $this->activeUrl->anchor;
  }

  public function getActiveAddress() {
    return $this->activeUrl->address;
  }

  public function getActiveHref() {
    $href = '';
    $url = $this->activeUrl;
    if ($url) {
      if ($url->inheritance) {
        $parent = $this->parent;
        if ($parent && $parent->inheritance_url) {
          $href = $parent->activeHref . '/' . $url->address;
        } else {
          $href = '/' . $url->address;
        }
      }
      else
        $href = '/' . $url->address;
    }
    return normalizeHref($href);
  }

  public function getActiveTitle() {

  }

  public function beforeSave() {
    if($this->isNewRecord) {
      $parent = $this->parent;
      if($parent)
        $this->position = ($parent->childCount+1);
    }
    else {
      $articles = $this->articles;
      $old = Page::model()->findByPk($this->id);
      foreach($articles as $article) {
        if($article->title == $old->name) {
          $article->title = $this->name;
          $article->save();
        }
      }
    }

    return parent::beforeSave();
  }

  public function afterSave() {
    if($this->inheritance_url && $this->module_id==Module::getModuleId('Gallery') && !$this->articles) {
      $article = new Article();
      $article->setAttributes(array(
          'page_id'=>$this->id,
          'lang_id'=>Yii::app()->params['defaultLang']->id,
          'title'=>$this->name
      ));
      $article->save();
    }
  }

//  public function afterSave() {
//    $url = $this->defaultUrl;
//    if (!$url) {
//      $url = new Url();
//      $url->setAttributes(array(
//          'module_id' => Page::getModuleId(),
//          'element_id' => $this->id,
//          'lang_id' => Yii::app()->params['defaultLang']->id,
//          'autoupdate' => 1,
//          'blank' => 0,
//          'anchor' => $this->name,
//          'address' => $this->name,
//      ));
//      $url->save();
//    } elseif ($url->autoupdate) {
//      $url->setAttributes(array(
//          'anchor' => $this->name,
//          'address' => $this->name,
//      ));
//      $url->save();
//    }
//    return parent::afterSave();
//  }

  public function isActive() {
    return $this->status;
  }

  public function getName($calculated = 0) {
    if($calculated) {
      switch ($this->module_id) {
        case Module::getModuleId('Article'):
          $articles = $this->articles;
          if($articles) return $articles[0]->title;
          else return null;
          break;

        default:
          break;
      }
    }
    return parent::__get('name');
  }

  public function getTranslationLangIcons() {
    $icons = array();
    foreach (Lang::model()->findAll(array('order' => 'position')) as $lang) {
      $exist = Article::model()->findByPk(array('page_id' => $this->id, 'lang_id' => $lang->id));
      array_push($icons, '<a href="' . CHtml::encode(Yii::app()->createUrl('article/edit', array('id' => $this->id, 'lang_id' => $lang->id))) . '">' . getLangIcon($exist, $lang) . '</a>');
    }
    return implode('&nbsp;&nbsp;&nbsp;', $icons);
  }

  public function getCalculatedAddress() {
    $url = $this->url;
    $href = '';
    if ($url->inheritance) {
      $parent = $this->parent;
      if($parent && $parent->inheritance_url) {
        $urlParent = $parent->url;
        if($urlParent) {
          $href = $urlParent->calculatedAddress . '/' . $url->address;
        }
        else {
          $href = '/' . $url->address;
        }
      }
      else {
        $href = '/' . $url->address;
      }
    }
    else {
      $href = '/' . $url->address;
    }
    return normalizeHref($href);
  }

  public function getActiveUrl() {
    $href = $this->calculatedAddress;
    if($href && $href!='/') {
      $href .= '.html';
    }
    return $href;
  }

  static public function switchStatus($id) {
    Yii::app()->db->createCommand('CALL SwitchPageStatus(:id);')->bindValue(':id',$id)->query();
  }

  static public function countRootNodes() {
    $row = Yii::app()->db->createCommand('SELECT COUNT(id) AS cnt FROM `pages` WHERE depth = 0')->queryRow();
    return intval($row['cnt']);
  }

  public function canGoUp() {
    if($this->position>1) return true;
    return false;
  }

  public function canGoDown() {
    if($this->parent_id) {
      if($this->position<$this->parent->childCount) return true;
    }
    else {
      if($this->position<self::countRootNodes()) return true;
    }
    return false;
  }

  public function moveUp() {
    if($this->canGoUp()) {
      Yii::app()->db->createCommand('CALL PAGE_ORDER(:id,:newPosition);')->bindValues(array(':id'=>$this->id,':newPosition'=>($this->position-1)))->query();
    }
  }

  public function moveDown() {
    if($this->canGoDown()) {
      Yii::app()->db->createCommand('CALL PAGE_ORDER(:id,:newPosition);')->bindValues(array(':id'=>$this->id,':newPosition'=>($this->position+1)))->query();
    }
  }

  public function pickUpProduct($productId) {
    $productPackege = ProductPackege::model()->findByPk(array('product_id'=>$productId,'packege_id'=>$this->id));
    if($productPackege) {
      $this->productPackege = $productPackege;
      return true;
    }
    else {
      $this->productPackege = null;
      //unset($this->productPackege);
      return false;
    }
  }

  public function pickUpPackege($packegeId) {
    $productPackege = ProductPackege::model()->findByPk(array('product_id'=>$this->id,'packege_id'=>$packegeId));
    if($productPackege) {
      $this->productPackege = $productPackege;
      return true;
    }
    else {
      $this->productPackege = null;
      return false;
    }
  }

  public function isPackegePage() {
    if($this->id==Yii::app()->params['packegesPageId']) return true;
    return false;
  }

  public function pageOrder($newPosition) {
    Yii::app()->db->createCommand('CALL PAGE_ORDER(:id,:newPosition);')->bindValues(array(':id' => $this->id, ':newPosition' => ($newPosition)))->query();
  }

}