<?php

class Settlement extends CActiveRecord {

  public $date_from;
  public $date_to;
  public $summary;
  public $byMonth;

  public static function model($className = __CLASS__) {
    return parent::model($className);
  }

  public function tableName() {
    return 'settlements';
  }

  public function rules() {
    return array(
        array('user_id', 'required'),
        array('title,note', 'safe'),
        array('title,note', 'default', 'value' => null),
        array('date', 'date', 'format' => 'yyyy-M-d H:m:s'),
        array('value', 'match', 'pattern' => '/^[0-9]{1,3}(\.[0-9]{0,2})?$/', 'allowEmpty' => true),
        array('value', 'default', 'value' => 0.0),
        array('id, date_from, date_to, byMonth', 'safe', 'on' => 'search')
    );
  }

  public function relations() {
    return array(
        'user' => array(self::BELONGS_TO, 'User', 'user_id'),
    );
  }

  public function attributeLabels() {
    return array(
        'user_id' => 'Użytkownik',
        'title' => 'Tytuł',
        'note' => 'Notatka',
        'date' => 'Data',
        'value' => 'Należność',
        'user.name'=>'Imię i nazwisko',
        'user.fullName'=>'Imię i nazwisko',
        'user.emailAndPhone'=>'Email i telefon',
        'formattedValue'=>'Należność'
    );
  }

  public function search($user = false, $withDate = true) {
    $criteria = new CDbCriteria;
    $criteria->with = array('user');
    $criteria->compare('`t`.`id`', $this->id);
    $criteria->compare('`t`.`value`', $this->value);
    $criteria->compare('`t`.`title`', $this->title, true);
    $criteria->compare('`t`.`note`', $this->note, true);

    if ($this->user_id) {
      $criteria->compare('user_id', $this->user_id);
    } elseif ($user) {
      $criteria->compare('user.email', $user->email, true);
      if ($user->name) {
        $criteria->addcondition("(user.name LIKE '%" . $user->name . "%' OR user.surname LIKE '%" . $user->name . "%')");
      }
    }

    if ($withDate) {
      if ($this->byMonth) {
        $date = strtotime($this->byMonth);
        $criteria->addBetweenCondition('`date`', date('Y-m-d', $date), date('Y-m-t', $date));
      } else {
        if ($this->date_from) {
          $criteria->addCondition('`date`>=:date_from');
          $criteria->params += array(':date_from' => $this->date_from);
        }
        if ($this->date_to) {
          $criteria->addCondition('`date`<=:date_to');
          $criteria->params += array(':date_to' => $this->date_to);
        }
      }
    }
    return new CActiveDataProvider($this, array(
                'criteria' => $criteria,
                'pagination' => array(
                    'pageSize' => Yii::app()->user->getState('pageSize', Yii::app()->params['defaultPageSize']),
                ),
                'sort' => array(
                    'defaultOrder' => 'date DESC',
                ),
            ));
  }

  public function getSum($user = false) {
    $criteria = $this->search($user);
    $criteria->pagination = false;
    $criteria->criteria->select = 'SUM(value) as summary';
    $data = $criteria->data;
    if ($data)
      return $criteria->data[0]->summary;
    else
      return 0;
  }

  public function getFirstDate($user) {
    $criteria = $this->search($user, false);
    $criteria->pagination = array('pageSize' => 1);
    $criteria->sort = array('defaultOrder' => 'date ASC');
    $data = $criteria->data;
    if ($data)
      return $criteria->data[0]->date;
    else
      return false;
  }

  public function getLastDate($user) {
    $criteria = $this->search($user, false);
    $criteria->pagination = array('pageSize' => 1);
    $criteria->sort = array('defaultOrder' => 'date DESC');
    $data = $criteria->data;
    if ($data)
      return $criteria->data[0]->date;
    else
      return false;
  }

  public function buildByMonthSelect($user) {
    $dateMin = $this->getFirstDate($user);
    $dateMax = $this->getLastDate($user);
    if ($dateMin && $dateMax) {
      $options = array();
      $dateMin = strtotime(date('Y-m', strtotime($dateMin)));
      $dateMax = strtotime(date('Y-m', strtotime($dateMax)));

      for ($i = $dateMax; $i >= $dateMin; $i = strtotime("-1 Month", $i)) {
        $options += array(date('Y-m', $i) => getMonthsName($i) . ' ' . date('Y', $i));
      }
      return $options;
    }
    else
      return false;
  }

  public function getFormattedValue() {
    return number_format(floatval($this->value),2,',',' ').' zł';
  }

}