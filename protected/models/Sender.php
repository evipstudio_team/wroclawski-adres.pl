<?php

class Sender extends CActiveRecord {

  public static function model($className = __CLASS__) {
    return parent::model($className);
  }

  public function tableName() {
    return 'senders';
  }

  public function rules() {
    return array(
        array('email, name, login, password, host', 'required'),
        array('sender', 'safe'),
        array('sender', 'email', 'allowEmpty'=>true),
        array('email', 'email'),
    );
  }

  public function relations() {
    return array(
    );
  }

  public function attributeLabels() {
    return array(
        'id' => 'ID',
        'name' => 'Nazwa',
        'email' => 'E-mail',
        'login' => 'Nazwa użytkownika',
        'password' => 'Hasło użytkownika',
        'host' => 'Adres hosta',
        'sender' => 'Adres zwrotów wiadomości błędnych',
    );
  }

  public function search() {
    $criteria = new CDbCriteria;
    return new CActiveDataProvider($this, array(
                'criteria' => $criteria,
                'sort'=>array(
                    'defaultOrder'=>'name',
                ),
            ));
  }
}