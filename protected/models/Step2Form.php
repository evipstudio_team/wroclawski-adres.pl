<?php

class Step2Form extends CFormModel {

  public $passCode;
  public $savePassCode;

  public $file;
  public $saveFiles;


  public function rules() {
    return array(
        array('savePassCode, saveFiles, file, passCode', 'safe'),
        array('file','file', 'types'=>'jpg, gif, png, pdf, doc, docx', 'on'=>'saveFile'),
        array('passCode','required','on'=>'savePassCode'),
        array('passCode','passCodeValidation', 'on'=>'savePassCode'),
    );
  }

  public function passCodeValidation($attribute, $params) {
    $checkSumOfInt = preg_replace('/[^0-9]/', '', $this->passCode);
    if(!preg_match('/[0-9]{8,}/', $checkSumOfInt)) {
      $this->addError('passCode','Elektroniczny kod dostepu powinien składać się co najmniej z 8 cyfr.');
    }
  }

  public function attributeLabels() {
    return array(
        'passCode' => 'Elektroniczny kod dostepu',
        'file' => 'Dokument',
    );
  }

  public function beforeValidate() {
//    if(!$this->files && $this->saveFiles) {
//      $this->addError('files', "Musisz wskazać pliki dokumentów.");
//    }
//    elseif(!$this->passCode && $this->savePassCode) {
//      $this->addError('passCode', "Musisz podać elektroniczny kod dostępu do akt.");
//    }
    if($this->saveFiles) {
      $this->setScenario('saveFile');
    }
    elseif($this->savePassCode) {
      $this->setScenario('savePassCode');
    }

    return parent::beforeValidate();
  }

  public function serve($causeId, $userId) {
    $commit = true;
    $transaction = Yii::app()->db->beginTransaction();
    if($this->scenario=='savePassCode') {
      $cause = Cause::model()->findByPk($causeId);
      $cause->pass_code = $this->passCode;
      if(!$cause->save()) $commit = false;
    }
    elseif($this->scenario=='saveFile') {
      $causeFile = new CauseFile();
      $causeFile->file = $this->file;
      $causeFile->cause_id = $causeId;
      $causeFile->created_by = $userId;
      if(!$causeFile->save()) $commit = false;
    }
//    if($saved) {
////      $cause = Cause::model()->findByPk($causeId);
////      $lastComment = $cause->lastComment;
//////      $lastComment->created_by = $userId;
////      if($this->scenario=='saveFile') {
////        $lastComment->comment = 'Użytkownik wysłał plik dokumentu.';
////      }
////      elseif($this->scenario=='savePassCode') {
////        $lastComment->comment = 'Użytkownik wysłał elektroniczny kod dostepu.';
////      }
////      if(!$lastComment->save()) $commit = false;
//    }
    if ($commit) {
      $transaction->commit();
    }
    else {
      $transaction->rollback();
    }
    return $commit;
  }

}
