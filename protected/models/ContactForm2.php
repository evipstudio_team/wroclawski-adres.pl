<?php

class ContactForm2 extends CFormModel {

  public $subject;
  public $comment;
  public $file;

  public function rules() {
    return array(
        array('subject', 'required'),
        array('comment', 'safe'),
        array('file', 'file', 'types' => 'jpg, gif, png, pdf, doc, docx, xls, xlsx', 'allowEmpty' => true),
    );
  }

  public function attributeLabels() {
    return array(
        'subject' => 'Temat',
        'comment' => 'Wiadomość',
        'file' => 'Załącznik',
    );
  }

  public function beforeValidate() {
    $this->file = CUploadedFile::getInstance($this,'file');
    return parent::beforeValidate();
  }

  public function send() {
    $user = User::model()->findByPk(Yii::app()->user->id);
    $params = array(
        'subject'=>'Wiadomość od klienta '.$user->company_name.' '.$user->fullName,
        'from'=>array(
            'name'=>$user->company_name.' '.$user->fullName,
            'email'=>$user->email
        ),
        'varables'=>array(
            $this->getAttributeLabel('subject') =>$this->subject,
            $this->getAttributeLabel('comment') =>$this->comment,
        )
    );
    if($this->file) {
      $params['attachments'] = array(
          array(
              'name'=>$this->file->name,
              'path'=>$this->file->tempName,
              'type'=>$this->file->type
          )
      );
    }
    return Email::ContactEmail($params);
  }

}
