<?php

/*
 *

  I have two part of application. The first is a manager panel and it is pointed to http://mysite.com/. Other is a personal product page and they are pointed to own domein http://product_domain.com/ . These are parts have two different router maps but common models, components and widgets. The list of all product domain I have saved in database.

  What the best way to separate this parts?

  P.S. Maybe i should have two different application or config?



 * What we usually do is use a combination of a "prerouter" and modile.

  Yii supports this by way of an "onbeginrequest" => array('Class', 'function') in your config.

  In that function you can detect where the user came from and make the needed changes. For example activate a specific module (if all the functionality is grouped in one), or set a different defaultController. Perhaps you want to load a custom theme?

  You can do it all in there before anything is done.

 */

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');
// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
require_once( dirname(__FILE__) . '/../components/helpers.php');
require_once( 'dbaccess.php');
return array(
//    'defaultController' => 'show',
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
//    'baseUrl' => 'cms.local',
    'name' => stripFromWww($_SERVER['HTTP_HOST']),
    // preloading 'log' component
    'preload' => array('log'),
    // autoloading model and component classes
    'import' => array(
        'application.models.*',
        'application.components.*',
    ),
    'theme' => 'panel',
    'modules' => array(
        // uncomment the following to enable the Gii tool
        //'gii' => array(
        //    'class' => 'system.gii.GiiModule',
        //    'password' => 'alkatrass',
        // If removed, Gii defaults to localhost only. Edit carefully to taste.
        //    'ipFilters' => array('127.0.0.1', '::1'),
        //),
        'rbam' => array(
            'development' => TRUE,
//            'initialise'=>array(
//                'class'=>'CDbAuthManager',
//                'connectionID'=>'db',
//            ),
            'userClass' => 'User',
            'userCriteria' => array(),
            'userIdAttribute' => 'id',
            'userNameAttribute' => array(' ', 'name', 'email'),
            'authenticatedRole'=>'Authenticated',
        //'userNameAttribute' => ' ,name,name,.email',
        // RBAM Configuration
        ),
    ),
    'behaviors' => array('ApplicationConfigBehavior'),
    // application components
    'components' => array(
        'clientScript' => array(
          'class' => 'NLSClientScript',
          //'excludePattern' => '/\.tpl/i', //js regexp, files with matching paths won't be filtered is set to other than 'null'
          //'includePattern' => '/\.php/' //js regexp, only files with matching paths will be filtered if set to other than 'null'
        ),
        'user' => array(
            // enable cookie-based authentication
            'allowAutoLogin' => false,
            'loginUrl' => array('site/login'),
        ),
        'ZHtml' => 'ZHtml',
        'urlManager' => array(
            'showScriptName' => false,
            'urlFormat' => 'path',
            'rules' => array(
                array('show/index', 'pattern' => ''),
                array('site/login', 'pattern' => 'login'),
                array('site/index', 'pattern' => 'admin'),
                array('site/logout', 'pattern' => 'logout'),
//                array('show/step2', 'pattern' => 'krok-2.html'),
                array('min/serve','pattern'=>'min/<g:\w+>/<lm:\d+>/'),
                array('show/login', 'pattern' => 'zaloguj.html'),
                array('show/resetPass', 'pattern' => 'resetuj-haslo.html'),
                array('show/step2', 'pattern' => '<causeId:[0-9]+>/krok-2.html'),
                array('clientPanel/GetFile', 'pattern' => 'pobierz-dokument/<hash1:.*>/<hash2:.*>'),
                array('show/packeges', 'pattern' => 'Cennik.html'),
                array('show/homeByUrl', 'pattern' => '[^/]+/<address:.*>.html'),
                array('show/homeByUrl', 'pattern' => '<address:.*>.html'),
            ),
        ),
        // uncomment the following to enable URLs in path-format
        /*
          'urlManager'=>array(
          'urlFormat'=>'path',
          'rules'=>array(
          '<controller:\w+>/<id:\d+>'=>'<controller>/view',
          '<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
          '<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
          ),
          ),
         */
//		'db'=>array(
//			'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
//		),
        // uncomment the following to use a MySQL database
//        'cache'=>array(
//            'class'=>'CDbCache',
//            'connectionID'=>'db'
//        ),
        'db' => dbaccess(),
        'authManager' => array(
            'class' => 'CDbAuthManager',
            'connectionID' => 'db',
            'defaultRoles' => array('Guest')
        ),
        'errorHandler' => array(
            // use 'site/error' action to display errors
            'errorAction' => 'show/error',
        ),
        'log' => array(
            'class' => 'CLogRouter',
            'routes' => array(
                array(
                    'class' => 'CFileLogRoute',
                    'levels' => 'error, warning',
                ),
            // uncomment the following to show log messages on web pages
            /*
              array(
              'class'=>'CWebLogRoute',
              ),
             */
            ),
        ),
    ),
    // application-level parameters that can be accessed
    // using Yii::app()->params['paramName']
    'params' => array(
        // this is used in contact page
        'adminEmail' => 'webmaster@example.com',
        'blocks' => array(
            1 => array('id' => 23), // górne menu
            2 => array('id' => 31), // artykuł na stronie głównej
            3 => array('id' => 32), // Szare bloki strony głównej
            4 => array('id' => 33), // slider
        ),
        'smtp' => array(
            'default' => array(
                'host' => 'mail.evipstudio.pl',
                'username' => 'patrycjusz@evipstudio.pl',
                'port' => '587',
                'password' => 'fQcbn{vVQ81A',
                'from' => array('email' => 'patrycjusz@evipstudio.pl', 'name' => 'Patrycjusz Ożlanski'),
                'SMTPSecure' => false,
            )
        )
    ),
);
