<?php

require_once( dirname(__FILE__) . '/../components/helpers.php');
require_once( 'dbaccess.php');
return array(
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name' => 'App',
    'import' => array(
        'application.models.*',
        'application.components.*',
    ),
    'components' => array(
        'db' => dbaccess(),
        'request' => array(
            'hostInfo' => 'wroclawski-adres.pl',
            'baseUrl' => '',
            'scriptUrl' => '',
        ),
        'urlManager' => array(
            'showScriptName' => false,
            'urlFormat' => 'path',
            'rules' => array(
                array('show/index', 'pattern' => ''),
                array('site/login', 'pattern' => 'login'),
                array('site/index', 'pattern' => 'admin'),
                array('site/logout', 'pattern' => 'logout'),
                array('show/homeByUrl', 'pattern' => '^<address:[a-zA-Z0-9-]+>.html'),
                array('show/homeByUrl', 'pattern' => '[a-zA-Z0-9-]+/<address:[a-zA-Z0-9-]+>.html'),
            ),
        ),
    ),
    'params' => array(
        'adminEmail' => 'webmaster@example.com',
        'smtp' => array(
            'default' => array(
                'host' => 'mail.evipstudio.pl',
                'username' => 'patrycjusz@evipstudio.pl',
                'port' => '587',
                'password' => 'fQcbn{vVQ81A',
                'from' => array('email' => 'patrycjusz@evipstudio.pl', 'name' => 'Patrycjusz Ożlanski'),
                'SMTPSecure' => false,
            )
        )
    ),
);
