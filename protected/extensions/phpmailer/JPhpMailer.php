<?php

/**
 * JPhpMailer class file.
 *
 * @version alpha 2 (2010-6-3 16:42)
 * @author jerry2801 <jerry2801@gmail.com>
 * @required PHPMailer v5.1
 *
 * A typical usage of JPhpMailer is as follows:
 * <pre>
 * Yii::import('ext.phpmailer.JPhpMailer');
 * $mail=new JPhpMailer;
 * $mail->IsSMTP();
 * $mail->Host='smpt.163.com';
 * $mail->SMTPAuth=true;
 * $mail->Username='yourname@yourdomain';
 * $mail->Password='yourpassword';
 * $mail->SetFrom('name@yourdomain.com','First Last');
 * $mail->Subject='PHPMailer Test Subject via smtp, basic with authentication';
 * $mail->AltBody='To view the message, please use an HTML compatible email viewer!';
 * $mail->MsgHTML('<h1>JUST A TEST!</h1>');
 * $mail->AddAddress('whoto@otherdomain.com','John Doe');
 * $mail->Send();
 * </pre>
 */
//require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . 'class.phpmailer.php';

require_once __DIR__ . '/../phpmailer2/src/PHPMailer.php';
require_once __DIR__ . '/../phpmailer2/src/SMTP.php';
require_once __DIR__ . '/../phpmailer2/src/Exception.php';

class JPhpMailer extends PHPMailer\PHPMailer\PHPMailer {

  public function imgToCid() {
    $string = $this->Body;
    if (preg_match_all('/<img.*(src="([^"]+)")[^>]+>/', $string, $results)) {
      $images = $results[2];
      foreach ($images as $index=>$image) {
        $imageParsed = parse_url($image);
        $fullPath = Yii::app()->basePath.'/..'.$imageParsed['path'];
        if(file_exists($fullPath)) {
          $cid = md5($fullPath.time());
          $string = str_replace($results[1][$index], 'src="cid:' . $cid . '"', $string);
          $this->AddEmbeddedImage($fullPath, $cid, '', 'base64', getMimeType($fullPath));
        }

      }
    }
    $this->Body = $string;
  }

    public function getTo() {
        $to = array();
        foreach($this->to as $to_array) {
            array_push($to, $to_array[0]);
        }
        return implode(';', $to);
    }

}