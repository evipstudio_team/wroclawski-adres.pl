<div id="file-uploader"></div>
<?php
$filesPath = User::getUserFilesFullPath(Yii::app()->user->id);
$filesUrl = 'http://'.Yii::app()->name.User::getUserFilesPath(Yii::app()->user->id);

$this->widget("ext.ezzeelfinder.ElFinderWidget", array(
    'selector' => "div#file-uploader",
    'clientOptions' => array(
        'lang' => "pl",
        'resizable' => false,
        'wysiwyg' => "ckeditor"
    ),
    'connectorRoute' => "site/fileUploaderConnector",
    'connectorOptions' => array(
        'roots' => array(
            array(
                'driver' => "LocalFileSystem",
                'path' => $filesPath,
                'URL' => $filesUrl,
                'tmbPath' => $filesPath . DIRECTORY_SEPARATOR . ".thumbs",
                'mimeDetect' => "internal",
                'accessControl' => "access"
            )
        )
    )
));
?>