<?Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/helpers.js');?>
<?php
$this->breadcrumbs=array(
	Yii::t('cms', 'Rezerwacje')=>$this->createUrl('reservations/index'),
  Yii::t('cms', 'Lista produktów')
);
?>
<div class="form">
  <div class="row buttons">
    <input type="button" value="Dodaj nową salę/biurko" onclick="$('#DialogBox').data('url', '<?=$this->createUrl('reservation/createRoom')?>');$('#DialogBox').dialog('open');" />
  </div>
</div>
<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'rooms-grid',
    'dataProvider' => $room->search(),
    'summaryText' => summaryTextLayout('rooms-grid'),
    'rowCssClassExpression'=>'"item_{$data->id}"',
    'afterAjaxUpdate'=>'js: function() {applyEvent()}',
    'filter' => $room,
    'enableSorting'=>false,
    'columns' => array(
        array(
          'class' => 'CCheckBoxColumn',
          'header' => 'Akcja',
          'selectableRows'=>2,
          'headerTemplate'=>'{item}',
          'value'=>'$data->id',
          'footer'=>'
            <select onchange="if($(this).val()==\'delete\'){gridDeleteElements([\'rooms-grid\'], \'rooms-grid_c0[]\',\''.$this->createUrl('reservation/deleteRoomsByIds').'\');}">
              <option></option>
              <option value="delete">Usuń</option>
            </select>
          ',
          'htmlOptions'=>array('class'=>'center'),
        ),
        array(
            'header'=>'Typ',
            'value'=>'$data->type'
        ),
        array(
            'name'=>'name',
        ),
        array(
            'header'=>'Należy do',
            'value'=>'($data->parent_id)? $data->parent->name:\'\''
        ),
        array(
            'name'=>'cost_per_hour',
            'value'=>'number_format($data->cost_per_hour,\'2\',\',\',\' \').\' zł\''
        ),
        array(
            'name'=>'cost_per_day',
            'value'=>'number_format($data->cost_per_day,\'2\',\',\',\' \').\' zł\''
        ),
        array(
            'class' => 'CButtonColumn',
            'header' => 'Akcja',
            'template'=>'{update}&nbsp;&nbsp;&nbsp;{delete}',
            'updateButtonUrl'=>'Yii::app()->createUrl(\'reservation/editRoom\',array(\'id\'=>$data->id))',
            'deleteButtonUrl'=>'Yii::app()->createUrl(\'reservation/deleteRoom\',array(\'id\'=>$data->id))',
            'deleteButtonOptions'=>array('class'=>'delete'),
            'updateButtonOptions'=>array('class'=>'edit'),
        ),


    ),
));
?>
<?$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id'=>'DialogBox',
    'options'=>array(
        'title'=>  '',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>'500',
        'height'=>'400',
        'open' => 'js:function(){
          $(this).html($(\'#LoadingDialogBox\').html());
            jQuery.ajax({
              "url": $(this).data(\'url\'),
              "success":function(data){
                $(\'#DialogBox\').html($(data));
              }
            });
          return false;}',
        'close'=>'js:function(){
          $.fn.yiiGridView.update(\'rooms-grid\');
          $(this).html($(\'#LoadingDialogBox\').html());
        }',
    ),
));?>
<?$this->endWidget('zii.widgets.jui.CJuiDialog');?>
<script type="text/javascript">
  function applyEvent() {
    $('.edit').click(function() {
      var id = $(this).parent().parent().attr('class').replace(/[^0-9]+/g,'');
      $('#DialogBox').data('url', '<?=$this->createUrl('reservation/editRoom')?>/id/'+id);
      $('#DialogBox').dialog('open');
      return false;
    });
  }
  $(function() {
    applyEvent();
  });
</script>