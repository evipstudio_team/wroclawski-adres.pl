<div class="form">
  <?php $form = $this->beginWidget('CActiveForm', array('action' => $action, 'id' => 'roomForm')); ?>
  <?= $form->errorSummary($model); ?>

  <div class="row">
    <?= $form->labelEx($model, 'parent_id'); ?>
    <label>(Pozostaw puste jeśli sala)</label>
    <?= $form->dropDownList($model, 'parent_id',array(''=>'')+CHtml::listData(Room::model()->findAll('`parent_id` IS NULL'), 'id', 'name')) ?>
  </div>

  <div class="row">
    <?= $form->labelEx($model, 'name'); ?>
    <label>(Nazwa jest widoczna użytkownikom i służy do identyfikacji sali/biurka)</label>
    <?= $form->textField($model, 'name') ?>
  </div>

  <div class="row">
    <?= $form->labelEx($model, 'cost_per_hour'); ?>
    <?= $form->textField($model, 'cost_per_hour') ?>
  </div>

  <div class="row">
    <?= $form->labelEx($model, 'cost_per_day'); ?>
    <?= $form->textField($model, 'cost_per_day') ?>
  </div>
<?php $this->endWidget(); ?>
</div>
<script type="text/javascript">
  $(function() {
    $('#Room_cost_per_hour').change(function() {
      $(this).val(validateCostPerDay($(this).val()));
    });
    $('#Room_cost_per_day').change(function() {
      $(this).val(validateCostPerDay($(this).val()));
    });

    $( "#DialogBox" ).dialog( "option", "buttons", [
      {
        text: "Zamknij",
        click: function() {
          $( this ).dialog( "close" );
        }
      },
      {
        text: "Zapisz",
        click: function() {
          $('#LoadingDialogBox').dialog('open');
          $.ajax({
            url: $('#roomForm').attr('action'),
            type: 'post',
            data: $('#roomForm').serializeArray(),
            success: function(data) {
              $( "#DialogBox" ).html(data);
              $('#LoadingDialogBox').dialog('close');
            }
          });
        }
      }
    ]);
  });

</script>