<div class="row">
  <?= CHtml::activeLabelEx(new Reservation, 'room_id')?>
  <?if($rooms):?>
    <?=CHtml::activeDropDownList(new Reservation, 'room_id', CHtml::listData($rooms,'id','name'))?>
  <?else:?>
    <p style="color: red">
      Przepraszamy, w wybranym okresie wszystkie obiekty są zajęte.
    </p>
  <?endif?>
</div>