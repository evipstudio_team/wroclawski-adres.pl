<?Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/helpers.js');?>
<?php
$this->breadcrumbs=array(
	Yii::t('cms', 'Rezerwacje')=>$this->createUrl('reservations/index'),
  Yii::t('cms', 'Tabela rezerwacji')
);
?>
<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'reservations-grid',
    'dataProvider' => $reservation->search($user),
    'summaryText' => summaryTextLayout('reservations-grid'),
    'rowCssClassExpression'=>'"item_{$data->id}"',
    'filter' => $reservation,
    'enableSorting'=>false,
    'columns' => array(
        array(
          'class' => 'CCheckBoxColumn',
          'header' => 'Akcja',
          'selectableRows'=>2,
          'headerTemplate'=>'{item}',
          'value'=>'$data->id',
          'footer'=>'
            <select onchange="if($(this).val()==\'delete\'){gridDeleteElements([\'reservations-grid\'], \'reservations-grid_c0[]\',\''.$this->createUrl('reservation/deleteByIds').'\');}">
              <option></option>
              <option value="delete">Usuń</option>
            </select>
          ',
          'htmlOptions'=>array('class'=>'center'),
        ),
        array(
            'name'=>'user.email',
            'filter'=>CHtml::activeTextField($user,'email')
        ),
        array(
            'name'=>'user.company_name',
            'filter'=>CHtml::activeTextField($user,'company_name')
        ),
        array(
            'name'=>'user.fullName',
            'filter'=>CHtml::activeTextField($user,'name')
        ),
        array(
            'name'=>'date_from_helper',
            'value'=>'date(\'d/m/Y H:i\',strtotime($data->date_from_helper))',
            'htmlOptions'=>array('class'=>'center'),
            'filter'=>false
        ),
        array(
            'name'=>'date_to_helper',
            'value'=>'date(\'d/m/Y H:i\',strtotime($data->date_to_helper))',
            'htmlOptions'=>array('class'=>'center'),
            'filter'=>false
        ),
        array(
            'name'=>'type',
            'value'=>'$data->TranslateEnumValue(\'type\')',
            'filter'=>ZHtml::enumItem($reservation,'type')
        ),
        array(
            'name'=>'room_id',
            'value'=>'$data->room->name'
        ),

    ),
));
?>
<input type="hidden" name="selectedRow" id="selectedRow" value="<?= $select?>" onchange="selectRow($(this).val())" />

<script type="text/javascript">
$(function() {
  $('#selectedRow').change();
});
</script>