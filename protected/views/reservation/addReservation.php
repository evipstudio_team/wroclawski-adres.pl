<?php
$this->breadcrumbs=array(
	Yii::t('cms', 'Rezerwacje')=>$this->createUrl('reservations/index'),
  Yii::t('cms', 'Kalendarz rezerwacji')
);
?>
<?
  $baseUrl = Yii::app()->baseUrl;
  $cs = Yii::app()->getClientScript();
  $cs->registerScriptFile($baseUrl.'/js/fullcalendar/fullcalendar.js');
  $cs->registerCssFile($baseUrl.'/js/fullcalendar/fullcalendarPanel.css');
  $cs->registerCoreScript('jquery');
  $cs->registerCoreScript('jquery.ui');
?>
<div class="form">
  <div class="row">
    <div style="float: left">
      <label>Wskaż typ rezerwacji</label>
      <?= CHtml::activeDropDownList($reservation, 'type', array(''=>'')+ZHtml::enumItem($reservation, 'type'), array('id'=>'ReservationTypeListChoose'))?>
    </div>
    <div style="float: left; margin-left: 20px">
      <?if($rooms):?>
        <label>Wskaż interesującą Ciebie salę/biuro</label>
        <?= CHtml::activeDropDownList($reservation, 'room_id', array(''=>'')+CHtml::listData($rooms, 'id', 'name'), array('id'=>'ReservationRoomListChoose'))?>
      <?endif?>
    </div>
    <div style="clear: both"></div>
  </div>
</div>
<script type='text/javascript'>
  $(function() {
    $('#ReservationTypeListChoose').change(function() {
      $('#LoadingDialogBox').dialog('open');
      $.ajax({
        url: '<?= $this->createUrl('reservation/addReservation',array('ajax'=>true)) ?>/type/'+$(this).val(),
        type: 'get',
        success: function(data) {
          $('#content table.wrap_table tr td.right').html(data);
          $('#LoadingDialogBox').dialog('close');
        }
      });
    });
    $('#ReservationRoomListChoose').change(function() {
      $('#LoadingDialogBox').dialog('open');
      $.ajax({
        url: '<?= $this->createUrl('reservation/addReservation',array('ajax'=>true)) ?>/type/'+$('#ReservationTypeListChoose').val()+'/room_id/'+$(this).val(),
        type: 'get',
        success: function(data) {
          $('#content table.wrap_table tr td.right').html(data);
          $('#LoadingDialogBox').dialog('close');
        }
      });
    });
  });

  function refreshCalendar() {
    $('#LoadingDialogBox').dialog('open');
    var type = $('#ReservationTypeListChoose').val();
    var room_id = ($('#ReservationRoomListChoose').length)? $('#ReservationRoomListChoose').val():0;
    $.ajax({
      url: '<?= $this->createUrl('reservation/addReservation',array('ajax'=>true)) ?>/type/'+type+'/room_id/'+room_id,
      type: 'get',
      success: function(data) {
        $('#content table.wrap_table tr td.right').html(data);
        $('#LoadingDialogBox').dialog('close');
      }
    });
  }

	$(document).ready(function() {

		var date = new Date();
		var d = date.getDate();
		var m = date.getMonth();
		var y = date.getFullYear();

		var calendar = $('#calendar').fullCalendar({
      theme: true,
			header: {
				left: 'prev,next today',
				center: 'title',
				right: 'month,agendaWeek,agendaDay'
			},
      defaultView: 'agendaWeek',
      columnFormat: {
        month: 'ddd',    // Mon
        week: 'ddd d MMM', // Mon 9/7
        day: 'dddd d MMM'  // Monday 9/7
      },
      timeFormat: {
        agenda: 'H:mm{ - H:mm}',
        month: 'H(:mm){ - H:mm}'
      },
      firstDay: 1,
			selectable: true,
			selectHelper: true,
			select: function(start, end, allDay) {
        date_from = start.getTime();
        date_to = end.getTime();

        $('#DialogBox').data('url','<?=$this->createUrl('reservation/createReservation',array('ajax'=>true))?>?Reservation[room_id]='+$('#ReservationRoomListChoose').val()+'&'+'Reservation[type]='+$('#ReservationTypeListChoose').val()+'&'+'Reservation[date_from]='+date_from+'&'+'Reservation[date_to]='+date_to+'&'+'Reservation[allDay]='+(allDay? 1:0));
        $('#DialogBox').dialog('open');
				calendar.fullCalendar('unselect');
			},
			editable: true,
      axisFormat: 'H:mm',
      minTime: <?= intval(array_shift(explode(':', Yii::app()->params['AvailabilityFrom'])))?>,
      maxTime: <?= intval(array_shift(explode(':', Yii::app()->params['AvailabilityTo'])))?>,
      disableResizing: true,
      eventRender: function(event, element) {
        if(!event.allDay) {
          if(event.end-event.start>1800000) {
            var hour = event.start.getHours();
            var minutes = event.start.getMinutes();
            if(minutes<10) minutes = '0'+minutes;
            $(element).find('.fc-event-time:first').html(hour+':'+minutes);
            var hour = event.end.getHours();
            var minutes = event.end.getMinutes();
            if(minutes<10) minutes = '0'+minutes;
            $(element).children().append('<div class="fc-event-bottom fc-event-skin"><div class="fc-event-time">'+hour+':'+minutes+'</div></div>');
          }
        }


    },
			events: <?=  json_encode(Reservation::reservationsToJson($reservations)) ?>,
      eventClick: function(calEvent, jsEvent, view) {
        $('#DialogBox').data('url','<?=$this->createUrl('reservation/editReservation')?>/id/'+calEvent.id);
        $('#DialogBox').dialog('open');
      },
      eventDrop: function(event,dayDelta,minuteDelta,allDay,revertFunc) {
        $('#DialogBox').data('url','<?=$this->createUrl('reservation/editFromMoveReservation')?>/id/'+event.id+'?Reservation[date_from]='+event.start.getTime()+'&'+'Reservation[date_to]='+((event.end!=null)? event.end.getTime():event.start.getTime())+'&'+'Reservation[allDay]='+(allDay? 1:0));
        $('#DialogBox').dialog('open');
      }
		});
	});

</script>
<div id='calendar' style=""></div>
<?$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id'=>'DialogBox',
    'options'=>array(
        'title'=>  '',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>'600',
        'height'=>'700',
        'open' => 'js:function(){
          $(this).html($(\'#LoadingDialogBox\').html());
            jQuery.ajax({
              "url": $(this).data(\'url\'),
              "success":function(data){
                $(\'#DialogBox\').html($(data));
              }
            });
          return false;}',
        'close'=>'js:function(){
          $(this).html($(\'#LoadingDialogBox\').html());
        }',
    ),
));?>
<?$this->endWidget('zii.widgets.jui.CJuiDialog');?>