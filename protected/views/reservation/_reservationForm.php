<div class="form resrv">
  <?php $form = $this->beginWidget('CActiveForm', array('action' => $action, 'id' => 'reservationForm', 'htmlOptions' => array('enctype' => 'multipart/form-data'))); ?>
  <?= $form->errorSummary($model); ?>

  <?if(Yii::app()->user->checkAccess('CmsUser')):?>
    <div class="row">
      <?= $form->labelEx($model, 'user_id'); ?>
      <?= $form->dropDownList($model, 'user_id', array('' => '') + CHtml::listData(User::model()->findAll(array('condition' => '`role`=\'client\' AND status=\'active\'', 'order' => 'surname')), 'id', 'fullName')) ?>
    </div>
  <?endif?>

  <div class="row">
    <?= $form->labelEx($model, 'type'); ?>
    <?= $form->dropDownList($model, 'type', array('' => '') + ZHtml::enumItem($model, 'type')) ?>
  </div>

  <div class="row">
    <?= CHtml::activeLabelEx($model, 'date_from'); ?>
    <?
    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
        'model' => $model,
        'attribute' => 'date_from',
        'language'=>Yii::app()->language,
//        'options'=>array('dateFormat'=>'dd/mm/yy'),
        'options'=>array('dateFormat'=>'yy-mm-dd', 'minDate'=>1),
//        'htmlOptions' => array('size' => 10, 'maxlength' => 10, 'value'=>$model->formatDate('date_from')),
        'htmlOptions' => array('size' => 10, 'maxlength' => 10, 'onChange'=>'loadRooms()'),
    ));
    ?>
    <?
    $this->widget('application.extensions.jui_timepicker.JTimePicker', array(
        'model' => $model,
        'attribute' => 'time_from',
        'options' => array(
            'rows'=>1,
            'hourText'=>'Godzina',
            'minuteText'=>'Minuta',
            'showPeriod' => false,
            'showPeriodLabels'=>false,
            'hours'=> array('starts'=>intval(array_shift(explode(':', Yii::app()->params['AvailabilityFrom']))), 'ends'=>intval(array_shift(explode(':', Yii::app()->params['AvailabilityTo'])))),
            'minutes'=> array('starts'=>0, 'ends'=>30, 'interval'=>30)
        ),
        'htmlOptions' => array('size' => 8, 'maxlength' => 8, 'value'=>$model->formatTime('time_from'), 'onChange'=>'loadRooms()'),
    ))
    ?>
    (cały dzień: <?= $form->checkBox($model, 'from_all_day') ?>)
  </div>

  <div class="row">
    <?= CHtml::activeLabelEx($model, 'date_to'); ?>
     <?
    $model->formatDate('date_to');
    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
        'model' => $model,
        'attribute' => 'date_to',
        'language'=>Yii::app()->language,
//        'options'=>array('dateFormat'=>'dd/mm/yy'),
        'options'=>array('dateFormat'=>'yy-mm-dd', 'minDate'=>1),
//        'htmlOptions' => array('size' => 10, 'maxlength' => 10, 'value'=>$model->formatDate('date_to')),
        'htmlOptions' => array('size' => 10, 'maxlength' => 10, 'onChange'=>'loadRooms()'),
    ));
    ?>
    <?
    $this->widget('application.extensions.jui_timepicker.JTimePicker', array(
        'model' => $model,
        'attribute' => 'time_to',
        'options' => array(
            'rows'=>1,
            'hourText'=>'Godzina',
            'minuteText'=>'Minuta',
            'showPeriod' => false,
            'showPeriodLabels'=>false,
            'hours'=> array('starts'=>intval(array_shift(explode(':', Yii::app()->params['AvailabilityFrom']))), 'ends'=>intval(array_shift(explode(':', Yii::app()->params['AvailabilityTo'])))),
            'minutes'=> array('starts'=>0, 'ends'=>30, 'interval'=>30)
        ),
        'htmlOptions' => array('size' => 8, 'maxlength' => 8, 'value'=>$model->formatTime('time_to'), 'onChange'=>'loadRooms()'),
    ))
    ?>
    (cały dzień: <?= $form->checkBox($model, 'to_all_day') ?>)
  </div>

  <div id="roomsPossibilites">

  </div>

  <div class="row">
<?= $form->labelEx($model, 'comment'); ?>
<?= $form->textArea($model, 'comment', array('rows' => 6, 'cols' => 30, 'style' => 'width: 98%')); ?>
  </div>
  *pola wymagane
<?php $this->endWidget(); ?>
</div>
<script type="text/javascript">
  function loadRooms() {
    if($('#Reservation_type').val() && $('#Reservation_date_from').val() && $('#Reservation_date_to').val()) {
      $('#LoadingDialogBox').dialog('open');
      $.ajax({
        url: '<?= $this->createUrl('reservation/ShowRoomPossibilites') ?>/type/'+$('#Reservation_type').val()+'/id/'+<?=(($model->id)? $model->id:0)?>+'/date_from/'+$('#Reservation_date_from').val()+'/time_from/'+$('#Reservation_time_from').val()+'/from_all_day/'+$('#Reservation_from_all_day').is(':checked')+'/date_to/'+$('#Reservation_date_to').val()+'/time_to/'+$('#Reservation_time_to').val()+'/to_all_day/'+$('#Reservation_to_all_day').is(':checked'),
        type: 'get',
        success: function(data) {
          $('#roomsPossibilites').html(data);
          $('#LoadingDialogBox').dialog('close');
        }
      });
    }
    else {
      $('#roomsPossibilites').html('');
    }
  }
  $(function() {
    $( "#DialogBox" ).dialog( "option", "width", '550');
    $( "#DialogBox" ).dialog( "option", "height", 'auto');
    $( "#DialogBox" ).dialog( "option", "position", 'center');
    $('#Reservation_type').change(function() {loadRooms()});
    $('#Reservation_date_from').change(function() {loadRooms()});
    $('#Reservation_date_to').change(function() {loadRooms()});

    loadRooms();

    $( "#DialogBox" ).dialog( "option", "buttons", [
      <?if(!$model->isNewRecord):?>
      {
        text: "Usuń",
        click: function() {
          if(confirm("Czy jesteś pewien że chcesz usunąć rezerwację?")) {
            $.ajax({
              url: '<?=$this->createUrl('reservation/delete',array('id'=>$model->id))?>',
              type: 'get',
              success: function(data) {
                refreshCalendar();
              }
            });
          }
          $( this ).dialog( "close" );
        }
      },
      <?endif?>
      {
        text: "Anuluj",
        click: function() {
          refreshCalendar();
          $( this ).dialog( "close" );
        }
      },
      {
        text: "<?=$model->saveButtonText()?>",
        click: function() {
          $('#LoadingDialogBox').dialog('open');
          $.ajax({
            url: $('#reservationForm').attr('action'),
            type: 'post',
            data: $('#reservationForm').serializeArray(),
            success: function(data) {
              $( "#DialogBox" ).html(data);
              $('#LoadingDialogBox').dialog('close');
            }
          });
        }
      }
    ]);
    var tmp_from;
    $('#Reservation_from_all_day').change(function() {
      if($(this).is(':checked') && !$('#Reservation_time_from').is(':disabled')) {
        $('#Reservation_time_from').attr('disabled','disabled');
        tmp_from = $('#Reservation_time_from').val();
        $('#Reservation_time_from').val('cały dzień');
      }
      else
        if($('#Reservation_time_from').is(':disabled')){
          $('#Reservation_time_from').attr('disabled',false);
          $('#Reservation_time_from').val(tmp_from);
        }
      if($('#Reservation_date_from').val() == $('#Reservation_date_to').val()) {
        if($(this).is(':checked') && !$('#Reservation_to_all_day').is(':checked')) {
          $('#Reservation_to_all_day').attr('checked',true);
          $('#Reservation_to_all_day').change();
        }
        else
          if(!$(this).is(':checked') && $('#Reservation_to_all_day').is(':checked')) {
            $('#Reservation_to_all_day').attr('checked',false);
            $('#Reservation_to_all_day').change();
          }
      }
    });
    var tmp_to;
    $('#Reservation_to_all_day').change(function() {
      if($(this).is(':checked') && !$('#Reservation_to_from').is(':disabled')) {
        $('#Reservation_time_to').attr('disabled','disabled');
        tmp_to = $('#Reservation_time_to').val();
        $('#Reservation_time_to').val('cały dzień');
      }
      else
        if($('#Reservation_time_to').is(':disabled')){
          $('#Reservation_time_to').attr('disabled',false);
          $('#Reservation_time_to').val(tmp_to);
        }
      if($('#Reservation_date_from').val() == $('#Reservation_date_to').val()) {
        if($(this).is(':checked') && !$('#Reservation_from_all_day').is(':checked')) {
          $('#Reservation_from_all_day').attr('checked',true);
          $('#Reservation_from_all_day').change();
        }
        else
          if(!$(this).is(':checked') && $('#Reservation_from_all_day').is(':checked')) {
            $('#Reservation_from_all_day').attr('checked',false);
            $('#Reservation_from_all_day').change();
          }
      }
    });
    $('#Reservation_from_all_day').change();
    $('#Reservation_to_all_day').change();
  });
</script>