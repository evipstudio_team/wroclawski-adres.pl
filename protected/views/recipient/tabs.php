<?$this->renderPartial('../shared/_breadcrumbs', array('page' => $page));?>
<?$this->breadcrumbs = array_merge(
        $this->breadcrumbs,
        array(Yii::t('cms', 'Panel zarządzania odbiorcami')=>$this->createUrl('recipient/index',array('page_id'=>$page->id))),
        array($this->pageTitle)
        );?>
<?
$this->widget('CTabView', array(
    'tabs' => $tabs,
    'cssFile' => Yii::app()->theme->baseUrl.'/css/jquery.yiitab.css',
    'activeTab'=>Yii::app()->getController()->action->id,
    'viewData'=>  isset($additionalParams)? array_merge($additionalParams,array('model'=>$model)):array('model'=>$model)
))
?>