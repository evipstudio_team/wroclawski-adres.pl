<?Yii::app()->getClientScript()->registerCoreScript( 'jquery.ui' );?>
<?Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/helpers.js');?>
<?
$str_js = "
installSortable('multimedia-grid','".$this->createUrl('//multimedia/sort')."');
function reInstallSortable(id, data) {
  installSortable('multimedia-grid','".$this->createUrl('//multimedia/sort')."');
}
";

Yii::app()->clientScript->registerScript('sortable-project', $str_js);
?>
<div id="uploadFormDiv">
  <form id="uploadForm" action="<?= CHtml::encode($this->createUrl('multimedia/upload', array('page_id' => $multimedia->page_id))) ?>" method="post" enctype="multipart/form-data" id="uploadForm">
    <div class="form">
        <span class="input_label"><?= Yii::t('cms', 'Wskaż plik lub pliki do zapisania')?></span>
        <input type="file" name="files[]" multiple="" />
        <input type="submit" value="Zapisz plik" />
        <input type="hidden" name="redirectAction" value="<?= $redirectAction?>" />
        <input type="hidden" name="redirectController" value="<?= $redirectController?>" />
    </div>
  </form>
</div>
<?$excludedFromDescription = CHtml::listData(PageRelations::getChildNodes(124), 'child_id', 'child_id')?>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'multimedia-grid',
	'dataProvider'=>$multimedia->search($withChilds),
  'enableSorting'=>false,
  'rowCssClassExpression'=>'"items_{$data->id}"',
  'afterAjaxUpdate' => 'reInstallSortable',
  'ajaxUrl'=>$this->createUrl('multimedia/index',array('page_id'=>$multimedia->page_id,'redirectAction'=>$redirectAction, 'redirectController'=>$redirectController, 'withChilds'=>$withChilds)),
  //'summaryText'=>summaryTextLayout('multimedia-grid'),
	'columns'=>array(
    gridViewsortIcon(),
		'filename',
    'typeTranslated',
    'defaultTitle',
    array(
        'name'=>'defaultDescription',
        'visible'=>(in_array($page->id,$excludedFromDescription)? false:true)
    ),
    array(
        'class' => 'CCheckBoxColumn',
        'headerTemplate'=>'Roler',
        'value'=>'$data->id',
        'selectableRows'=>2,
        'checked'=>'$data->roller',
        'htmlOptions'=>array('class'=>'center', 'style'=>'width: 40px'),
        'checkBoxHtmlOptions'=>array('onclick'=>'switchRoller($(this),\''.$this->createUrl('multimedia/switchRoller').'\',[\'multimedia-grid\'])'),
        'visible'=>(in_array($page->id,$excludedFromDescription)? true:false)
    ),
      array(
            'class' => 'CCheckBoxColumn',
            'headerTemplate' => 'Główny',
            'value' => '$data->id',
            'selectableRows' => 2,
            'checked' => '$data->main',
            'htmlOptions' => array('class' => 'center', 'style' => 'width: 40px'),
            'checkBoxHtmlOptions' => array('onclick' => 'switchMain($(this),\'' . $this->createUrl('multimedia/switchMain') . '\',[\'multimedia-grid\'])'),
            'visible' => (in_array($page->id, $excludedFromDescription) ? true : false)
        ),
    array(
      'name'=>  Yii::t('cms', 'Podgląd'),
      'type'=>'raw',
      'value'=>'$data->mini',
    ),
      array(
        'class' => 'CButtonColumn',
        'template' => '{update}',
        'header'=>  Yii::t('cms', 'Edycja'),
        'htmlOptions'=>array('class'=>'center'),
        'buttons' => array(
            'update' => array(
                'label'=>  Yii::t('cms', 'Edytuj'),
                'url' => 'Yii::app()->createUrl("multimedia/edit", array("id"=>$data->id))',
            ),
        )
      ),
		array(
        'class' => 'CButtonColumn',
        'template' => '{delete}',
        'htmlOptions'=>array('class'=>'center'),
        'header'=>  Yii::t('cms', 'Usuń'),
        'buttons' => array(
            'delete' => array(
                'url' => 'Yii::app()->createUrl("multimedia/delete", array("id"=>$data->id))',
            ),
        )
      ),
    array(
      'class' => 'CCheckBoxColumn',
      'header' => 'Akcja',
      'selectableRows'=>2,
      'headerTemplate'=>'{item}',
      'footer'=>'
        <select onchange="if($(this).val()==\'delete\'){gridDeleteElements([\'pages-grid\',\'multimedia-grid\'],\'multimedia-grid_c8[]\',\''.$this->createUrl('multimedia/deleteByIds').'\');}">
          <option></option>
          <option value="delete">Usuń</option>
        </select>
      ',
      'htmlOptions'=>array('class'=>'center'),
    ),
	),
)); ?>

