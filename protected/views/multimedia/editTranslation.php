<div class="form">
    <? $tabs = array() ?>
    <? foreach (Yii::app()->params['langs'] as $lang): ?>
      <? $url = $this->createUrl('multimedia/editTranslation', array('multimedia_id' => $model->id, 'lang_id' => $lang->id)); ?>
      <? $title = getLangIcon(!$model->translations[$lang->id]->getIsNewRecord(), $lang).' '.Yii::t('cms', 'Język').' '.$lang->name?>
      <? if ($lang->id == $translation->lang_id): ?>
        <?
        $tabs['Translation_' . $lang->id] = array(
            'title' => $title,
            'view' => '_multimedia_translation_form',
            'data' => array('model' => $translation, 'lang' => $lang)
                )
        ?>
      <? else: ?>
        <?
        $tabs['Translation_' . $lang->id] = array(
            'title' => $title,
            'url' => CHtml::encode($url),
                )
        ?>
      <? endif ?>
    <? endforeach ?>
    <? $this->widget('CTabView', array('tabs' => $tabs, 'activeTab' => 'Translation_' . $translation->lang_id)); ?>
</div>