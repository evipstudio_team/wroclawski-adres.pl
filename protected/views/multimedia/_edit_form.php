<div class="form">
  <?$form=$this->beginWidget('CActiveForm', array('htmlOptions'=>array('enctype'=>'multipart/form-data')))?>
  <div id="uploadFormDiv">
      <div class="form">
          <span class="input_label"><?= Yii::t('cms', 'Wskaż plik do podmiany')?></span>
          <input type="file" name="file" />
      </div>
  </div>
  <div class="row">
      <?= CHtml::activeLabelEx($model,'created_by');?>
      <?= ZHtml::enumDropDownList($model, 'created_by') ?>
    </div>
    <div class="row">
      <?= CHtml::activeLabelEx($model,'created_at'); ?>
      <?Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');?>
      <?$this->widget('CJuiDateTimePicker', array(
          'attribute'=>'created_at',
          'language'=>Yii::app()->language,
          'model'=>$model,
          'options'=>array('dateFormat'=>'yy-mm-dd','timeFormat'=>'h:m:ss')
          ));?>
    </div>
    <div class="button_bar">
    <div class="button_add">
      <?php echo CHtml::submitButton(Yii::t('cms', 'Zapisz zmiany')); ?>
    </div>
    </div>
  <?php $this->endWidget(); ?>
</div>