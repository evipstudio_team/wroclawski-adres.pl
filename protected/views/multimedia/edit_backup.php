<?$this->renderPartial('../shared/_breadcrumbs', array('page' => $relatedElementPage));?>
<?$this->breadcrumbs = array_merge($this->breadcrumbs, array($this->pageTitle));?>
<?$tabs = array(
    'tab1'=>array(
        'title'=>'Ogólne',
        'view'=>'_edit_form',
        'data'=>array('model'=>$multimedia)
    )
)?>
<?foreach(Yii::app()->params['langs'] as $lang):?>
  <?  $tabs['Translation'.$lang->id]=array(
      'title' => getLangIcon(!$multimedia->translations[$lang->id]->getIsNewRecord(), $lang).' '.Yii::t('cms', 'Język').' '.$lang->name,
      'view' => '_multimedia_translation_form',
      'data'=>array('lang'=>$lang,'model'=>$multimedia)
  )?>
<?endforeach?>
<?$this->widget('CTabView',array('tabs'=>$tabs,'cssFile' => Yii::app()->theme->baseUrl.'/css/jquery.yiitab.css','activeTab'=>isset($activeTab)? $activeTab:null));?>