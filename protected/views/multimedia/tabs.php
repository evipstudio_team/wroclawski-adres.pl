<?$this->renderPartial('../shared/_breadcrumbs', array('page' => $page));?>
<?$this->breadcrumbs = array_merge(
        $this->breadcrumbs,
        array($page->getName(1)=>$this->createUrl($page->module->controller.'/edit',array('id'=>$page->id))),
        array('Obrazki i pliki'=>$this->createUrl($page->module->controller.'/files',array('id'=>$page->id))),
        array($this->pageTitle)
);?>

<?
$this->widget('CTabView', array(
    'tabs' => $tabs,
    'activeTab'=>Yii::app()->getController()->action->id,
    'viewData'=>  isset($additionalParams)? array_merge($additionalParams,array('model'=>$multimedia)):array('model'=>$multimedia)
))
?>