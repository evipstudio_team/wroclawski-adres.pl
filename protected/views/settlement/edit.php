<?php
$this->breadcrumbs = array(
    Yii::t('cms', 'Rozliczenia') => $this->createUrl('settlement/index'),
    Yii::t('cms', 'Edycja rozliczenia')
);
?>

<?= $this->renderPartial('_form', array('model' => $settlement)) ?>
<script type="text/javascript">
  $('#DialogBox').dialog("option","position","center");
  $(function() {
    $('#Settlement_value').change(function() {
      $(this).val(validateCostPerDay($(this).val()));
    });
    $( "#DialogBox" ).dialog( "option", "buttons", [
      {
        text: "Zamknij",
        click: function() {
          $( this ).dialog( "close" );
        }
      },
      {
        text: "Zapisz",
        click: function() {
          $('#LoadingDialogBox').dialog('open');
          $.ajax({
            url: '<?=$this->createUrl('settlement/edit',array('id'=>$settlement->id))?>',
            type: 'post',
            data: $('#settlementForm').serializeArray(),
            success: function(data) {
              $( "#DialogBox" ).html(data);
              $('#LoadingDialogBox').dialog('close');
            }
          });
        }
      }
    ]);
  });
</script>