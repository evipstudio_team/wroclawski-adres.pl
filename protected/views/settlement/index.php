<?
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/helpers.js');
Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');
?>
<?php
$this->breadcrumbs=array(
	Yii::t('cms', 'Rozliczenia')=>$this->createUrl('settlement/index'),
  Yii::t('cms', 'Tabela rozliczeń')
);
?>
<div class="form">
  <div class="row buttons">
    <?= CHtml::Button(Yii::t('cms', 'Dodaj pozycję'),array('class'=>'addSettlement')); ?>
  </div>
  <?php $form=$this->beginWidget('CActiveForm', array('action'=>Yii::app()->createUrl($this->route),'method'=>'get')); ?>
  <div class="row">
    <label><a style="cursor: pointer" onclick="$('#searchForm').slideToggle()">Ogranicz wyniki</a></label>
    <div id="searchForm"<?if(!$settlement->date_from && !$settlement->date_to && !$settlement->byMonth):?> style="display: none"<?endif?>>
      <?$options = $settlement->buildByMonthSelect($user);?>
    <?if($options):?>
      <label>Wskaż miesiąc: </label>
      <?= CHtml::activeDropDownList($settlement, 'byMonth', array(''=>'')+$options)?>
    <?endif?>
    <label>data od</label>
    <?
    $this->widget('CJuiDateTimePicker', array(
              'attribute'=>'date_from',
              'language'=>Yii::app()->language,
              'model'=>$settlement,
              'options'=>array('dateFormat'=>'yy-mm-dd','timeFormat'=>'h:m')))
            ?>
    <label>data do</label>
    <?
    $this->widget('CJuiDateTimePicker', array(
              'attribute'=>'date_to',
              'language'=>Yii::app()->language,
              'model'=>$settlement,
              'options'=>array('dateFormat'=>'yy-mm-dd','timeFormat'=>'h:m')))
            ?>
    <?= $form->hiddenField($user,'email')?>
    <?= $form->hiddenField($user,'company_name')?>
    <?= $form->hiddenField($user,'name')?>
    <?= $form->hiddenField($settlement,'title')?>
    <?= $form->hiddenField($settlement,'note')?>
    <?= $form->hiddenField($settlement,'value')?>
    <div class="row buttons">
    <?= CHtml::submitButton('Odśwież'); ?>&nbsp;&nbsp;&nbsp;
    </div>

    </div>
    <input type="submit" name="xls" value="Eksport do xls" />
  </div>
  <?php $this->endWidget(); ?>
</div>
<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'settlements-grid',
    'dataProvider' => $settlement->search($user),
    'summaryText' => summaryTextLayout('settlements-grid'),
    'rowCssClassExpression'=>'"item_{$data->id}"',
    'filter' => $settlement,
    'afterAjaxUpdate'=>'function() {reInstallElements();}',
    'enableSorting'=>true,
    'columns' => array(
        array(
            'name'=>'user.company_name',
            'filter'=>CHtml::activeTextField($user,'company_name')
        ),
        array(
            'name'=>'user.fullName',
            'filter'=>CHtml::activeTextField($user,'name')
        ),
        array(
            'name'=>'user.emailAndPhone',
            'type'=>'html',
            'filter'=>CHtml::activeTextField($user,'email')
        ),
        array(
            'name'=>'date',
            'filter'=>false
        ),
        'title',
        'note',
        array(
            'name'=>'formattedValue',
//            'value'=>'number_format($data->value,2,\',\',\' \').\' zł\'',
            'footer'=>'suma (wszystkich): <div style="text-align: right"><span style="white-space: nowrap">'.number_format($settlement->getSum($user),2,',',' ').' zł</span></div>'
        ),
        array(
            'class' => 'CButtonColumn',
            'template' => '{update}<span style="margin-left: 15px"></span>{delete}',
            'header'=>  Yii::t('cms', 'Akcja'),
            'htmlOptions'=>array('class'=>'center'),
            'updateButtonUrl'=>'Yii::app()->createUrl(\'settlement/edit\',array(\'id\'=>$data->id))',
            'updateButtonOptions'=>array('class'=>'editSettlement'),
            'buttons' => array(

            )
        ),
        array(
          'class' => 'CCheckBoxColumn',
          'header' => 'Akcja',
          'selectableRows'=>2,
          'headerTemplate'=>'{item}',
          'value'=>'$data->id',
          'footer'=>'
            <select onchange="if($(this).val()==\'delete\'){gridDeleteElements([\'settlements-grid\'], \'settlements-grid_c9[]\',\''.$this->createUrl('settlement/deleteByIds').'\');}">
              <option></option>
              <option value="delete">Usuń</option>
            </select>
          ',
          'htmlOptions'=>array('class'=>'center'),
        ),
    ),
));
?>

<?$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id'=>'DialogBox',
    'options'=>array(
        'title'=>  '',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>'500',
        'open' => 'js:function(){
          $(\'#LoadingDialogBox\').dialog(\'open\');
            jQuery.ajax({
              "url": $(this).data(\'url\'),
              "success":function(data){
                $(\'#DialogBox\').html($(data));
                $(\'#LoadingDialogBox\').dialog(\'close\');
              }
            });
          return false;}',
        'close'=>'js:function(){
          $.fn.yiiGridView.update(\'settlements-grid\');
          $(this).html(\'\');
        }',
    ),
));?>
<?$this->endWidget('zii.widgets.jui.CJuiDialog');?>

<script type="text/javascript">
  function reInstallElements() {
    $('.addSettlement').click(function() {
      if($('#User_email').val()) {
        $('#DialogBox').data('url','<?= $this->createUrl('settlement/create') ?>?User[email]='+$('#User_email').val());
      }
      else
        $('#DialogBox').data('url','<?= $this->createUrl('settlement/create') ?>');
      $('#DialogBox').dialog('open');
    });
    $('.editSettlement').click(function() {
      $('#DialogBox').data('url',$(this).attr('href'));
      $('#DialogBox').dialog('open');
      return false;
    });
  }
  $(function() {
    reInstallElements();
  });
</script>