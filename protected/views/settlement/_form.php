<div class="form">
  <?php $form = $this->beginWidget('CActiveForm', array('id' => 'settlementForm')); ?>
  <?= $form->errorSummary($model); ?>

  <div class="row">
    <?= $form->labelEx($model, 'user_id'); ?>
    <?= $form->dropDownList($model, 'user_id', array('' => '') + CHtml::listData(User::model()->findAll('`role` = \'Client\''), 'id', 'name')) ?>
    <?= $form->error($model, 'user_id'); ?>
  </div>

  <div class="row">
    <?= CHtml::activeLabelEx($model,'date'); ?>
    <?Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');?>
    <?$this->widget('CJuiDateTimePicker', array(
        'attribute'=>'date',
        'language'=>Yii::app()->language,
        'model'=>$model,
        'options'=>array('dateFormat'=>'yy-mm-dd','timeFormat'=>'h:m:ss')
        ));?>
    <?= $form->error($model,'date'); ?>
  </div>

  <div class="row">
    <?= $form->labelEx($model, 'value'); ?>
    <?= $form->textField($model, 'value') ?>
    <?= $form->error($model, 'value'); ?>
  </div>

  <div class="row">
    <?= $form->labelEx($model, 'title'); ?>
    <?= $form->textField($model, 'title') ?>
    <?= $form->error($model, 'title'); ?>
  </div>

  <div class="row">
    <?= $form->labelEx($model, 'note'); ?>
    <?= $form->textArea($model, 'note', array('rows' => '5', 'cols' => '5', 'style' => 'width: 90%')) ?>
    <?= $form->error($model, 'note'); ?>
  </div>
  <?php $this->endWidget(); ?>
</div>