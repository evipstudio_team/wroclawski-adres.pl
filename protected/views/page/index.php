<?Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/helpers.js');?>
<?php
$this->breadcrumbs=array(
    $this->pageTitle,
);
?>
<div class="form">
  <form method="get" action="<?= $this->createUrl('page/create')?>">
    <div class="row buttons">
      <?= CHtml::submitButton(Yii::t('cms', 'Dodaj nową podstronę'),array('name'=>'')); ?>
    </div>
  </form>
</div>


<?$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'pages-grid',
    'dataProvider' => $page->search(),
    'filter' => $page,
    'enablePagination'=>false,
    'selectableRows'=>0,
    'rowCssClassExpression'=>'"item_{$data->id}"',
    'afterAjaxUpdate'=>'function() {$(\'#selectedRow\').change(); loadingBoxClose();}',
    'summaryText'=>$page->parent_id? '<a href="'.$this->createUrl('page/index',array('parent_id'=>$page->parent->parent_id)).'"><img src="'.Yii::app()->theme->baseUrl.'/images/expand_up.png" alt="" title="" /> - Idź do kategorii nadrzędnej</a>':'<br />',
    //'emptyText'=>$page->countDepth()? Yii::t('cms','Brak wyników').'.&nbsp;&nbsp;&nbsp; <a href="'.$this->createUrl('page/index',array('parent_id'=>$page->parent->parent_id)).'"><img src="'.Yii::app()->theme->baseUrl.'/images/expand_up.png" alt="" title="" /> - Idź do kategorii nadrzędnej</a>':Yii::t('cms','Brak wyników'),
    'columns' => array(
        array(
            'name'=>'id',
            'sortable'=>false,
            'htmlOptions'=>array('style'=>'width: 30px'),
        ),
        array(
            'name'=>'name',
            'value'=>'(($data->depth)? str_repeat("--- ", $data->depth):\'\').$data->getName(1)',
            'type'=>'html'
        ),
        array(
            'name'=>  Yii::t('cms', 'Moduł'),
            'value'=>'$data->module->TranslatedName',
            'htmlOptions'=>array('style'=>'text-align: right; padding-right: 10px'),
            'filter'=>false
        ),
        array(
            'header'=>  Yii::t('cms', 'Kolejność'),
            'value'=>'(($data->canGoUp())? \'<a href="#moveUp" onclick="moveUpElement(\'.$data->id.\')" style="text-decoration: none">
              <img src="/images/icons/up.png" alt="O jeden w górę" title="O jeden w górę" />
              </a>\':\'<img src="/images/icons/upInactive.png" alt="O jeden w górę" title="Ten element jest już pierwszy" />\').\' \'.
              (($data->canGoDown())? \'<a href="#moveDown" onclick="moveDownElement(\'.$data->id.\')" style="text-decoration: none">
              <img src="/images/icons/down.png" alt="O jeden w dół" title="O jeden w dół" />
              </a>\':\'<img src="/images/icons/downInactive.png" alt="O jeden w dół" title="Ten element jest już ostatni" />\')
',
            'type'=>'raw'
        ),
        array(
            'class' => 'SButtonColumn',
            'template' => '{status}',
            'header'=>  Yii::t('cms', 'Status'),
            'htmlOptions'=>array('class'=>'center'),
            'buttons' => array(
                'status' => array(
                    'expressionImage' => true,
                    'expressionLabel' => true,
                    'label' => 'getStatusExpression($data->status)',
                    'type' => 'html',
                    'url' => 'Yii::app()->createUrl("page/statusSwitch", array("id"=>$data->id))',
                    'imageUrl' => 'getStatusIcon2($data->status)',
                    'click' => "function(event) {
                      event.preventDefault();
                      loadingBoxOpen();
                      var element = $(this);
                      $.ajax({
                        url: $(this).attr('href'),
                        success: function(data) {
                          $.fn.yiiGridView.update('pages-grid');
                          loadingBoxClose();
                        }
                      });
                      return false;}
                      ",
                ),
            )
        ),
        array(
            'class' => 'CButtonColumn',
            'template' => '{add}',
            'header'=>  Yii::t('cms', 'Dodaj'),
            'htmlOptions'=>array('class'=>'center'),
            'buttons' => array(
                'add' => array(
                    'label' => Yii::t('cms', 'Dodaj podkategorię'),
                    'url' => 'Yii::app()->createUrl("page/create", array("parent_id"=>$data->id))',
                    'imageUrl'=>Yii::app()->theme->baseUrl.'/css/gridview/add.png'
                ),
            )
        ),
        array(
            'class' => 'CButtonColumn',
            'template' => '{view}',
            'header'=>  Yii::t('cms', 'Podgląd'),
            'htmlOptions'=>array('class'=>'center'),
            'buttons' => array(
                'view' => array(
                    'label' => Yii::t('cms', 'Podgląd'),
                    'url' => 'Yii::app()->createUrl($data->module->controller."/index", array("page_id"=>$data->id))',
                ),
            )
        ),
        array(
            'class' => 'CButtonColumn',
            'template' => '{update}',
            'header'=>  Yii::t('cms', 'Edycja'),
            'htmlOptions'=>array('class'=>'center'),
            'buttons' => array(
                'update' => array(
                    'label' => Yii::t('cms', 'Edycja kategorii'),
                    'url' => 'Yii::app()->createUrl("page/edit", array("id"=>$data->id))',
                ),
            )
        ),
        array(
            'class' => 'CButtonColumn',
            'template' => '{delete}',
            'header'=>  Yii::t('cms', 'Usuń'),
            'htmlOptions'=>array('class'=>'center'),
        ),
        array(
          'class' => 'CCheckBoxColumn',
          'header' => 'Akcja',
          'selectableRows'=>2,
          'headerTemplate'=>'{item}',
          'value'=>'$data->id',
          'footer'=>'
            <select onchange="if($(this).val()==\'delete\'){gridDeleteElements([\'pages-grid\'], \'pages-grid_c9[]\',\''.$this->createUrl('page/deleteByIds').'\');}">
              <option></option>
              <option value="delete">Usuń</option>
            </select>
          ',
          'htmlOptions'=>array('class'=>'center'),
        ),
    ),
));
?>
<input type="hidden" name="selectedRow" id="selectedRow" value="<?if(isset($select)):?><?=$select?><?endif?>" onchange="selectRow($(this).val())" />

<?if(isset($select)):?>
<script type="text/javascript">
  jQuery(function() {
    $('#selectedRow').change();
  });
</script>
<?endif?>