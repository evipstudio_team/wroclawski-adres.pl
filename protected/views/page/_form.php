<div class="form">
  <? $form = $this->beginWidget('CActiveForm'); ?>
  <?= $form->errorSummary($page) ?>
  <div class="row">
    <?= $form->labelEx($page, 'name'); ?>
    <?= $form->textField($page, 'name', array('size' => 60)) ?>
    <?= $form->error($page, 'name'); ?>
  </div>
  <div class="row">
		<?=$form->labelEx($page,'status'); ?>
    <?= ZHtml::enumDropDownList($page, 'status') ?>
		<?=$form->error($page,'status'); ?>
	</div>
  <?if(Yii::app()->user->checkAccess('Root')):?>
    <?$avaiblePages = Page::model()->findAll();?>
    <? if ($avaiblePages): ?>
      <div class="row">
        <?= $form->labelEx($page, 'parent_id'); ?>
        <?= $form->dropDownList($page, 'parent_id', CHtml::listData($avaiblePages, 'id', 'name', 'parent.name')) ?>
        <?= $form->error($page, 'type'); ?>
      </div>
    <? endif ?>
    <div class="row">
      <?= $form->labelEx($page, 'module_id'); ?>
      <?= $form->dropDownList($page, 'module_id', CHtml::listData(Module::model()->findAll(array('order'=>'name')), 'id', 'name')) ?>
      <?= $form->error($page, 'module_id'); ?>
    </div>
    <div class="row">
      <?= $form->labelEx($page, 'managed'); ?>
      <?= ZHtml::enumDropDownList($page, 'managed') ?>
      <?= $form->error($page, 'managed'); ?>
    </div>
    <div class="row">
      <?= $form->labelEx($page, 'inheritance_url'); ?>
      <?= ZHtml::enumDropDownList($page, 'inheritance_url') ?>
      <?= $form->error($page, 'inheritance_url'); ?>
    </div>
    <div class="row">
      <?= $form->labelEx($page, 'url_edit_form'); ?>
      <?= $form->textField($page, 'url_edit_form') ?>
      <?= $form->error($page, 'url_edit_form'); ?>
    </div>
    <div class="row">
      <?= $form->labelEx($page, 'view_template'); ?>
      <?= $form->textField($page, 'view_template') ?>
      <?= $form->error($page, 'view_template'); ?>
    </div>
  <?endif?>
  <div class="button_bar">
    <div class="button_add">
      <?php echo CHtml::submitButton(Yii::t('cms', 'Zapisz')); ?>
    </div>
  </div>
  <? $this->endWidget(); ?>
</div>