<?
$globalId = 'CreateExistingPage';
$gridId = $globalId.'PageGrid';
$gridDialogId = $gridId.'Dialog';
$gridDialogArticleId = $gridId.'ArticleDialog';
$gridFormId = $globalId.'Form';
$gridFormRelatedToId = $gridFormId.'_related_element_id';
?>
<?
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
  'id'=>"$gridDialogId",
  'options'=>array(
      'title'=>  Yii::t('cms', 'Wybierz podstronę'),
      'autoOpen'=>false,
      'modal'=>true,
      'width'=>'800',
      'open'=>'js:function(){
          jQuery.ajax({
            "url": "'.$this->createUrl('link/AssignPageListing').'",
            "success":function(data){
              $(\'#'.$gridDialogId.'\').html($(data));
            }
          });
          return false;}',
      'buttons'=>array(
          'Anuluj'=>'js:function(){$(this).dialog(\'close\');$(\'#'.$gridFormRelatedToId.'\').val(\''.$link->related_element_id.'\');}',
      )
  ),
));
$this->endWidget('zii.widgets.jui.CJuiDialog');
?>

<?
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
  'id'=>"$gridDialogArticleId",
  'options'=>array(
      'title'=>  Yii::t('cms', 'Wybierz artykuł'),
      'autoOpen'=>false,
      'modal'=>true,
      'width'=>'800',
      'open'=>'js:function(){
          jQuery.ajax({
            "url": "'.$this->createUrl('link/AssignArticleListing').'",
            "success":function(data){
              $(\'#'.$gridDialogArticleId.'\').html($(data));
            }
          });
          return false;}',
      'buttons'=>array(
          'Anuluj'=>'js:function(){$(this).dialog(\'close\');$(\'#'.$gridFormRelatedToId.'\').val(\''.$link->related_element_id.'\');}',
      )
  ),
));
$this->endWidget('zii.widgets.jui.CJuiDialog');
?>

<div class="form">
  <?$form=$this->beginWidget('CActiveForm',array('action'=>$this->createUrl('link/createRelated',array('page_id'=>$page->id,'id'=>$id)),'htmlOptions' => array('id'=>$gridFormId,'enctype' => 'multipart/form-data'))); ?>
    <div class="row">
      <?= $form->hiddenField($link,'related_module_id')?>
      <?$id = $gridFormId.'_related_element_id'?>
      <?= CHtml::label(Yii::t('cms', 'Podstrona na którą wskazuje link'), $id)?>
      <?= CHtml::activeDropDownList($link, 'related_element_id', array($link->related_element_id=>(($link->related_element_id)? $link->module->translatedName:''),'PagePicker'=>'Kliknij aby wybrać podstronę','ArticlePicker'=>'Kliknij aby wybrać artykuł'),
              array('onclick'=>'if($(this).val()=="PagePicker"){$(\'#'.$gridDialogId.'\').dialog(\'open\');}else if($(this).val()=="ArticlePicker"){$(\'#'.$gridDialogArticleId.'\').dialog(\'open\');}','id'=>$id)) ?>
      <?php echo $form->error($link, 'related_element_id'); ?>
    </div>
    <div class="row buttons">
      <?php echo CHtml::submitButton(Yii::t('cms', 'Zapisz')); ?>
    </div>
  <?$this->endWidget();?>
</div>
