<?
$parent = $page;
while($parent = $parent->parent) {
  if($parent->id>1)
    $this->breadcrumbs = array_merge($this->breadcrumbs, array(Yii::t('cms', $parent->name)=>$this->createUrl('link/index',array('page_id'=>$parent->id))));
  else break;
}
$this->breadcrumbs = array_merge($this->breadcrumbs, array(Yii::t('cms', $page->name)=>$this->createUrl('link/index',array('page_id'=>$page->id))));

?>