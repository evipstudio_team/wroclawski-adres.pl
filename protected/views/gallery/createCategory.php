<?$this->renderPartial('../shared/_breadcrumbs', array('page' => $newPage));?>
<?$this->breadcrumbs = array_merge($this->breadcrumbs, array($this->pageTitle));?>

<div class="form">
  <? $form = $this->beginWidget('CActiveForm'); ?>
  <?= $form->errorSummary($newPage) ?>
  <div class="row">
    <?= $form->labelEx($newPage, 'name'); ?>
    <?= $form->textField($newPage, 'name', array('size' => 60)) ?>
    <?= $form->error($newPage, 'name'); ?>
  </div>
  
  <div class="button_bar">
    <div class="button_add">
      <?php echo CHtml::submitButton(Yii::t('cms', 'Zapisz')); ?>
    </div>
  </div>
  <? $this->endWidget(); ?>
</div>