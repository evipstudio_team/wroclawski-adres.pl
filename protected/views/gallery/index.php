<?Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/helpers.js');?>
<?$this->renderPartial('../shared/_breadcrumbs', array('page' => $page));?>
<?$this->breadcrumbs = array_merge($this->breadcrumbs, array($this->pageTitle));?>

<h3><?= 'Lista podkategorii'?></h3>
<?$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'pages-grid',
    'dataProvider' => $subPages->search(),
    //'filter' => $page,
    'enablePagination'=>false,
    'enableSorting'=>false,
    'selectableRows'=>0,
    'rowCssClassExpression'=>'"item_{$data->child->id}"',
    'afterAjaxUpdate'=>'function() {$(\'#selectedRow\').change(); loadingBoxClose();}',
    'ajaxUrl'=>$this->createUrl('gallery/index',array('id'=>$subPages->parent_id, 'select'=>$page->id)),
    'summaryText'=>'',
    'columns' => array(
        array(
            'name'=>'id',
            'value'=>'$data->child->id',
            'sortable'=>false,
            'htmlOptions'=>array('style'=>'width: 30px'),
        ),
        array(
            'name'=>'name',
            'value'=>'(($data->child->depth)? str_repeat("--- ", $data->child->depth):\'\').CHtml::link($data->child->getName(1),Yii::app()->createUrl(\'gallery/index\',array(\'page_id\'=>$data->child->id)))',
            'type'=>'html'
        ),
        array(
            'header'=>  Yii::t('cms', 'Ilość plików'),
            'value'=>'$data->child->filesCount',
            'htmlOptions'=>array('style'=>'width: 40px', 'class'=>'right'),
        ),
        array(
            'header'=>  Yii::t('cms', 'Kolejność'),
            'value'=>'('.$subPages->parent_id.'!=$data->child->id)? ((($data->child->canGoUp())? \'<a href="#moveUp" onclick="moveUpElement(\'.$data->child->id.\')" style="text-decoration: none">
              <img src="/images/icons/up.png" alt="O jeden w górę" title="O jeden w górę" />
              </a>\':\'<img src="/images/icons/upInactive.png" alt="O jeden w górę" title="Ten element jest już pierwszy" />\').\' \'.
              (($data->child->canGoDown())? \'<a href="#moveDown" onclick="moveDownElement(\'.$data->child->id.\')" style="text-decoration: none">
              <img src="/images/icons/down.png" alt="O jeden w dół" title="O jeden w dół" />
              </a>\':\'<img src="/images/icons/downInactive.png" alt="O jeden w dół" title="Ten element jest już ostatni" />\')):\'\'
',
            'type'=>'raw',
            'htmlOptions'=>array('style'=>'width: 60px', 'class'=>'center'),
        ),
        array(
            'class' => 'SButtonColumn',
            'template' => '{status}',
            'header'=>  Yii::t('cms', 'Status'),
            'htmlOptions'=>array('class'=>'center'),
            'buttons' => array(
                'status' => array(
                    'expressionImage' => true,
                    'expressionLabel' => true,
                    'label' => 'getStatusExpression($data->child->status)',
                    'type' => 'html',
                    'url' => 'Yii::app()->createUrl("page/statusSwitch", array("id"=>$data->child->id))',
                    'imageUrl' => 'getStatusIcon2($data->child->status)',
                    'click' => "function(event) {
                      event.preventDefault();
                      loadingBoxOpen();
                      var element = $(this);
                      $.ajax({
                        url: $(this).attr('href'),
                        success: function(data) {
                          $.fn.yiiGridView.update('pages-grid');
                          loadingBoxClose();
                        }
                      });
                      return false;}
                      ",
                ),
            )
        ),
        array(
            'class' => 'CButtonColumn',
            'template' => '{add}',
            'header'=>  Yii::t('cms', 'Dodaj'),
            'htmlOptions'=>array('class'=>'center'),
            'buttons' => array(
                'add' => array(
                    'label' => Yii::t('cms', 'Dodaj podkategorię'),
                    'url' => 'Yii::app()->createUrl("gallery/createCategory", array("parent_id"=>$data->child->id))',
                    'imageUrl'=>Yii::app()->theme->baseUrl.'/css/gridview/add.png'
                ),
            )
        ),
        array(
            'class' => 'CButtonColumn',
            'template' => '{update}',
            'header'=>  Yii::t('cms', 'Edycja'),
            'htmlOptions'=>array('class'=>'center'),
            'buttons' => array(
                'update' => array(
                    'label' => Yii::t('cms', 'Edycja kategorii'),
                    'url' => 'Yii::app()->createUrl("page/edit", array("id"=>$data->child->id))',
                ),
            )
        ),
        array(
            'class' => 'CButtonColumn',
            'template' => '{delete}',
            'header'=>  Yii::t('cms', 'Usuń'),
            'deleteButtonUrl'=>'Yii::app()->createUrl("page/delete", array("id"=>$data->child->id))',
            'htmlOptions'=>array('class'=>'center'),
            'buttons'=>array(
            'delete'=>array(
                //'visible'=>'($data->child->id!='.$page->id.')? true:false'
            ))
        ),
        array(
          'class' => 'CCheckBoxColumn',
          'header' => 'Akcja',
          'selectableRows'=>2,
          'headerTemplate'=>'{item}',
            'value'=>'$data->child->id',
          'footer'=>'
            <select onchange="if($(this).val()==\'delete\'){gridDeleteElements([\'pages-grid\',\'multimedia-grid\'], \'pages-grid_c8[]\',\''.$this->createUrl('page/deleteByIds').'\');}">
              <option></option>
              <option value="delete">Usuń</option>
            </select>
          ',
          'htmlOptions'=>array('class'=>'center'),
        ),
    ),
));
?>
<input type="hidden" name="selectedRow" id="selectedRow" value="<?= $select?>" onchange="selectRow($(this).val())" />
<br />
<?Yii::app()->clientScript->registerScript('loadAttachments', '
  jQuery.ajax({
  "url":"'.$this->createUrl('multimedia/index',array('page_id'=>$page->id,'redirectAction'=>Yii::app()->controller->getAction()->getId(),'redirectController'=>Yii::app()->controller->getId(),'withChilds'=>($subPages->parent_id==$page->id)? true:false)).'",
    "success":function(data){
              $(\'#multimediaListing\').html(data);
            }
})');?>
<div id="multimediaListing">
  <?= loadingString()?>
</div>
<script type="text/javascript">
$(function() {
  $('#selectedRow').change();
});
</script>