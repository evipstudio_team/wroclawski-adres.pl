<?php
$this->breadcrumbs=array(
	Yii::t('cms', 'Zdarzenia')=>$this->createUrl('event/index'),
  Yii::t('cms', 'Edycja zdarzenia'),
);
?>

<div>
  <div style="width: 49%; float: left">
    <h1>Formularz</h1>
    <?=$this->renderPartial('_form',array('event'=>$event))?>
  </div>
  <div style="width: 49%; float: left">
    <h1>Dokumenty</h1>
    <?php $this->widget('zii.widgets.grid.CGridView', array(
      'id'=>'files-grid',
      'dataProvider'=>$eventFile->search(),
      'summaryText'=>'',
      'columns'=>array(
        'filename',
        'created_at',
        array(
          'name' => Yii::t('cms', 'Pobierz'),
          'value' => '\'<a href="\'.$data->linkTo().\'"><img src="\'.Yii::app()->theme->baseUrl.\'/images/download.png" alt="" title="" /></a>\'',
          'htmlOptions'=>array('class'=>'center'),
          'filter' => false,
          'type' => 'raw'
        ),
        array(
              'class' => 'CButtonColumn',
              'template' => '{delete}',
              'header' => Yii::t('cms', 'Usuń'),
              'htmlOptions' => array('class' => 'center'),
              'buttons' => array(
                  'delete' => array(
                      'url' => 'Yii::app()->createUrl("event/fileDelete", array("id"=>$data->id))',
                  ),
              )
          ),
      ),
    )); ?>
  </div>
  <div style="clear: both"></div>
</div>

