<?php
$this->breadcrumbs=array(
	Yii::t('cms', 'Zdarzenia')=>$this->createUrl('event/index'),
  Yii::t('cms', 'Dodaj nowy wpis'),
);
?>

<?=$this->renderPartial('_form',array('event'=>$event))?>