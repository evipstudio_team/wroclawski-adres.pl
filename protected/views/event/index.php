<?Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/helpers.js');?>
<?php
$this->breadcrumbs=array(
	Yii::t('cms', 'Zdarzenia'),
);
?>
<?if($typ):?>
  <div class="form">
    <form method="post" action="<?= $this->createUrl('event/create')?>">
        <?= CHtml::activeHiddenField($event, 'type')?>
      <div class="row buttons">
        <?= CHtml::submitButton(Yii::t('cms', 'Dodaj nowy'),array('name'=>'')); ?>
      </div>
    </form>
  </div>
<?endif?>
<?if($user->email && User::model()->find('`email`=:email',array(':email'=>$user->email))):?>
  <?$existingUser = User::model()->find('`email`=:email',array(':email'=>$user->email))?>
  <?if($existingUser):?>
  <div class="form">
    <?foreach(ZHtml::enumItem($event, 'type') as $typeIndex=>$type):?>
    <div style="float: left; margin-right: 10px">
      <form method="post" action="<?= $this->createUrl('event/create')?>">
          <?= CHtml::activeHiddenField($event, 'type', array('value'=>$typeIndex))?>
          <?= CHtml::activeHiddenField($event, 'user_id', array('value'=>$existingUser->id))?>
        <div class="row buttons">
          <?= CHtml::submitButton('Dodaj '.$type,array('name'=>'')); ?>
        </div>
      </form>
      </div>
    <?endforeach?>
    <div style="clear: both"></div>
  </div>
  <?endif?>
<?endif?>
<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'events-grid',
    'dataProvider' => $event->search($user),
    'summaryText' => summaryTextLayout('events-grid'),
    'rowCssClassExpression'=>'"item_{$data->id}"',
    'filter' => $event,
    'columns' => array(
        array(
            'name' => 'id',
            'htmlOptions' => array('style' => 'width: 30px')
        ),
        array(
            'name'=>'user.email',
            'filter'=>CHtml::activeTextField($user,'email')
        ),
        array(
            'name'=>'user.fullName',
            'filter'=>CHtml::activeTextField($user,'name')
        ),
        array(
            'name'=>'created_at',
            'value'=>'date(\'d/m/Y\',strtotime($data->created_at))',
            'htmlOptions'=>array('class'=>'center'),
            'filter'=>false
        ),
        array(
            'name'=>'type',
            'value'=>'$data->TranslateEnumValue(\'type\')',
            'filter'=>ZHtml::enumItem($event,'type')
        ),
        array(
            'name'=>'files',
            'value'=>'$data->filesToString()',
            'filter'=>false,
            'type'=>'raw'
        ),
        array(
            'name'=>'comment',
            'htmlOptions'=>array('style'=>'max-width: 400px'),
        ),
        array(
            'class' => 'CButtonColumn',
            'template' => '{update}',
            'header' => Yii::t('cms', 'Edycja'),
            'htmlOptions' => array('class' => 'center'),
            'buttons' => array(
                'update' => array(
                    'url' => 'Yii::app()->createUrl("event/edit", array("id"=>$data->id))',
                ),
            )
        ),
        array(
            'class' => 'CButtonColumn',
            'template' => '{delete}',
            'header' => Yii::t('cms', 'Usuń'),
            'htmlOptions' => array('class' => 'center'),
            'buttons' => array(
                'delete' => array(
                    'url' => 'Yii::app()->createUrl("event/delete", array("id"=>$data->id))',
                ),
            )
        ),
        array(
          'class' => 'CCheckBoxColumn',
          'header' => 'Akcja',
          'selectableRows'=>2,
          'headerTemplate'=>'{item}',
            'value'=>'$data->id',
          'footer'=>'
            <select onchange="if($(this).val()==\'delete\'){gridDeleteElements([\'events-grid\'], \'events-grid_c8[]\',\''.$this->createUrl('event/deleteByIds').'\');}">
              <option></option>
              <option value="delete">Usuń</option>
            </select>
          ',
          'htmlOptions'=>array('class'=>'center'),
        ),
    ),
));
?>
<input type="hidden" name="selectedRow" id="selectedRow" value="<?= $select?>" onchange="selectRow($(this).val())" />

<script type="text/javascript">
$(function() {
  $('#selectedRow').change();
});
</script>