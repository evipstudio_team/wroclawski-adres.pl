<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array('htmlOptions' => array('enctype' => 'multipart/form-data'))); ?>
	<?= $form->errorSummary($event); ?>

  <div class="row">
		<?= $form->labelEx($event,'user_id'); ?>
    <?= $form->dropDownList($event,'user_id',array(''=>'')+CHtml::listData(User::model()->findAll(array('condition'=>'`role`=\'client\' AND status=\'active\'','order'=>'surname')),'id','fullName'))?>
		<?= $form->error($event,'user_id'); ?>
	</div>

	<div class="row">
		<?= $form->labelEx($event,'type'); ?>
    <?= $form->dropDownList($event,'type',array(''=>'')+ZHtml::enumItem($event, 'type'))?>
		<?= $form->error($event,'type'); ?>
	</div>

  <div class="row">
    <?= CHtml::activeLabelEx($event,'date'); ?>
    <?Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');?>
    <?$this->widget('CJuiDateTimePicker', array(
        'attribute'=>'date',
        'language'=>Yii::app()->language,
        'model'=>$event,
        'options'=>array('dateFormat'=>'yy-mm-dd','timeFormat'=>'h:m:ss')
        ));?>
    <?= $form->error($event,'date'); ?>
  </div>

  <div class="row">
		<?= $form->labelEx($event,'created_by'); ?>
    <?= $form->dropDownList($event,'created_by',CHtml::listData(User::model()->findAll(array('condition'=>'`role` IN (\'root\',\'administrator\') AND status=\'active\'','order'=>'surname')),'id','fullName'))?>
		<?= $form->error($event,'created_by'); ?>
	</div>

  <div class="row">
    <?= CHtml::activeLabelEx($event,'created_at'); ?>
    <?Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');?>
    <?$this->widget('CJuiDateTimePicker', array(
        'attribute'=>'created_at',
        'language'=>Yii::app()->language,
        'model'=>$event,
        'options'=>array('dateFormat'=>'yy-mm-dd','timeFormat'=>'h:m:ss')
        ));?>
  </div>

  <?if($event->isNewRecord):?>
    <div class="row">
      <?= $form->labelEx($event,'sendEmail'); ?>
      <?= $form->dropDownList($event,'sendEmail',array(''=>'','0'=>'Nie','new'=>'Tak'))?>
      <?= $form->error($event,'sendEmail'); ?>
    </div>
  <?else:?>
    <div class="row">
      <?= $form->labelEx($event,'sendEmail'); ?>
      <?= $form->dropDownList($event,'sendEmail',array(''=>'','0'=>'Nie','edit'=>'Tak - z komunikatem o edycji','exist'=>'Tak - bez komunikatu'))?>
      <?= $form->error($event,'sendEmail'); ?>
    </div>
  <?endif?>

  <div class="row">
    <?= $form->labelEx($event, 'formFiles'); ?>
    <?= $form->fileField($event, 'formFiles', array('multiple'=>'multiple', 'name'=>'Event[formFiles][]')) ?>
  </div>

  <div class="row">
		<?= $form->labelEx($event,'comment'); ?>
		<?= $form->textArea($event,'comment',array('rows'=>15, 'cols'=>40)); ?>
		<?= $form->error($event,'comment'); ?>
	</div>




	<div class="row buttons">
		<?= CHtml::submitButton($event->isNewRecord ? Yii::t('cms','Zapisz nowe zdarzenie') : Yii::t('cms','Zapisz zmiany'),array('name'=>'save')); ?>
	</div>

<?php $this->endWidget(); ?>

</div>