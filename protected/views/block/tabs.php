<?$this->breadcrumbs=array(
	Yii::t('cms', 'Zarządzanie blokami')=>$this->createUrl('block/index'),
    $this->pageTitle
);
?>

<?
$this->widget('CTabView', array(
    'cssFile' => Yii::app()->theme->baseUrl.'/css/jquery.yiitab.css',
    'tabs' => $tabs,
    'activeTab'=>Yii::app()->getController()->action->id,
    'viewData'=>  isset($additionalParams)? array_merge($additionalParams,array('block'=>$block)):array('block'=>$block)
))
?>