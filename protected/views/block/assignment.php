<?//Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/helpers.js');?>
<?//Yii::app()->clientScript->registerCoreScript('jquery.ui');?>
<div class="form">
  <p class="note">Wypełnij poniższy formularz aby dodać nową regułę przypisania</p>
  <?php $form=$this->beginWidget('CActiveForm'); ?>
	<?= $form->errorSummary($newBlockAssignment); ?>
  <?= $form->hiddenField($newBlockAssignment,'block_id')?>
	<div class="row">
		<?= $form->labelEx($newBlockAssignment,'module_id'); ?>
		<?= $form->dropDownList($newBlockAssignment, 'module_id', array(''=>'')+CHtml::listData(Module::model()->findAll(), 'id', 'name')) ?>
	</div>
  <div class="row">
		<?= $form->labelEx($newBlockAssignment,'page_id'); ?>
		<?= $form->dropDownList($newBlockAssignment, 'page_id', array(''=>'')+CHtml::listData(Page::model()->findAll(), 'id', 'name', 'parent.name')) ?>
	</div>
  <div class="button_bar">
    <div class="button_add">
      <?= CHtml::submitButton(Yii::t('cms', 'Zapisz'))?>
    </div>
  </div>
  <div style="clear: both"></div>
  <?php $this->endWidget(); ?>
</div>
<h1 style="margin-top: 10px"><?= Yii::t('cms', 'Lista przypisanych elementów') ?></h1>

<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'assignments-grid',
    'dataProvider' => $blockAssignment->search(),
    'enablePagination' => false,
    'columns' => array(
        array(
            'name'=>  Yii::t('cms', 'Moduł'),
            'value'=>'$data->module? $data->module->translatedName:\'-\'',
            'filter'=>false
        ),
        array(
            'name'=>  Yii::t('cms', 'Podstrona'),
            'value'=>'$data->page? $data->page->getName(1):\'-\'',
            'filter'=>false
        ),
        array(
            'class' => 'SButtonColumn',
            'template' => '{delete}',
            'header'=>  Yii::t('cms', 'Usuń'),
            'htmlOptions'=>array('class'=>'center'),
            'buttons' => array(
                'delete' => array(
                    'label' => Yii::t('cms', 'Usuń przypisanie'),
                    'url' => 'Yii::app()->createUrl("block/deleteAssignment", array("block_id"=>$data->block_id,"module_id"=>$data->module_id,"page_id"=>$data->page_id))',
                ),
            )
        ),
    ),
));
?>