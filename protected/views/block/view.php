<div class="form">
  <?Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/helpers.js');?>
  <?Yii::app()->clientScript->registerCoreScript('jquery.ui');?>
  <?
  Yii::app()->clientScript->registerScript('sortable-project', "installSortable('block-grid','" . $this->createUrl('//block/sort', array('block_id' => $block->id)) . "');");
  ?>

  <h1 style="margin-top: 10px">Lista elementów przypisanych</h1>
  <?php

  $this->widget('zii.widgets.grid.CGridView', array(
      'id' => 'block-grid',
      'dataProvider' => $blockPage->search(),
      'enablePagination' => false,
      'rowCssClassExpression' => '"items_{$data->page_id}"',
      'afterAjaxUpdate' => 'installSortable(\'block-grid\',\'' . $this->createUrl('//block/sort', array('block_id' => $block->id)) . '\')',
      'ajaxUrl' => $this->createUrl('block/view', array('id' => $block->id)),
      'columns' => array(
          gridViewsortIcon(),
          array(
              'name'=>  Yii::t('cms', 'Tytuł'),
              'value'=>'$data->page->getName(1)',
              'filter'=>false
          ),
          array(
              'name'=>  Yii::t('cms', 'Moduł'),
              'value'=>'$data->page->module->translatedName',
              'filter'=>false
          ),
          array(
            'class' => 'SButtonColumn',
            'template' => '{edit_assignment}',
            'header' => Yii::t('cms', 'Usuń przypisanie'),
            'buttons' => array(
                'edit_assignment' => array(
                    'expressionImage' => true,
                    'label' => 'Usuń element z bloku',
                    'url' => 'Yii::app()->createUrl("block/AjaxAssignPage", array("page_id"=>$data->page->id,"block_id"=>' . $block->id . '))',
                    'type' => 'html',
                    'click' => "function() {
                  $.fn.yiiGridView.update('usignedPages-grid', {
                    type: 'POST',
                    url: $(this).attr('href'),
                    success: function() {
                      $.fn.yiiGridView.update('usignedPages-grid');
                      $.fn.yiiGridView.update('block-grid');
                    }
                  });
                return false;}",
                    'imageUrl' => 'getStatusIcon(1)'
                ),
            )
        )
      ),
  ));
  ?>
</div>

<h1 style="margin-top: 10px">Lista elementów nieprzypisanych</h1>
<?php

  $this->widget('zii.widgets.grid.CGridView', array(
      'id' => 'usignedPages-grid',
      'dataProvider' => $unsignedPage->searchForBlockAssign($block),
      'ajaxUrl' => $this->createUrl('block/view', array('id' => $block->id)),
      'enablePagination' => true,
      'columns' => array(
          array(
              'name'=>  Yii::t('cms', 'Tytuł'),
              'value'=>'$data->getName(1)',
              'filter'=>false
          ),
          array(
              'name'=>  Yii::t('cms', 'Moduł'),
              'value'=>'$data->module->translatedName',
              'filter'=>false
          ),
          array(
            'class' => 'SButtonColumn',
            'template' => '{edit_assignment}',
            'header' => Yii::t('cms', 'Przypisz'),
            'buttons' => array(
                'edit_assignment' => array(
                    'expressionImage' => true,
                    'label' => 'Przypisz element do bloku',
                    'url' => 'Yii::app()->createUrl("block/AjaxAssignPage", array("page_id"=>$data->id,"block_id"=>' . $block->id . '))',
                    'type' => 'html',
                    'click' => "function() {
                  $.fn.yiiGridView.update('usignedPages-grid', {
                    type: 'POST',
                    url: $(this).attr('href'),
                    success: function() {
                      $.fn.yiiGridView.update('usignedPages-grid');
                      $.fn.yiiGridView.update('block-grid');
                    }
                  });
                return false;}",
                    'imageUrl' => 'getStatusIcon(0)'
                ),
            )
        )
      ),
  ));
  ?>
<?php
/*
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'blockUnsignedPages-grid',
    'dataProvider' => $page->searchByBlock($model->id, false),
    'filter' => $page,
    'rowCssClassExpression' => '"items_{$data->id}"',
    'columns' => array(
        array(
            'name' => 'name',
            'value' => '$data->name',
        ),
        array(
            'class' => 'SButtonColumn',
            'template' => '{edit_assignment}',
            'header' => Yii::t('cms', 'Przypisz'),
            'buttons' => array(
                'edit_assignment' => array(
                    'expressionLabel' => true,
                    'expressionImage' => true,
                    'label' => '$data->isAssignedToBlockTranslate(' . $model->id . ')',
                    'url' => 'Yii::app()->createUrl("block/AjaxAssignPage", array("page_id"=>$data->id,"block_id"=>' . $model->id . '))',
                    'visible' => 'Yii::app()->user->checkAccess("Block:AjaxAssignPage")',
                    'type' => 'html',
                    'click' => "function() {
                  $.fn.yiiGridView.update('blockUnsignedPages-grid', {
                    type: 'POST',
                    url: $(this).attr('href'),
                    success: function() {
                      $.fn.yiiGridView.update('blockUnsignedPages-grid');
                      $.fn.yiiGridView.update('block-grid');
                    }
                  });
                return false;}",
                    'imageUrl' => '$data->isAssignedToBlockIcon(' . $model->id . ')'
                ),
            )
        ),
    ),
));
 *
 */
?>