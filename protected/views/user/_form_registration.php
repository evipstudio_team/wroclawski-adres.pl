<div class="form">

<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'user-registration-form',
    'enableClientValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
    ),
    'focus' => array($model, 'name'),
        ));
?>

  <p class="note">
    <?= Yii::t('cms', 'Pola oznaczone <span class="required">*</span> są wymagane.')?><br />
    <?= Yii::t('cms', 'Po wypełnieniu i zapisaniu formularza, na wskazany adres e-mail zostanie wysłana wiadomość potwierdzająca rejestrację.')?>
  </p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>

  <div class="row">
		<?php echo $form->labelEx($model,'surname'); ?>
		<?php echo $form->textField($model,'surname',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'surname'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>
  <?if($model->isNewRecord):?>
    <div class="row">
      <?php echo $form->labelEx($model,'password'); ?>
      <?php echo $form->passwordField($model,'password',array('size'=>60,'maxlength'=>40)); ?>
      <?php echo $form->error($model,'password'); ?>
    </div>

    <div class="row">
      <?php echo $form->labelEx($model,'repeat_password'); ?>
      <?php echo $form->passwordField($model,'repeat_password',array('size'=>60,'maxlength'=>40)); ?>
    </div>
  <?endif?>
  <?php if(CCaptcha::checkRequirements()): ?>
    <div class="row">
      <div class="hint"><?= Yii::t('cms','Prosimy o przepisanie treści z obrazka, system nie rozróżnia wielkości liter.')?><br /><?= Yii::t('cms','W przypadku problemów z odczytaniem obrazka, kliknij w link "Pobierz nowy kod".')?></div>
      <div>
        <?php $this->widget('CCaptcha'); ?>
        <?php echo $form->textField($model,'verifyCode'); ?>
      </div>
      <?php echo $form->error($model,'verifyCode'); ?>
    </div>
	<?php endif; ?>
  <div class="row">
      <span class="required">*</span> <?= Yii::t('cms', 'Akceptuję warunki regulaminu dostępnego pod tym adresem')?>: <a href="<?= $this->createUrl('/site/page',array('view'=>'terms'))?>" target="_blank"><?= Yii::t('cms','Regulamin')?></a>
      <?= $form->checkbox($model,'terms')?>
      <?php echo $form->error($model,'terms'); ?>
  </div>
	<div class="row buttons">
		<?php echo CHtml::submitButton(Yii::t('cms','Zarejestruj')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->