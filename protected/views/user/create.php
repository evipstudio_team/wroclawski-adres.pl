<?php
$this->breadcrumbs=array(
	Yii::t('cms', 'Użytkownicy')=>$this->createUrl('/user/index'),
	Yii::t('cms', 'Dodaj użytkownika'),
);
?>

<h1><?= Yii::t('cms', 'Dodaj użytkownika')?></h1>
<div class="form">
  <p class="note">Po utworzeniu konta użytkownika, na wskazany adres e-mail zostanie wysłana wiadomość z danymi do logowania.</p>
</div>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>