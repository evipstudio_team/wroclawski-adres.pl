<?php
$this->breadcrumbs=array(
	Yii::t('cms', 'Użytkownicy'),
);
?>
<div class="form">
  <form method="get" action="<?= $this->createUrl('user/create')?>">
    <div class="row buttons">
      <?= CHtml::SubmitButton(Yii::t('cms', 'Dodaj nowego użytkownika')); ?>
    </div>
  </form>
</div>
<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'users-grid',
    'dataProvider' => $user->search(),
    'summaryText' => summaryTextLayout('users-grid'),
    'filter' => $user,
    'columns' => array(
        array(
            'name' => 'id',
            'htmlOptions' => array('style' => 'width: 30px')
        ),
        array(
            'name' => 'company_name',
            'htmlOptions' => array()
        ),
        array(
            'name' => 'email',
            'htmlOptions' => array()
        ),
        array(
            'name' => 'name',
            'htmlOptions' => array()
        ),
        array(
            'name' => 'surname',
            'htmlOptions' => array()
        ),
        array(
            'name' => 'role',
            'value'=>'$data->TranslateEnumValue(\'role\')',
            'htmlOptions' => array(),
            'filter'=>Chtml::activeDropDownList($user,'role',array(''=>'')+User::model()->findByPk(Yii::app()->user->id)->getPossibleRoles())
        ),
        array(
            'name' => 'created_at',
            'value' => 'date(\'d/m/Y\',strtotime($data->created_at))',
            'filter'=>false,
            'htmlOptions' => array()
        ),
        array(
            'name' => 'status',
            'value' => '$data->statusTranslated',
            'htmlOptions' => array('class'=>'right'),
            'filter'=>Chtml::activeDropDownList($user,'status',array(''=>'','new'=>'Nowy','active'=>'Aktywny','inactive'=>'Nieaktywny'))
        ),
        array(
            'header' => Yii::t('cms', 'Zdarzenia'),
            'value'=>'$data->sumEvents',
            'cssClassExpression' => '$data->email',
            'htmlOptions' => array('onclick' => 'window.location.href = \''.$this->createUrl('event/index').'?User[email]=\'+$(this).attr(\'class\').replace(/^.* .* /g,\'\');', 'style' => 'cursor: pointer;', 'class' => 'center clickable', 'title' => Yii::t('cms', 'Kliknij aby zobaczyć')),
            'filter' => false,
            'type' => 'raw'
        ),
        array(
            'header' => Yii::t('cms', 'Należności'),
            'value'=>'number_format($data->sumSettlements,2,\',\',\' \').\' zł\'',
            'cssClassExpression' => '$data->email',
            'htmlOptions' => array('onclick' => 'window.location.href = \''.$this->createUrl('settlement/index').'?User[email]=\'+$(this).attr(\'class\').replace(/^.* .* /g,\'\');', 'style' => 'cursor: pointer;', 'class' => 'center clickable', 'title' => Yii::t('cms', 'Kliknij aby zobaczyć')),
            'filter' => false,
            'type' => 'raw'
        ),
        array(
            'class' => 'SButtonColumn',
            'template' => '{update}',
            'header'=>  Yii::t('cms', 'Edycja'),
            'htmlOptions'=>array('class'=>'center'),
            'buttons' => array(
                'update' => array(
                    'label' => Yii::t('cms', 'Edycja użytkownika'),
                    'url' => 'Yii::app()->createUrl("user/edit", array("id"=>$data->id))',
                ),
            )
        ),
        array(
            'class' => 'SButtonColumn',
            'template' => '{delete}',
            'header'=>  Yii::t('cms', 'Usuń'),
            'htmlOptions'=>array('class'=>'center'),
            'buttons' => array(
                'delete' => array(
                    'label' => Yii::t('cms', 'Usuń użytkownika'),
                    'url' => 'Yii::app()->createUrl("user/delete", array("id"=>$data->id))',
                ),
            )
        ),
    ),
));
?>