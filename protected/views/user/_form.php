<div class="form">
  <?$form = $this->beginWidget('CActiveForm');?>
    <p class="note"><?= Yii::t('Fields required', 'Pola oznaczone <span class="required">*</span> są wymagane') ?>.</p>
    <?= $form->errorSummary($model); ?>
    <div class="row">
      <?= $form->labelEx($model, 'name'); ?>
      <?= $form->textField($model, 'name', array('size' => 60, 'maxlength' => 128)); ?>
      <?= $form->error($model, 'name'); ?>
    </div>

    <div class="row">
      <?= $form->labelEx($model, 'surname'); ?>
      <?= $form->textField($model, 'surname', array('size' => 60, 'maxlength' => 128)); ?>
      <?= $form->error($model, 'surname'); ?>
    </div>

    <div class="row">
      <?= $form->labelEx($model, 'email'); ?>
      <?= $form->textField($model, 'email', array('size' => 60, 'maxlength' => 128)); ?>
      <?= $form->error($model, 'email'); ?>
    </div>
    <div class="row">
      <?= $form->labelEx($model, 'phone'); ?>
      <?= $form->textField($model, 'phone', array('size' => 60, 'maxlength' => 128)); ?>
      <?= $form->error($model, 'phone'); ?>
    </div>
    <div class="row">
      <?= $form->labelEx($model, 'password'); ?>
      <?= $form->passwordField($model, 'password', array('size' => 60, 'maxlength' => 40, 'value' => '')); ?>
      <?= $form->error($model, 'password'); ?>
    </div>
    <div class="row">
      <?= $form->labelEx($model, 'repeat_password'); ?>
      <?= $form->passwordField($model, 'repeat_password', array('size' => 60, 'maxlength' => 40, 'value' => '')); ?>
      <?= $form->error($model, 'password'); ?>
    </div>
    <div class="row">
      <?= $form->labelEx($model, 'role'); ?>
      <?= $form->dropDownList($model, 'role', array('' => '') + User::model()->findByPk(Yii::app()->user->id)->getPossibleRoles()) ?>
      <?= $form->error($model, 'role'); ?>
    </div>
    <div class="row">
      <?= $form->labelEx($model, 'status'); ?>
      <?= $form->dropDownList($model, 'status', ZHtml::enumItem($model, 'status')) ?>
      <?= $form->error($model, 'status'); ?>
    </div>

    <h1>Dane do faktury</h1>
    <div class="row">
      <?= $form->labelEx($model, 'company_name'); ?>
      <?= $form->textField($model, 'company_name'); ?>
      <?= $form->error($model, 'company_name'); ?>
    </div>
    <div class="row">
      <?= $form->labelEx($model, 'nip'); ?>
      <?= $form->textField($model, 'nip'); ?>
      <?= $form->error($model, 'nip'); ?>
    </div>
    <div class="row">
      <?= $form->labelEx($model, 'postcode'); ?>
      <?= $form->textField($model, 'postcode'); ?>
      <?= $form->error($model, 'postcode'); ?>
    </div>
    <div class="row">
      <?= $form->labelEx($model, 'city'); ?>
      <?= $form->textField($model, 'city'); ?>
      <?= $form->error($model, 'city'); ?>
    </div>
    <div class="row">
      <?= $form->labelEx($model, 'street'); ?>
      <?= $form->textField($model, 'street'); ?>
      <?= $form->error($model, 'street'); ?>
    </div>

    <h1>Dane do wysyłki</h1>
    <p class="note">Jeśli inne niż dane do faktury</p>
    <div class="row">
      <?= $form->labelEx($model, 'post_postcode'); ?>
      <?= $form->textField($model, 'post_postcode'); ?>
      <?= $form->error($model, 'post_postcode'); ?>
    </div>
    <div class="row">
      <?= $form->labelEx($model, 'post_city'); ?>
      <?= $form->textField($model, 'post_city'); ?>
      <?= $form->error($model, 'post_city'); ?>
    </div>
    <div class="row">
      <?= $form->labelEx($model, 'post_street'); ?>
      <?= $form->textField($model, 'post_street'); ?>
      <?= $form->error($model, 'post_street'); ?>
    </div>

    <div class="row buttons">
      <?= CHtml::submitButton($model->isNewRecord ? Yii::t('cms', 'Utwórz nowego użytkownika') : Yii::t('cms', 'Zapisz zmiany')); ?>
    </div>
  <? $this->endWidget(); ?>
</div>