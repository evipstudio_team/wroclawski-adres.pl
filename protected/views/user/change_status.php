<?php
$this->breadcrumbs=array(
	Yii::t('cms', 'Użytkownicy') => $this->createUrl('/user/index'),
	$model->name." ".$model->surname." ".$model->email=>$this->createUrl('/user/view', array('id'=>$model->id)),
	Yii::t('cms', 'Edycja') => $this->createUrl('/user/update', array('id'=>$model->id)),
  Yii::t('cms', 'Zmień status')
);

$this->menu=array(
	array('label' => Yii::t('cms', 'Lista użytkowników'), 'url' => $this->createUrl('/user/index'), 'visible' => Yii::app()->user->checkAccess('User:Index')),
	array('label' => Yii::t('cms', 'Dodaj użytkownika'), 'url' => array('/user/create'), 'visible' => Yii::app()->user->checkAccess('User:Create')),
	array('label'=>Yii::t('cms', 'Karta użytkownika'), 'url'=>$this->createUrl('/user/view', array('id'=>$model->id)), 'visible' => Yii::app()->user->checkAccess('User:View')),
	array('label'=>Yii::t('cms', 'Zarządzaj użytkownikami'), 'url'=>array('admin'), 'visible'=>Yii::app()->user->checkAccess('User:Admin')),
);
?>

<h1><?= Yii::t('cms', 'Zmiana statusu użytkownika')?></h1>

<?php echo $this->renderPartial('_form_status', array('model'=>$model)); ?>