<?php
$this->breadcrumbs = array(
    Yii::t('cms', 'Użytkownicy') => $this->createUrl('/user/index'),
    Yii::t('cms', 'Zarządzaj użytkownikami'),
);

$this->menu = array(
    array('label' => Yii::t('cms', 'Lista użytkowników'), 'url' => $this->createUrl('/user/index'), 'visible' => Yii::app()->user->checkAccess('User:Index')),
    array('label' => Yii::t('cms', 'Dodaj użytkownika'), 'url' => array('/user/create'), 'visible' => Yii::app()->user->checkAccess('User:Create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('user-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1><?= Yii::t('cms', 'Zarządzaj użytkownikami') ?></h1>

<p>
  Możesz opcjonalnie wykorzystać operatory porównań (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
  lub <b>=</b>) na początku każdej wyszukiwanej wartości aby wyznaczyć sposób porównania.
</p>

<?php echo CHtml::link(Yii::t('cms', 'Zaawansowane wyszukiwanie'), '#', array('class' => 'search-button')); ?>
<div class="search-form" style="display:none">
  <?php
  $this->renderPartial('_search', array(
      'model' => $model,
  ));
  ?>
</div><!-- search-form -->

<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'user-grid',
    'cssFile' => Yii::app()->theme->baseUrl.'/css/gridview/gridview.css',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        'id',
        'name',
        'surname',
        'email',
        array('name'=>'status', 'filter'=>false),
        array(
            'class' => 'CButtonColumn',
            'template' => '{view}{update}{delete}',
            'buttons' => array(
                'view' => array(
                    'url' => 'Yii::app()->createUrl("user/view", array("id"=>$data->id))',
                    'visible' => 'Yii::app()->user->checkAccess("User:View")'
                ),
                'update' => array(
                    'url' => 'Yii::app()->createUrl("user/update", array("id"=>$data->id))',
                    'visible' => 'Yii::app()->user->checkAccess("User:Update")'
                ),
                'delete' => array(
                    'url' => 'Yii::app()->createUrl("user/delete", array("id"=>$data->id))',
                    'visible' => 'Yii::app()->user->checkAccess("User:Delete")'
                ),
            )
        ),
    ),
));
?>
