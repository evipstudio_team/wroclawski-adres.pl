<?
foreach(PageRelations::getNodes($page->id) as $pageRelation) {
  $parent = $pageRelation->parent;
  if($parent->parent_id)
  $this->breadcrumbs = array_merge($this->breadcrumbs, array(Yii::t('cms', $parent->name)=>$this->createUrl($parent->module->controller.'/index',array('page_id'=>$parent->id))));
}
?>