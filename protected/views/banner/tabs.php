<?$this->renderPartial('../shared/_breadcrumbs', array('page' => $page));?>
<?$this->breadcrumbs = array_merge($this->breadcrumbs, array($this->pageTitle));?>
<?$this->renderPartial('_leftMenu', array('page' => $page));?>

<?
$this->widget('CTabView', array(
    'cssFile' => Yii::app()->theme->baseUrl.'/css/jquery.yiitab.css',
    'tabs' => $tabs,
    'activeTab'=>Yii::app()->getController()->action->id,
    'viewData'=>  isset($additionalParams)? array_merge($additionalParams,array('banner'=>$banner)):array('banner'=>$banner)
))
?>