<?= $this->renderPartial('//shared/_flash')?>
<div class="form">
<div style="margin-top:15px">
  <legend><strong>Grupy odbiorców wiadomości</strong></legend>
<div class="nsltt_group">
  <?foreach($groups as $index=>$group):?>
    <?$id = 'group_'.$group->id?>
    <span style="white-space: nowrap; margin-right: 10px;">
      <?= $group->name?> - <input type="checkbox" onclick="jQuery.ajax({
        'url':'<?=$this->createUrl('newsletter/switchNewsletterGroup',array('newsletter_id'=>$newsletter->id,'group_id'=>$group->id))?>',
        'success':function(data) {
          $('#newsletterGroups').html(data);
          $.fn.yiiGridView.update('recipients-grid');
        }
      })" value="<?= $group->id?>"<?if(NewsletterGroup::model()->findByPk(array('newsletter_id'=>$newsletter->id,'group_id'=>$group->id))):?>checked="checked"<?endif?> />
    </span>
  <?endforeach?>
</div>
</div>
  </div>