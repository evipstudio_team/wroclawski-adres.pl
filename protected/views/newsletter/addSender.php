<div class="form">
  <?if($sender->getIsNewRecord()==false):?>
    <ul class="flashes" style="list-style: none; margin-top: 20px">
      <li><div class="flash-success"><?= Yii::t('cms', 'Odbiorca został dodany.') ?></div></li>
    </ul>
    <?
      $cs = Yii::app()->getClientScript();
      $cs->registerScript(
        'rehresfSenderIdSelect','
          $(\'#AddSenderDialog\').dialog( "option", "buttons", { "Zamknij": function() { $(this).dialog("close"); } } );
          $.ajax({
            \'url\':\''.$this->createUrl('newsletter/SenderSelect',array('id'=>$newsletter->id,'selectedId'=>$sender->id)).'\',
              \'success\':function(data) {$(\'#senderSelect\').html(data)}
          })
        ',
        CClientScript::POS_END
      );
    ?>
  <?else:?>
    <? $form = $this->beginWidget('CActiveForm',array('action'=>$this->createUrl('newsletter/addSender',array('id'=>$newsletter->id)),'id'=>'AddSenderForm')); ?>
    <?= $this->renderPartial('_senderForm',array('form'=>$form,'sender'=>$sender))?>
    <? $this->endWidget(); ?>
  <?endif?>
</div>