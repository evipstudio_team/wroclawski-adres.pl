<?$this->renderPartial('../shared/_breadcrumbs', array('page' => $page));?>
<?$this->breadcrumbs = array_merge($this->breadcrumbs, array($this->pageTitle));?>
<?$this->renderPartial('_leftMenu',array('page'=>$page));?>
<div class="form">
  <? $form = $this->beginWidget('CActiveForm'); ?>
  <?= $form->errorSummary($newsletter)?>
  <div class="row">
		<?= $form->labelEx($newsletter,'page_id'); ?>
    <?= $form->dropDownList($newsletter,'page_id',  CHtml::listData(Page::model()->findAll(array('with'=>array('module'),'condition'=>'module.name=:moduleName','params'=>array(':moduleName'=>'Newsletters'))), 'id', 'name','parent.name'))?>
		<?= $form->error($newsletter,'page_id'); ?>
	</div>
  <div class="row">
		<?= $form->labelEx($newsletter,'title'); ?>
		<?= $form->textField($newsletter,'title', array('size'=>60))?>
		<?= $form->error($newsletter,'title'); ?>
	</div>
	<div class="button_bar">
  <div class="button_add">
		<?php echo CHtml::submitButton(Yii::t('cms', 'Zapisz')); ?>
	</div>
	</div>
  <? $this->endWidget(); ?>
</div>