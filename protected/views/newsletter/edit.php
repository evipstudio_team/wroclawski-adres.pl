<div class="form">
  <? $form = $this->beginWidget('CActiveForm'); ?>
  <?= $form->errorSummary($newsletter)?>
  <div id="senderSelect">
    <div class="row">
      <?= $form->labelEx($newsletter,'sender_id'); ?>
      <?= $this->renderPartial('senderSelect',array('newsletter'=>$newsletter))?>
      <?= $form->error($newsletter,'sender_id'); ?>
    </div>
  </div>
  <div class="row">
		<?= $form->labelEx($newsletter,'page_id'); ?>
    <?= $form->dropDownList($newsletter,'page_id',  CHtml::listData(Page::model()->findAll(array('with'=>array('module'),'condition'=>'module.name=:moduleName','params'=>array(':moduleName'=>'Newsletters'))), 'id', 'name','parent.name'))?>
		<?= $form->error($newsletter,'page_id'); ?>
	</div>
  <div class="row">
		<?= $form->labelEx($newsletter,'title'); ?>
		<?= $form->textField($newsletter,'title', array('size'=>60))?>
		<?= $form->error($newsletter,'title'); ?>
	</div>
  <div class="row">
		<?= $form->labelEx($newsletter,'body_html'); ?>
    <?$this->widget('ext.editMe.ExtEditMe', array(
      'model' => $newsletter,
      'attribute' => 'body_html',
      'width'=>'650',
      'htmlOptions' => array('rows' => '30', 'cols' => '30'),
      "filebrowserImageUploadUrl"=>$this->createUrl("site/fileUploader"),
      "filebrowserImageBrowseUrl"=>$this->createUrl("site/fileUploader"),
      'toolbar' => array(
          array('Templates',),
          array('PasteFromWord', 'Undo', 'Redo',),
          array('Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript'),
          array('NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'),
          array('Link', 'Unlink'),
          array('Image', 'Table', 'HorizontalRule', 'SpecialChar'),
          array('Format', 'FontSize',),
          array('TextColor', 'BGColor'),
          array('Maximize', 'About', 'Source')
          )
      ));?>
    <?= $form->error($newsletter,'body_html'); ?>
	</div>
  <div class="row">
		<?= $form->labelEx($newsletter,'body_text'); ?>
    <?$this->widget('ext.editMe.ExtEditMe', array(
      'model' => $newsletter,
      'attribute' => 'BodyTextWithBr',
      'htmlOptions' => array('name' => 'Newsletter[body_text]','rows' => '30', 'cols' => '30'),
      'width'=>'650',
      'toolbar' => array(
          array('PasteFromWord', 'Undo', 'Redo',),
          )
      ));?>
    <?= $form->error($newsletter,'body_text'); ?>
	</div>
	<div class="button_bar">
  <div class="button_add">
		<?php echo CHtml::submitButton(Yii::t('cms', 'Zapisz')); ?>
	</div>
	</div>
  <? $this->endWidget(); ?>
</div>

<?
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id'=>'AddSenderDialog',
    'options'=>array(
        'autoOpen'=>false,
        'title'=>  Yii::t('cms', 'Dodaj nowego nadawcę wiadomości'),
        'modal'=>true,
        'width'=>'900px',
        'open'=>'js:function(){
          jQuery.ajax({
            "url": "'.$this->createUrl('newsletter/addSender',array('id'=>$newsletter->id)).'",
            "success":function(data){
              $(\'#AddSenderDialog\').html($(data));
            }
          });
          return false;}',
        'buttons'=>array(
            Yii::t('cms', 'Zamknij')=>'js:function(){$(this).dialog("close")}',
              Yii::t('cms','Dodaj')=>'js:function(){
                $(\'#AddSenderDialog\').prepend(\'<div style="text-align: center;"><img src="'.Yii::app()->theme->baseUrl.'/images/loading.gif" alt="" title="" /> - proszę czekać...</div><br /><br />\');
                jQuery.ajax({
                  "url":$(\'#AddSenderForm\').attr(\'action\'),
                  "type":"POST",
                  "data":$(\'#AddSenderForm\').serializeArray(),
                  "success":function(data){
                    $(\'#AddSenderDialog\').html(data);
                  }
                })
              }',
            )
    ))
);

$this->endWidget('zii.widgets.jui.CJuiDialog');
?>