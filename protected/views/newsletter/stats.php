<?
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'recipients-grid',
    'cssFile' => Yii::app()->theme->baseUrl.'/css/gridview/gridview.css',
    'dataProvider' => $newsletterRecipient->search(),
    'filter' => $newsletterRecipient,
    'columns' => array(
        array(
            'name'=>  Yii::t('cms', 'Adres email'),
            'value'=>'$data->recipient->email',
            'filter'=>  CHtml::activeTextField($recipient, 'email')
        ),
        array(
            'name'=>  Yii::t('cms', 'Grupa(y) z której pochodzi odbiorca'),
            'value'=>'implode(\', \',CHtml::listData($data->recipient->groups(array(\'condition\'=>\'`id` IN ('.implode(',', CHtml::listData($newsletter->groups, 'id', 'id')).')\')),\'id\',\'name\'))',
            'filter'=>  CHtml::activeDropDownList($newsletterGroup, 'group_id', array(''=>'')+CHtml::listData($newsletter->groups, 'id', 'name'))
        ),
        array(
            'name'=>'sent_at',
            'value'=>'$data->sentAtNormalized',
            'filter'=>CHtml::activeDropDownList($newsletterRecipient, 'sent_at',  array(''=>'','notSended'=>'Niewysłana','sended'=>'Wysłana'))
        )

    ),
));
?>