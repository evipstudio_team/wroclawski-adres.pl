Witamy,

Twoje konto w serwisie <?= Yii::app()->name?> zostało utworzone.
Poniżej podane zostały Twoje dane logowania, w tym hasło (dla bezpieczeństwa niezwłocznie po zalogowaniu zmień je w zakładce moje dane):

login: <?= $login?>

hasło: <?= $password?>


Ten e-mail został wygenerowany automatycznie.
<?= CConsoleCommand::renderFile(Yii::app()->basePath.'/views/email_templates/_footer.php',array(),true);?>