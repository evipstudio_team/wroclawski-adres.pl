Witaj,
dokonaliśmy edycji zdarzenia typu <?= $event->translateEnumValue('type')?>.
Poniżej szczegółowe informacje (ukazujące aktualny stan zdrarzenia), ewentualne dokumenty zostały dołączone w formie załączników.

Data: <?= date('H:i d/m/Y',strtotime($event->date))?>

Typ: <?= $event->translateEnumValue('type')?>

<?if($event->comment):?>
Notatka:
  <?= $event->comment?>
<? else: ?>
Brak notatki
<? endif; ?>


<?= CConsoleCommand::renderFile(Yii::app()->basePath.'/views/email_templates/_footer.php',array(),true);?>


Ta wiadomość została wygenerowana automatycznie, możesz bezpośrednio skontaktować się z Nami w tej sprawie odpowiadając na tą wiadomość.