Witaj,
przez formularz umieszczony na stronie <?= Yii::app()->name?> zostały wysłane następujące informacje:

Imię: <?= $contactForm->name?>

Telefon: <?= $contactForm->phone?>

Email: <?= $contactForm->email?>

Treść: <?= $contactForm->body?>



Możesz bezpośrednio skontaktować się z tą osobą wysyłając odpowiedź na tą wiadomość.
<?= CConsoleCommand::renderFile(Yii::app()->basePath.'/views/email_templates/_footer.php',array(),true);?>