Witamy,

aby dokończyć proces resetowania hasła, prosimy kliknąć ten link (lub skopiować go do okna przeglądarki):
<a href="<?=Yii::app()->createAbsoluteUrl('/site/ServeConfirmationData', array('checksum'=>$checksum))?>"><?=Yii::app()->createAbsoluteUrl('/site/ServeConfirmationData', array('checksum'=>$checksum))?></a>

<?= CConsoleCommand::renderFile(Yii::app()->basePath.'/views/email_templates/_footer.php',array(),true);?>