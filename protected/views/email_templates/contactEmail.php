Witaj,
przez formularz umieszczony na stronie <?= Yii::app()->name?> zostały wysłane następujące informacje:

<?if(isset($params['varables']) && $params['varables']):?>
<?foreach($params['varables'] as $name=>$value):?>
<?= $name?>:<?="\n\t"?><span style="margin-left: 10px"></span><?=$value?>


<?endforeach?>
<?endif?>

Ewentualne załączniki zostały dodane do tej wiadomości.
Możesz bezpośrednio skontaktować się z tą osobą wysyłając odpowiedź na tą wiadomość.
<?= CConsoleCommand::renderFile(Yii::app()->basePath.'/views/email_templates/_footer.php',array(),true);?>