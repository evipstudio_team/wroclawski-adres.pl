<div class="form">
  <p class="note">
    Panel aktywacji konta użytkownika.
  </p>
</div>
<?=$this->renderPartial('//shared/_flash',array('dialogBox'=>false))?>
<?if($cause->user->status=='new'):?>
  <p style="margin-top: 10px">
    Użytkownik zostanie poinformowany mailowo o aktywacji Jego konta, wraz z informacją o wygenerowanym haśle.
  </p>
  <p style="margin-top: 10px">
    Kliknij przycisk "Aktywuj" aby aktywować konto.
  </p>

  <script type="text/javascript">
    $("#DialogBox").dialog({ buttons: {
        Aktywuj: function() {
          $('#LoadingDialogBox').dialog('open');
          $.ajax({
            url : '<?=$this->createUrl('cause/activateUser',array('id'=>$cause->id))?>',
            data: 'activate=true',
            type: 'post',
            success: function(data) {
              $('#DialogBox').html(data);
              $('#LoadingDialogBox').dialog('close');
            }
          });
        },
        Zamknij: function() { $( this ).dialog( "close" ); }
      } });
  </script>
<?else:?>
  <p style="margin-top: 10px">
    Konto użytkownika jest aktywne.
  </p>
  <script type="text/javascript">
    $("#DialogBox").dialog({ buttons: {
        Zamknij: function() { $( this ).dialog( "close" ); }
      } });
  </script>
<? endif; ?>
