<div class="form">
  <? $form = $this->beginWidget('CActiveForm', array('id' => 'commentForm'));?>
    <?=$form->errorSummary($model); ?>
    <div class="row">
      <?= $form->labelEx($model, 'step'); ?>
      <?= $form->dropDownList($model, 'step', array(''=>'')+$nextSteps); ?>
    </div>
    <div class="row">
      <?= $form->labelEx($model, 'sendEmail'); ?>
      <?= $form->dropDownList($model,'sendEmail',array(''=>'',1=>'tak',2=>'nie'));?>
    </div>
    <div class="row">
      <?= $form->labelEx($model, 'comment'); ?>
      <?= $form->textArea($model, 'comment', array('rows' => 8, 'cols' => 40, 'style' => 'width:90%')); ?>
    </div>

  <? $this->endWidget(); ?>
</div>