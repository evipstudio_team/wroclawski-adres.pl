<?$this->renderPartial('../shared/_breadcrumbs', array('page' => $page));?>
<?$this->breadcrumbs = array_merge($this->breadcrumbs,
        //array($page->getName(1)=>$this->createUrl('article/index',array('page_id'=>$page->id))),
        array($this->pageTitle)
        );
?>

<?Yii::app()->getClientScript()->registerCoreScript( 'jquery.ui' );?>
<?Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/helpers.js');?>
<?
$str_js = "
installSortable('articles-grid','".$this->createUrl('//article/sort')."');
function reInstallSortable(id, data) {
  installSortable('articles-grid','".$this->createUrl('//article/sort')."');
}
";
Yii::app()->clientScript->registerScript('sortable-project', $str_js);
?>
<?
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'articles-grid',
    'dataProvider' => $searchPage->searchArticles(),
    'filter' => $searchPage,
    'rowCssClassExpression'=>'"items_{$data->id}"',
    'summaryText'=>summaryTextLayout('articles-grid'),
    'columns' => array(
        array(
          'class' => 'CCheckBoxColumn',
          'header' => 'Akcja',
          'selectableRows'=>2,
          'headerTemplate'=>'{item}',
          'value'=>'$data->id',
          'footer'=>'
            <select onchange="if($(this).val()==\'delete\'){gridDeleteElements([\'articles-grid\'], \'articles-grid_c0[]\',\''.$this->createUrl('page/deleteByIds').'\');}">
              <option></option>
              <option value="delete">Usuń</option>
            </select>
          ',
          'htmlOptions'=>array('class'=>'center'),
        ),
        array(
            'name'=>'id',
            'htmlOptions'=>array('style'=>'width: 20px')
        ),
        gridViewsortIcon(),
        array(
            'name'=>Yii::t('cms', 'Tytuł'),
            'value'=>'$data->articles[0]->title',
            'filter'=>false
        ),
//        array(
//            'name'=>Yii::t('cms', 'Krótki opis'),
//            'value'=>'$data->articles[0]->short_content? $data->articles[0]->short_content:\'-\'',
//            'filter'=>false
//        ),
        array(
            'name'=>Yii::t('cms', 'Ilość plików'),
            'value'=>'$data->filesCount',
            'filter'=>false
        ),
//        array(
//            'name'=>Yii::t('cms', 'Meta Title'),
//            'value'=>'$data->urls? $data->urls[0]->title:\'-\'',
//            'filter'=>false
//        ),
//        array(
//            'name'=>Yii::t('cms', 'Meta Description'),
//            'value'=>'$data->urls? $data->urls[0]->description:\'-\'',
//            'filter'=>false
//        ),
//        array(
//            'name'=>Yii::t('cms', 'Meta Keywords'),
//            'value'=>'$data->urls? $data->urls[0]->keywords:\'-\'',
//            'filter'=>false
//        ),
        array(
            'header'=>  Yii::t('cms', 'Powiązania'),
            'value'=>'implode(\', \',CHtml::listData($data->blocks,\'name\',\'name\'))',
            'cssClassExpression'=>'$data->id',
            'htmlOptions'=>array('onclick'=>'$(\'#AssignedBlocksDialog\').data(\'element_id\', $(this).attr(\'class\').replace(/[^0-9]+/g,\'\'));$(\'#AssignedBlocksDialog\').dialog(\'open\'); $(".ui-dialog-titlebar").hide();', 'style'=>'cursor: pointer;','class'=>'clickable','title'=>  Yii::t('cms', 'Kliknij aby edytować')),
            'filter'=>  CHtml::activeDropDownList($block, 'id', array(''=>'')+CHtml::listData(Block::model()->findAll(), 'id', 'name')),
            'type'=>'raw'
        ),
        array(
          'class' => 'CCheckBoxColumn',
          'selectableRows'=>2,
          'headerTemplate'=>'Występuje w porównaniu',
          'value'=>'$data->id',
          'checked'=>'($data->price_packege)? true:false',
          'htmlOptions'=>array('class'=>'center'),
          'visible'=>(($page->id==Yii::app()->params['packegesPageId'] || $page->id==Yii::app()->params['pricePageId'])? true:false),
          'checkBoxHtmlOptions'=>array('class'=>'switchAsPackegesPage')
        ),
        array(
            'class' => 'SButtonColumn',
            'template' => '{status}',
            'header'=>  Yii::t('cms', 'Status'),
            'htmlOptions'=>array('class'=>'center'),
            'buttons' => array(
                'status' => array(
                    'expressionImage' => true,
                    'expressionLabel' => true,
                    'label' => 'getStatusExpression($data->status)',
                    'type' => 'html',
                    'url' => 'Yii::app()->createUrl("page/statusSwitch", array("id"=>$data->id))',
                    'imageUrl' => 'getStatusIcon2($data->status)',
                    'click' => "function(event) {
                      event.preventDefault();
                      loadingBoxOpen();
                      var element = $(this);
                      $.ajax({
                        url: $(this).attr('href'),
                        success: function(data) {
                          $.fn.yiiGridView.update('articles-grid');
                          loadingBoxClose();
                        }
                      });
                      return false;}
                      ",
                ),
            )
        ),
        array(
            'class' => 'SButtonColumn',
            'template' => '{update}',
            'header'=>  Yii::t('cms', 'Edycja'),
            'htmlOptions'=>array('class'=>'center'),
            'buttons' => array(
                'update' => array(
                    'label' => Yii::t('cms', 'Edycja artykułu'),
                    'url' => 'Yii::app()->createUrl("article/edit", array("id"=>$data->id))',
                ),
            )
        ),
        array(
            'class' => 'SButtonColumn',
            'template' => '{delete}',
            'header'=>  Yii::t('cms', 'Usuń'),
            'htmlOptions'=>array('class'=>'center'),
            'buttons' => array(
                'delete' => array(
                    'label' => Yii::t('cms', 'Usuń artykuł'),
                    'url' => 'Yii::app()->createUrl("article/deleteAll", array("page_id"=>$data->id))',
                ),
            )
        ),

    ),
));
?>

<?$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id'=>'AssignedBlocksDialog',
    'options'=>array(
        'title'=>  '',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>'500',
        'open' => 'js:function(){
            jQuery.ajax({
              "url": "'.$this->createUrl('article/blocks').'/ajax/true/id/"+$(this).data(\'element_id\'),
              "success":function(data){
                $(\'#AssignedBlocksDialog\').html($(data));
              }
            });
          return false;}',
        'close'=>'js:function(){
            $.fn.yiiGridView.update(\'articles-grid\');}',
        'buttons'=>array(
            Yii::t('cms', 'Zamknij')=>'js:function(){$(this).dialog("close");}',
        )
    ),
));?>
<?$this->endWidget('zii.widgets.jui.CJuiDialog');?>

<script type="text/javascript">
  $(function() {
    $('.switchAsPackegesPage').change(function(){
      loadingBoxOpen();
      $.ajax({
        url: '<?=$this->createUrl('page/switchPricePackege')?>/id/'+$(this).attr('value'),
        success: function(data) {
          loadingBoxClose();
        }
      })
    });
  });
</script>