<? Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/helpers.js'); ?>
<?$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'products-grid',
    'dataProvider' => $product->searchProducts(),
    'enablePagination'=>false,
    'rowCssClassExpression'=>'"item_{$data->id}"',
    'afterAjaxUpdate'=>'function() {reInstallElements()}',
    'summaryText'=>'',
    'columns' => array(
        array(
            'name'=>'id',
            'sortable'=>false,
            'htmlOptions'=>array('style'=>'width: 30px'),
        ),
        array(
            'header'=>'Tytuł',
            'value'=>'$data->article->title',
            'sortable'=>false,
            'htmlOptions'=>array('style'=>'width: 30px'),
        ),
        array(
            'header'=>'Ilość darmowych jednostek w pakiecie',
            'sortable'=>false,
            'htmlOptions'=>array('style'=>'width: 30px'),
            'value'=>'($data->pickUpPackege('.$page->id.'))? number_format($data->productPackege->free_limit,\'0\',\'\',\' \'):\'\'',
            'visible'=>'($data->pickUpPackege('.$page->id.'))? true:false'
        ),
        array(
            'header'=>'Cena jednostkowa po przekroczeniu',
            'sortable'=>false,
            'htmlOptions'=>array('style'=>'width: 30px'),
            'value'=>'($data->pickUpPackege('.$page->id.'))? number_format($data->productPackege->unit_price,\'2\',\',\',\' \'). \' zł\':\'\'',
            'visible'=>'($data->pickUpPackege('.$page->id.'))? true:false'
        ),
        array(
          'class' => 'CCheckBoxColumn',
          'selectableRows'=>2,
          'headerTemplate'=>'',
          'value'=>'$data->id',
          'checked'=>'(ProductPackege::model()->findByPk(array(\'product_id\'=>$data->id,\'packege_id\'=>'.$page->id.')))? true:false',
          'htmlOptions'=>array('class'=>'center'),
          'checkBoxHtmlOptions'=>array('class'=>'assignProduct')
        ),
    ),
));
?>

<?$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id'=>'DialogBox',
    'options'=>array(
        'title'=>  '',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>'500',
        'open' => 'js:function(){
          $(\'#LoadingDialogBox\').dialog(\'open\');
            $(this).dialog("option","buttons","");
            jQuery.ajax({
              url: $(this).data(\'url\'),
              success:function(data){
                $(\'#DialogBox\').html(data);
                $(\'#LoadingDialogBox\').dialog(\'close\');
              }
            });
          return false;}',
        'close'=>'js:function(){
          $.fn.yiiGridView.update(\'products-grid\');
          $(this).dialog("option","buttons","");
          }',
    ),
));?>
<?$this->endWidget('zii.widgets.jui.CJuiDialog');?>

<script type="text/javascript">
  function reInstallElements() {
    $('.assignProduct').change(function() {
      var productId = $(this).val();
      $('#DialogBox').data('url','<?=$this->createUrl('article/assignProduct')?>/packegeId/<?=$page->id?>/productId/'+productId);
      $('#DialogBox').dialog('open');
    });
  }
  $(function() {
    reInstallElements();
  });
</script>