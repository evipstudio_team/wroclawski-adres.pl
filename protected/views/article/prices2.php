<div class="form">
  <? $form = $this->beginWidget('CActiveForm'); ?>
    <div class="row">
      <?= $form->labelEx($page,'0_months_price'); ?>
      <?= $form->textField($page, '0_months_price') ?>
      <?= $form->error($page,'0_months_price'); ?>
    </div>
    <div class="row">
      <?= $form->labelEx($page,'6_months_price'); ?>
      <?= $form->textField($page, '6_months_price') ?>
      <?= $form->error($page,'6_months_price'); ?>
    </div>
    <div class="row">
      <?= $form->labelEx($page,'12_months_price'); ?>
      <?= $form->textField($page, '12_months_price') ?>
      <?= $form->error($page,'12_months_price'); ?>
    </div>
    <div class="row">
      <?= $form->labelEx($page,'individual_price'); ?>
      <label>Pole to przyjmuję treść jako format danych, np. "od 250 zł"</label>
      <?= $form->textField($page, 'individual_price') ?>
      <?= $form->error($page,'individual_price'); ?>
    </div>
    <div class="button_bar">
      <div class="button_add">
        <?php echo CHtml::submitButton(Yii::t('cms', 'Zapisz')); ?>
      </div>
    </div>
  <? $this->endWidget(); ?>
</div>