<div class="form">
  <? $form = $this->beginWidget('CActiveForm'); ?>
  <? if (count(Yii::app()->params['langs']) > 1): ?>
    <? $tabs = array() ?>
    <? foreach (Yii::app()->params['langs'] as $lang): ?>
      <? $url = $this->createUrl('article/edit', array('id' => $article->page_id, 'lang_id' => $lang->id)); ?>
      <? $title = getLangIcon(Article::model()->findByPk(array('page_id'=>$article->page_id,'lang_id'=>$lang->id)), $lang) . ' ' . Yii::t('cms', 'Język') . ' ' . $lang->name; ?>
      <? if ($lang->id == $article->lang_id): ?>
        <?
        $tabs['ArticleTranslation_' . $lang->id] = array(
            'title' => $title,
            'view' => '_content_edit',
            'data' => array('model' => $article, 'form' => $form, 'lang' => $lang, 'showSubmit' => true)
                )
        ?>
      <? else: ?>
        <?
        $tabs['ArticleTranslation_' . $lang->id] = array(
            'title' => $title,
            'url' => CHtml::encode($url),
                )
        ?>
      <? endif ?>
    <? endforeach ?>
    <? $this->widget('CTabView', array('tabs' => $tabs, 'activeTab' => 'ArticleTranslation_' . $article->lang_id)); ?>

  <? else: ?>
    <?= $this->renderPartial('_content_edit', array('model' => $article, 'form' => $form, 'lang' => Yii::app()->params['lang'])) ?>
  <? endif ?>
  <? $this->endWidget(); ?>
</div>
