<?=$this->renderPartial('//shared/_flash',array('dialogBox'=>false))?>
<script type="text/javascript">
  $( "#DialogBox" ).dialog( "option", "width", 'auto');
  $( "#DialogBox" ).dialog( "option", "height", 'auto');
  $( "#DialogBox" ).dialog( "option", "position", 'center');
  $( "#DialogBox" ).dialog( "option", "buttons", [
      {
        text: "Zamknij",
        click: function() {
          $( this ).dialog( "close" );
        }
      },
    ]);
</script>