<? Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/helpers.js'); ?>
<?$this->renderPartial('../shared/_breadcrumbs', array('page' => $page));?>
<?$this->breadcrumbs = array_merge(
        $this->breadcrumbs,
        array($page->getName(1)=>$this->createUrl('article/edit',array('id'=>$page->id,'lang_id'=>(isset($article))? $article->lang_id:Yii::app()->params['lang']->id))),
        array($this->pageTitle)
);?>

<div class="form">
  <?if(!$page->status):?>
    <p class="warning">Artykuł jest w tej chwili nieaktywny. Aby aktywować kliknij: <?= CHtml::link(Yii::t('cms', 'Aktywuj artykuł'), $this->createUrl('article/statusSwitch',array('id'=>$page->id,'confirm'=>true)))?></p>
  <?endif?>
</div>
<?
$this->widget('CTabView', array(
    'tabs' => $tabs,
    'activeTab'=>Yii::app()->getController()->action->id,
    'viewData'=>  isset($additionalParams)? array_merge($additionalParams,array('article'=>$article)):array('article'=>$article)
))
?>