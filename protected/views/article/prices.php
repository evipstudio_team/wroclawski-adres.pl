<div class="form">
  <? $form = $this->beginWidget('CActiveForm'); ?>
    <div class="row">
      <?= $form->labelEx($page,'unit_price'); ?>
      <?= $form->textField($page, 'unit_price') ?>
      <?= $form->error($page,'unit_price'); ?>
    </div>
    <script type="text/javascript">
      $(function() {
        $('#Page_unit_price').change(function(){
          $(this).val(validateCostPerDay($(this).val()));
        });
      });
    </script>
    <div class="button_bar">
      <div class="button_add">
        <?php echo CHtml::submitButton(Yii::t('cms', 'Zapisz')); ?>
      </div>
    </div>
  <? $this->endWidget(); ?>
</div>