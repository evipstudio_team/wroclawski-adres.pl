<div class="form">
  <?php $form = $this->beginWidget('CActiveForm', array('action' => $this->createUrl('article/assignProduct', array('packegeId' => $productPackege->packege_id, 'productId' => $productPackege->product_id)), 'id' => 'assignProductForm')); ?>
  <div class="row">
    <?= $form->labelEx($productPackege, 'free_limit'); ?>
    <?= $form->textField($productPackege, 'free_limit') ?>
    <?= $form->error($productPackege, 'free_limit'); ?>
  </div>
  <div class="row">
    <?= $form->labelEx($productPackege, 'unit_price'); ?>
    <?= $form->textField($productPackege, 'unit_price') ?>
    <?= $form->error($productPackege, 'unit_price'); ?>
  </div>
  <script type="text/javascript">
    $(function() {
      $('#ProductPackege_unit_price').change(function(){
        $(this).val(validateCostPerDay($(this).val()));
      });
      $('#ProductPackege_free_limit').change(function(){
        $(this).val($(this).val().replace(/[^0-9]/g,''));
      });

      $('#DialogBox').dialog("option","buttons",[
        {
          text: "Zamknij",
          click: function() {
            $(this).dialog('close');
          }
        },
        {
          text: "Zapisz",
          click: function() {
            $('#LoadingDialogBox').dialog('open');
            $.ajax({
              url: $('#assignProductForm').attr('action'),
              type: 'post',
              data: $('#assignProductForm').serializeArray(),
              success: function(data) {
                $( "#DialogBox" ).html(data);
                $('#LoadingDialogBox').dialog('close');
              }
            });
          }
        }
      ]);
    });
  </script>
  <?php $this->endWidget(); ?>
</div>