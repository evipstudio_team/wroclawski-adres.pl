  <?= $form->errorSummary($model); ?>
  <div class="row">
    <?=$form->labelEx($model,'title', array('class'=>'input_label'))?>
    <?=$form->textField($model, 'title', array('size'=>60)) ?>
    <?=$form->error($model,'title'); ?>
  </div>
  <div class="row">
    <?=$form->labelEx($model,'content', array('class'=>'input_label'))?>
    <?
    $this->widget('ext.editMe.ExtEditMe', array(
        'model' => $model,
        'attribute' => 'content',
        'htmlOptions' => array('rows' => '30', 'cols' => '30'),
        "filebrowserImageUploadUrl"=>$this->createUrl("site/fileUploader"),
        "filebrowserImageBrowseUrl"=>$this->createUrl("site/fileUploader"),
        'toolbar' => array(
            array('Templates',),
            array('PasteFromWord', 'Undo', 'Redo',),
            array('Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript'),
            array('NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'),
            array('Link', 'Unlink'),
            array('Image', 'Table', 'HorizontalRule', 'SpecialChar'),
            array('Format', 'FontSize',),
            array('TextColor', 'BGColor'),
            array('Maximize', 'About', 'Source')
        )
    ));
    ?>
  </div>
  <div class="row">
    <?=$form->labelEx($model,'short_content', array('class'=>'input_label'))?>
    <?=$form->textArea($model, 'short_content', array('rows'=>8,'cols'=>60)) ?>
    <?=$form->error($model,'short_content'); ?>
  </div>
  <div class="row">
    <?= $form->hiddenField($model,'lang_id')?>
  </div>
<div class="button_bar">
  <?if($model->getIsNewRecord()==false):?>
    <div class="button_delete">
		  <?= CHtml::link(Yii::t('cms', 'Usuń treść w języku').' '.$lang->getNameVariety(), $this->createUrl('article/delete',array('id'=>$model->page_id,'lang_id'=>$model->lang_id)))?>
	  </div>
  <?endif?>
  <div class="button_add">
    <?php echo CHtml::submitButton(Yii::t('cms', 'Zapisz treść')); ?>
  </div>
</div>


