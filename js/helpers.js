$(function() {
  $('#Page_parent_id').click(function() {
    if($(this).val()=='PickUpPage')
      $('#newBlockDialog2').dialog('open');
  })
});

function setUrlActiveTab() {
  $('#url-form').find('ul.tabs li a').each(function(i,z) {
    if($(this).hasClass('active')) {
      $('#activeTab').val($(this).attr('href'));
    }
  });
}

function setActiveTab(form_id, activeTabId) {
  $('#'+form_id).find('ul.tabs li a').each(function(i,z) {
    if($(this).hasClass('active')) {
      $('#'+activeTabId).val($(this).attr('href'));
    }
  });
}

var fixHelper = function(event, ui) {
    $(ui).width($(ui).width());
    $(ui).height($(ui).height());
    ui.children().each(function(i) {
      $(ui).parent().parent().find('thead tr:nth-child(1) th:nth-child('+(i+1)+')').width($(this).width());
      $(this).width($(this).width());
    });
    return ui;
};

function installSortable(tableId,url) {
  $('#'+tableId+' table.items tbody').sortable({
    forcePlaceholderSize: true,
    axis: 'y',
    forceHelperSize: true,
    items: 'tr',
    start: function(e, ui ){
     ui.placeholder.height(ui.helper.outerHeight());
},
    update : function () {
      serial = $('#'+tableId+' table.items tbody').sortable('serialize', {
        key: 'items[]',
        attribute: 'class'
      });
      var page = parseInt($('#'+tableId+' div.pager ul li.page.selected a').text());
      if(isNaN(page)) page = 0;
      else page = (page-1);
      var perPage = parseInt($('#'+tableId+' div.summary select[name=pageSize]').val());
      if(isNaN(perPage)) perPage = 0;
      var positionPrefix = (page*perPage);
      $.ajax({
        'url': url,
        'type': 'post',
        'data': serial+'&positionPrefix='+positionPrefix,
        'success': function(data){

        },
        'error': function(request, status, error){
          alert('We are unable to set the sort order at this time.  Please try again in a few minutes.');
        }
      });
    },
    helper: fixHelper
  }).disableSelection();
}

function installAssignedBlocks() {
  $('.AssignedBlocks').click(function(){
    var id = $(this).attr('id').replace(new RegExp('[^0-9]', 'g'),'');
    var element_name = $(this).attr('id').replace(new RegExp('[0-9_]', 'g'),'');
    $('#AssignedBlocksDialog').data({
      element_id: id,
      element_name: element_name
    }).dialog('open');
  });
}

function GroupsToSelectWhileRecipientsImport(url,groupsToSelect) {
  $.ajax({
    'url': url,
    'type':'post',
    'data':'groupsToSelect='+groupsToSelect,
    'success': function(data) {
      $('#GroupsToSelectWhileRecipientsImport').html(data);
    }
  });
}

function expandPages(element,id) {
  loadingBoxOpen();
  if($(element).hasClass('plus')) {
    jQuery.ajax({
      'type':'post',
      'url':$(element).attr('href'),
      'cache':false,
      'success':function(html){
        var new_data = $(html).find('tbody').html();
        $(element).parent().parent().after(new_data);
        $(element).removeClass('plus');
        $(element).addClass('minus');
        loadingBoxClose();
      }
    });
  }
  else
  if($(element).hasClass('minus')) {
    var startElement = $(element).parent().parent();
    var toDelete = [];
    while($(startElement).next().hasClass('expanded')) {
      startElement = $(startElement).next();
      toDelete.push(startElement);
    }
    $.each(toDelete,function(z,x){
      $(x).remove();
      });
    $(element).removeClass('minus');
    $(element).addClass('plus');
    loadingBoxClose();
  }
  return false;
}

function loadingBox(mode) {
  $('#LoadingDialogBox').dialog(mode);
}

function loadingBoxOpen() {
  $('#LoadingDialogBox').dialog('open');
}

function loadingBoxClose() {
  $('#LoadingDialogBox').dialog('close');
}

function selectRow(id) {
  $('tr').each(function(i,z) {
    if($(z).hasClass('selected')) $(z).removeClass('selected');
  });
  $('.item_'+id+':first').addClass('selected');
}

function moveUpElement(page_id) {
  $('#selectedRow').val(page_id);
  $('#selectedRow').change();
  loadingBoxOpen();
  $.ajax({
    url: '/page/moveUp/id/'+page_id,
    success: function(data) {
      $.fn.yiiGridView.update('pages-grid');
    }
  });
}

function moveDownElement(page_id) {
  $('#selectedRow').val(page_id);
  $('#selectedRow').change();
  loadingBoxOpen();
  $.ajax({
    url: '/page/moveDown/id/'+page_id,
    success: function(data) {
      $.fn.yiiGridView.update('pages-grid');
    }
  });
}

function gridDeleteElements(gridIds, name, url) {
  if(confirm("Czy jesteś pewien?")) {
    loadingBoxOpen();
    var ids = [];
    $('input[name="'+name+'"]').each(function(i,z) {
      if($(z).is(':checked')) {
        ids.push($(z).val());

      }
    });
    if(ids.length) {
      $.ajax({
          url: url,
          type: 'post',
          data: {ids: ids},
          success: function() {
            $.each(gridIds, function(index, gridId) {
              $.fn.yiiGridView.update(gridId);
            });
//            gridIds.each(function(i,z) {
//              console.debug(i);
//              console.debug(z);
//            })
            //
            loadingBoxClose();
          }
        });
    }
  }
}

function switchRoller(element, url, gridIds) {
  loadingBoxOpen();
  $.ajax({
    url: url+'/id/'+element.val(),
    type: 'get',
    success: function() {
      $.each(gridIds, function(index, gridId) {
        $.fn.yiiGridView.update(gridId);
      });
      loadingBoxClose();
    }
  });
}

function switchMain(element, url, gridIds) {
  loadingBoxOpen();
  $.ajax({
    url: url+'/id/'+element.val(),
    type: 'get',
    success: function() {
      $.each(gridIds, function(index, gridId) {
        $.fn.yiiGridView.update(gridId);
      });
      loadingBoxClose();
    }
  });
}

function validateCostPerDay(val) {
    val = val.replace(/,/g, '.');
    val = val.replace(/[^0-9\.]+/g, '');
    return val;
  }